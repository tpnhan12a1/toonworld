using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneName:byte
{
    SceneLobby = 0,
    SceneVirtualWorld=1,
    SceneHome = 2,
    ScenePreviewCharacter= 3,
    SceneRunningGame = 4,
    SceneGameLobby=5,
    SceneRender=6,
    ScenePre= 7
}
