using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OnSelected : MonoBehaviour, IPointerDownHandler, IPointerUpHandler,IDragHandler
{
    [SerializeField]
    [Range(0f,30f)] private float _maxDistanceRayCast = 30f;
    [SerializeField]
    [Range(0f, 10f)] private float _radious = 2f;
    [SerializeField] private LayerMask layerMask = new LayerMask();
    
    private Action<GameObject> OnSelectedObject;
    public Action<PointerEventData> OnPointerDownAction;
    public Action<PointerEventData> OnPointerUpAction;
    public Action<PointerEventData> OnDragAction;
    private GameObject _selectedObject = null;
    public void RegisterOnSelectedObject(Action<GameObject> onSelectedObject)
    {
        OnSelectedObject += onSelectedObject;
    }
    public void UnRegisterOnSelectedObject(Action<GameObject> onSelectedObject)
    {
        OnSelectedObject -= onSelectedObject;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        OnPointerDownAction?.Invoke(eventData);
        Ray ray = CameraManager.Instance.mainCamera.ScreenPointToRay(eventData.position);
        if (Physics.SphereCast(ray, _radious, out RaycastHit hit, _maxDistanceRayCast, layerMask, QueryTriggerInteraction.Collide))
        {
            _selectedObject = hit.collider.gameObject;
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        OnPointerUpAction?.Invoke(eventData);
        CoroutineHandler.Instance.StartCoroutine(ActionOnClick(eventData));
    }
    public void OnDrag(PointerEventData eventData)
    {
        OnDragAction?.Invoke(eventData);
    }
    private IEnumerator ActionOnClick(PointerEventData eventData)
    {
        yield return new WaitForFixedUpdate();

        Ray ray = CameraManager.Instance.mainCamera.ScreenPointToRay(eventData.position);
        if (Physics.SphereCast(ray, _radious, out RaycastHit hit, _maxDistanceRayCast, layerMask, QueryTriggerInteraction.Collide))
        {
            if(hit.collider.gameObject == _selectedObject)
            {
                if (hit.collider.gameObject.TryGetComponent<Button>(out Button button))
                {
                    button.onClick.Invoke();
                }
                OnSelectedObject?.Invoke(hit.collider.gameObject);
                Debug.LogWarning(hit.collider.gameObject.name);
            } 
        }
        _selectedObject = null;
    }
}
