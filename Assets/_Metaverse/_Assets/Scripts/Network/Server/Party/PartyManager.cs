using System.Collections.Generic;
using System.Linq;

public class PartyManager<P>  : IPartyManager<P> where P: Party
{
    private List<P> partys;

    public PartyManager()
    {
        partys = new List<P>();
    }
    public void AddParty(P p)
    {
        ulong id = (ulong)partys.Count;
        p.id = id;
        partys.Add(p);
    }

    public bool Contain(P p)
    {
        return partys.Contains(p);
    }

    public P GetParty(ulong id)
    {
       return partys.Where(x => x.id == id).FirstOrDefault();
    }

    public Party GetPartyByPlayfabID(string playfabId)
    {
        return partys.Where(x => x.IsIn(playfabId)).FirstOrDefault();
    }

    public void RemoveParty(P p)
    {
        partys.Remove(p);
    }

    public void RemoveParty(ulong id)
    {
        partys.RemoveAll(x => x.id == id);
    }
}
