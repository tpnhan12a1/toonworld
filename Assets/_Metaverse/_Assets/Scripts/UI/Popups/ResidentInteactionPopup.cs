using DG.Tweening;
using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResidentInteactionPopup : UIPopup
{
    [SerializeField]
    private Button _btnClosePopup;
    [SerializeField]
    private Button _btnNext;
    [SerializeField]
    private TMP_Text _txtStory;
    [SerializeField]
    private RectTransform _rectContent;
    [SerializeField]
    private float typeSpeed = 15f;

    private ResidentController _residentController;
    private int curentIndexStory = -1;
    protected override void OnInit()
    {
        base.OnInit();
        _btnClosePopup.onClick.AddListener(ClosePopup);
        _btnNext.onClick.AddListener(NextStory);
    }
    protected override void OnShowing()
    {
        base.OnShowing();
        _residentController = (ResidentController)Parameter;
        _btnNext.gameObject.SetActive(curentIndexStory < _residentController.residentStorySO.stories.Count -1);
        
    }
    protected override void OnShown()
    {
        base.OnShown();
        NextStory();
    }
    protected override void OnHidden()
    {
        base.OnHidden();
        _residentController.OnStopInteraction();
        _residentController = null;
        curentIndexStory = -1;
    }
    private void NextStory()
    {
        if (_rectContent == null) return;
        curentIndexStory++;
        string text = "";
        DOTween.To(() => text, 
            x => text = x,
            _residentController.residentStorySO.stories[curentIndexStory],
            _residentController.residentStorySO.stories[curentIndexStory].Length/typeSpeed)
            .OnUpdate(() =>
        {
            _txtStory.text = text;
        });

        _btnNext.gameObject.SetActive(curentIndexStory < _residentController.residentStorySO.stories.Count-1);
    }

    private void ClosePopup()
    {
        Hide();
    }

}
