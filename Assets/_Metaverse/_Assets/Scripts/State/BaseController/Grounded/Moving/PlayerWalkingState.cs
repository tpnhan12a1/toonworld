using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerWalkingState : PlayerMovingState
{
    public override StateType GetStateType()
    {
        return StateType.Walking;
    }
    public override void OnEnterState()
    {
        PlayerDataManager.Instance.reusableData.movementSpeedModifier =
            PlayerDataManager.Instance.playerData.walkSpeed;
        base.OnEnterState();
        PlayerDataManager.Instance.reusableData.currentJumpForce =
           PlayerDataManager.Instance.playerData.weakForce;
        StartAnimation(PlayerDataManager.Instance.animationData.WalkParameterHash);
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        if (PlayerDataManager.Instance.reusableData.ShouldWalk()) return;
        StopWalking();
        StopAnimation(PlayerDataManager.Instance.animationData.WalkParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
    }
    protected override void OnMovementCanceled(InputAction.CallbackContext context)
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        _stateMachine.ChangeState(stateMachine.states[StateType.LightStopping]);
    }
    #region main method
    public void StopWalking()
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        if (PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero)
        {
            stateMachine.ChangeState(stateMachine.states[StateType.Idling]);
            return;
        }

        if (!PlayerDataManager.Instance.reusableData.ShouldWalk())
        {
            stateMachine.ChangeState(stateMachine.states[StateType.Running]);
        }
    }
    #endregion
}
