using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdlingState : PlayerGroundedState
{
    public override StateType GetStateType()
    {
        return StateType.Idling;
    }
    public override void OnEnterState()
    {
        PlayerDataManager.Instance.reusableData.movementSpeedModifier = 0f;
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Idling]);
        PlayerDataManager.Instance.reusableData.currentJumpForce =
            PlayerDataManager.Instance.playerData.stationaryForce;
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.IdleParameterHash);
    }

    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.IdleParameterHash);
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        if(PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero) return;
        OnMove();
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
    }
}
