using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent (typeof(Button))]
public class SpriteTab : MonoBehaviour, ITab
{
    [SerializeField] private Sprite _selectSprite;
    [SerializeField] private Sprite _deSelectSprite;
    
    public bool _isTabInstance= false;

    private TabController _tabController;
    private Image _image;
    private Button _button;
    public TabController tabController { get { return _tabController; } }
    public void Awake()
    {
        _image = GetComponent<Image>();  
        _button = GetComponent<Button>();
        if(_isTabInstance )
        {
            _image.sprite = _selectSprite;
        }
        else
        {
            _image.sprite = _deSelectSprite;
        }
        _button.onClick.AddListener(SelectedThis);

    }

    private void SelectedThis()
    {
        _tabController.SelectedTab(this);
    }

    public void Deselected()
    {
        if(_deSelectSprite != null)
        {
            _image.SetActive(true);
            _image.sprite = _deSelectSprite;
        }
        else
        {
            _image.SetActive(false);
        }
    }

    public void Selected()
    {
        if(_selectSprite != null)
        {
            _image.SetActive(true);
            _image.sprite = _selectSprite;
        }
    }

    public void SetTabController(TabController controller)
    {
        _tabController = controller;
    }
}
