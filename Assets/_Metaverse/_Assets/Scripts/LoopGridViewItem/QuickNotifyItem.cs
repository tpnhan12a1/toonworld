using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
[RequireComponent(typeof(RectTransform))]
public class QuickNotifyItem : MonoBehaviour
{
    [SerializeField] private TMP_Text _txtContent;
    [SerializeField] private Button _btnAccepted;
    [SerializeField] private Button _btnDeny;

    [SerializeField]
    [Range(0f, 10f)] float _timeToHide = 5f;
    [SerializeField]
    [Range(0f, 1f)] float _durationAnimation = 0.3f;
    private RectTransform _rectTransform;
    private event Action _onAccepted;
    private event Action _onDeny;
    private Coroutine _coroutine;
    private bool _isShow = false;
    [SerializeField]
    private float _hidenPositionY;
    [SerializeField]
    private float _shownPositonY;
    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        _btnDeny.onClick.AddListener(OnClickDeny);
        _btnAccepted.onClick.AddListener(OnClickAccepted);
        OnHide();
    }
    
    public void OnShow(string content, Action acceptedAction, Action denyAction)
    {
        if (_isShow)
        {
            OnHide(() =>
            {
                SetContent(content, acceptedAction, denyAction);
            });
        }
        else
        {
            SetContent(content, acceptedAction, denyAction);
        }
       
    }

    private void SetContent(string content, Action acceptedAction, Action denyAction)
    {
        //Start animation show
        gameObject.SetActive(true);
        _rectTransform.DOKill();
        _rectTransform.DOAnchorPosY(_shownPositonY, _durationAnimation).SetEase(Ease.InQuad).OnComplete(() =>
        {
        });

        _isShow = true;
        _txtContent.text = content;
        _onAccepted = acceptedAction;
        _onDeny = denyAction;
        _coroutine = CoroutineHandler.Instance.StartStaticCoroutine(StartEnd());
    }

    private IEnumerator StartEnd()
    {
        yield return new WaitForSeconds(_timeToHide);
        OnHide();
    }
    private void OnHide(Action OnHiden = null)
    {
        CoroutineHandler.Instance.StopStaticCoroutine(_coroutine);
        _coroutine = null;
        //Start animation hide
        _rectTransform.DOKill();
        _rectTransform.DOAnchorPosY(_hidenPositionY, _durationAnimation).SetEase(Ease.InQuad).OnComplete(() =>
        {
            gameObject.SetActive(false);
            _isShow = false;
            OnHiden?.Invoke();
        });
    }
    private void OnClickDeny()
    {
        _onDeny?.Invoke();
        OnHide();
    }
    private void OnClickAccepted()
    {
        _onAccepted?.Invoke();
        OnHide();
    }
}
