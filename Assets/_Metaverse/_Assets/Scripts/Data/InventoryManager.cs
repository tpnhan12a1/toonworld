using System;
using System.Collections.Generic;
using System.Linq;

[System.Serializable]
public class InventoryManager
{
    private List<Item> _items;

    public Action<List<Item>> OnListItemChanged;
    public List<Item> items { get { return _items; } }

    public void RegisterOnListItemChanged(Action<List<Item>> onListItemChanged)
    {
        OnListItemChanged += onListItemChanged;
        onListItemChanged?.Invoke(_items);
    }

    public void UnRegisterOnListItemChanged(Action<List<Item>> onListItemChanged)
    {
        OnListItemChanged -= onListItemChanged;
    }

    public InventoryManager()
    {
        _items = new List<Item>();
    }

    public void InitListItems(List<Item> listItems)
    {
        if(listItems != null)
        {
            _items = listItems;
            _items.OrderByDescending(x => x.displayName).ToList();
            OnListItemChanged?.Invoke(_items);
        }
    }

    public List<Item> GetListItems()
    {
        List<Item> list = new List<Item>();
        list.AddRange(_items);
        return list;
    }

    public Item GetItem(string itemId, string itemInstanceId)
    {
        foreach(Item item in _items)
        {
            if(item.itemId.Equals(itemId) && item.itemInstanceId.Equals(itemInstanceId))
            {
                return item;
            }
        }
        return null;
    }
}
