using Imba.UI;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FriendItem : LoopGridViewItem
{
    [SerializeField] private Image _imageLeaderBoard;
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private TMP_Text _txtLevel;
    [SerializeField] private Image _avatar;
    [SerializeField] private TMP_Text _txtRank;
    [SerializeField] private TMP_Text _txtStatus;

    [SerializeField] private Button _btnDeleteFriend;
    [SerializeField] private Button _btnTeleport;
    [SerializeField] private Button _btnInviteTeleport;
    [SerializeField] private Button _btnChat;

    private FriendGridViewController _friendGridViewController;
    private ToonWorld.Friend _friend;
    public void Awake()
    {
        _btnDeleteFriend.onClick.AddListener(RemoveFriend);
        _btnChat.onClick.AddListener(OpenChatPopup);
    }

    private void OpenChatPopup()
    {
        FriendChatInfo friendChatInfo = new FriendChatInfo()
        {
            displayName = _friend.displayName,
            friendPlayfabId = _friend.friendPlayfabId
        };
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.ChatPopup, friendChatInfo);
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.FriendPopup);
    }

    private void RemoveFriend()
    {
    
        APIModels.FriendRequest friendRequest = new APIModels.FriendRequest();
        friendRequest.friendPlayfabId = _friend.friendPlayfabId;
        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
            PlayfabContant.FunctionName.RemoveFriend,
            friendRequest,
            false,
            null,
            null);

        NetCodeModels.FriendContent friendContent = new NetCodeModels.FriendContent();
        friendContent.message = PlayfabContant.FriendSyncTag.RemoveFriend;
        friendContent.playfabReciverId = _friend.friendPlayfabId;
        friendContent.friend = new NetCodeModels.Friend(PlayfabManager.Instance.PlayfabID,
           PlayerDataManager.Instance.avatarUrl,
           PlayerDataManager.Instance.displayName,
           PlayerDataManager.Instance.level,
           new List<string>() { PlayfabContant.FriendTag.Friend });
        ServerManager.Instance.FriendControllerServerRpc(friendContent);
        PlayerDataManager.Instance.friendManager.RemoveFriend(_friend);

    }
    public void OnRefresh(ToonWorld.Friend friend, FriendGridViewController friendGridViewController)
    {
        _friendGridViewController = friendGridViewController;
        _friend = friend;

        Sprite avatar = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, friend.avatarUrl);
        _avatar.sprite = avatar;

        _txtDisplayName.text = friend.displayName;
        _txtLevel.text = friend.level.ToString();
    }

    internal void SetLeaderBoard(Sprite sprite)
    {
        _txtRank.SetActive(false);
        _imageLeaderBoard.SetActive(true);
        _imageLeaderBoard.sprite = sprite;
    }

    internal void SetOutTop(int index)
    {
        _imageLeaderBoard.SetActive(false);
        _txtRank.SetActive(true);
        _txtRank.text = (index+1).ToString();
    }
}
