using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class GameRoom : Room
{
    private GamePlayType _gamePlayType;
    private int[] slots;
    private IGamePlayController _controller;
    public IGamePlayController controller { get { return _controller; } }

    public GamePlayType gameType { get { return _gamePlayType; } }

    private List<Player> _playerReady = new List<Player>();
    public GameRoom(RoomManager<Room> roomManager,
        SceneName sceneName,
        int[] slots,
        string owner = "",
        bool isDestroyWhenNoOne = false,
        
        GamePlayType gamePlayType = GamePlayType.None) : base(roomManager, sceneName, owner, isDestroyWhenNoOne)
    {
        this.slots = slots;
        _gamePlayType = gamePlayType;
        switch (gamePlayType)
        {
            case GamePlayType.None:
                return;
            case GamePlayType.Running:
                InitRunningGamePlay();
                break;
        }
    }
    public override void OnSpawnPlayerInRoom(Player player, NetworkObject playerObject)
    {
        base.OnSpawnPlayerInRoom (player, playerObject);
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = new ulong[] { player.GetClientId() },
            }
        };
        ClientManager.Instance.ResponseLobbyCloseClientRpc(clientRpcParams);
    }
    public override void PlayerJoinRoom(Player player)
    {
        base.PlayerJoinRoom(player);
    }
    public override void PlayerOutRoom(Player player)
    {
        base.PlayerOutRoom(player);
        if (_playerReady.Count == playerManager.GetPlayerCount())
        {
            controller.StartGame();
        }
    }
    public override void OnPlayerReadyInRoom(Player player)
    {
        base.OnPlayerReadyInRoom(player);
        _playerReady.Add(player);
        if(_playerReady.Count == playerManager.GetPlayerCount())
        {
            controller.StartGame();
        }
    }
    #region Main Method
    private void InitRunningGamePlay()
    {
        _controller = ServerManager.Instance.spawnManager.SpawnObjectInRoom(
            ServerManager.Instance.spawnManager.runningGamePlayController,
            this,
            Vector3.zero,
            Quaternion.identity
            ).GetComponent<RunningGamePlayNetworkController>();
        _controller.room = this;
    }

    #endregion
}
