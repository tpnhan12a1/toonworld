using SuperScrollView;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneRenderManager : ManualSingletonMono<SceneRenderManager>
{
    [SerializeField] CharacterRender _characterRender;
    [SerializeField] GameObject _objectRender;

    public CharacterRender characterRender { get { return _characterRender; } }

    public void SetDataCharacterRender(APIModels.PlayerItem itemData)
    {
        characterRender.LoadPlayerItem(itemData);
    }
}
