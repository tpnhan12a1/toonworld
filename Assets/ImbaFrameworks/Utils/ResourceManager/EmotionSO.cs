﻿
using Imba.Audio;
using UnityEngine;

namespace Imba.Utils
{
    [CreateAssetMenu(fileName = "New Emotion", menuName = "Custom/Emotion")]
    public class EmotionSO : ScriptableObject
    {
        [SerializeField] private string _spriteName;
        [SerializeField] private GameObject _particleObject;
        [SerializeField] private AudioName _audioName = AudioName.Emotion;
        [SerializeField] private ParticleSystem _particleSystem;
        private void OnValidate()
        {
            _particleSystem = _particleObject.GetComponent<ParticleSystem>();
        }
        public AudioName audioName {  get { return _audioName; } }  
        public string spriteName { get { return _spriteName; } }
        public GameObject particleObject { get { return _particleObject; } }
        public ParticleSystem particleSystem { get { return _particleSystem; } }
    }


}