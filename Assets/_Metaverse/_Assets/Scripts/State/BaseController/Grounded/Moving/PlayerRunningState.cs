using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerRunningState : PlayerMovingState
{
    public override StateType GetStateType()
    {
        return StateType.Running;
    }
    public override void OnEnterState()
    {
       PlayerDataManager.Instance.reusableData.movementSpeedModifier = 
            PlayerDataManager.Instance.playerData.runSpeed;
        base.OnEnterState();
        PlayerDataManager.Instance.reusableData.currentJumpForce =
             PlayerDataManager.Instance.playerData.mediumForce;
        StartAnimation(PlayerDataManager.Instance.animationData.RunParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.RunParameterHash);
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        if (!PlayerDataManager.Instance.reusableData.ShouldWalk()) return;
        StopRunning();
    }

    #region main method
    public virtual void StopRunning()
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        if (PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero)
        {
            stateMachine.ChangeState(stateMachine.states[StateType.Idling]);
            return;
        }

        if (PlayerDataManager.Instance.reusableData.ShouldWalk())
        {
            stateMachine.ChangeState(stateMachine.states[StateType.Walking]);
        }
    }
    #endregion

    protected override void OnMovementCanceled(InputAction.CallbackContext context)
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        _stateMachine.ChangeState(stateMachine.states[StateType.MediumStopping]);
    }
}
