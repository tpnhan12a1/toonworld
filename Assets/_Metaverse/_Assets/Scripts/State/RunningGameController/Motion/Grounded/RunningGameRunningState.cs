using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameRunningState : RunningGameGroundedState
{
    public override StateType GetStateType()
    {
        return StateType.RunningGameRunningState;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        ResetVerticalVelocity();
        _runningGameStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.RunParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.RunParameterHash);

    }
    public override void OnUpdate()
    {
        base.OnUpdate();
      
        base.OnUpdate();
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.PlusSpeedModifier(
          (_runningGameStateMachine.playerRunningGameController.playerData.speedGainEachMinute/60)*Time.deltaTime);
    }

    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        Vector3 forward = _runningGameStateMachine.playerRunningGameController.gameObject.transform.forward;
        forward.x = 0f;
        forward.x = 0f;
        Vector3 ZVelocity = GetHorizontalVelocity();
        ZVelocity.x = 0f;

        Vector3 forceDir = forward *
            _runningGameStateMachine.playerRunningGameController.runningGameReusableData.movementSpeedModifier
            - ZVelocity;

        _runningGameStateMachine.playerRunningGameController.rb
            .AddForce(forceDir,
            ForceMode.VelocityChange);
    }
}
