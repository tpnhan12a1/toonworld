using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResidentStateMachine : StateMachine
{
    private ResidentController _residentController;

    protected Dictionary<StateType, IState> _states = new Dictionary<StateType, IState>();
    public Dictionary<StateType, IState> states
    {
        get { return _states; }
        set { _states = value; }
    }
    public ResidentStateMachine(ResidentController residentController) 
    { 
        _residentController = residentController;
    }
    public ResidentController residentController { get { return _residentController; } }

    public override StateMachineType GetStateMachineType()
    {
        return StateMachineType.ResidentStateMachine;
    }
    public void AddState(IState state)
    {
        _states.Add(state.GetStateType(), state);
    }

    public override void Init()
    {
       
    }
}
