using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;

public class OnDragTwoPointRectTransform : OnScreenControl, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    [InputControl(layout = "Axis")]
    [SerializeField]
    private string m_ControlPath;

    private List<PointerEventData> _pointerEventDatas = new List<PointerEventData>();
    protected override string controlPathInternal
    {
        get => m_ControlPath;
        set => m_ControlPath = value;
    }

    [SerializeField]
    private float _startDistance = 0f;
    [SerializeField][Range(100f, 1000f)] float _maxZoom = 1000f;

    public void OnPointerDown(PointerEventData eventData)
    {
        if (!_pointerEventDatas.Contains(eventData))
        {
            _pointerEventDatas.Add(eventData);
        }
        if (_pointerEventDatas.Count == 2)
        {
            PlayerInput.Instance.InputAction.Player.Zoom.Enable();
            _startDistance = Vector2.Distance(_pointerEventDatas[0].position, _pointerEventDatas[1].position);
            return;
        }
        SendValueToControl(0f);
        PlayerInput.Instance.InputAction.Player.Zoom.Disable();


    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (_pointerEventDatas.Count == 2)
        {
            SendValueToControl(0f);
            PlayerInput.Instance.InputAction.Player.Zoom.Disable();
        }
        _pointerEventDatas.Remove(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (_pointerEventDatas.Count == 2)
        {
            float distance = Vector2.Distance(_pointerEventDatas[0].position, _pointerEventDatas[1].position);
            float value = _startDistance - distance;
            value /= _maxZoom;
            //_startDistance = value;
            SendValueToControl(value);
        }
    }
    #region Networking

    #endregion
}