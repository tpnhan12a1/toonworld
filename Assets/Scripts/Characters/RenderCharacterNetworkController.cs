using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using Cinemachine;
public class RenderCharacterNetworkController : NetworkBehaviour
{
    [SerializeField] private OwnerNetworkTransform _ownerNetworkTranform;
    [SerializeField] private Animator _animator;
    [SerializeField] private OwnerNetworkAnimator _ownerNetworkAnimator;
    [SerializeField] private LookAtCamera _lookAtCamera;
    [SerializeField] private RectTransform _uiRectranform;

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        CinemachineCore.CameraUpdatedEvent.AddListener(OnCimemachineUpdateEvent);
    }

    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        CinemachineCore.CameraUpdatedEvent.RemoveListener(OnCimemachineUpdateEvent);
    }
    private void OnCimemachineUpdateEvent(CinemachineBrain cinemachineBrain)
    {
        if (IsOwner)
            ChangePosition();
        if (IsClient)
            _lookAtCamera.LookAt();
    }

    public void OnSlotIndexChanged(int slotIndex)
    {
        ChangePosition();
    }
    public void ChangePosition()
    {
        Vector3 lookAtDir = CameraManager.Instance.mainCamera.transform.position - transform.position;
        lookAtDir.y = 0f;
        Quaternion rotation = Quaternion.LookRotation(lookAtDir);
        transform.rotation = rotation;
    }
}
