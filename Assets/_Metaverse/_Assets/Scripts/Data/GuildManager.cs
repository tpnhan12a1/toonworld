using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GuildInformation
{
    public PlayFab.GroupsModels.EntityKey guildEntity;
    public string introduction = string.Empty;
    public string guildName = string.Empty;
    public override int GetHashCode()
    {
        return guildEntity.Id.GetHashCode();
    }
    public override bool Equals(object obj)
    {
        GuildInformation other = obj as GuildInformation;
        if(other == null) { return  false; }
        return other.guildEntity.Id == guildEntity.Id;
    }
}
[System.Serializable]
public class GuildManager
{
    private bool _isHaveGuild = false;
    private PlayFab.GroupsModels.EntityKey _guildEntity;
    private string _guildName = string.Empty;
    private string _guildIntroduction = string.Empty;
    private List<GuildMember> _members;
    private List<GuildMember> _inventations;

    private List<GuildInformation> _guildInventations;

    private Action<bool> _onIsHaveGuildChanged;
    private Action<PlayFab.GroupsModels.EntityKey> _onGuildEntityChanged;
    private Action<string> _onGuildNameChanged;
    private Action<string> _onGuildIntroductionChanged;
    private Action<List<GuildMember>> _onMembersChanged;
    private Action<List<GuildMember>> _onInventationChanged;
    private Action<List<GuildInformation>> _onGuildInventationChanged;
    public Action _onStepDown;

   
    public bool isAdmin { get;private set; }
    public bool isSubAdmin { get;private set; }
    public event Action OnStepDown
    {
        add
        {
            _onStepDown += value;
        }
        remove 
        { 
            _onStepDown -= value;
        }
    }
    public event Action<string> OnGuildIntroductionChanged
    {
        add
        {
             _onGuildIntroductionChanged += value;
            value?.Invoke(_guildIntroduction);
        }
        remove
        {
            _onGuildIntroductionChanged -= value;
        }
    }
    public event Action<List<GuildInformation>> OnGuildInventationChanged
    {
        add
        {
            _onGuildInventationChanged += value;
            value?.Invoke(_guildInventations);
        }
        remove
        {
            _onGuildInventationChanged -= value;
        }
    }
    public event Action<List<GuildMember>> OnInventationChanged
    {
        add { _onInventationChanged += value;
            value?.Invoke(_inventations);
        }
        remove
        { 
            _onInventationChanged -= value;
        }
    }
    public event Action<List<GuildMember>> OnMembersChanged
    {
        add
        {
            _onMembersChanged += value;
            value?.Invoke(_members);
        }
        remove
        {
            _onMembersChanged -= value;
        }
    }
    public event Action<string> OnGuildNameChanged
    {
        add 
        { 
            _onGuildNameChanged += value;
            value?.Invoke(_guildName);
        }
        remove { _onGuildNameChanged -= value;}
    }
    public event Action<PlayFab.GroupsModels.EntityKey> OnGuildEntityChanged
    {
        add 
        {
            _onGuildEntityChanged += value;
            value?.Invoke(_guildEntity);
        }
        remove 
        { 
            _onGuildEntityChanged -= value; 
        }
    }
    public event Action<bool> OnIsHaveGuildChanged
    {
        add
        {
            _onIsHaveGuildChanged += value;
            value?.Invoke(_isHaveGuild);
        }
        remove
        {
            _onIsHaveGuildChanged -= value;
        }
    }
    public bool isHaveGuild
    {
        get { return _isHaveGuild; }
        set
        {
            _isHaveGuild = value;
            _onIsHaveGuildChanged?.Invoke(_isHaveGuild);
        }
    }

    public PlayFab.GroupsModels.EntityKey guildEntity { get { return _guildEntity; } }
    public string guildName { get { return _guildName; }set {

            _guildName = value;
            _onGuildNameChanged?.Invoke(_guildName);
        } 
    }
    public string guildIntroduction { get { return _guildIntroduction; }

        set {
            _guildIntroduction = value;
            _onGuildIntroductionChanged?.Invoke(_guildIntroduction);
        }
    }
    public List<GuildMember> members { get {  return _members; } }
    public List<GuildMember> inventations { get { return _inventations; } }  
    public List<GuildInformation> guildInventations { get { return _guildInventations;} }
    public void SetInventation(List<GuildMember> members)
    {
        _inventations.Clear();
        _inventations.AddRange(members);
        _onInventationChanged?.Invoke(_members);
    }
    public void OutGuild()
    {
        isHaveGuild = false;
    }
    public void Init(bool isHaveGuild,
        PlayFab.GroupsModels.EntityKey guildEntity,
        string guildName,
        string guidIntroduction,
        List<GuildMember> members)
    {
        _isHaveGuild=isHaveGuild;
        if(isHaveGuild)
        {
            _guildEntity = guildEntity;
            _guildName = guildName;
            _guildIntroduction = guidIntroduction;
            _members = new List<GuildMember>();
            _members.AddRange(members);
            isAdmin = IsAdmin(PlayfabManager.Instance.PlayfabID);
            isSubAdmin = IsSubAdmin(PlayfabManager.Instance.PlayfabID);
            if (isAdmin || isSubAdmin)
            {
                _inventations = new List<GuildMember>();
                _onInventationChanged?.Invoke(_inventations);
            }
        }
        else
        {
            _guildEntity = null;
            _guildName = string.Empty;
            _members?.Clear();
        }
        _guildInventations = new List<GuildInformation>();
        _onIsHaveGuildChanged?.Invoke(_isHaveGuild);
        _onGuildEntityChanged?.Invoke(_guildEntity);
        _onGuildNameChanged?.Invoke(_guildName);
        _onMembersChanged?.Invoke(_members);
    }
    public void Edit(GuildMember member)
    {
        GuildMember mem =  _members.Where(x =>  x.playfabId == member.playfabId).FirstOrDefault();
        if(mem != null)
        {
            mem.Edit(member);  
        }
        _onMembersChanged?.Invoke(_members);
    }
    public void RemoveMember(GuildMember member)
    {
        _members.Remove(member);
        _onMembersChanged?.Invoke(_members);
    }
    public void RemoveMember(string playfabId)
    {
        GuildMember member = _members.Where(x=> x.playfabId.Equals(playfabId)).FirstOrDefault();
        if(member != null)
        {
            _members.Remove(member);
            _onMembersChanged?.Invoke(_members);
        }
    }
    public void RemoveInventation(GuildMember member)
    {
        _inventations.Remove(member);
        _onInventationChanged?.Invoke(_inventations);
    }
    public void AddGuildMember(GuildMember member)
    {
        GuildMember guildMember =  _members.Where(x => x.playfabId == member.playfabId).FirstOrDefault();
        if(guildMember!= null)
        {
            guildMember.Edit(member);
        }
        else
        {
            _members.Add(member);
        }
        _onMembersChanged?.Invoke(_members);
    }
    public void AddGuildInventations(List<GuildInformation> inventations)
    {
        inventations.ForEach(x =>
        {
            if (!_guildInventations.Contains(x))
            {
                _guildInventations.Add(x);
            }
        });
        _onGuildInventationChanged.Invoke(_guildInventations);
    }
    public bool IsAdmin(string playfabId)
    {
        return _members.Where(
            x => x.role ==PlayfabContant.PlayerTag.Admintrators
            
            && x.playfabId == playfabId).Any();
    }
    public bool IsSubAdmin(string playfabId)
    {
        return _members.Where(
            x => x.role == PlayfabContant.PlayerTag.SubAdmintrators
            && x.playfabId == playfabId).Any();
    }

    internal void StepDown()
    {
       GuildMember guildMember =  _members.Where(x => x.playfabId == PlayfabManager.Instance.PlayfabID).FirstOrDefault();
       guildMember.role = PlayfabContant.PlayerTag.Members;
       _onStepDown?.Invoke();
    }
    internal void StepDown(string playfabId)
    {
        GuildMember guildMember = _members.Where(x => x.playfabId == playfabId).FirstOrDefault();
        guildMember.role = PlayfabContant.PlayerTag.Members;
        _onMembersChanged?.Invoke(members);
    }
}
