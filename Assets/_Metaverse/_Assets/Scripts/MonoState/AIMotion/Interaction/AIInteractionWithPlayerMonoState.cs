using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class AIInteractionWithPlayerMonoState : AIMotionMonoState
{
    public override StateType GetStateType()
    {
        return StateType.AIInteractionWithPlayerMonoState;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        ((ResidentStateMachine)_stateMachine).residentController.navMeshAgent.isStopped = true;
        ((ResidentStateMachine)_stateMachine).residentController.residentReusableData.isIntractWithPlayer = true;
        ((ResidentStateMachine)_stateMachine).residentController.buttonObject.SetActive(true);
        StartAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsInteractionWithPlayerParamenterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsInteractionWithPlayerParamenterHash);
       ((ResidentStateMachine)_stateMachine).residentController.buttonObject.SetActive(false);
    }
    public override void OnContactWithPlayerExit(Collider collider)
    {
        ((ResidentStateMachine)_stateMachine).residentController.residentReusableData.playerCollider = null;
        _stateMachine.ChangeState(((ResidentStateMachine)_stateMachine).states[StateType.AIIdelingMonoState]);
    }
}
