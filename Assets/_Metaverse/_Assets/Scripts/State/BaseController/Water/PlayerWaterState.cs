using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerWaterState : PlayerMotionState
{
    private Vector3 _waterLevel = Vector3.zero;
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.WaterParameterHash);
        Vector3 boxColiderCenterWaterCheck = ((MotionStateMachine)_stateMachine).playerController.waterCheckCollider.transform.position;
        _waterLevel = boxColiderCenterWaterCheck - new Vector3(0f, ((MotionStateMachine)_stateMachine).playerController.waterCheckCollider.bounds.size.y, 0f);
        PlayerDataManager.Instance.reusableData.isWater = true;
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.WaterParameterHash);
        PlayerDataManager.Instance.reusableData.isWater = false;

    }
    public override void OnUpdate()
    {
        base.OnUpdate();
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        Foating();
    }

    private void Foating()
    {
        Vector3 capsuleColliderCenterInSpace = ((MotionStateMachine)_stateMachine)
           .playerController.waterCheckCollider.gameObject.transform.position;

        Ray downwardRayFromCapsuleHead = new Ray(capsuleColliderCenterInSpace, Vector3.down);

        if(Physics.Raycast(downwardRayFromCapsuleHead, out RaycastHit hit, PlayerDataManager.Instance.playerData.floatRayDistance,
            PlayerDataManager.Instance.layerData.waterMask, QueryTriggerInteraction.Ignore))
        {
            float distanceToFloatingPoint =
                 ((MotionStateMachine)_stateMachine)
           .playerController.waterCheckCollider.bounds.extents.y

           - hit.distance + Mathf.Sin(Time.time *PlayerDataManager.Instance.playerData.frequencyFloating) * PlayerDataManager.Instance.playerData.amplitudeFloating;

            float amountToLift =
               distanceToFloatingPoint * PlayerDataManager.Instance.playerData.stepReachForce - GetPlayerVerticalVelocity().y;
            Vector3 liftForce = new Vector3(0, amountToLift, 0);

            ((MotionStateMachine)_stateMachine).playerController.rgbody.AddForce(liftForce,ForceMode.VelocityChange);
        }
    }
    protected virtual void OnMove()
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        stateMachine.ChangeState(stateMachine.states[StateType.Swimming]);
    }
    protected override void OnContactWithGround(Collider collider)
    {
        base.OnContactWithGround(collider);
        MotionStateMachine stateMachine =
            (MotionStateMachine)_stateMachine;
        stateMachine.ChangeState(stateMachine.states[StateType.Idling]);
    }
    public override void OnTriggerExit(Collider collider)
    {
        if (PlayerDataManager.Instance.layerData.IsWaterLayer(collider.gameObject.layer))
        {
            OnContactWithWaterExited(collider);
            return;
        }
    }
}
