using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "Placement", menuName = "Custom/Home/Placement")]
public class PlacementSO : ScriptableObject
{
    public string displayName = string.Empty;
    public string description = string.Empty;
    public PlacementObject placementObject;
    public string spriteName = string.Empty;
}
