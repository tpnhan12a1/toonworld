
/// <summary>
/// Name of Atlas using to get sprite
/// </summary>
public enum AtlasName
{
    Common = 0,
    Card = 1,
    Car = 2,
    PowerUp = 3,
    Avatar = 4,
    Emotion = 5,
    Item = 6,
    Placement = 7

}