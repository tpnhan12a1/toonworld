using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyData
{
    private Nullable<NetCodeModels.Party> _party;
    private Action<NetCodeModels.Party, NetCodeModels.Party> OnPartyChange;
    public Nullable<NetCodeModels.Party> party 
    {   get
        {
            return _party; 
        }
        set 
        {
            _party = value; 
        }
    }
    public PartyData()
    {
        
    }
    public bool IsOwner(string playfabId)
    {
        if(_party != null)
        {
            return _party.Value.owner == playfabId;
        }
        return false;
    }
    public void RegisterOnPartyChange(Action<NetCodeModels.Party,NetCodeModels.Party> onPartyChange)
    {
        OnPartyChange += onPartyChange;
        onPartyChange?.Invoke(_party.Value,_party.Value);
    }
    public void UnRegisterOnPartyChange(Action<NetCodeModels.Party, NetCodeModels.Party> onPartyChange)
    {
        OnPartyChange -= onPartyChange;
    }
}
