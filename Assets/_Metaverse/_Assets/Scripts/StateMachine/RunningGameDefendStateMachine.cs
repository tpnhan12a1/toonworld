using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameDefendStateMachine : StateMachine
{
    private PlayerRunningGameController _playerController;
    protected Dictionary<StateType, IState> _states = new Dictionary<StateType, IState>();
    public Dictionary<StateType, IState> states
    {
        get { return _states; }
        set { _states = value; }
    }
    public PlayerRunningGameController playerController { get { return _playerController; } set { _playerController = value; } }

    public RunningGameDefendStateMachine(PlayerRunningGameController playerController)
    {
        this.playerController = playerController;
        Init();
    }
    public override StateMachineType GetStateMachineType()
    {
        return StateMachineType.RunningGameDefendStateMachine;
    }

    public override void Init()
    {
        RunningGameEmptyDefendState runningGameEmptyDefendState = new RunningGameEmptyDefendState();
        runningGameEmptyDefendState.SetStateMachine(this);
        states.Add(runningGameEmptyDefendState.GetStateType(), runningGameEmptyDefendState);

        RunningGameShieldDefendState runningGameShieldDefendState = new RunningGameShieldDefendState();
        runningGameShieldDefendState.SetStateMachine(this);
        states.Add(runningGameShieldDefendState.GetStateType(),runningGameShieldDefendState);
    }
    public bool IsTakeDamge()
    {
        IDefend idefend = CurentState as IDefend;
       if(idefend != null)
       {
            return idefend.IsTakeDamge();
       }
        return true;
    }
}
