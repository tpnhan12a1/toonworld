using Imba.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountDownController : MonoBehaviour
{
    public Action OnEndCountDownAction;
    public void OnEndCountDown()
    {
        OnEndCountDownAction?.Invoke();
    }
    public void OnTimeTick()
    {
        AudioManager.Instance.PlaySFX(AudioName.TimeCountDownTick4321);
    }
}
