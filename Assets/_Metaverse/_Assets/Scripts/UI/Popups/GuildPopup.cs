using Imba.UI;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing.Design;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GuildPopup : UIPopup
{
    [SerializeField] private TMP_Text _txtGuildName;
    [SerializeField] private TMP_Text _txtLeader;
    [SerializeField] private TMP_Text _txtIntroduction;

    [SerializeField]
    private Button _btnEditGuildName;
    [SerializeField]
    private Button _btnEditIntroduction;
    [SerializeField]
    private Button _btnClose;
    [SerializeField] private GameObject _introductionGameObject;
    [SerializeField] private GameObject _guildNameObject;
    [SerializeField] private Button _btnCloseIntroduce;
    [SerializeField] private Button _btnCloseGuildName;

    [SerializeField] private Button _btnDeleteGuild;
    [Header("Righ Top")]
    [SerializeField] private Button _btnMember;
    [SerializeField] private Button _btnInventation;

    [SerializeField] private Button _btnConfirmNewGuildName;
    [SerializeField] private Button _btnComfirmNewIntroduction;
    [SerializeField] private TMP_InputField _ipfNewGuildName;
    [SerializeField] private TMP_InputField _ipfNewIntroduction;

   
    [SerializeField] private GuildListViewController _guildListViewController;

    

    [SerializeField] private TabController _tabController;
    [SerializeField] private SpriteTab _tabMember;
    [SerializeField] private List<GameObject> _hiddenGameObjectsWhenNotAdmins;
    [SerializeField] private List<GameObject> _hiddenGameObjectWhenIsNotSubAdminsOrAdmin;
    [SerializeField] private GameObject _inGuildObject;
    
    protected override void OnInit()
    {
        base.OnInit();
        _guildListViewController.OnInit();
        _btnMember.onClick.AddListener(SelectMember);
        _btnInventation.onClick.AddListener(SelectInventation);
        _btnClose.onClick.AddListener(CloseThis);

        _btnEditGuildName.onClick.AddListener(OpenGuildName);
        _btnEditIntroduction.onClick.AddListener(OpenIntroduce);
        _btnCloseGuildName.onClick.AddListener(CloseGuildName);
        _btnCloseIntroduce.onClick.AddListener(CloseIntroduction);
        _btnConfirmNewGuildName.onClick.AddListener(ChangedGuildName);
        _btnComfirmNewIntroduction.onClick.AddListener(ChangedIntroduction);
        _btnDeleteGuild.onClick.AddListener(DeleteGuild);
    }

    private void DeleteGuild()
    {
        
    }

    private void ChangedIntroduction()
    {
        string newIntroduction = _ipfNewIntroduction.text;
        
        if(!string.IsNullOrEmpty(newIntroduction))
        {
            APIModels.GuildRequest request = new APIModels.GuildRequest()
            {
                requestType = PlayfabContant.GuildTag.UpdateIntroduce,
                subFunc = new APIModels.ChangedIntroductionRequest()
                {
                    guild = PlayerDataManager.Instance.guildManager.guildEntity,
                    introduction = newIntroduction
                }
            };
            PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
                PlayfabContant.FunctionName.Guild,
                request,
                true,
                (obj, data) =>
                {
                    PlayerDataManager.Instance.guildManager.guildIntroduction = newIntroduction;   
                    
                    //Brocat to all client in guild
                }, null);
            _ipfNewIntroduction.text = string.Empty;
            _introductionGameObject.SetActive(false);
        }
    }

    private void ChangedGuildName()
    {
       string newGuildName = _ipfNewGuildName.text;
       if (!string.IsNullOrEmpty(newGuildName)) 
       {
            APIModels.GuildRequest request = new APIModels.GuildRequest()
            {
                requestType = PlayfabContant.GuildTag.UpdateGuildName,
                subFunc = new APIModels.ChangedGuildNameRequest()
                {
                    guild = PlayerDataManager.Instance.guildManager.guildEntity,
                    guildName = newGuildName
                }
            };
            PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
                PlayfabContant.FunctionName.Guild,
                request,
                true,
                (obj, data) =>
                {
                    PlayerDataManager.Instance.guildManager.guildName = newGuildName;

                    //Brocat to all client in guild
                }, null);
            _ipfNewGuildName.text = string.Empty;
            _guildNameObject.SetActive(false);
        }
    }

    private void CloseIntroduction()
    {
       _introductionGameObject.SetActive(false);
    }

    private void CloseGuildName()
    {
        _guildNameObject.SetActive(false);
    }

    private void OpenIntroduce()
    {
        _introductionGameObject.SetActive(true);
    }

    private void OpenGuildName()
    {
        _guildNameObject.SetActive(true);
    }

    private void CloseThis()
    {
        Hide();
    }

    private void SelectInventation()
    {
        ShowInvention();
    }

    private void SelectMember()
    {
        ShowMember();
    }

    protected override void OnShowing()
    {
        base.OnShowing();
        _inGuildObject.SetActive(PlayerDataManager.Instance.guildManager.isHaveGuild);
        if (PlayerDataManager.Instance.guildManager.isHaveGuild)
        {
            SetVisulize();
           // _tabController.SelectedTab(_tabMember);
            ShowMember();
        }
    }

    private void SetVisulize()
    {
        PlayerDataManager.Instance.guildManager.OnGuildNameChanged += OnGuildNameChanged;
        PlayerDataManager.Instance.guildManager.OnGuildIntroductionChanged += OnGuildIntroductionChanged;
        bool isAdmin = PlayerDataManager.Instance.guildManager.isAdmin;
        bool isSubAdmin = PlayerDataManager.Instance.guildManager.isSubAdmin;
        foreach (var g in _hiddenGameObjectsWhenNotAdmins)
        {
            g.SetActive(isAdmin);
        }
        foreach (var g in _hiddenGameObjectWhenIsNotSubAdminsOrAdmin)
        {
            g.SetActive(isAdmin || isSubAdmin);
        }
    }

    private void OnGuildIntroductionChanged(string introduction)
    {
        _txtIntroduction.text = introduction;
    }

    private void OnGuildNameChanged(string guildName)
    {
        _txtGuildName.text = guildName;
    }

    private void ShowMember()
    {
        List<GuildMember> membsers = PlayerDataManager.Instance.guildManager.members;
        PlayerDataManager.Instance.guildManager.OnMembersChanged += OnGuildMemberChanged;
    }

    private void OnGuildMemberChanged(List<GuildMember> guildMembers)
    {
        _guildListViewController.OnShow(guildMembers);
        _txtLeader.text = guildMembers.Where(x => x.role == PlayfabContant.PlayerTag.Admintrators).FirstOrDefault()?.displayName;
    }

    private void ShowInvention()
    {
        List<GuildMember> inventations = PlayerDataManager.Instance.guildManager.inventations;
        _guildListViewController.OnShow(inventations);
    }
    protected override void OnHiding()
    {
        base.OnHiding();
        _guildListViewController.OnHide();
        PlayerDataManager.Instance.guildManager.OnMembersChanged -= OnGuildMemberChanged;
    }
}
