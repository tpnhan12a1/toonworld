using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SuperScrollView;

public interface ISelected
{
    public void UpdateStatus<T>(T item);

}
public class CustomLoopGridView<T> : LoopGridView 
{
    T itemSelected;
    public void SetItemSelected(T item)
    {
        itemSelected = item;
    }

   public void CheckSelected(ISelected item)
   {
        item.UpdateStatus<T>(itemSelected);
   }
}
