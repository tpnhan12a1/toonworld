using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SegmentName: byte
{
    IndustrialWarehouse01,
    IndustrialWarehouse02,
    IndustrialWarehouse03,
    SuburbsGarage01,
    SuburbsHouse01,
    SuburbsHouse02,
    SuburbsHouse03,
    UrbanBrickWalls01,
    UrbanBrickWalls02,
    UrbanBrickWalls03,
    UrbanBuilding01,
    UrbanBuilding02,
    UrbanBuilding03,
    UrbanBuilding04,
    UrbanDumpsters01,
    UrbanDumpsters02,
    UrbanPlasterWalls01,
    UrbanPlasterWalls02,
    UrbanPlasterWalls03,
    UrbanRoadIntersection,
    UrbanRoadTSection,     
}