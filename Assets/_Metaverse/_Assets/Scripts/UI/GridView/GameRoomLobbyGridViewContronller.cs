using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class GameRoomLobbyGridViewContronller : LoopGridView
{
   public void OnInit()
   {
        InitGridView(PlayerDataManager.Instance.globalReusableData.gameRoomLobbyManager.gameRoomLobbies.Count, OnGetGameRoomLobbyItemByRowColumn);
    }

    private LoopGridViewItem OnGetGameRoomLobbyItemByRowColumn(LoopGridView gridView, int index, int row, int column)
    {
        if (PlayerDataManager.Instance.globalReusableData.gameRoomLobbyManager.gameRoomLobbies.Count == 0) return null;
        GameRoomLobbyItem gameLobbyItem = (GameRoomLobbyItem)gridView.NewListViewItem("GameRoomLobbyItem");
        gameLobbyItem.OnRefresh(PlayerDataManager.Instance.globalReusableData.gameRoomLobbyManager.gameRoomLobbies[index], this);
        return gameLobbyItem;
    }

    public void OnShow(GamePlayType gamePlayType)
    {
        PlayerDataManager.Instance.globalReusableData.gameRoomLobbyManager.RegisterOnGameGameRoomLobbyChanged(OnGameRoomLobbyChanged);
        ServerManager.Instance.GetLobbyListServerRpc(gamePlayType);
    }

   
    public void OnHide()
    {
        PlayerDataManager.Instance.globalReusableData.gameRoomLobbyManager.UnRegisterOnGameGameRoomLobbyChanged(OnGameRoomLobbyChanged);
    }

    public void RefreshItem()
    {
        RefreshAllShownItem();
    }

    private void OnGameRoomLobbyChanged(List<ToonWorld.GameRoomLobby> lobbies)
    {
        SetListItemCount(lobbies.Count);
        RefreshItem();
    }
}
