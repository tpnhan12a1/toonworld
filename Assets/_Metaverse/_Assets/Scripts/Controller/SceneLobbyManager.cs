using Imba.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneLobbyManager : MonoBehaviour
{
    [SerializeField] private GameObject renderForLobbySystem;
    public void Start()
    {
        //UIManager.Instance.PopupManager.HidePopup(UIPopupName.MainPopup);
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.LobbyPopup);
        //UIManager.Instance.PopupManager.ShowPopup(UIPopupName.RunningGameControlPopup);
        //UIManager.Instance.PopupManager.ShowPopup(UIPopupName.LoginPopup);
        //UIManager.Instance.PopupManager.ShowPopup(UIPopupName.EditorCharacterPopup);
        SGSceneManager.Instance.LoadSceneAdditive(SceneName.SceneVirtualWorld.ToString());
         GameObject.Instantiate(renderForLobbySystem);
    }
    private void OnDestroy()
    {
        SGSceneManager.Instance.UnLoadSceneAdditive(SceneName.SceneVirtualWorld.ToString());
    }
}
