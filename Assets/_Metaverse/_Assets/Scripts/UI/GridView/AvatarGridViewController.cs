using SuperScrollView;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class AvatarGridViewController : LoopGridView
{
    [SerializeField] private TabController _tabController;
    private List<string> _avatarNames;
    private string _selectedAvatar = "";

   public string selectedAvatar { get { return  _selectedAvatar; } set { _selectedAvatar = value; } }

   public void Init()
   {
        _avatarNames = Imba.Utils.ResourceManager.Instance.GetAllIdInSpriteAtlas(AtlasName.Avatar);
        InitGridView(_avatarNames.Count, OnGetAvatarByRowColumn);
   }
    public void OnShow()
    {
        _selectedAvatar = PlayerDataManager.Instance.avatarUrl;
        RefreshAllShownItem();
    }
    private LoopGridViewItem OnGetAvatarByRowColumn(LoopGridView gridView, int index, int row, int clolumn)
    {
        if (_avatarNames.Count == 0) return null;

        AvatarItem avatarItem = (AvatarItem)gridView.NewListViewItem("AvatarItem");
        avatarItem.SetTabController(_tabController);
        avatarItem.OnRefresh(_avatarNames[index], this);
        if (_avatarNames[index].Equals(_selectedAvatar))
        {
            avatarItem.OnSelect();
        }
        return avatarItem;
    }
}
