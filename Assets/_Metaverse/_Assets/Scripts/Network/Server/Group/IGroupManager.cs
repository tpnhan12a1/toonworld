using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGroupManager<G> where G : Group
{
    public void AddGroup(G group);
    public void RemoveGroup(G group);
    public void RemoveAllGroups();
    public void AddGroups(IEnumerable<G> groups);
}
