using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerManager<P> where P: Player
{
    public delegate void Consumer(P p);
    public delegate bool Predicate(P p);
    P GetPlayer(ulong clientId);
    P GetFirstPlayer();
    List<P> GetAllPlayer();
    int GetPlayerCount();
    bool ContainPlayer(ulong clientId);
    bool ContainPlayer(P p);
    void AddPlayer(P p);
    void AddPlayer(ulong clientId, P p);
    void AddPlayers(ICollection<P> list);
    void RemovePlayer(P p);
    void RemovePlayer(ulong clientId);
    void RemovePlayers(ICollection<P> list);
    void ForEach(Consumer action);
    List<P> FilterPlayers(Predicate predicate);
    bool IsEmpty();
}