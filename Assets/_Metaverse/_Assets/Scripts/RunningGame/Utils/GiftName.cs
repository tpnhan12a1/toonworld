using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GiftName :byte
{
   Random = 0,
   Bullet=1,
   Shield=2,
   Dash= 3,
}
