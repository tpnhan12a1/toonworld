using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPrefsManager : Singleton<PlayerPrefsManager>
{
    public const string EMAIL_KEY = "user";
    public const string PASSWORD_KEY = "password";
    public const string REMEMBER_ACCOUNT_KEY = "rememberAccount";
    public void SaveUserLogin(string email, string password)
    {
        PlayerPrefs.SetString(EMAIL_KEY, email);
        PlayerPrefs.SetString(PASSWORD_KEY, password);
    }
    public void SaveRememberAcount(bool status)
    {
        PlayerPrefs.SetInt(EMAIL_KEY, status?1:0);
    }
    public void TryGetRememberAccountStatus(out bool status)
    {
        status = PlayerPrefs.GetInt(EMAIL_KEY, 0)==0?false:true;
    }
    public void RemoveUser()
    {
        PlayerPrefs.DeleteKey(EMAIL_KEY);
        PlayerPrefs.DeleteKey(PASSWORD_KEY);
    }

    public bool TryGetUserLogin(out string email, out string password)
    {
        email = PlayerPrefs.GetString(EMAIL_KEY);
        password = PlayerPrefs.GetString(PASSWORD_KEY);

        if(string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
        {
            return false;
        }
        return true;
    }
}
