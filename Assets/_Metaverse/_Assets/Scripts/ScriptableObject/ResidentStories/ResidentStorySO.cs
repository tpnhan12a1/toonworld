using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player", menuName = "Custom/Resident/Stories")]
public class ResidentStorySO : ScriptableObject
{
    [SerializeField]
    private List<string> _stories;
    public List<string> stories {  get { return _stories; } }
}
