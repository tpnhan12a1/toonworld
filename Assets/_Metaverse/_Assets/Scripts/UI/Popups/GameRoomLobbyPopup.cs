using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using ToonWorld;
using UnityEngine;
using UnityEngine.UI;

public class GameRoomLobbyPopup : UIPopup
{
    [SerializeField] private TMP_Text _txtGameName;
    [SerializeField] private GameRoomLobbyGridViewContronller _gameRoomLobbyGridViewController;
    [SerializeField] private Button _btnClosePopup;
    [SerializeField] private Button _btnQuickJoin;
    [SerializeField] private Button _btnCreateNewLobby;
    [SerializeField] private Button _btnRefeshList;
    [SerializeField] private TMP_InputField _inputFieldLobbyId;

    protected override void OnInit()
    {
        base.OnInit();
        _gameRoomLobbyGridViewController.OnInit();
        _btnClosePopup.onClick.AddListener(HidePopup);
        _btnQuickJoin.onClick.AddListener(QuickJoin);
        _btnCreateNewLobby.onClick.AddListener(CreateNewLobby);
        _btnRefeshList.onClick.AddListener(RefreshList);
        _inputFieldLobbyId.onSubmit.AddListener(JoinLobbyID);
    }

    private void JoinLobbyID(string lobbyId)
    {
        if (String.IsNullOrEmpty(lobbyId))
        {
            return;
        }
        ServerManager.Instance.JoinLobbyServerRpc((GamePlayType)Parameter, int.Parse(lobbyId.Trim()));
        _inputFieldLobbyId.text = string.Empty;
    }

    private void RefreshList()
    {
        ServerManager.Instance.GetLobbyListServerRpc((GamePlayType)Parameter);
    }

    private void CreateNewLobby()
    {
        ServerManager.Instance.CreateLobbyServerRpc((GamePlayType)Parameter);
    }

    public void OnCreateNewLobby()
    {
        
    }
    private void QuickJoin()
    {
         ServerManager.Instance.QuickJoinLobbyServerRpc((GamePlayType)Parameter);
    }

    private void HidePopup()
    {
        Hide();
    }

    protected override void OnShowing()
    {
        base.OnShowing();
        GamePlayType gamePlayType = (GamePlayType)Parameter;
        _txtGameName.text = gamePlayType.ToString();
        _gameRoomLobbyGridViewController.OnShow(gamePlayType);
        //PlayerDataManager.Instance.globalReusableData.RegisterOnLobbyChanged(OnLobbyChanged);
        ClientManager.Instance.OnResponseStatusJoinRoom += OnResponseStatusJoinRoom;
    }

    private void OnResponseStatusJoinRoom(bool status,string message)
    {
        if(!status)
        {
            UIManager.Instance.AlertManager.ShowAlertMessage(message);
            ServerManager.Instance.GetLobbyListServerRpc((GamePlayType)Parameter);
            
        }
        else
        {
            if(SceneGameLobbyManager.Instance  == null)
            {
                SGSceneManager.Instance.LoadSceneAdditiveAsync(SceneName.SceneGameLobby, () =>
                {
                    UIManager.Instance.PopupManager.ShowPopup(UIPopupName.GameLobbyPopup,(GamePlayType)Parameter); 
                    Hide();
                });
            }
            else
            {
                UIManager.Instance.PopupManager.ShowPopup(UIPopupName.GameLobbyPopup, (GamePlayType)Parameter);
                Hide();
            }
        }
    }

    protected override void OnHiding()
    {
        base.OnHiding();
        _gameRoomLobbyGridViewController.OnHide();
        //PlayerDataManager.Instance.globalReusableData.UnRgisterOnLobbyChanged(OnLobbyChanged);
        ClientManager.Instance.OnResponseStatusJoinRoom -= OnResponseStatusJoinRoom;
    }


    //private void OnLobbyChanged(ILocalLobby oldLobby, ILocalLobby newLobby)
    //{
    //    if(newLobby == null)
    //    {
    //        oldLobby?.UnRegisterOnLobbyDataChanged(OnLobbyDataChanged);
    //    }
    //    else
    //    {
    //        newLobby?.RegisterOnLobbyDataChanged(OnLobbyDataChanged);
    //    }
    //}

    //private void OnLobbyDataChanged(GameRoomLobby gameRoomLobbyData)
    //{
        
    //    UIManager.Instance.PopupManager.ShowPopup(UIPopupName.GameLobbyPopup);
    //}
}
