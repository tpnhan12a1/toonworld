using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;


public class SGSceneManager : ManualSingletonMono<SGSceneManager>
{
    [SerializeField] private SceneLoad _sceneLoad = null;

    private SceneName _currentScene;
    private object _currentSceneData;
    private object _preSceneData;
    private SpawnPosition _spawnPositionInScene;
    public Action<SceneName, SceneName> OnChangeScene;
    public SceneName  currentSceneName { get { return _currentScene; } }

   public SpawnPosition spawnPositionInScene { get { return _spawnPositionInScene; } }
    public override void Awake()
    {
        base.Awake();
        _currentScene = Enum.Parse<SceneName>(UnityEngine.SceneManagement.SceneManager.GetActiveScene().name);
    }

    public void LoadSceneAdditive(string sceneName)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName, UnityEngine.SceneManagement.LoadSceneMode.Additive);
    }
    public void UnLoadSceneAdditive(string sceneName)
    {
        AsyncOperation operation = UnityEngine.SceneManagement.SceneManager.UnloadSceneAsync(sceneName);
    }

    public void LoadSceneAdditiveAsync(SceneName sceneName, Action OnLoadSceneSuccess = null)
    {
        StartCoroutine(StartLoadSceneAdditiveAsync(sceneName, OnLoadSceneSuccess));
    }

    public IEnumerator StartLoadSceneAdditiveAsync(SceneName sceneName, Action OnLoadSceneSuccess= null)
    {
        AsyncOperation asyncOperation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName.ToString(), UnityEngine.SceneManagement.LoadSceneMode.Additive);

        while (!asyncOperation.isDone)
        {
            yield return null;
        }
        OnLoadSceneSuccess?.Invoke();

    }
   
    public void ChangeScene(SceneName sceneName, object data = null)
    {
        
        _currentScene = sceneName;
        _preSceneData = _currentSceneData;
        if (data != null)
        {
            _currentSceneData = data;
        }
        else
        {
            _currentSceneData = null;
        }
        UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName.ToString());
        _spawnPositionInScene = FindObjectOfType<SpawnPosition>();
    }
    public void ChangeSceneAsync(SceneName sceneName, bool activeSceneLoad = true, object data = null, Action callback = null)
    {
        OnChangeScene?.Invoke(_currentScene, sceneName);
        _currentScene = sceneName;
        _preSceneData = _currentSceneData;
        if (data != null)
        {
            _currentSceneData = data;
        }
        else
        {
            _currentSceneData = null;
        }

        if (activeSceneLoad)
        {
            _sceneLoad.gameObject.SetActive(true);
        }
        StartCoroutine(StartChangeScene(sceneName, activeSceneLoad, callback));

    }

    private IEnumerator StartChangeScene(SceneName sceneName, bool activeSceneLoad = true, Action callback = null)
    {
        AsyncOperation asyncOperation = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName.ToString());
        while (!asyncOperation.isDone)
        {
            if (activeSceneLoad)
            {
                float proccess = Mathf.Clamp01(asyncOperation.progress / 0.9f);
                _sceneLoad.SetLoadValue(proccess);
                Debug.Log(proccess);
            }
            yield return null;
        }
        _spawnPositionInScene = FindObjectOfType<SpawnPosition>();
        callback?.Invoke();
        yield return new WaitForSeconds(2);
        if (activeSceneLoad)
        {
            _sceneLoad.gameObject.SetActive(false);
        }
        yield return null;
    }
}