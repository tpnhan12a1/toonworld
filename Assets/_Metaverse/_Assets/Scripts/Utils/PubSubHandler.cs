
//using UnityEngine;
//using Google.Cloud.PubSub.V1;
//using Grpc.Core;
//using System;
//using System.Threading.Tasks;
//using System.IO;
//using Google.Apis.Services;
//using Google.Apis.Auth.OAuth2;
//using Grpc.Auth;
//using System.Threading;

//public class PubSubHandler : ManualSingletonMono<PubSubHandler>
//{
//    [SerializeField] private  string _projectId = "toonworld-gra";
//    [SerializeField] private string _serviceAccountName = "toonworld-gra-f19f7f2f5f2e.json";

//    private PublisherServiceApiClient _publisher;
//    private SubscriberServiceApiClient _subscriber;
//    private SubscriberClient subscriber;
//    private async void Start()
//    {
//        await Setup();
//        //await CreateTopicAsync(_projectId, "TestTpoc");
//        CreateSubscription(_projectId, "TestTpoc", "TesSub");
//        subscriber = await SubscriberClient.CreateAsync(new SubscriptionName(_projectId, "TesSub"));
//        StartListening();
//    }

//    private async Task Setup()
//    {
//        string serviceAccountKeyFile = Application.dataPath + "/" + _serviceAccountName;
//        Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", serviceAccountKeyFile);
//        _publisher = await PublisherServiceApiClient.CreateAsync();
//        _subscriber = await SubscriberServiceApiClient.CreateAsync();
        
//    }
//    private async Task CreateTopicAsync(string projectId, string topicName)
//    {
//        TopicName topicId = new TopicName(projectId, topicName);
//        try
//        {
//            Topic topic = await _publisher.CreateTopicAsync(topicId);
//            Debug.Log($"Topic {topicId} created.");
//        }
//        catch (RpcException e)
//        {
//            Debug.LogError($"Error creating topic: {e.Status}");
//        }
//        catch (Exception ex)
//        {
//            Debug.LogError($"An error occurred: {ex}");
//        }
//    }
//    private  void CreateSubscription(string projectId, string topicId, string subscriptionId)
//    {
//        TopicName topicName = new TopicName(projectId, topicId);
//        SubscriptionName subscriptionName = new SubscriptionName(projectId, subscriptionId);

//        try
//        {
//           _subscriber.CreateSubscriptionAsync(subscriptionName, topicName, pushConfig: null, ackDeadlineSeconds: 60);
//            Debug.Log($"Subscription {subscriptionName} created.");
//        }
//        catch (RpcException e)
//        {
//            Debug.LogError($"Error creating subscription: {e.Status}");
//        }
//        catch (Exception ex)
//        {
//            Debug.LogError($"An error occurred: {ex}");
//        }
//    }
//    private void StartListening()
//    {
//        Task.Run(async () =>
//        {
//            await subscriber.StartAsync((PubsubMessage message, CancellationToken cancel) =>
//            {
//                string messageData = message.Data.ToStringUtf8();
//                HandleMessage(messageData);
//                return Task.FromResult(SubscriberClient.Reply.Ack);
//            });
//        });
//    }

//    private void HandleMessage(string message)
//    {
//        // Process the received message here
//        Debug.Log($"Received message: {message}");
//    }

//    private void OnApplicationQuit()
//    {
//        subscriber?.StopAsync(TimeSpan.FromSeconds(15)).Wait();
//    }
//}
