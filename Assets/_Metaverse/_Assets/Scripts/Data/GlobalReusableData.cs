using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TargetChat
{
    public string displayName = "";
    public string playfabId = "";
}
public class GlobalReusableData
{
    private TypeChat _typeChat = TypeChat.Global;
    private TargetChat _targetChat = new TargetChat();
    private ulong _currentRoomId;
    private GamePlayType _currentGamePlayType;
    private ILocalLobby _curentLocalLobby;
    private Action<ILocalLobby, ILocalLobby> OnLobbyChanged;
    private GameRoomLobbyManager _gameRoomLobbyManager;
   
    

    private Action<TypeChat, TypeChat> OnTypeChatChanged;
    public ulong currentRoomId { get { return _currentRoomId; } set { _currentRoomId = value; } }
    public  TargetChat targetChat { get { return _targetChat; } }
    public GameRoomLobbyManager gameRoomLobbyManager
    {
        get
        {
            if(_gameRoomLobbyManager == null)
                _gameRoomLobbyManager = new GameRoomLobbyManager();
            return _gameRoomLobbyManager;
        }
    }
    public TypeChat typeChat { 
        get 
        {
            return _typeChat;
        } 
        set {
            OnTypeChatChanged?.Invoke(_typeChat, value);
            if(value != TypeChat.Private)
            {
                _targetChat.playfabId = "";
                _targetChat.displayName = "";
            }
            _typeChat = value;
        } 
    }
    public ILocalLobby localLobby { get { return _curentLocalLobby;} set 
        { 
            OnLobbyChanged?.Invoke(_curentLocalLobby, value);
            _curentLocalLobby = value;
        } 
    }
    public void SetTargetChat(TargetChat targetChat)
    {
        _targetChat.playfabId= targetChat.playfabId;
        _targetChat.displayName= targetChat.displayName;
    }
    public void RegisterOnTypeChatChanged(Action<TypeChat, TypeChat> onTypeChatChanged)
    {
        OnTypeChatChanged += onTypeChatChanged;
        onTypeChatChanged?.Invoke(_typeChat, _typeChat);
    }
    public void UnRegisterOnTypeChatChanged(Action<TypeChat, TypeChat> onTypeChatChanged)
    {
        OnTypeChatChanged -= onTypeChatChanged;
    }

    public void PlayerJoinLobby(NetCodeModels.JoinLobbyResponse joinLobbyResponse)
    {
        _curentLocalLobby = new LocalLobby(joinLobbyResponse);
        OnLobbyChanged?.Invoke(null, _curentLocalLobby);
    }
    public void PlayerLeaveLobby()
    {
        OnLobbyChanged?.Invoke(_curentLocalLobby,null);
        _curentLocalLobby = null;
    }
    public void RegisterOnLobbyChanged(Action<ILocalLobby,ILocalLobby> onLobbyChanged)
    {
        OnLobbyChanged += onLobbyChanged;
        onLobbyChanged?.Invoke(_curentLocalLobby,_curentLocalLobby);
    }
    public void UnRgisterOnLobbyChanged(Action<ILocalLobby, ILocalLobby> onLobbyChanged)
    {
        OnLobbyChanged -= onLobbyChanged;
    }
}
