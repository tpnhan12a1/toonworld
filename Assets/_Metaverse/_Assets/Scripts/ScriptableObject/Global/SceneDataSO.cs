
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SceneData", menuName = "Custom/Global/SceneData")]
public class SceneDataSO : ScriptableObject
{
    [SerializeField]
    private SceneName _sceneName;
    [SerializeField]
    private List<Vector3> _spawnPosition = new List<Vector3>();
    [SerializeField]
    private Quaternion _spawnRotation = Quaternion.identity;

    public SceneName sceneName { get { return _sceneName; } }
    public List<Vector3> spawnPosition { get {  return _spawnPosition; } }
    public Quaternion spawnRotation { get { return _spawnRotation; } }
    public Vector3 GetRandomPosition()
    {
        int count = spawnPosition.Count;
        if(count == 0) return Vector3.zero;
        System.Random random = new System.Random();
        int index = random.Next(0, count);
        return spawnPosition[index];
    }
    public Vector3 GetPositionByIndex(int index)
    {
        if (_spawnPosition.Count >= index  &&  index >= 0)
            return _spawnPosition[index];
        return Vector3.zero;    
    }
}
