using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class AnimationItemGridViewController : LoopGridView, IGroupTab
{
    private List<AnimationItem> _animationItemList;

    private CharacterRender _characterRender;
    public void OnInit(CharacterRender characterRender)
    {
        _characterRender = characterRender;

        _animationItemList = new List<AnimationItem>(Resources.LoadAll<AnimationItem>("Item"));

        InitGridView(_animationItemList.Count, OnGetAnimationItemByRowColum);
    }

    private LoopGridViewItem OnGetAnimationItemByRowColum(LoopGridView gridView, int index, int row, int column)
    {
        if (_animationItemList.Count == 0) return null;

        AnimationPrefab animationPrefab = (AnimationPrefab)gridView.NewListViewItem("AnimationPrefab");
        animationPrefab.InitAnimationItem(_animationItemList[index], this, _characterRender);

        return animationPrefab;
    }

    public void RefreshItem()
    {
        SetListItemCount(_animationItemList.Count);
        RefreshAllShownItem();
    }

    public void Select()
    {
        gameObject.SetActive(true);
    }

    public void DesSelect()
    {
        gameObject.SetActive(false);
    }
}
