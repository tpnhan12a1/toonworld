using UnityEngine;
using System.Collections;
using System.Collections.Generic;



	[AddComponentMenu("Imba/Utils/Global Data")]
	public class GlobalData : MonoBehaviour
	{
		[SerializeField] private string _gamePlayDataName = "GamePlayData";
		public static List<GamePlayDataSO> _gamePlayDataSOs;
		private static Dictionary<string, GameObject> cache = new Dictionary<string, GameObject>();
		public static Dictionary<string, Texture> cacheAvatarFbId = new Dictionary<string, Texture>();

		// Use this for initialization
		public static List<GamePlayDataSO> gamePlayDataSOs { get { return _gamePlayDataSOs; } }
        void Awake()
		{
			DontDestroyOnLoad(gameObject);
			if (cache.ContainsKey(name))
			{
//			Debug.LogWarning("Object [" + name + "] exists. Destroy new one");
				Object.Destroy(this.gameObject);
			}
			else
				cache[name] = gameObject;

			_gamePlayDataSOs = Imba.Utils.ResourceManager.Instance.GetAllResourseByName<GamePlayDataSO>(_gamePlayDataName);
		}
	}
