using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHardStopSwimmingState : PlayerWaterStoppingState
{
    public override StateType GetStateType()
    {
        return StateType.HardStopSwimming;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        _stateMachine.ChangeState(stateMachine.states[StateType.WaterIdling]);
    }

    public override void OnAnimationTransitionEvent()
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        _stateMachine.ChangeState(stateMachine.states[StateType.WaterIdling]);
    }

    protected override void AddInputActionsCallbacks()
    {
    }
        

    protected override void RemoveInputActionCallbacks()
    {  
    }
}
