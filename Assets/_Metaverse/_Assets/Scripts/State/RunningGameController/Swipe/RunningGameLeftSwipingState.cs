using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameLeftSwipingState : RunningGameSwipingState
{
    public override StateType GetStateType()
    {
        return StateType.RunningGameLeftSwipingState;
    }

    public override void OnEnterState()
    {
        base.OnEnterState();
        _runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.currentLane -= 1;
        LeftSwipe();
        _runningGameSwipeStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.LeftSwipeParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameSwipeStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.LeftSwipeParameterHash);
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        ChangeEmptyState();
    }
    public void ChangeEmptyState()
    {
        Vector3 currentPosition = _runningGameSwipeStateMachine.playerRunningGameController.transform.position;
        if (currentPosition.x <= _runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.GetXCurrentLocationOfLane())
        {
            _runningGameSwipeStateMachine.ChangeState(_runningGameSwipeStateMachine.states[StateType.RunningGameEmptySwipingState]);
        }
    }
    private void LeftSwipe()
    {
        Vector3 dir = -_runningGameSwipeStateMachine.playerRunningGameController.transform.right;
        dir.y = 0f;
        Vector3 force = dir * _runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.swipeForce;
            //- GetHorizontalVelocity();
        
        _runningGameSwipeStateMachine.playerRunningGameController.rb.AddForce(
            force
           , ForceMode.VelocityChange);

    }
}
