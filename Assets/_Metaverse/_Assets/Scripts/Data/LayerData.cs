using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class LayerData
{
    [SerializeField]
    private LayerMask _groundedLayer = new LayerMask();
    [SerializeField]
    private LayerMask _waterLayer = new LayerMask();

    [SerializeField]
    private LayerMask _playerLayer = new LayerMask();   

    public LayerMask groundedMask { get { return _groundedLayer; } }
    public LayerMask waterMask { get { return _waterLayer; } }
    public LayerMask playerMask { get { return _playerLayer; } }

    public static bool ContainLayer(LayerMask layerMask, int layer)
    {
        return (1 << layer & layerMask) != 0;
    }
    public bool IsGroundLayer(int layer)
    {
        return ContainLayer(_groundedLayer, layer);
    }
    public bool IsWaterLayer(int layer) 
    {
        return ContainLayer(_waterLayer, layer);
    }
    public bool IsPlayerLayer(int layer)
    {
        return ContainLayer(_playerLayer, layer);
    }
}
