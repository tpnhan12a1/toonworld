using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum StateMachineType
{
    Motion =0,
    Emote =1,
    RunningGameMotionStateMachine = 2,
    RunningGameSwipeStateMachine = 3,
    RunningGameStatusStateMachine = 4,
    RunningGameAttackingStateMachine = 5,
    RunningGameDefendStateMachine = 6,
    ResidentStateMachine = 7


}
public abstract class StateMachine
{
    private IState _currentState;
    private StateType _currentStateType;

    #region getter, setter
    public IState CurentState { get { return _currentState; } set { _currentState = value; } }
    public StateType CurrentStateType { get { return _currentStateType; } set { _currentStateType = value; } }
    #endregion
    public abstract StateMachineType GetStateMachineType();
    public StateMachine()
    {

    }

    public void ChangeState(IState newState)
    {
        if (newState.GetStateType() == _currentStateType) return;

        _currentState?.OnExitState();
        _currentStateType = newState.GetStateType();
        _currentState = newState;
        _currentState?.OnEnterState();
    }
    public abstract void Init();

    public void HandleInput()
    {
        _currentState?.OnHandleInput();
    }
    public void Update()
    {
        _currentState?.OnUpdate();
    }
    public void PhysicsUpdate()
    {
        _currentState?.OnPhysicsUpdate();
    }
    public void OnAnimationEnterEvent()
    {
        _currentState?.OnAnimationEnterEvent();
    }
    public void OnAnimationExitsEvent()
    {
        _currentState?.OnAnimationExitsEvent();
    }
    public void OnAnimationTransitionEvent()
    {
        _currentState?.OnAnimationTransitionEvent();
    }
    public void OnTriggerEnter(Collider collider)
    {
        _currentState?.OnTriggerEnter(collider);
    }
    public void OnTriggerExit(Collider collider)
    {
        _currentState?.OnTriggerExit(collider);
    }
}
