using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerInforInLobbyItem : LoopGridViewItem
{
    [SerializeField] private Image _avatar;
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private TMP_Text _txtLevel;
    [SerializeField] private Button _btnKick;
    [SerializeField] private TMP_Text _txtStatus;
    [SerializeField] private RectTransform _rectTransformStatus;

    private NetCodeModels.PlayerData _playerData;
    private GameLobbyGridViewController _gameLobbyGridViewController;

    public void Awake()
    {
        _btnKick.onClick.AddListener(KickMember);
        _txtStatus.SetActive(false);
        _rectTransformStatus.SetActive(false);
        
    }

    private void KickMember()
    {
        ServerManager.Instance.KickMemberInLobbyServerRpc(_playerData.playfabId);
    }

    internal void OnRefresh(NetCodeModels.PlayerData playerData, GameLobbyGridViewController gameLobbyGridViewController)
    {
        _playerData = playerData;
        _gameLobbyGridViewController = gameLobbyGridViewController;
        _avatar.sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, _playerData.avatarUrl);
        _txtDisplayName.text = _playerData.displayName;
        _txtLevel.text = _playerData.level.ToString();
        if(PlayerDataManager.Instance.globalReusableData.localLobby?.GetGameRoomLobbyData().ownerPlayfabId
            == PlayfabManager.Instance.PlayfabID)
        {
            _btnKick.SetActive(_playerData.playfabId !=
          PlayerDataManager.Instance.globalReusableData.localLobby?.GetGameRoomLobbyData().ownerPlayfabId);
        }
        else
        {
            _btnKick.SetActive(false);
        }
    }
}
