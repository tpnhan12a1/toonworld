﻿

using UnityEngine;
using System.Collections.Generic;
using UnityEngine.U2D;
using System.Linq;
using TMPro;
using System.Text;

namespace Imba.Utils
{

    /// <summary>
    /// Manage resource to load in game
    /// </summary>
    public class ResourceManager : ManualSingletonMono<ResourceManager>
    {
        public List<SpriteAtlasSO> listAtlasData;//TODO: convert to use Scriptable Object
        public Dictionary<string, Object> objectCache;

        [SerializeField] private Sprite _defaultAvatar;
        [SerializeField] private Sprite _defaultItem;

        [SerializeField] private string _emotionPath = "Emotion";
        public override void Awake()
        {
            base.Awake();
            objectCache = new Dictionary<string, Object>();
        }

        public List<Sprite> GetAllSpriteInSpriteAtlas(AtlasName atlasName)
        {
            SpriteAtlasSO spriteAtlasSO = listAtlasData.Where(x=>x.atlasName == atlasName).First();
            if (spriteAtlasSO == null) return null;

            Sprite[] sprites = new Sprite[spriteAtlasSO.atlas.spriteCount];
            spriteAtlasSO.atlas.GetSprites(sprites);
            return sprites.ToList();
        }
        public List<string> GetAllIdInSpriteAtlas(AtlasName atlasName)
        {
            List<Sprite> sprites = GetAllSpriteInSpriteAtlas(atlasName);
            if (sprites == null) return null;
            List<string> ids = new List<string>();
            sprites.ForEach(x =>
            {
                ids.Add(x.name.Replace("(Clone)", ""));
            });
            return ids;
        }
        public EmotionSO GetEmotionByName(string name)
        {
            return Resources.Load<EmotionSO>(new StringBuilder().Append(_emotionPath).Append('/').Append(name).ToString());
        }
       
        public List<EmotionSO> GetAllEmotion()
        {
            return Resources.LoadAll<EmotionSO>(_emotionPath).ToList();
        }
        public Sprite GetSpriteById(AtlasName atlasName, string spriteName)
        {
            SpriteAtlasSO atlasData = listAtlasData.Find(r => r.atlasName.Equals(atlasName));
            if (atlasData == null)
            {
                //Debug.LogError("Cannot find atlas " + atlasName);
                return null;
            }

            Sprite s = atlasData.atlas.GetSprite(spriteName);
            if (s == null)
            {
                //Debug.LogError("Cannot find sprite " + spriteName + " in atlas " + atlasName);
                switch (atlasName)
                {
                    case AtlasName.Avatar:
                        return _defaultAvatar;
                    case AtlasName.Item:
                        return _defaultItem;
                }
                return null;
            }
            return s;

        }
        
        public T GetResourceByName<T>(string name) where T : Object
        {
            Object resource = null;
            if (objectCache.TryGetValue(name, out resource))
            {
                return (T) resource;
            }
            else
            {
                resource = Resources.Load<T>(name);
                if (resource == null)
                {
                    //Debug.LogError($"Dont have resource with {name}");
                    return null;
                }
                objectCache.Add(name, resource);
                return (T) resource;
            }
        }
        public List<T> GetAllResourseByName<T>(string name) where T : Object
        {
            return  Resources.LoadAll<T>(name).ToList();
        }
    }
}