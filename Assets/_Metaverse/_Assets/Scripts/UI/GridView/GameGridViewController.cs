using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameGridViewController : LoopGridView
{
    private List<GamePlayDataSO> gamePlayDataSOs;
    public void OnInit()
    {
        gamePlayDataSOs = GlobalData.gamePlayDataSOs;
        InitGridView(gamePlayDataSOs.Count, OnGetGamePlayDataByRowColum);
    }

    private LoopGridViewItem OnGetGamePlayDataByRowColum(LoopGridView gridView, int index, int row, int column)
    {
        if (gamePlayDataSOs.Count == 0) return null;
        GameItem gamePlayItem = (GameItem)gridView.NewListViewItem("GameItem");
        gamePlayItem.OnRefresh(gamePlayDataSOs[index], this);
        return gamePlayItem;
    }

    public void OnShow()
    {
        //SetListItemCount(gamePlayDataSOs.Count);
        //RefreshAllShownItem();
    }
    public void OnHide()
    {

    }
}
