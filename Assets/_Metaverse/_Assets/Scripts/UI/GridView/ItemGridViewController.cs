﻿using SuperScrollView;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ItemGridViewController : LoopGridView, IGroupTab
{
    public List<Item> _itemList;

    private CharacterRenderController _characterRenderController;

    private ButtonType _currentButtonSelected = ButtonType.BtnHair;

    public bool isMenuAppearance = true;

    [SerializeField] private GameObject _content;

    public void OnInit()
    {
        _itemList = new List<Item>();

        InitGridView(_itemList.Count, OnGetItemByRowColumn);
    }

    public void OnInitCharacterRenderController(CharacterRenderController characterRenderController)
    {
        _characterRenderController = characterRenderController;
    }

    private LoopGridViewItem OnGetItemByRowColumn(LoopGridView gridView, int index, int row, int column)
    {
        if (_itemList.Count == 0) return null;

        if (_itemList == null) return null;

        ItemPrefab itemPrefab = (ItemPrefab) gridView.NewListViewItem("ItemPrefab");
        itemPrefab.OnRefresh(_itemList[index], this, _characterRenderController);

        return itemPrefab;
    }

    public void OnListItemsChanged(List<Item> items)
    {
        _itemList = items.Where(x => x.itemClass.Contains(PlayfabContant.ItemClass.Item)).OrderByDescending(x => x.displayName).ToList();
        RefreshItem();
    }

    public void RefreshItem()
    {
        List<Item> list = PlayerDataManager.Instance.inventoryManager.GetListItems().Where(x => x.itemClass.Contains(PlayfabContant.ItemClass.Item)).OrderByDescending(x => x.displayName).ToList();

        List<Item> appearanceList = list.Where(item => item.itemId.StartsWith("H") ||
            item.itemId.StartsWith("Eb") ||
            item.itemId.StartsWith("E") ||
            item.itemId.StartsWith("Sk")
            ).ToList();

        if (isMenuAppearance)
        {
            _itemList = appearanceList;
        }
        else
        {
            _itemList = list.Except(appearanceList).ToList();
        }

        switch (_currentButtonSelected)
        {
            case ButtonType.BtnHair:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("H")).ToList();
                break;
            case ButtonType.BtnEyebrow:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("Eb")).ToList();
                break;
            case ButtonType.BtnEyes:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("E")).ToList();
                break;
            case ButtonType.BtnSkin:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("S")).ToList();
                break;
            case ButtonType.BtnCap:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("C")).ToList();
                break;
            case ButtonType.BtnGlasses:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("Gla")).ToList();
                break;
            case ButtonType.BtnTorso:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("T")).ToList();
                break;
            case ButtonType.BtnPants:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("P")).ToList();
                break;
            case ButtonType.BtnShoes:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("S")).ToList();
                break;
            case ButtonType.BtnGloves:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("Glo")).ToList();
                break;
            case ButtonType.BtnBackpack:
                _itemList = _itemList.Where(x => x.itemId.StartsWith("B")).ToList();
                break;
        }

        SetListItemCount(_itemList.Count);
        RefreshAllShownItem();
    }

    public void SetCurrentBtn(ButtonType buttonType)
    {
        _currentButtonSelected = buttonType;
        RefreshItem();
    }

    public void Select()
    {
        gameObject.SetActive(true);
        PlayerDataManager.Instance.inventoryManager.RegisterOnListItemChanged(OnListItemsChanged);
    }

    public void DesSelect()
    {
        gameObject.SetActive(false);
        PlayerDataManager.Instance.inventoryManager.UnRegisterOnListItemChanged(OnListItemsChanged);
    }
}
