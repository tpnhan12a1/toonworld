using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneGameLobbyManager : ManualSingletonMono<SceneGameLobbyManager>
{
    [SerializeField] private List<GameObject> slots;
    [SerializeField] private Transform _cameraTransformRender;
    [SerializeField] private float offet = 0.2f;

    public Transform cameraTransformRender { get { return _cameraTransformRender; } }
    public override void Awake()
    {
        base.Awake();
        foreach (var slot in slots)
        {
            slot.SetActive(false);
        }
    }
    
    public void GetSlot(out Vector3 position,out Quaternion rotation, int slotIndex)
    {
        Transform transform = slots[slotIndex]?.transform;
        position = new Vector3(transform.position.x, transform.position.y+offet, transform.position.z);
        rotation = transform.rotation;
        
    }
    public void ShowSlots(uint slotIndex)
    {
        foreach (var slot in slots)
        {
            if(slots.IndexOf(slot)< slotIndex)
            {
                slot.SetActive(true);
            }
            else
            {
                slot.SetActive(false);
            }
        }
    }

}
