using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public abstract class AIMotionMonoState : MonoState
{
    [SerializeField] LayerData layerData;
    public override void OnTriggerEnter(Collider collider)
    {
        base.OnTriggerEnter(collider);
        if(layerData.IsPlayerLayer(collider.gameObject.layer))
        {
            OnContactWithPlayerEnter(collider);
        }
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
       

    }
    public override void OnTriggerExit(Collider collider)
    {
        base.OnTriggerExit(collider);
        if (layerData.IsPlayerLayer(collider.gameObject.layer))
        {
            OnContactWithPlayerExit(collider);
        }
    }
    public virtual void OnContactWithPlayerExit(Collider collider)
    {
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        residentStateMachine.residentController.residentReusableData.playerCollider = null;
    }

    public virtual void OnContactWithPlayerEnter(Collider collider)
    {
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        residentStateMachine.residentController.residentReusableData.playerCollider = collider;
        residentStateMachine.residentController.residentReusableData.isIntractWithPlayer = true;
        //NetworkObject playerObject = ((ResidentStateMachine)_stateMachine).residentController.residentReusableData.playerCollider.GetComponent<NetworkObject>();
        //if (playerObject != null && playerObject.IsOwner)
        //{
           
        //}
    }
    public virtual void OnMove()
    {

    }
    protected void StartAnimation(int animationHash)
    {
        ((ResidentStateMachine)_stateMachine).residentController.animator.SetBool(animationHash, true);

    }
    protected void StopAnimation(int animationHash)
    {
        ((ResidentStateMachine)_stateMachine).residentController.animator.SetBool(animationHash, false);
    }
}
