/*using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;
using System.Linq;

public class MenuButtonEquipment : MonoBehaviour
{
    public enum Mode {Cap, Glasses, Torso, Pants, Shoes }

    public Mode currentMode;

    public GameObject[] equipmentButtons;

    //public ListEquipmentCreator listEquipmentCreator;

    public Dictionary<Mode, List<EquipmentItem>> modeToEquipmentItemList = new Dictionary<Mode, List<EquipmentItem>>();

    public float transitionDuration = 0.2f;

    private void Start()
    {
        EquipmentItem[] allEquipmentItems = Resources.LoadAll<EquipmentItem>("Item");
        modeToEquipmentItemList[Mode.Cap] = allEquipmentItems.Where(item => item.name.StartsWith("C")).ToList();
        modeToEquipmentItemList[Mode.Glasses] = allEquipmentItems.Where(item => item.name.StartsWith("G")).ToList();
        modeToEquipmentItemList[Mode.Torso] = allEquipmentItems.Where(item => item.name.StartsWith("T")).ToList();
        modeToEquipmentItemList[Mode.Pants] = allEquipmentItems.Where(item => item.name.StartsWith("P")).ToList();
        modeToEquipmentItemList[Mode.Shoes] = allEquipmentItems.Where(item => item.name.StartsWith("S")).ToList();
    }

    public void SetModeToCap()
    {
        SetMode(Mode.Cap);
    }

    public void SetModeToGlasses()
    {
        SetMode(Mode.Glasses);
    }

    public void SetModeToTorso()
    {
        SetMode(Mode.Torso);
    }

    public void SetModeToPants()
    {
        SetMode(Mode.Pants);
    }

    public void SetModeToShoes()
    {
        SetMode(Mode.Shoes);
    }

    public void SetMode(Mode mode)
    {
        this.currentMode = mode;

        switch (mode)
        {
            case Mode.Cap:
            case Mode.Glasses:
            case Mode.Torso:
            case Mode.Pants:
            case Mode.Shoes:
                DoTransition(equipmentButtons[(int)mode]);
                break;
        }

        if (modeToEquipmentItemList.TryGetValue(mode, out List<EquipmentItem> itemEquipmentList))
        {
            *//*if (listEquipmentCreator != null)
            {
                listEquipmentCreator.UpdateEquipmentItems(itemEquipmentList);
            }
            else
            {
                Debug.LogError("listEquipmentCreator is null!");
            }*//*
        }
    }

    private void DoTransition(GameObject targetObject)
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(targetObject.transform.DOScale(0, transitionDuration / 2));
        sequence.Append(targetObject.transform.DOScale(1, transitionDuration / 2));
    }
}
*/