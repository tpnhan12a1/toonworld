using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderForLobbySystem : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _cinemachineVirtualCamera;
    [SerializeField] private float speedTransformCamera = 0.1f;

    private CinemachineTrackedDolly _cinemachineTrackedDolly;
    private void Start()
    {
        _cinemachineTrackedDolly = _cinemachineVirtualCamera.GetCinemachineComponent<CinemachineTrackedDolly>();
    }
    public void Update()
    {
        _cinemachineTrackedDolly.m_PathPosition += (speedTransformCamera* Time.deltaTime);
    }
}
