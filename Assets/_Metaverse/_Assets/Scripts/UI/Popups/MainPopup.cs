using Imba.UI;
using System;
using System.Collections;
using System.IO;
using TMPro;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainPopup : UIPopup
{
    [SerializeField] private TMP_Text _txtCoin;
    [SerializeField] private TMP_Text _txtGem;
    [SerializeField] private Image _avatar;

    [SerializeField] private Button _btnProfile;
    [SerializeField] private Button _btnBag;
    [SerializeField] private Button _btnPresent;
    [SerializeField] private Button _btnEditHouse;
    [SerializeField] private GameObject _header;
    [SerializeField] private OnSelected _onSelected;

    [SerializeField] private Button _btnMenu;
    [SerializeField] private RectTransform _rectTransformGround;
    [SerializeField] private RectTransform _rectTransformWater;

    private bool _isSelectPlayer = true;
    public bool isSelectPlayer { get { return _isSelectPlayer; } set { _isSelectPlayer = value; } }

    public OnSelected onSelected { get { return _onSelected; } }
    
    public void SetActiveHeader(bool status)
    {
        _header.SetActive(status);
    }
    protected override void OnInit()
    {
        base.OnInit();
        _btnProfile.onClick.AddListener(OpenProfilePopup);
        _btnMenu.onClick.AddListener(OpenMenuPopup);
        _btnEditHouse.onClick.AddListener(OpenPlacementPopup);
    }
    protected override void OnShowing()
    {
        base.OnShowing();
        PlayerDataManager.Instance.RegisterOnCurrencyChanged(OnCurrencyChanged);
        PlayerDataManager.Instance.RegisterOnAvatarIdChanged(OnAvatarIdChanged);
        onSelected.RegisterOnSelectedObject(OnSelectedPlayer);
        UIManager.Instance.PopupManager.GetPopup(UIPopupName.QuickChatPopup)?.MoveToTop();
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.QuickNotifyPopup);
        _rectTransformGround.SetActive(PlayerDataManager.Instance.reusableData.isGround);
        _rectTransformWater.SetActive(PlayerDataManager.Instance.reusableData.isWater);
        PlayerDataManager.Instance.reusableData.OnIsGroundChanged += OnIsGroundChanged;
        PlayerDataManager.Instance.reusableData.OnWaterChanged += OnWaterChanged;
        SGSceneManager.Instance.OnChangeScene += OnChangeScene;

        _btnEditHouse.SetActive(SGSceneManager.Instance.currentSceneName == SceneName.SceneHome); // CHeck owner
    }

    private void OnChangeScene(SceneName oldSceneName, SceneName newSceneName)
    {
        if (newSceneName == SceneName.SceneHome)//and check owwnwe
        {
            _btnEditHouse.SetActive(newSceneName == SceneName.SceneHome);
        } 
    }

    protected override void OnHidden()
    {
        base.OnHidden();
        PlayerDataManager.Instance.UnRegisterOnCurrencyChanged(OnCurrencyChanged);
        PlayerDataManager.Instance.UnRegisterOnAvatarIdChanged(OnAvatarIdChanged);
        onSelected.UnRegisterOnSelectedObject(OnSelectedPlayer);
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.QuickNotifyPopup);
        PlayerDataManager.Instance.reusableData.OnIsGroundChanged -= OnIsGroundChanged;
        PlayerDataManager.Instance.reusableData.OnWaterChanged -= OnWaterChanged;
        SGSceneManager.Instance.OnChangeScene -= OnChangeScene;
    }
    private void OpenProfilePopup()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.ProfilePopup);
    }
    private void OpenPlacementPopup()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.PlacementPopup);
    }
    private void OpenMenuPopup()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.MenuPopup);
    }

    private void OnCurrencyChanged(APIModels.Currency oldValue, APIModels.Currency newValue)
    {
        _txtCoin.text = newValue.coin.ToString();
        _txtGem.text = newValue.gem.ToString();
    }
    private void OnAvatarIdChanged(string oldValue, string newValue)
    {
        _avatar.sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, newValue);
    }
    private void OnSelectedPlayer(GameObject gameObject)
    {
        if (!isSelectPlayer) return;
        if (gameObject.tag != "Player") return;
        NetworkObject networkObject = gameObject.GetComponent<NetworkObject>();
        if (networkObject.IsOwner) return;

        PlayerNetworkData playerNetworkData = gameObject.GetComponent<PlayerNetworkData>();

        Debug.Log(playerNetworkData.playfabId.Value.ToString());
        Debug.Log(playerNetworkData.displayName.Value.ToString());


        UserProfile userProfile = new UserProfile();
        userProfile.playfabId = playerNetworkData.playfabId.Value.ToString();
        userProfile.displayName = playerNetworkData.displayName.Value.ToString();
        userProfile.playerItem = playerNetworkData.playerItem.Value.ToClass();
        
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.OtherProfilePopup, userProfile);
    }
    private void OnWaterChanged(bool newValue)
    {
        _rectTransformWater.SetActive(newValue);
    }

    private void OnIsGroundChanged(bool newValue)
    {
        _rectTransformGround.SetActive(newValue);
    }

}
