using UnityEngine;
using DG.Tweening;
using System.Collections.Generic;
using System.Linq;

public class MenuButtonAppearance : MonoBehaviour
{
    public enum Mode { Hair, Eyebrow, Skin}

    public Mode currentMode;

    public GameObject[] appearanceButtons;

    public Dictionary<Mode, List<BaseItem>> modeToAppearanceItemList = new Dictionary<Mode, List<BaseItem>>();

    public float transitionDuration = 0.2f;

    private void Start()
    {
        BaseItem[] allAppearanceItems = Resources.LoadAll<BaseItem>("Item");
        modeToAppearanceItemList[Mode.Hair] = allAppearanceItems.Where(item => item.name.StartsWith("H")).ToList();
        modeToAppearanceItemList[Mode.Eyebrow] = allAppearanceItems.Where(item => item.name.StartsWith("Eb")).ToList();
        modeToAppearanceItemList[Mode.Skin] = allAppearanceItems.Where(item => item.name.StartsWith("Sk")).ToList();
    }

    public void SetModeToHair()
    {
        SetMode(Mode.Hair);
    }

    public void SetModeToEyebrow()
    {
        SetMode(Mode.Eyebrow);
    }

    public void SetModeToSkin()
    {
        SetMode(Mode.Skin);
    }

    public void SetMode(Mode mode)
    {
        this.currentMode = mode;

        switch (mode)
        {
            case Mode.Hair:
            case Mode.Eyebrow:
            case Mode.Skin:
                DoTransition(appearanceButtons[(int)mode]);
                break;
        }

        if (modeToAppearanceItemList.TryGetValue(mode, out List<BaseItem> itemAppearanceList))
        {
            /*if (listAppearanceCreator != null)
            {
                listAppearanceCreator.UpdateAppearanceItems(itemAppearanceList);
            }
            else
            {
                Debug.LogError("listAppearanceCreator is null!");
            }*/
        }
    }

    private void DoTransition(GameObject targetObject)
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(targetObject.transform.DOScale(0, transitionDuration / 2));
        sequence.Append(targetObject.transform.DOScale(1, transitionDuration / 2));
    }
}
