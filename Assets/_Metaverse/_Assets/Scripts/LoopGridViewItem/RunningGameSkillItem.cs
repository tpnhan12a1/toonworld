using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class RunningGameSkillItem : LoopGridViewItem
{
    [SerializeField] private TMP_Text _txtQuantity;
    [SerializeField] private Image _imageIcon;
    [SerializeField] private Button _button;

    private RunningGameDataSkill _skill;
    private RunningGameSkillGridViewController _runningGameSkillItemGridViewController;
    public void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);
       
    }

    private void OnClick()
    {
        _runningGameSkillItemGridViewController.playerRunningGameController.runningGameReusableData.UseSkill(_skill.giftName);
    }
    
    public void OnRefresh(RunningGameDataSkill skill, RunningGameSkillGridViewController runningGameSkillGridViewController)
    {
        _skill = skill;
        _runningGameSkillItemGridViewController = runningGameSkillGridViewController;

        _txtQuantity.text =_skill.count.ToString();
        RuninngGameSkillSO runningGAmeSkillSO = Imba.Utils.ResourceManager.Instance.GetResourceByName<RuninngGameSkillSO>(
            new StringBuilder().Append("RunningGame/Skill/").Append(_skill.giftName).ToString());
        _imageIcon.sprite = runningGAmeSkillSO.icon;
        _button.interactable = _skill.status;
    }
}
