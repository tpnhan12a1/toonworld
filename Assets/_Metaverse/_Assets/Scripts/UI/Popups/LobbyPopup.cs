using Imba.UI;
using GUIPack;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;
using UnityEngine.UI;

public class LobbyPopup : UIPopup
{
    [SerializeField] private Button _btnClickJoinGame;
    [SerializeField] private Button _btnLogin;
    [SerializeField] private Button _btnLogout;
    [SerializeField] private Button _btnRegister;
    [SerializeField] private GameObject _isLoginSuccessObject;
    protected override void OnInit()
    {
        base.Awake();
        _btnClickJoinGame.onClick.AddListener(OnClickJoinGame);
        _btnLogin.onClick.AddListener(OnCLickLogin);
        _btnRegister.onClick.AddListener(OnClickRegister);
        _btnLogout.onClick.AddListener(OnClickLogout);
    }

    private void OnClickLogout()
    {
        PlayfabManager.Instance.isLoginSucess = false;
        SetLogin();
    }

    private void OnClickRegister()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.SignupPopup);
    }

    private void OnCLickLogin()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.LoginPopup);
    }

    private void OnClickJoinGame()
    {
        if (PlayfabManager.Instance.isLoginSucess)
        {
            NetworkManager.Singleton.StartClient();
        }
    }

    public void SetLogin()
    {
        _btnClickJoinGame.SetActive(PlayfabManager.Instance.isLoginSucess);
        _btnLogin.SetActive(!PlayfabManager.Instance.isLoginSucess);
        _btnRegister.SetActive(!PlayfabManager.Instance.isLoginSucess);
        _btnLogout.SetActive(PlayfabManager.Instance.isLoginSucess);
        _isLoginSuccessObject.SetActive(PlayfabManager.Instance.isLoginSucess);
       // _txtServer.text = NetworkManager.Singleton.gameObject.GetComponent<UnityTransport>();
    }
    protected override void OnShowing()
    {
        //base.OnShowing();
        SetLogin();
        NetworkManager.Singleton.OnClientStarted += OnClientStarted;
    }

    private void OnClientStarted()
    {
        APIModels.GuildRequest guildRequest = new APIModels.GuildRequest()
        {
            requestType = PlayfabContant.GuildTag.Get,
        };

        PlayfabManager.Instance.ExecuteFunction<APIModels.GetGuidResponse>
            (PlayfabContant.FunctionName.Guild, guildRequest, true, 
            OnGetGuildSuccess, 
            OnGetGuildFalure);
    }

    protected override void OnHidden()
    {
        base.OnHidden();
        NetworkManager.Singleton.OnClientStarted -= OnClientStarted;
    }
    private void OnGetGuildFalure(object arg1, string arg2)
    {

    }
    private void OnGetGuildSuccess(object arg1, APIModels.GetGuidResponse data)
    {
        PlayerDataManager.Instance.guildManager.Init(data.isHaveGuild, data.guildEntity, data.guildName, data.introduction, data.members);
        Hide();
        if (data.isHaveGuild && PlayerDataManager.Instance.guildManager.isSubAdmin ||
            PlayerDataManager.Instance.guildManager.isAdmin)
        {
            APIModels.GuildRequest guildRequest = new APIModels.GuildRequest()
            {
                requestType = PlayfabContant.GuildTag.ListInventation,
                subFunc = data.guildEntity
            };
            PlayfabManager.Instance.ExecuteFunction<APIModels.GuildListResponse>(PlayfabContant.FunctionName.Guild,
                guildRequest, true, (obj, data) =>
                {
                    PlayerDataManager.Instance.guildManager.SetInventation(data.members);
                }, null);

            
        }
        if(data.isHaveGuild )
        {
            ServerManager.Instance.ResponseGroupIdServerRpc(data.guildEntity.Id);
        }
    }
}
