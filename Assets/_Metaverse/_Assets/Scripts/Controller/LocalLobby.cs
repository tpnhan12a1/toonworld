using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ToonWorld;
using UnityEngine;

public class LocalLobby : ILocalLobby
{
    private ToonWorld.GameRoomLobby _gameRoomLobby;
    private List<NetCodeModels.PlayerData> _playerDatas;
    private Action<ToonWorld.GameRoomLobby> OnLobbyDataChanged;
    private Action<List<NetCodeModels.PlayerData>> OnListDataChanged;
    private Action<int> OnSlotIndexChanged;
    private int _slotIndex = 0;
    public ToonWorld.GameRoomLobby GetGameRoomLobbyData()
    {
        return _gameRoomLobby;
    }
    public int GetLocalIndex()
    {
        Debug.LogWarning($"GetSlotIndex {_slotIndex}");
        return _slotIndex;
    }
    public LocalLobby(NetCodeModels.JoinLobbyResponse joinLobbyResponse)
    {
        _gameRoomLobby = joinLobbyResponse.gameRoomLobby.ToClass();
        _playerDatas = new List<NetCodeModels.PlayerData>();
        _playerDatas = joinLobbyResponse.playerDatas;
        _slotIndex = joinLobbyResponse.slotIndex;
        OnSlotIndexChanged?.Invoke(_slotIndex);
        OnListDataChanged?.Invoke(_playerDatas);
        OnLobbyDataChanged?.Invoke(_gameRoomLobby);

        Debug.LogWarning($"Slot Index {joinLobbyResponse.slotIndex}");
    }
  
    public List<NetCodeModels.PlayerData> GetPlayerDatas()
    {
        return _playerDatas;
    }
    //Other player join this lobby
    public void OnPlayerJoinLobby(NetCodeModels.PlayerData playerData)
    {
        _playerDatas.Add(playerData);
        OnListDataChanged?.Invoke(_playerDatas);
    }
    // other player leave lobby
    public void OnPlayerLeaveLobby(string playfabId)
    {
        NetCodeModels.PlayerData playerData = _playerDatas.Where(x => x.playfabId == playfabId).FirstOrDefault();
        _playerDatas?.Remove(playerData);
        OnListDataChanged?.Invoke(_playerDatas);
    }

    public void RegisterOnListDataChanged(Action<List<NetCodeModels.PlayerData>> onListDataChanged)
    {
        OnListDataChanged += onListDataChanged;
        onListDataChanged?.Invoke(_playerDatas);
    }

    public void RegisterOnLobbyDataChanged(Action<GameRoomLobby> onLobbyDataChanged)
    {
        OnLobbyDataChanged += onLobbyDataChanged;
        onLobbyDataChanged?.Invoke(_gameRoomLobby);
    }
    public void RegisterOnSlotIndexChanged(Action<int> onSlotIndexChanged)
    {
        OnSlotIndexChanged+= onSlotIndexChanged;
        onSlotIndexChanged?.Invoke(_slotIndex);
    }
    public void UnRegisterOnSLotIndexChanged(Action<int> onSLotIndexChanged)
    {
        OnSlotIndexChanged -= onSLotIndexChanged;
    }
    public void UnRegisterOnListDataChanged(Action<List<NetCodeModels.PlayerData>> onListDataChanged)
    {
        OnListDataChanged -= onListDataChanged;
    }

    public void UnRegisterOnLobbyDataChanged(Action<GameRoomLobby> onLobbyDataChanged)
    {
       OnLobbyDataChanged -= onLobbyDataChanged;
    }
}
