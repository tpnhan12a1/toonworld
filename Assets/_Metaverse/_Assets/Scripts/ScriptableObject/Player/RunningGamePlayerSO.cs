using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RuningPlayer", menuName = "Custom/Characters/RuningPlayer")]
public class RunningGamePlayerSO : ScriptableObject
{
    [SerializeField]
    [Range(0f, 10f)] public float runSpeed = 5f;
    [SerializeField]
    [Range(0f, 20f)] public float dashSpeed = 20f;
    [SerializeField]
    [Range(0f, 10f)] public float lightJumpForce = 5f;
    [SerializeField]
    [Range(0f, 10f)] public float mediumJumpforce = 7f;
    [SerializeField]
    [Range(0f, 10f)] public float strongJumpForce = 10f;

    [SerializeField] public float timeToDash = 1f;

    [SerializeField] public float timeToRolling = 3f;

    [SerializeField] public float rollingForce = 7f;

    [SerializeField] public float speedWhenStumbling = -2f;
    [SerializeField] public float speedGainEachMinute = 1f;
    [SerializeField] public float timeUseShield = 3f;
}
