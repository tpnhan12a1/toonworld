using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class PlayerMotionState : IState
{
    protected StateMachine _stateMachine;
    public abstract StateType GetStateType();
    public virtual void OnAnimationEnterEvent()
    {

    }
    public virtual void OnAnimationExitsEvent()
    {

    }
    public virtual void OnAnimationTransitionEvent()
    {

    }
    protected void ResetVelocity()
    {
        ((MotionStateMachine)_stateMachine).playerController.rgbody.velocity = Vector3.zero;
    }

    public virtual void OnEnterState()
    {
        Debug.Log("Entering " + GetType().Name);
        AddInputActionsCallbacks();
    }
    public virtual void OnExitState()
    {
        RemoveInputActionCallbacks();
    }
    public virtual void OnHandleInput()
    {
        ReadPlayerInput();
    }
    public virtual void OnPhysicsUpdate()
    {
        Move();
    }
    public virtual void OnUpdate()
    {

    }

    public virtual void OnTriggerEnter(Collider collider)
    {
        if (PlayerDataManager.Instance.layerData.IsWaterLayer(collider.gameObject.layer))
        {
            OnContactWithWater(collider);
            return;
        }
        if (PlayerDataManager.Instance.layerData.IsGroundLayer(collider.gameObject.layer))
        {
            OnContactWithGround(collider);
        }
    }
   
    public virtual void OnTriggerExit(Collider collider)
    {
        if (PlayerDataManager.Instance.layerData.IsGroundLayer(collider.gameObject.layer))
        {
            OnContactWithGroundExited(collider);
            return;
        }
       
    }
    protected void StartAnimation(int animationHash)
    {
        ((MotionStateMachine)_stateMachine).playerController.animator.SetBool(animationHash, true);
    }
    protected void StopAnimation(int animationHash)
    {
        ((MotionStateMachine)_stateMachine).playerController.animator.SetBool(animationHash, false);
    }

    public virtual void SetStateMachine(StateMachine stateMachine)
    {
        _stateMachine = stateMachine;
    }

    #region main method
    protected bool IsMovingHorizontally(float minimumMagnitude = 0.1f)
    {
        Vector3 playerHorizontalVelocity = GetPlayerHorizontalVelocity();
        Vector2 playerHorizontalMovement = new Vector2(playerHorizontalVelocity.x, playerHorizontalVelocity.z);
        return playerHorizontalMovement.magnitude > minimumMagnitude;
    }
    private void Move()
    {
        if(PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero || PlayerDataManager.Instance.reusableData.movementSpeedModifier == 0f) return;
        Vector3 movementDirection = GetMovementDirection();
        float targetRotationYAngle = Rotate(movementDirection);
        Vector3 targetRotationDirection = GetTargetRotaionDirection(targetRotationYAngle);


        float movementSpeed = GetMovementSpeed();
        Vector3 currentPlayerVelocity = GetPlayerHorizontalVelocity();

        ((MotionStateMachine)_stateMachine).playerController.rgbody.AddForce(targetRotationDirection * movementSpeed - currentPlayerVelocity, ForceMode.VelocityChange);
    }
    protected Vector3 GetPlayerVerticalVelocity()
    {
        return new Vector3(0, ((MotionStateMachine)_stateMachine).playerController.rgbody.velocity.y, 0);
    }
    protected void ResetVerticalVelocity()
    {
        Vector3 playerHorizontalVelocity = GetPlayerHorizontalVelocity();
        ((MotionStateMachine)_stateMachine).playerController.rgbody.velocity = playerHorizontalVelocity;
    }
    protected Vector3 GetPlayerHorizontalVelocity()
    {
        Vector3 playerHorizontalVelocity = 
            ((MotionStateMachine)_stateMachine).playerController.rgbody.velocity;
        playerHorizontalVelocity.y = 0f;
        return playerHorizontalVelocity;
    }
    protected float GetMovementSpeed(bool shouldConsiderSlope = true)
    {
        float movementSpeed = PlayerDataManager.Instance.playerData.baseSpeed * 
            PlayerDataManager.Instance.reusableData.movementSpeedModifier;
        return movementSpeed;
    }
    protected bool IsMovingUp(float minumVelocity = 0.1f)
    {
        return GetPlayerVerticalVelocity().y > minumVelocity;
    }
    protected Vector3 GetTargetRotaionDirection(float targetAngle)
    {
        return Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
    }
    private float Rotate(Vector3 direction)
    {
        float directionAngle = UpdateTargetRotation(direction);
        RotateTowardsTargetRotation();
        return directionAngle;
    }
    protected void RotateTowardsTargetRotation()
    {
        float currentYAngle = ((MotionStateMachine)_stateMachine).playerController.transform.rotation.eulerAngles.y;

        if (currentYAngle == PlayerDataManager.Instance.reusableData.currentTargetRotation.y) return;
        float smoothYAngle = Mathf.SmoothDampAngle(currentYAngle, PlayerDataManager.Instance.reusableData.currentTargetRotation.y,
            ref PlayerDataManager.Instance.reusableData.damgedTargetRotationCurrentVelocity.y,
            PlayerDataManager.Instance.reusableData.timeReachTargetRotation.y -
            PlayerDataManager.Instance.reusableData.damgedTargetRotationPassedTime.y);
        PlayerDataManager.Instance.reusableData.damgedTargetRotationPassedTime.y += Time.deltaTime;
        Quaternion targetRotation = Quaternion.Euler(0f, smoothYAngle, 0f);
        //if (Mathf.Abs(smoothYAngle) > 5f)
        ((MotionStateMachine)_stateMachine).playerController.rgbody.MoveRotation(targetRotation);
    }

    protected float UpdateTargetRotation(Vector3 direction, bool shouldConsiderCameraRoatation = true)
    {
        float directionAngle = GetDirectionAngle(direction);

        if (shouldConsiderCameraRoatation)
        {
            directionAngle = AddCameraRotationToAngle(directionAngle);
        }
        if (directionAngle != PlayerDataManager.Instance.reusableData.currentTargetRotation.y)
        {
            UpdateTargetRotationData(directionAngle);
        }

        return directionAngle;
    }
    protected void UpdateTargetRotationData(float targetAngle)
    {
        PlayerDataManager.Instance.reusableData.currentTargetRotation.y = targetAngle;
        PlayerDataManager.Instance.reusableData.damgedTargetRotationPassedTime.y = 0f;
    }
    private float GetDirectionAngle(Vector3 direction)
    {
        float directionAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        if (directionAngle < 0f) directionAngle += 360f;
        return directionAngle;
    }
    private float AddCameraRotationToAngle(float angle)
    {
        angle += ((MotionStateMachine)_stateMachine).playerController.mainCameraTransform.eulerAngles.y;

        if (angle > 360) angle -= 360f;
        return angle;
    }
    public void SetBaseRotatonData()
    {
        PlayerDataManager.Instance.reusableData.targetRotationReachTime = PlayerDataManager.Instance.playerData.targetRotationReachTime;
        PlayerDataManager.Instance.reusableData.timeReachTargetRotation = PlayerDataManager.Instance.playerData.targetRotationReachTime;
    }
    protected virtual void OnFall()
    {
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Falling]);
    }
    protected virtual void OnContactWithGroundExited(Collider collider)
    {
        Debug.Log("OnContactWithGroundExited");
    }
    protected Vector3 GetMovementDirection()
    {
        MotionStateMachine stateMachine = ((MotionStateMachine)_stateMachine);
        return new Vector3(PlayerDataManager.Instance.reusableData.movementInput.x, 0f,
            PlayerDataManager.Instance.reusableData.movementInput.y);
    }
    private void ReadPlayerInput()
    {
        if (_stateMachine.GetType() == typeof(MotionStateMachine))

            PlayerDataManager.Instance.reusableData.movementInput =
                PlayerInput.Instance.InputAction.Player.Move.ReadValue<Vector2>();
    }
    protected virtual void AddInputActionsCallbacks()
    {
        PlayerInput.Instance.PlayerActions.Move.started += OnMouseMovementStarted;
        PlayerInput.Instance.PlayerActions.Move.canceled += OnMovementCanceled;
        PlayerInput.Instance.PlayerActions.Move.performed += OnMovementPerformed;
    }
    protected virtual void RemoveInputActionCallbacks()
    {
        PlayerInput.Instance.PlayerActions.Move.started -= OnMouseMovementStarted;
        PlayerInput.Instance.PlayerActions.Move.performed -= OnMovementPerformed;
        PlayerInput.Instance.PlayerActions.Move.canceled -= OnMovementCanceled;
    }
    #endregion

    protected virtual void OnMovementCanceled(InputAction.CallbackContext context)
    {

    }
    protected virtual void OnMovementPerformed(InputAction.CallbackContext context)
    {

    }

    protected virtual void OnMouseMovementStarted(InputAction.CallbackContext context)
    {

    }
    protected virtual void OnContactWithGround(Collider collider)
    {

    }
    protected virtual void ResetSprintState()
    {

    }
    protected virtual void OnContactWithWater(Collider collider)
    {
       
    }
    protected virtual void OnContactWithWaterExited(Collider collider) { }

}
