using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Cinemachine;
[RequireComponent(typeof(NavMeshAgent))]
public class ResidentController : MonoBehaviour
{
    [SerializeField]
    private Animator _animator;
    [SerializeField]
    private List<MonoState> _states;
    [SerializeField]
    private List<Transform> _waitPointTransforms;

    [SerializeField] private GameObject _buttonObject;
    [SerializeField] private Button _btnInteraction;
    [SerializeField] private ResidentSO _residentSO;
    [SerializeField] private ResidentReusableData _residentReusableData;
    [SerializeField] private ResidentAnimationData _residentAnimationData;
    [SerializeField] private ResidentStorySO _residentStorySO;
    [SerializeField] private CinemachineVirtualCamera _cinemachineVirtualCamera;
    [SerializeField] private LayerMask _layerMask;

    private NavMeshAgent _navMeshAgent;
    private ResidentStateMachine _residentStateMachine;

    public int curentIndex = 0;
    public NavMeshAgent navMeshAgent { get { return _navMeshAgent; } }
    public ResidentStateMachine residentStateMachine { get {  return _residentStateMachine; } }
    public GameObject buttonObject { get { return _buttonObject; } }    
    public ResidentSO residentSO { get { return _residentSO; } }
    public ResidentReusableData residentReusableData { get { return _residentReusableData; } }
    public ResidentAnimationData residentAnimationData { get { return _residentAnimationData; } } 
    public Animator animator { get { return _animator; } }
    public ResidentStorySO residentStorySO { get { return _residentStorySO; } }
    public void Awake()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _states = GetComponents<MonoState>().ToList();
        _residentStateMachine = new ResidentStateMachine(this);
        if (_states.Count > 0)
        {
            _states.ForEach(state =>
            {
                _residentStateMachine.AddState(state);
                state.SetStateMachine(_residentStateMachine);
            });
        }

        if (_residentStateMachine != null)
        {
            IState newState = null;
            if (_residentStateMachine.states.TryGetValue(StateType.AIIdelingMonoState, out newState))
            {
                _residentStateMachine.ChangeState(newState);
                _residentStateMachine.CurentState = newState;
                _residentStateMachine.CurrentStateType = newState.GetStateType();
            }
        }
        _btnInteraction.onClick.AddListener(OnInteraction);
        _residentAnimationData.Initialize();
        _cinemachineVirtualCamera.SetActive(false);

    }

  
    public Transform GetNextWaitPoint()
    {
        if (_waitPointTransforms.Count() == 0) return null;
        return _waitPointTransforms[(curentIndex+1)%_waitPointTransforms.Count()];
    }
    public Transform GetCurrentWaitPoint()
    {
        return _waitPointTransforms[(curentIndex) % _waitPointTransforms.Count()];
    }
    public void UpdateCurrentWaitPoint()
    {
        curentIndex = (curentIndex +1) % _waitPointTransforms.Count();
        _navMeshAgent.SetDestination(GetCurrentWaitPoint().position);
        //Debug.Log("Update curentIndex");
    }
    public void SetCurrentDestination()
    {
        _navMeshAgent.SetDestination(GetCurrentWaitPoint().position);
    }
    private void OnInteraction()
    {
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.MainPopup);
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.QuickChatPopup);
        _btnInteraction.SetActive(false);
        CinemachineBlendDefinition cinemachineBlendDefinition = new CinemachineBlendDefinition();
        cinemachineBlendDefinition.m_Style = CinemachineBlendDefinition.Style.EaseOut;
        cinemachineBlendDefinition.m_Time = 1f;
       
        CameraManager.Instance.cinemachineBrain.m_DefaultBlend = cinemachineBlendDefinition;
        _cinemachineVirtualCamera.SetActive(true);
        CameraManager.Instance.mainCamera.cullingMask = _layerMask;
        CoroutineHandler.Instance.StartCoroutine(OnWaitVituralCamera(cinemachineBlendDefinition.m_Time,()
            =>
        {
            
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.ResidentInteactionPopup, this);
        }));
    }
 
    public void OnStopInteraction()
    {
        
        _cinemachineVirtualCamera.SetActive(false);
        CameraManager.Instance.mainCamera.cullingMask = CameraManager.Instance.layerMask ;
        CoroutineHandler.Instance.StartCoroutine(OnWaitVituralCamera(CameraManager.Instance.cinemachineBrain.m_DefaultBlend.m_Time, () =>
        {
            CameraManager.Instance.cinemachineBrain.m_DefaultBlend = CameraManager.Instance.cinemachineBlendDefinition;
            _btnInteraction.SetActive(true);
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.MainPopup);
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.QuickChatPopup);
        }));
    }
    private IEnumerator OnWaitVituralCamera(float waitTime, Action OnWaitDonn)
    {
        yield return new WaitForSeconds(waitTime);
        OnWaitDonn?.Invoke();
    }
    private void Update()
    {
        _residentStateMachine?.Update();
    }
    private void FixedUpdate()
    {
        _residentStateMachine?.PhysicsUpdate();
    }
    private void OnTriggerEnter(Collider other)
    {
        _residentStateMachine?.OnTriggerEnter(other);
    }
    private void OnTriggerExit(Collider other)
    {
        _residentStateMachine?.OnTriggerExit(other);
    }
    public void OnAnimationTransitionEvent()
    {
        _residentStateMachine?.OnAnimationTransitionEvent();
    }
    public void OnAnimationStartEvent()
    {
        _residentStateMachine?.OnAnimationEnterEvent();
    }
    public void OnAnimationExitsEvent()
    {
        _residentStateMachine?.OnAnimationExitsEvent();
    }
}
