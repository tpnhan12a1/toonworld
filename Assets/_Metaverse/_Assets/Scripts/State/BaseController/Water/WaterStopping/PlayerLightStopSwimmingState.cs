using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLightStopSwimmingState : PlayerWaterStoppingState
{
    public override StateType GetStateType()
    {
        return StateType.LightStopSwimming;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.LightStopSwimmingParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.LightStopSwimmingParameterHash);
    }
    public override void OnAnimationTransitionEvent()
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        _stateMachine.ChangeState(stateMachine.states[StateType.WaterIdling]);
    }
}
