using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerLandingState : PlayerGroundedState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.LandingParameterHash);

    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.LandingParameterHash);
    }
}
