using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameDashingState : RunningGameMotionState
{
    private float escapeTime = 0f;
    public override StateType GetStateType()
    {
        return StateType.RunningGameDashingState;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        _runningGameStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.DashParameterHash);
        if(!_runningGameStateMachine.playerRunningGameController.runningGameReusableData.isGrounded)
        {
            _runningGameStateMachine.playerRunningGameController.collisionCheckColider.direction = 2;
        }
        Dash();
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.LockSkill(GiftName.Dash);
        escapeTime = 0f;

        _runningGameStateMachine.playerRunningGameController.collisionCheckColider.enabled = false;
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.isVisible = true;
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        escapeTime += Time.deltaTime;
        if(escapeTime > _runningGameStateMachine.playerRunningGameController.playerData.timeToDash) 
        {
            _runningGameStateMachine.ChangeState(_runningGameStateMachine.states[StateType.RunningGameRunningState]);
        }
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        if (_runningGameStateMachine.playerRunningGameController.runningGameReusableData.isGrounded)
        {
            base.Float();
        }
        else
        {
            Float();
        }
    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.DashParameterHash);
        _runningGameStateMachine.playerRunningGameController.collisionCheckColider.direction = 1;
        escapeTime = 0f;
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.UnLockSkill(GiftName.Dash);
        _runningGameStateMachine.playerRunningGameController.collisionCheckColider.enabled = true;
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.isVisible = false;
    }
    public override void OnAnimationTransitionEvent()
    {
        if (_runningGameStateMachine.playerRunningGameController.runningGameReusableData.isGrounded)
        {
            _runningGameStateMachine.ChangeState(_runningGameStateMachine.states[StateType.RunningGameRunningState]);
            return;
        }
        else
        {
            _runningGameStateMachine.ChangeState(_runningGameStateMachine.states[StateType.RunningGameFallingState]);
            return;
        }
       
    }
    public override void Float()
    {
        //base.Float();
        Vector3 velocty = _runningGameStateMachine.playerRunningGameController.rb.velocity;
        Vector3 liftForce = Vector3.zero;
        liftForce.y = -velocty.y;
        _runningGameStateMachine.playerRunningGameController.rb.AddForce(liftForce, ForceMode.VelocityChange);
    }
    private void Dash()
    {
        Rigidbody rb = _runningGameStateMachine.playerRunningGameController.rb;
        Vector3 dir = _runningGameStateMachine.playerRunningGameController.gameObject.transform.forward;
        dir.y = 0f;
        rb.AddForce(dir * _runningGameStateMachine.playerRunningGameController.playerData.dashSpeed, ForceMode.VelocityChange);
    }
    protected override void AddInputActionsCallbacks()
    {
        
    }
    protected override void RemoveInputActionCallbacks()
    {
       
    }
}
