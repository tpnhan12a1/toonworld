using Imba.Audio;
using Imba.UI;
using PlayFab.ProfilesModels;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static NetCodeModels;

public class OtherProfilePopup : UIPopup
{
    [SerializeField] private Button _btnClosePopup;
    [SerializeField] private Button _btnAddFriend;
    [SerializeField] private Button _btnRemoveFriend;
    [SerializeField] private Button _btnInventParty;
    [SerializeField] private Button _btnChat;
    [SerializeField] private Button _btnGuildInvite;

    private UserProfile _userProfile;
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private TMP_Text _txtLevel;
    [SerializeField] private TMP_Text _txtIntroduction;
    [SerializeField] private Image _avatar;


    protected override void OnInit()
    {
        base.OnInit();
        _userProfile = new UserProfile();

        _btnClosePopup.onClick.AddListener(ClosePopup);
        _btnAddFriend.onClick.AddListener(OnClickAddFriend);
        _btnInventParty.onClick.AddListener(OnClickInventToParty);
        _btnChat.onClick.AddListener(OnClickChat);
        _btnGuildInvite.onClick.AddListener(OnClickGuildInvite);
        _btnRemoveFriend.onClick.AddListener(OnClickRemoveFriend);
    }

    private void OnClickRemoveFriend()
    {
        APIModels.FriendRequest friendRequest = new APIModels.FriendRequest();
        friendRequest.friendPlayfabId = _userProfile.playfabId;
        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
            PlayfabContant.FunctionName.RemoveFriend,
            friendRequest,
            false,
            null,
            null);

        NetCodeModels.FriendContent friendContent = new NetCodeModels.FriendContent();
        friendContent.playfabReciverId = _userProfile.playfabId;
        friendContent.message = PlayfabContant.FriendSyncTag.RemoveFriend;
        friendContent.friend = new NetCodeModels.Friend(PlayfabManager.Instance.PlayfabID,
            PlayerDataManager.Instance.avatarUrl,
            PlayerDataManager.Instance.displayName,
            PlayerDataManager.Instance.level,
            new List<string>() { PlayfabContant.FriendTag.Friend});

        ServerManager.Instance.FriendControllerServerRpc(friendContent);
        PlayerDataManager.Instance.friendManager.RemoveFriend(_userProfile.ToFriend());
    }

    private void OnClickGuildInvite()
    {
        //APIModels.GuildRequest guildRequest = new APIModels.GuildRequest()
        //{
        //    requestType = PlayfabContant.GuildTag.Invite,
        //    subFunc = new APIModels.GuildInviteRequest()
        //    {
        //        guild = PlayerDataManager.Instance.guildManager.guildEntity,
        //        playfabIdOfMember = _userProfile.playfabId
        //    }
        //};
        //PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
        //    PlayfabContant.FunctionName.Guild,
        //    guildRequest,
        //    false,
        //    (obj, data) =>
        //    {

        //    }, null);

        NetCodeModels.EntityKey guildEntity = new NetCodeModels.EntityKey()
        {
            Id = PlayerDataManager.Instance.guildManager.guildEntity.Id,
            Type = PlayerDataManager.Instance.guildManager.guildEntity.Type,
        };

        ServerManager.Instance.GuildInviteServerRpc(_userProfile.playfabId, guildEntity);

    }

    private void OnClickChat()
    {
        TargetChat targetChat = new TargetChat();

        targetChat.displayName = _userProfile.displayName;
        targetChat.playfabId = _userProfile.playfabId;
        PlayerDataManager.Instance.globalReusableData.SetTargetChat(targetChat);
        PlayerDataManager.Instance.globalReusableData.typeChat = TypeChat.Private;
        AudioManager.Instance.PlaySFX(AudioName.ClickButton);
        Hide();
    }

    private void OnClickInventToParty()
    {
        AudioManager.Instance.PlaySFX(AudioName.ClickButton);


        //Invent to party
    }

    protected override void OnShowing()
    {
        base.OnShowing();
      
        _userProfile = (UserProfile) Parameter;

        _btnAddFriend.gameObject.SetActive(!PlayerDataManager.Instance.friendManager.ContainInList(_userProfile.playfabId));
        _btnRemoveFriend.gameObject.SetActive(PlayerDataManager.Instance.friendManager.IsFriend(_userProfile.playfabId));

        _btnGuildInvite.gameObject.SetActive(PlayerDataManager.Instance.guildManager.isAdmin || PlayerDataManager.Instance.guildManager.isSubAdmin);

        _txtDisplayName.text = _userProfile.displayName;
        _txtLevel.text = _userProfile.level.ToString();
        _avatar.sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar,_userProfile.avatarUrl);
        //Show Model
        if(_userProfile.playerItem != null)
        {
            RenderManager.Instance.RenderCharacter(_userProfile.playerItem);
        }
        GetProfile();

    }
    protected override void OnHidden()
    {
        base.OnHidden();
        _txtDisplayName.text = "";
        _txtLevel.text = "";
        _avatar.sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, "");
    }

    private void GetProfile()
    {
        APIModels.FriendRequest friendRequest = new APIModels.FriendRequest();
        friendRequest.friendPlayfabId = _userProfile.playfabId;
        PlayfabManager.Instance.ExecuteFunction<APIModels.GetPlayerInfoResponse>(
            PlayfabContant.FunctionName.GetPlayerInfo,
            friendRequest,
            false,
            OnGetInfoSuccess,
            null); 
    }

    private void OnGetInfoSuccess(object arg1, APIModels.GetPlayerInfoResponse data)
    {
       if(data.playfabId == _userProfile.playfabId)
       { 
            _txtDisplayName.text = data.displayName;
            _txtLevel.text = data.level.ToString();
            _txtIntroduction.text = data.introduction.ToString();
            _avatar.sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, data.avatarUrl);
            RenderManager.Instance.RenderCharacter(data.playerItem);
            //Show Model
       }
    }

    public void OnClickAddFriend()
    {
        AudioManager.Instance.PlaySFX(AudioName.ClickButton);
        AddFriend();
    }
    private void AddFriend()
    {
        APIModels.FriendRequest friendRequest = new APIModels.FriendRequest();
        friendRequest.friendPlayfabId = _userProfile.playfabId;
        PlayfabManager.Instance.ExecuteFunction<APIModels.AddFriendResponse>(PlayfabContant.FunctionName.AddFriend,
            friendRequest,
            false,
            (obj, data) =>
            {
                PlayerDataManager.Instance.friendManager.AddFriend(data.friend);
            },
            null);

        NetCodeModels.FriendContent friendContent = new FriendContent();
        friendContent.playfabReciverId = _userProfile.playfabId;
        friendContent.message = PlayfabContant.FriendSyncTag.RequestFriend;
        friendContent.friend = new NetCodeModels.Friend(PlayfabManager.Instance.PlayfabID,
           PlayerDataManager.Instance.avatarUrl,
           PlayerDataManager.Instance.displayName,
           PlayerDataManager.Instance.level,
           new List<string>() { PlayfabContant.FriendTag.Received});

        ServerManager.Instance.FriendControllerServerRpc(friendContent);

        Hide();
    }

    private void ClosePopup()
    {
        Hide();
        AudioManager.Instance.PlaySFX(AudioName.PopupClose);
    }
}
