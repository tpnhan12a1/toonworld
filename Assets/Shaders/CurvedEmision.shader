﻿Shader "Unlit/CurvedEmission"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _EmissionTex("Emission Texture", 2D) = "white" {}
        _CurveStrength("Curve Strength", Range(0, 1)) = 0.5
        _EmissionColor("Emission Color", Color) = (1, 1, 1, 1)
    }

        SubShader
        {
            Tags { "RenderType" = "Opaque" }
            LOD 100

            CGPROGRAM
            #pragma surface surf Lambert noforwardadd
            #pragma target 3.0
            // make fog work 

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            sampler2D _EmissionTex;
            float _CurveStrength;
            fixed4 _EmissionColor;

            struct Input
            {
                float2 uv_MainTex;
                float2 uv_EmissionTex;
            };

            void vert(inout appdata_full v)
            {
                // Calculate the curved effect
                float dist = UNITY_Z_0_FAR_FROM_CLIPSPACE(v.vertex.z);
                v.vertex.y -= _CurveStrength * dist * dist * _ProjectionParams.x;
            }

            void surf(Input IN, inout SurfaceOutput o)
            {
                // Sample the main texture
                fixed4 c = tex2D(_MainTex, IN.uv_MainTex);

                // Sample the emission texture
                fixed4 emission = tex2D(_EmissionTex, IN.uv_EmissionTex) * _EmissionColor;

                // Calculate the curved effect (for vertex shading, if needed)
                // float dist = UNITY_Z_0_FAR_FROM_CLIPSPACE(IN.worldPos.z);
                // IN.worldPos.y -= _CurveStrength * dist * dist * _ProjectionParams.x;

                // Apply color and emission
                o.Albedo = c.rgb;
                o.Emission = emission.rgb;
                o.Alpha = c.a;

                // Enable fog
               
            }
            ENDCG
        }
            FallBack "Diffuse"
}