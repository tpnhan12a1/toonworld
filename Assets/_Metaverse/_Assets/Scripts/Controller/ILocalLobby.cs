using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILocalLobby
{
    public ToonWorld.GameRoomLobby GetGameRoomLobbyData();
    public int GetLocalIndex();
    public void RegisterOnListDataChanged(Action<List<NetCodeModels.PlayerData>> onListDataChanged);
    public void UnRegisterOnListDataChanged(Action<List<NetCodeModels.PlayerData>> onListDataChanged);

    public void RegisterOnLobbyDataChanged(Action<ToonWorld.GameRoomLobby> onLobbyDataChanged);
    public void UnRegisterOnLobbyDataChanged(Action<ToonWorld.GameRoomLobby> onLobbyDataChanged);
    public void OnPlayerLeaveLobby(string playfabId);
    public void OnPlayerJoinLobby(NetCodeModels.PlayerData playerData);
    public List<NetCodeModels.PlayerData> GetPlayerDatas();
    public void RegisterOnSlotIndexChanged(Action<int> onSlotIndexChanged);
    public void UnRegisterOnSLotIndexChanged(Action<int> onSLotIndexChanged);
}
