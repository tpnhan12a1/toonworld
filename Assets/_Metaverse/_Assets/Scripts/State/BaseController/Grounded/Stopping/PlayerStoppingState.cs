using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class PlayerStoppingState : PlayerGroundedState
{
    public override void OnEnterState()
    {
        ResetVelocity();
        PlayerDataManager.Instance.reusableData.movementSpeedModifier = 0f;
        PlayerDataManager.Instance.reusableData.shouldSprint = false;
        
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.StoppingParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.StoppingParameterHash);
    }
    protected override void OnMouseMovementStarted(InputAction.CallbackContext context)
    {
        OnMove();
    }
    protected override void AddInputActionsCallbacks()
    {
        base.AddInputActionsCallbacks();
        PlayerInput.Instance.PlayerActions.Move.started += OnMovementStarted;

    }
    protected override void RemoveInputActionCallbacks()
    {
        base.RemoveInputActionCallbacks();
        PlayerInput.Instance.PlayerActions.Move.started -= OnMovementStarted;

    }
    public override void OnAnimationTransitionEvent()
    {
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Idling]);
    }
    private void OnMovementStarted(InputAction.CallbackContext context)
    {
        OnMove();
    }
    protected override void OnMove()
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        if (PlayerDataManager.Instance.reusableData.ShouldWalk())
        {
            _stateMachine.ChangeState(stateMachine.states[StateType.Walking]);
            return;
        }
        _stateMachine.ChangeState(stateMachine.states[StateType.Running]);
    }
}
