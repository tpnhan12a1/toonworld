using Imba.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameJumpingState : RunningGameAirborneState
{
    private bool canStartFalling;
    public override StateType GetStateType()
    {
        return StateType.RunningGameJumpingState;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        canStartFalling = false;
        AudioManager.Instance.PlaySoundFromSource(AudioName.Jump, _runningGameStateMachine.playerRunningGameController.audioSource);
        Jump();
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        if (!canStartFalling && IsMoveUp())
        {
            canStartFalling = true;
        }
        if (!canStartFalling || GetVerticalVelocity().y > 0) return;
        _runningGameStateMachine.ChangeState(_runningGameStateMachine.states[StateType.RunningGameFallingState]);

    }
    public override void OnExitState()
    {
        base.OnExitState();
        canStartFalling = false;
    }
    private void Jump()
    {
        Vector3 jumpForce = Vector3.up * _runningGameStateMachine.playerRunningGameController.runningGameReusableData.jumpForce;
        _runningGameStateMachine.playerRunningGameController.rb.AddForce(jumpForce, ForceMode.VelocityChange);
    }
}
