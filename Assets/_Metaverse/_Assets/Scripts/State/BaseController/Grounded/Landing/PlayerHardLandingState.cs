using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHardLandingState : PlayerLandingState
{
    public override StateType GetStateType()
    {
        return StateType.HardLanding;
    }
    public override void OnEnterState()
    {
        MotionStateMachine stateMachine =
            (MotionStateMachine)_stateMachine;
        PlayerDataManager.Instance.reusableData.movementSpeedModifier = 0f;
        PlayerInput.Instance.PlayerActions.Move.Disable();
        base.OnEnterState();
        ResetVelocity();
        StartAnimation(PlayerDataManager.Instance.animationData.HardLandParameterHash);
    }
    public override void OnAnimationExitsEvent()
    {
        PlayerInput.Instance.PlayerActions.Move.Enable();
    }
    public override void OnAnimationTransitionEvent()
    {
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Idling]);

    }
    public override void OnExitState()
    {
        base.OnExitState();
        MotionStateMachine stateMachine =
            (MotionStateMachine)_stateMachine;
        PlayerInput.Instance.PlayerActions.Move.Enable();
        StopAnimation(PlayerDataManager.Instance.animationData.HardLandParameterHash);
    }
    protected override void AddInputActionsCallbacks()
    {
        PlayerInput.Instance.PlayerActions.Move.started += OnMouseMovementStarted;
        PlayerInput.Instance.PlayerActions.Move.canceled += OnMovementCanceled;
        PlayerInput.Instance.PlayerActions.Move.performed += OnMovementPerformed;
    }
    protected override void RemoveInputActionCallbacks()
    {
        PlayerInput.Instance.PlayerActions.Move.started -= OnMouseMovementStarted;
        PlayerInput.Instance.PlayerActions.Move.canceled -= OnMovementCanceled;
        PlayerInput.Instance.PlayerActions.Move.performed -= OnMovementPerformed;
    }

}
