using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserProfile
{
    public string playfabId;
    public string displayName;
    public int level;
    public string introduction;
    public string avatarUrl;
    public APIModels.PlayerItem playerItem;

    public ToonWorld.Friend ToFriend()
    {
        return new ToonWorld.Friend(playfabId, avatarUrl,displayName,level, new List<string>());
    }
}
