using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenePreviewCharacterManager : ManualSingletonMono<ScenePreviewCharacterManager>
{
    [SerializeField]
    private CharacterRenderController _characterRenderController;
    public CharacterRenderController characterRenderController { get { return _characterRenderController; } }

    [SerializeField] private Transform _cameraTransformRender;
    public Transform cameraTransformRender { get { return _cameraTransformRender; } }
}
