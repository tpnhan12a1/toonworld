using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameRightSwipingState : RunningGameSwipingState
{
    public override StateType GetStateType()
    {
        return StateType.RunningGameRightSwipingState;
    }

    
    public override void OnEnterState()
    {
        base.OnEnterState();
        Debug.Log($"Enter the {GetStateType()}");
        _runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.currentLane += 1;
        RightSwipe();
        _runningGameSwipeStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.RightSwipeParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameSwipeStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.RightSwipeParameterHash);

    }

   
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        ChangeEmptyState();
    }
    public void ChangeEmptyState()
    {
        Vector3 currentPosition = _runningGameSwipeStateMachine.playerRunningGameController.transform.position;
        if (currentPosition.x >= _runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.GetXCurrentLocationOfLane())
        {
            _runningGameSwipeStateMachine.ChangeState(_runningGameSwipeStateMachine.states[StateType.RunningGameEmptySwipingState]);
        }
    }
    private void RightSwipe()
    {
        Vector3 dir = _runningGameSwipeStateMachine.playerRunningGameController.transform.right;
        dir.y = 0f;
        Vector3 force = dir * _runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.swipeForce;
       
        _runningGameSwipeStateMachine.playerRunningGameController.rb.AddForce(
            force
           , ForceMode.VelocityChange);
    }

   
}
