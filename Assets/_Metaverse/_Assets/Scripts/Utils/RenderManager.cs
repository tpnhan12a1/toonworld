using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderManager : ManualSingletonMono<RenderManager>
{
    private SceneRenderManager _sceneRenderManager;
    public void RenderCharacter(APIModels.PlayerItem playerItem)
    {
        if (SceneRenderManager.Instance == null)
        {
            SGSceneManager.Instance.LoadSceneAdditiveAsync(SceneName.SceneRender, () =>
            {
                RenderCharacterWithData(playerItem);
            });
        }
        else
        {
            RenderCharacterWithData(playerItem);
        }
    }

    private void RenderCharacterWithData(APIModels.PlayerItem playerItem)
    {
        _sceneRenderManager = SceneRenderManager.Instance;
        _sceneRenderManager.SetDataCharacterRender(playerItem);

    }
}
