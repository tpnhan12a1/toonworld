using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToonWorld{
    public class GameRoomLobby
    {
        public int id;
        public string ownerPlayfabId = "";
        public string avatarUrl = "";
        public string owner = "";
        public string password = "";
        public GamePlayType gamePlayType = GamePlayType.None;
        public uint maxPlayer;
    }

}
