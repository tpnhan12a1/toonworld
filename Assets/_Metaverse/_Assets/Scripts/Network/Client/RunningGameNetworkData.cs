using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class RunningGameNetworkData : PlayerNetworkData
{
    [SerializeField] private PlayerRunningGameController _controller;
    [SerializeField] private ParticleSystem _shieldParticle;
    NetworkVariable<bool> _isUseShield = new NetworkVariable<bool>(false, writePerm:NetworkVariableWritePermission.Owner);
    NetworkVariable<bool> _isVisible = new NetworkVariable<bool>(false, writePerm: NetworkVariableWritePermission.Owner);


    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        _isUseShield.OnValueChanged += OnUseShieldChanged;
        _isVisible.OnValueChanged += OnVisibleChanged;

        if(IsOwner)
        {
            _controller.runningGameReusableData.RegisterOnIsUseShieldChanged(OnIsUseShieldChanged);
            _controller.runningGameReusableData.RegisterOnIsVisibleChanged(OnIsVisibleChanged);
        }
        
    }
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        _isUseShield.OnValueChanged -= OnUseShieldChanged;
        _isVisible.OnValueChanged -= OnVisibleChanged;
        if(IsOwner)
        {
            _controller.runningGameReusableData.UnRegisterIsUseShieldChanged(OnIsUseShieldChanged);
            _controller.runningGameReusableData.UnRegisterIsVisibleChanged(OnIsVisibleChanged);
        }
    }
    private void OnIsUseShieldChanged(bool previousValue, bool newValue)
    {
        _isUseShield.Value = newValue;
    }

    private void OnIsVisibleChanged(bool previousValue, bool newValue)
    {
        _isVisible.Value = newValue;
    }


    private void OnUseShieldChanged(bool previousValue, bool newValue)
    {
       if(newValue == true)
        {
            _shieldParticle.Play();
        }
        else
        {
            _shieldParticle.Stop();
        }
    }
    private void OnVisibleChanged(bool previousValue, bool newValue)
    {
        
    }
}
