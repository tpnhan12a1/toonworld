using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SceneHomeManager : ManualSingletonMono<SceneHomeManager>
{
    [SerializeField]
    private PlacementSystem _placementSystem;
    [SerializeField]
    private Transform _placementParent;
    [SerializeField]
    private int amountItemLoadPerFrame = 20;
    public PlacementSystem playmentSystem { get { return _placementSystem; } }
    
    public void Start()
    {
        // CHeck owner
        PlayerDataManager.Instance.placementManager.RegisterOnPlacementDataChanged(OnPlacementDataChanged);
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        PlayerDataManager.Instance.placementManager.UnRegisterOnPlacementDataChanged(OnPlacementDataChanged);
        CoroutineHandler.Instance.StopCoroutine("LoadObjectCoroutine");
    }
    private void OnPlacementDataChanged(List<Placement> placementDatas)
    {
        Debug.LogWarning("Load object im house");
        CoroutineHandler.Instance.StartCoroutine(LoadObjectCoroutine( placementDatas));
    }

    private IEnumerator LoadObjectCoroutine(List<Placement> placementDatas)
    {
        int count = 0;
       List<PlacementObject> placementObjects = _placementParent.GetComponentsInChildren<PlacementObject>().ToList();
        foreach (PlacementObject placementObject in placementObjects)
        {
            Destroy(placementObject.gameObject);
        }
        foreach (var placement in placementDatas)
        {
            if (count == amountItemLoadPerFrame)
            {
                count = 0;
                yield return null;
            }
            count++;
            PlacementSO placementSO = Imba.Utils.ResourceManager.Instance.GetResourceByName<PlacementSO>(new StringBuilder().Append("Home/Placement/").Append(placement.itemId).ToString());
            PlacementObject placementObject = Instantiate(placementSO.placementObject, new Vector3()
            {
                x = placement.position.x,
                y = placement.position.y,
                z = placement.position.z
            },
            new Quaternion()
            {
                x = placement.rotation.x,
                y = placement.rotation.y,
                z = placement.rotation.z,
                w = placement.rotation.w
            },
            _placementParent.transform);
            Item item = PlayerDataManager.Instance.inventoryManager.GetItem(placement.itemId, placement.itemInstanceId);
            if(item != null)
            {
                placementObject.SetData(item);
            }
        }

        yield return null;
    }
}
