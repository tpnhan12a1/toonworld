using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class VFXManager : ManualSingletonMono<VFXManager> 
{

    [SerializeField] private string localPathVfx = "VFX";

    public GameObject GetVFXByName(VFXName name)
    {
       GameObject gameObject = Resources.Load<GameObject>(new StringBuilder().Append(localPathVfx).Append("/").Append(name).ToString());
       return gameObject;
    }

    public GameObject InitVFX(VFXName name, Vector3 position, Quaternion rotation, Transform parent)
    {
        GameObject gameObject = GetVFXByName(name);
        return Instantiate(gameObject, position, rotation, parent);
    }
}
