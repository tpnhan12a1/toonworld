﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class PlacementObject : MonoBehaviour
{
    [SerializeField]
    private List<MeshRenderer> _meshRenderers= new List<MeshRenderer>();
    [SerializeField]
    private List<TypePlacement> _typePlacements;
    [SerializeField] private Button _btnPlace;
    [SerializeField] private Button _btnStore;
    [SerializeField] private Button _btnRotation;
    [SerializeField] private Canvas _canvas;
    [SerializeField] private PlacementSystem _placementSystem;
    [SerializeField] private float _animationDuration = 0.5f;

    private Item _item;
    private Bounds _limitBounds;
    private Bounds _extendBounds;
    public Item item { get { return _item; } }
       
    public  Vector3Int size { get
        {
            Vector3Int intSize = new Vector3Int(
                Mathf.CeilToInt(_limitBounds.size.x),
                 Mathf.CeilToInt(_limitBounds.size.y),
                  Mathf.CeilToInt(_limitBounds.size.z));
            return intSize;
        } 
    } 
    public Bounds limitBounds { get { return _limitBounds; } }
    public Bounds extendBounds { get { return  _extendBounds; } }   
    public List<TypePlacement> typePlacements { get { return _typePlacements; } }
    private void OnValidate()
    {
        SetupBounds();
    }
    public void Awake()
    {
        SetupBounds();
        _btnStore.onClick.AddListener(StorePlacement);
        _btnRotation.onClick.AddListener(RotationPlacement);
        _btnPlace.onClick.AddListener(PlacePlacement);
    }
    public void SetPlacementSystem(PlacementSystem placementSystem)
    {
        _placementSystem = placementSystem;
    }
    public void SetStatusButton(bool status)
    {
        _canvas.transform.DOScale( Convert.ToInt32(status), _animationDuration).SetEase(Ease.OutBounce);
    }

    private void PlacePlacement()
    {
        _placementSystem?.PlaceObject();
    }

    private void RotationPlacement()
    {
        _placementSystem?.RotationObject();
    }

    private void StorePlacement()
    {
        _placementSystem?.StoreObject();
    }

    private void SetupBounds()
    {
        Bounds bounds = new Bounds();
        foreach (MeshRenderer meshRenderer in _meshRenderers)
        {
            bounds.Encapsulate(meshRenderer.bounds.size);
        }
        Vector3 offset = Vector3.zero;
        offset.y = _limitBounds.extents.y;
        _limitBounds.center = transform.position + offset;

        _limitBounds = bounds;

        Vector3 extendSize = Vector3.zero;
        extendSize.x = (int)_limitBounds.size.x + (_limitBounds.size.x% (int)_limitBounds.size.x == 0 ? 0 : 2);
        extendSize.y = (int)_limitBounds.size.y + (_limitBounds.size.y % (int)_limitBounds.size.y == 0 ? 0 : 2);
        extendSize.z = (int)_limitBounds.size.z + (_limitBounds.size.z % (int)_limitBounds.size.z == 0 ? 0 : 2);
        _extendBounds = new Bounds(_limitBounds.center, extendSize);
    }

    public int GetXZ()
    {
        Vector3Int size = this.size;
        return size.x * size.z;
    }
    public int GetXY()
    {
        Vector3Int size = this.size;
        return size.x * size.y;
    }
    public Vector3 GetTopXZPoint()
    {
        return new Vector3(transform.position.x - size.x / 2, 0f, transform.position.z - size.z / 2);
    }
    public void OnRotation()
    {
        SetupBounds();
    }
    public static LayerMask GetLayerMask(TypePlacement typePlacement)
    {
        return LayerMask.NameToLayer(typePlacement.ToString());
    }
    public void SetData(Item item )
    {
        _item = item;
    }
#if UNITY_EDITOR
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Vector3 centerBound = transform.position;
        centerBound.y += _limitBounds.center.y;

        Gizmos.DrawWireCube(centerBound, _limitBounds.size);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(centerBound, _extendBounds.size);  
    }
#endif
}
