using UnityEngine;

public class Item
{
    public string itemId;
    public string itemClass;
    public string displayName;
    public string itemInstanceId;
    public string unitCurrency;
    public string unitPrice;
    public override int GetHashCode()
    {
        return itemId.GetHashCode() ^ itemClass.GetHashCode();
    }
    public override bool Equals(object obj)
    {
       Item item = obj as Item;
       if(item == null) return false;
       return item.itemInstanceId == itemInstanceId && item.itemId == itemId;
    }
}
