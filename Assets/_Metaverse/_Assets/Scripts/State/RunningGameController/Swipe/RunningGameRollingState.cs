using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameRollingState : RunningGameSwipingState
{
    private float startTime = 0f;
    private Vector3 centerOfCollisionColider;
    private float heightOfColisionColider;
    private float radiousOfCollisionCollider;
    public override StateType GetStateType()
    {
        return StateType.RunningGameRollingState;
    }

  
    public override void OnEnterState()
    {
        base.OnEnterState();
        AddInputActionsCallbacks();
        Debug.Log($"Enter the {GetStateType()}");
        startTime = 0f;
        centerOfCollisionColider = _runningGameSwipeStateMachine.playerRunningGameController.collisionCheckColider.center;
        heightOfColisionColider = _runningGameSwipeStateMachine.playerRunningGameController.collisionCheckColider.height;
        radiousOfCollisionCollider = _runningGameSwipeStateMachine.playerRunningGameController.collisionCheckColider.radius;

        NarrowTheScopeOfCollision();
        if(!_runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.isGrounded)
        {
            AddDownForce();
        }
        //_runningGameSwipeStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.RollParameterHash);
    }

   
    public override void OnUpdate()
    {
        base.OnUpdate();
        startTime += Time.deltaTime;
        if( startTime > _runningGameSwipeStateMachine.playerRunningGameController.playerData.timeToRolling) 
        {
            _runningGameSwipeStateMachine.ChangeState(_runningGameSwipeStateMachine.states[StateType.RunningGameEmptySwipingState]);
        }
    }


    public override void OnExitState()
    {
        base.OnExitState();
        RemoveInputActionCallbacks();
        ExpandTheScopeOfCollision();
        startTime = 0f;
        //_runningGameSwipeStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.RollParameterHash);
    }

    private void NarrowTheScopeOfCollision()
    {
        _runningGameSwipeStateMachine.playerRunningGameController.collisionCheckColider.height = 2 * radiousOfCollisionCollider;
        _runningGameSwipeStateMachine.playerRunningGameController.collisionCheckColider.center = Vector3.zero;
    }
    private void ExpandTheScopeOfCollision()
    {
        _runningGameSwipeStateMachine.playerRunningGameController.collisionCheckColider.height = heightOfColisionColider;
        _runningGameSwipeStateMachine.playerRunningGameController.collisionCheckColider.center = centerOfCollisionColider;
    }
    private void AddDownForce()
    {
        Vector3 dir = Vector3.down;
        Vector3 force = _runningGameSwipeStateMachine.playerRunningGameController.playerData.rollingForce* dir;
        _runningGameSwipeStateMachine.playerRunningGameController.rb.AddForce(force, ForceMode.VelocityChange);
    }

    protected virtual void AddInputActionsCallbacks()
    {
        PlayerInput.Instance.InputAction.Player.Move.started += OnMoveStarted;
    }


    protected virtual void RemoveInputActionCallbacks()
    {
        PlayerInput.Instance.InputAction.Player.Move.started -= OnMoveStarted;

    }

    private void OnMoveStarted(UnityEngine.InputSystem.InputAction.CallbackContext context)
    {
        ExpandTheScopeOfCollision();
        Vector2 movementInput = context.ReadValue<Vector2>();
        if (movementInput == Vector2.zero) return;
        int lane = _runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.currentLane;

        if (Vector3.Angle(movementInput, Vector3.left) < 45f)
        {
            lane -= 1;
            if (lane < 1 || lane > 3) return;
            _runningGameSwipeStateMachine.ChangeState(_runningGameSwipeStateMachine.states[StateType.RunningGameLeftSwipingState]);
            return;
        }
        if (Vector3.Angle(movementInput, Vector3.right) < 45f)
        {
            lane += 1;
            if (lane < 1 || lane > 3) return;
            _runningGameSwipeStateMachine.ChangeState(_runningGameSwipeStateMachine.states[StateType.RunningGameRightSwipingState]);
            return;
        }
    }
    public override void OnAnimationTransitionEvent()
    {
        base.OnAnimationTransitionEvent();
        _runningGameSwipeStateMachine.ChangeState(_runningGameSwipeStateMachine.states[StateType.RunningGameEmptySwipingState]);
    }
}
