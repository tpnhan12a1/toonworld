using System;
using Unity.Netcode;
using Unity.Netcode.Transports.UTP;
using UnityEngine;

public class PreLoader : MonoBehaviour
{
    private const string internalServerIP = "0.0.0.0";
    private ushort _serverPort = 7777;

    [SerializeField] private NetworkObject _serverManager;
    [SerializeField] private NetworkObject _clientManager;
    [SerializeField] private NetworkObject _networkObjectPoolingManager;

    bool isServer = false;
    public void Start()
    {
        NetworkManager.Singleton.OnServerStarted += OnServerStarted;
        var args =  System.Environment.GetCommandLineArgs();
        for(int i=0;i< args.Length; i++)
        {
            if (args[i] == "-dedicatedServer")
            {
                isServer = true;
            }
            if (args[i] =="-port" && (i+1) < args.Length)
            {
                _serverPort = ushort.Parse(args[i+1]);
            }
        }
        if(isServer)
        {
            StartServer();
        }
        else
        {
            SGSceneManager.Instance.ChangeScene(SceneName.SceneLobby);
        }
    }

    private void OnServerStarted()
    {
       if(NetworkManager.Singleton.IsServer)
       {
            NetworkObject serverManager = Instantiate(_serverManager);
            serverManager.Spawn();
            NetworkObject clientManager = Instantiate(_clientManager);
            clientManager.Spawn();
            NetworkObject networkObjectPooling = Instantiate(_networkObjectPoolingManager);
            networkObjectPooling.Spawn();
       }
    }

    private void StartServer()
    {
        NetworkManager.Singleton.GetComponent<UnityTransport>().SetConnectionData(
            internalServerIP, _serverPort);

        NetworkManager.Singleton.StartServer();
        Debug.LogWarning($"Server listen on port: {_serverPort}");
    }
}
