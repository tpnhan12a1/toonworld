using Imba.UI;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ToonWorld;
public class GameRoomLobbyItem : LoopGridViewItem
{
    [SerializeField] private Button _btnJoinRoomLobby;
    [SerializeField] private TMP_Text _txtRoomOwner;
    [SerializeField] private Image _imageRoomLook;
    [SerializeField] private Image _avatar;

    private GameRoomLobby _gameRoomLobby;
    private GameRoomLobbyGridViewContronller _gameRoomLobbyGridViewContronller;

    public void Awake()
    {
        _btnJoinRoomLobby.onClick.AddListener(CheckAndJoinRoom);
    }

    public void OnRefresh(GameRoomLobby gameRoomLobby, GameRoomLobbyGridViewContronller gameRoomLobbyGridViewContronller)
    {
        _gameRoomLobby = gameRoomLobby;
        _gameRoomLobbyGridViewContronller = gameRoomLobbyGridViewContronller;
        _txtRoomOwner.text = _gameRoomLobby.owner;
        _imageRoomLook.SetActive(_gameRoomLobby.password != "");
        _avatar.sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, gameRoomLobby.avatarUrl);
    }
    private void CheckAndJoinRoom()
    {
        ServerManager.Instance.JoinLobbyServerRpc(_gameRoomLobby.gamePlayType, _gameRoomLobby.id);
    }
}
