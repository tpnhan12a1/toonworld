using Imba.Audio;
using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuPopup : UIPopup
{
    [SerializeField] private Button _btnClosePopup;
    [SerializeField] private Button _btnChatPopup;
    [SerializeField] private Button _btnOpenFriendPopup;
    [SerializeField] private Button _btnReturnHome;
    [SerializeField] private Button _btnCharacterReview;
    [SerializeField] private Button _btnReturnVituralWorld;
    [SerializeField] private Button _btnGuild;

    protected override void OnInit()
    {
        base.OnInit();
        _btnClosePopup.onClick.AddListener(ClosePopup);
        _btnChatPopup.onClick.AddListener(OpenChatPopup);
        _btnOpenFriendPopup.onClick.AddListener(OpenFriendPopup);
        _btnReturnHome.onClick.AddListener(ReturnHome);
        _btnCharacterReview.onClick.AddListener(CharacterReview);
        _btnReturnVituralWorld.onClick.AddListener(ReturnVituralWorld);
        _btnGuild.onClick.AddListener(OpenGuildPopup);
    }

    private void OpenGuildPopup()
    {
        if (PlayerDataManager.Instance.guildManager.isHaveGuild)
        {
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.GuildPopup);
        }
        else
        {
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.GuildInventationPopup);
        }
        Hide();
    }

    private void OpenChatPopup()
    {
        Hide();
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.ChatPopup);
    }

    private void CharacterReview()
    {
        Hide();
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.EditorCharacterPopup);
    }

    private void ReturnVituralWorld()
    {
        Hide();
        ServerManager.Instance.JoinVituralWorld();
    }
    private void ReturnHome()
    {
        Hide();
        NetCodeModels.CreateRoomRequest request = new NetCodeModels.CreateRoomRequest()
        {
            owner = PlayfabManager.Instance.PlayfabID,
            sceneName = SceneName.SceneHome
        };
        ServerManager.Instance.CreateOrJoinRoomServerRpc(request);
    }

    private void OpenFriendPopup()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.FriendPopup);
        Hide();
    }

    private void ClosePopup()
    {
        AudioManager.Instance.PlaySFX(AudioName.PopupClose);
        Hide();
    }
}
