using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ITab
{
    public void Selected();
    public void Deselected();
    public void SetTabController(TabController controller);
}
