using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuildListViewController : LoopListView2
{
    private List<GuildMember> _guildMembers;
    public void OnInit()
    {
        _guildMembers = new List<GuildMember>();
        InitListView(_guildMembers.Count, OnGetGuidMemberByIndex);
    }


    public void OnShow(List<GuildMember> guildMembers)
    {
        _guildMembers.Clear();
        _guildMembers.AddRange(guildMembers);
        Refresh();
    }
    public void OnHide()
    {
        _guildMembers?.Clear();
    }
    private LoopListViewItem2 OnGetGuidMemberByIndex(LoopListView2 listView, int index)
    {
        if (_guildMembers.Count == 0) return null;
        GuildMember guildMember = _guildMembers[index];
        if (guildMember.role != PlayfabContant.PlayerTag.Inventations)
        {
            GuildMemberItem guildMemberItem = (GuildMemberItem)listView.NewListViewItem("GuildMemberItem");
            guildMemberItem.OnRefresh(guildMember, this);
            return guildMemberItem;
        }
        else
        {
            GuildInventationItem guildInventationItem = (GuildInventationItem)listView.NewListViewItem("GuildInventationItem");
            guildInventationItem.OnRefresh(guildMember, this);
            return guildInventationItem;
        }
    }
    public void Refresh()
    {
        SetListItemCount(_guildMembers.Count);
        RefreshAllShownItem();
    }
}
