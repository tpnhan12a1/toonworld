using UnityEngine;
using SuperScrollView;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class AnimationPrefab : LoopGridViewItem
{
    [SerializeField] private Image _itemSprite;
    [SerializeField] private Button _btnItem;
    [SerializeField] private MeshFilter _itemMesh;
    [SerializeField] private MeshRenderer _ItemMeshRenderer;

    CharacterRender characterRender;

    private AnimationItemGridViewController _animationItemGridViewController;

    private AnimationItem _animationItem;

    void Awake()
    {
        _btnItem.onClick.AddListener(OnItemClick);
    }

    public void InitAnimationItem(AnimationItem item, AnimationItemGridViewController animationItemGridViewController, CharacterRender characterRender)
    {
        _animationItemGridViewController = animationItemGridViewController;

        this.characterRender = characterRender;

        _animationItem = item;

        if (_itemSprite != null)
        {
            _itemSprite.sprite = item.sprite;
        }
        else
        {
            Debug.LogError("Image is not assigned in the inspector");
        }
    }

    public void OnItemClick()
    {
        if (characterRender == null)
        {
            Debug.LogError("CharacterRender is not assigned in the inspector");
            return;
        }
        else
        {
            if (_animationItem != null)
            {
                //To DO
                //characterRender.ChangeMeshWithAppearanceType(_animationItem, characterRender);
            }
            else
            {
                Debug.LogError("AppearanceItem or EquipmentItem is not assigned in the inspector");
                return;
            }
        }
    }
}