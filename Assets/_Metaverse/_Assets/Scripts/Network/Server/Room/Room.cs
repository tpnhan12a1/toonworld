using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Unity.Netcode;
using UnityEngine;

public enum GamePlayType: byte
{
    Running,
    None
}
public enum GenrationGamePlay : byte
{
    PreGame,
    InGame,
    Summary
    
}
public class Room
{
    public ulong roomId;
    public ulong playerMaster;
    public SceneName sceneName;
    public string owwer = "";
    public bool isDestroyWhenNoOne;
    public Action OnRoomDestroy;
    public RoomManager<Room> roomManager;
    public PlayerManager<Player> playerManager;
    public List<NetworkObject> networkObjects;
    public List<NetworkObject> playerObjects;

    public Room(RoomManager<Room> roomManager,SceneName sceneName ,string owner = "", bool isDestroyWhenNoOne = false)
    {
        this.roomManager = roomManager;
        playerManager = new PlayerManager<Player>();
        networkObjects = new List<NetworkObject>();
        playerObjects = new List<NetworkObject>();
        this.sceneName = sceneName;
        this.owwer = owner;
        this.isDestroyWhenNoOne = isDestroyWhenNoOne;
    }
    public virtual void PlayerJoinRoom(Player player)
    {
        Debug.Log($"Player {player.GetClientId()} join room {roomId}");
        playerManager.AddPlayer(player);
        player.SetRoom(this);
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = new[] { player.GetClientId() },
            }
        };
        ClientManager.Instance.ResponseJoinRoomClientRpc(roomId,sceneName, clientRpcParams);
    }
    public virtual void OnPlayerReadyInRoom(Player player)
    {
        ShowObjectInRoomToNewPlayer(player);
        ShowPlayerInRoomToNewPlayer(player);
    }

    public NetworkObject SpawnPlayerObjectInRoom(Player player, Vector3 position, Quaternion rotation)
    {
        NetworkObject playerObject = NetworkManager.Singleton.ConnectedClients[player.GetClientId()].PlayerObject;
        if (playerObject == null)
        {
            playerObject =
                ServerManager.Instance
                .spawnManager
                .SpawnAsPlayerObject(player, position, rotation);
        }
        if (playerObject != null)
        {
            OnSpawnPlayerInRoom(player, playerObject);
            ShowPlayerObjectToOddPlayer(playerObject);
        }
        return playerObject;
    }

    public virtual void PlayerOutRoom(Player player)
    {
        Debug.Log($"Player {player.GetClientId()} out room {roomId}");
        playerManager.RemovePlayer(player);
        player.SetRoom(null);
        NetworkObject playerObject = NetworkManager.Singleton.ConnectedClients[player.GetClientId()].PlayerObject;
        if(playerObject != null)
        {
            playerObjects.Remove(playerObject);
            HidePlayerObjectToPlayerInRoom(playerObject);
        }
        HideAllObjectInRoomToPlayer(player);
        if(isDestroyWhenNoOne)
        {
            networkObjects.ForEach(x =>
            {
                ServerManager.Instance.spawnManager.DespawnObjectInRoom(x, this);
            });
            roomManager.RemoveRoom(this);
        }
    }
    public virtual void OnSpawnPlayerInRoom(Player player, NetworkObject playerObject)
    {
        playerObjects.Add(playerObject);
    }
    public virtual void Tick()
    {
        
    }

    #region Main method
    public void ShowPlayerObjectToOddPlayer(NetworkObject playerObject)
    {
        playerManager.ForEach(x =>
        {
            if (!playerObject.IsNetworkVisibleTo(x.GetClientId()))
            {
                playerObject.NetworkShow(x.GetClientId());
            }
        });
    }
    private void ShowPlayerInRoomToNewPlayer(Player player)
    {
        playerObjects.ForEach(playerObject =>
        {
            if (!playerObject.IsNetworkVisibleTo(player.GetClientId()))
            {
                playerObject.NetworkShow(player.GetClientId());
            }
        });
    }

    public void ShowObjectInRoomToNewPlayer(Player player)
    {
        networkObjects.ForEach(networkObject =>
        {
            if (!networkObject.IsNetworkVisibleTo(player.GetClientId()))
            {
  
                networkObject.NetworkShow(player.GetClientId());
            }
        });
    }

    private void HidePlayerObjectToPlayerInRoom(NetworkObject playerObject)
    {
        playerManager.ForEach(x =>
        {
            if (playerObject.IsNetworkVisibleTo(x.GetClientId()))
            {
                playerObject.NetworkHide(x.GetClientId());
            }
        });
    }

    private void HideAllObjectInRoomToPlayer(Player player)
    {
        networkObjects.ForEach(networkObject =>
        {
            if (networkObject.IsNetworkVisibleTo(player.GetClientId()))
            {
                networkObject.NetworkHide(player.GetClientId());
            }
        });
    }
    #endregion
}
