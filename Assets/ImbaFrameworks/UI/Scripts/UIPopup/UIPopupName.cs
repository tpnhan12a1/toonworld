﻿
using UnityEngine;
using System.Collections;

namespace Imba.UI
{
    /// <summary>
    /// Defines all of popup in game
    /// </summary>
    public enum UIPopupName
    {
        Sample = -1,
        MessageBox = 0,
        LoginPopup = 3,
        SignupPopup = 4,
        ProfilePopup =5,
        MainPopup = 9,
        MenuPopup = 11,
        FriendPopup = 13,
        ChatModePopup  = 15,
        OtherProfilePopup = 17,
        LobbyPopup = 19,
        RunningGameControlPopup = 21,
        EditorCharacterPopup = 23,
        GamePopup = 24,
        GameRoomLobbyPopup= 25,
        GameLobbyPopup=26,
        QuickChatPopup =27,
        ResidentInteactionPopup=28,
        PlacementPopup =29,
        ChatPopup=30,
        GuildPopup= 31,
        GuildInventationPopup = 32,
        QuickNotifyPopup = 33
    }
}