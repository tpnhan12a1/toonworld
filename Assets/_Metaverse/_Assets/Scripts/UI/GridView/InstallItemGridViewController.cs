using SuperScrollView;
using System;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using DG.Tweening;
using Imba.UI;
using static UIMessageBox;

public class InstallItemGridViewController : LoopGridView
{
    [SerializeField] private Button _btnRecall;
    [SerializeField] private PlacementSystem _placementSystem;
    [SerializeField] private Button _btnClose;
    [SerializeField] private RectTransform _parentRectransform;
    [SerializeField] private Toggle _toggleSellectAll;
    [SerializeField] private TMP_Text _label;
    [SerializeField] private TMP_Text _txtCountSelected;
    [SerializeField] private CanvasGroup _canvasGroup;
    [SerializeField] private float _durrationAnimation = 0.2f;
    List<Item> _item;
    List<Item> _itemSelected;
    public List<Item> itemSelected { get { return _itemSelected; } }
    public void OnInit()
    {
        _item = new List<Item>();
        _itemSelected  = new List<Item>();
        InitGridView(_item.Count, OnGetItemByRowColumn);
        _btnClose.onClick.AddListener(CloseThis);
        _toggleSellectAll.onValueChanged.AddListener(OnValueChanged);
        _btnRecall.onClick.AddListener(RecallPlacementItem);
    }


    private void CloseThis()
    {
       OnHide();
    }

    public void OnShow(PlacementSystem placementSystem)
    {
        _parentRectransform.gameObject.SetActive(true);
        _placementSystem = placementSystem;
        _itemSelected.Clear();
        _toggleSellectAll.Set(false, true);
        _txtCountSelected.text = new StringBuilder().Append("[").Append(_itemSelected.Count).Append("]").ToString();
        _placementSystem.placementManager.RegisterOnPlacementDataChanged(OnPlacementDataChanged);
        gameObject.SetActive(true);
        _canvasGroup.alpha = 0.5f;
        _canvasGroup.DOFade(1, _durrationAnimation).SetEase(Ease.InBack);
        _parentRectransform.transform.localScale = Vector3.one / 2;
        _parentRectransform.DOScale(Vector3.one, _durrationAnimation).SetEase(Ease.InBack);
    }
    public void OnHide()
    {
        _placementSystem.placementManager.UnRegisterOnPlacementDataChanged(OnPlacementDataChanged);
        
        _canvasGroup.DOFade(0.5f, _durrationAnimation).SetEase(Ease.InBack);
        _parentRectransform.DOScale(Vector3.one/2, _durrationAnimation).SetEase(Ease.InBack).OnComplete(() =>
        {
            _parentRectransform.gameObject.SetActive(false);
        });
    }
    private LoopGridViewItem OnGetItemByRowColumn(LoopGridView gridView, int index, int row, int column)
    {
        if (_item.Count == 0) return null;

        InstallPlacementItem installPlacementItem = (InstallPlacementItem)gridView.NewListViewItem("InstallPlacementItem");
        installPlacementItem.OnRefresh(_item[index], this, _itemSelected.Contains(_item[index]));
        return installPlacementItem;
    }
    private void SelectedAllItem()
    {
        _itemSelected.Clear();
        _itemSelected.AddRange(_item);
        Refresh();
    }
    public void DeselectedAllItem()
    {
        _itemSelected.Clear();
        Refresh();
    }
    public void AddItemSelected(Item item)
    {
        _itemSelected.Add(item);
        if(_itemSelected.Count == _item.Count)
        {
            _toggleSellectAll.Set(true, false);
        }
        _txtCountSelected.text = new StringBuilder().Append("[").Append(_itemSelected.Count).Append("]").ToString();
    }
    public void RemoveItemSelected(Item item)
    {
        _itemSelected.Remove(item);
        _toggleSellectAll.Set(false, false);
        _txtCountSelected.text = new StringBuilder().Append("[").Append(_itemSelected.Count).Append("]").ToString();
    }
    public void Refresh()
    {
        SetListItemCount(_item.Count);
        RefreshAllShownItem();
    }

    private void OnPlacementDataChanged(List<Placement> placementDatas)
    {
        List<Item> items =  PlayerDataManager.Instance.inventoryManager.items.Where(x => x.itemClass == PlayfabContant.ItemClass.Placement).ToList();
        items = items.Where(x => _placementSystem.placementManager.Contain(x.itemId, x.itemInstanceId)).ToList();
        _item.Clear();
        _item.AddRange(items);
        Refresh();
    }
    private void OnValueChanged(bool status)
    {
        if (status)
        {
            SelectedAllItem();
            _label.text = "Deselect all item";
        }
        else
        {
            DeselectedAllItem();
            _label.text = "Select all item";
        }
        _txtCountSelected.text = new StringBuilder().Append("[").Append(_itemSelected.Count).Append("]").ToString();
    }

    private void RecallPlacementItem()
    {
        if(_itemSelected.Count == 0) 
        {
            UIManager.Instance.AlertManager.ShowAlertMessage("Please select item to recall");
            return;
        }
        UIManager.Instance.PopupManager.ShowMessageDialog("Confirm", "Do you want to return these items to your inventory?", callback:OnConfirmRecall);
    }
    private bool OnConfirmRecall(MessageBoxAction action)
    {
       if(action == MessageBoxAction.Accept)
        {
            _placementSystem.RecallItem(_itemSelected);
            OnHide();
            return true;
        }
        else
        {
            return false;
        }
    }
}
