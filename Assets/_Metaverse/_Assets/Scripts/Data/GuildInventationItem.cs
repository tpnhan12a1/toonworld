using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GuildInventationItem : LoopListViewItem2
{
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private TMP_Text _txtLevel;
    [SerializeField] private TMP_Text _txtStatusOn;
    [SerializeField] private TMP_Text _txtStatusOff;
    [SerializeField] private Image _imageAatar;
    [SerializeField] private Button _btnDelete;
    private GuildMember _guildMember;
    private LoopListView2 _loopListView;
    public void Awake()
    {
        _btnDelete.onClick.AddListener(DeleteMember);
    }

    
    public void OnRefresh(GuildMember guildMember, LoopListView2 listView)
    {
        _guildMember = guildMember;
        _loopListView = listView;
        _txtDisplayName.text = _guildMember.displayName;
        _txtLevel.text = _guildMember.level.ToString();
        _imageAatar.sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, _guildMember.avatarUrl);
        SetStatus();
    }
    private void SetStatus()
    {
        bool status = ClientManager.Instance.clientConecnted.ContainsKey(_guildMember.playfabId);
        _txtStatusOn.SetActive(status);
        _txtStatusOff.SetActive(!status);
    }
    private void DeleteMember()
    {
       
    }

}
