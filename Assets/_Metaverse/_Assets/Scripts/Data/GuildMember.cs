using PlayFab;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuildMember
{
    public string playfabId = string.Empty;
    public string avatarUrl = string.Empty;
    public string displayName = string.Empty;
    public int level = 0;
    public string role = string.Empty;
    public string roleId = string.Empty;
    public PlayFab.GroupsModels.EntityKey entityKey;
    public GuildMember()
    {

    }
    public void Edit(GuildMember guildMember)
    {
        this.playfabId= guildMember.playfabId;
        this.avatarUrl= guildMember.avatarUrl;
        this.displayName= guildMember.displayName;
        this.level = guildMember.level;
        this.role = guildMember.role;
        this.roleId = guildMember.roleId;
    }
    public GuildMember(string playfabId, string avatarUrl, string displayName, int level, string role, PlayFab.GroupsModels.EntityKey entityKey)
    {
        this.playfabId = playfabId;
        this.avatarUrl = avatarUrl;
        this.displayName = displayName;
        this.level = level;
        this.role = role;
        this.entityKey = entityKey;
    }
    public override bool Equals(object obj)
    {
       GuildMember other = obj as GuildMember;
       if (other == null) return false;
        return playfabId == other.playfabId;
    }
    public override int GetHashCode()
    {
        return playfabId.GetHashCode();
    }

    public GuildMember ShallowCopy()
    {
        return (GuildMember)MemberwiseClone();
    }
    public NetCodeModels.GuildMemberInfor ToGuildMemberInfor()
    {
        return new NetCodeModels.GuildMemberInfor()
        {
            playfabId = playfabId,
            avatarUrl = avatarUrl,
            displayName = displayName,
            level = level,
            role = role,
            entityKey = new NetCodeModels.EntityKey()
            {
                Id = entityKey.Id,
                Type = entityKey.Type,
            }
        };
    }
}
