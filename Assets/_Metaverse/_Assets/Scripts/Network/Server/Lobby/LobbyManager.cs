using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LobbyManager<L> where L :Lobby
{
    Dictionary<GamePlayType, List<Lobby>> lobbies = new Dictionary<GamePlayType, List<Lobby>>();

    public void Add( GamePlayType gamePlayType,Lobby b)
    {
        if(lobbies.TryGetValue(gamePlayType, out List<Lobby> list)) {
            b.id = list.Count;
            list.Add(b);
        }
        else
        {
            list = new List<Lobby>();
            b.id = list.Count;
            list.Add(b);
            lobbies.Add(gamePlayType, list);
        }
    }
    public Lobby GetLobby(GamePlayType gamePlayType, int lobbyId)
    {
        if (lobbies.TryGetValue(gamePlayType, out List<Lobby> list))
        {
            return list.Where( x=>x.id == lobbyId).FirstOrDefault(); 
        }
        return null;
    }
    public Lobby GetLobby(GamePlayType type)
    {
        if(lobbies.TryGetValue(type, out List<Lobby> list))
        {
            return (Lobby)list.FirstOrDefault();
        }
        return null;
    }
    public void Remove( GamePlayType gamePlayType, Lobby lobby ) 
    {
        if(lobbies.TryGetValue(gamePlayType, out List<Lobby> list))
        {
            list.Remove(lobby);
            if(list.Count == 0)
            {
                lobbies.Remove(gamePlayType);
            }
        }
    }
    public List<NetCodeModels.Lobby> GetListLobbies(GamePlayType gamePlayType)
    {
        List<Lobby> list = GetLobbies(gamePlayType);
        if (list == null) return null;
        List< NetCodeModels .Lobby> listLobbies = new List<NetCodeModels.Lobby>();
        foreach (Lobby b in list)
        {
            listLobbies.Add(b.ToStruct());
        }
        return listLobbies;
    }
        
    public List<Lobby> GetLobbies(GamePlayType gamePlayType)
    {
        if(lobbies.TryGetValue(gamePlayType, out List<Lobby> list))
        {
            return list;
        }
        else
        {
            return null;
        }
    }
}
