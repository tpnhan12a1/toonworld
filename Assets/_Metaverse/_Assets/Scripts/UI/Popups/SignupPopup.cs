using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SignupPopup : UIPopup
{
    [SerializeField] private TMP_InputField _inputFieldUsername;
    [SerializeField] private TMP_InputField _inputFieldEmail;
    [SerializeField] private TMP_InputField _inputFieldPassword;
    [SerializeField] private Button _btnSignUp;
    [SerializeField] private Button _btnCancel;

    /*[SerializeField] private TextMeshProUGUI _usernameErrorMessage;
    [SerializeField] private TextMeshProUGUI _emailErrorMessage;
    [SerializeField] private TextMeshProUGUI _passwordErrorMessage;*/

    protected override void OnInit()
    {
        base.OnInit();

        _btnSignUp.onClick.AddListener(OnClickSignUp);
        _btnCancel.onClick.AddListener(() =>
        {
            Hide();
        });

        // Add listeners for the input fields
        _inputFieldUsername.onEndEdit.AddListener(delegate { ValidateUsername(_inputFieldUsername.text); });
        _inputFieldEmail.onEndEdit.AddListener(delegate { ValidateEmail(_inputFieldEmail.text); });
        _inputFieldPassword.onEndEdit.AddListener(delegate { ValidatePassword(_inputFieldPassword.text); });
    }

    protected override void OnShowing()
    {
        base.OnShowing();
    }
    protected override void OnShown()
    {
        base.OnShown();
    }
    protected override void OnHiding()
    {
        base.OnHiding();
    }

    protected override void OnHidden()
    {
        base.OnHidden();
    }

    private void OnClickSignUp()
    {
        string userName = _inputFieldUsername.text;
        string email = _inputFieldEmail.text; 
        string password = _inputFieldPassword.text;

        UIManager.Instance.ShowLoading();
        PlayfabManager.Instance.RegisterPlayfabUser(userName, email, password, OnSignupSuccess ,OnSignupFailure);
      
    }

   
    private bool IsUsernameValid(string username)
    {
        
        return username.Length >= 4 && !username.Contains(" ");
    }

    private bool IsEmailValid(string email)
    {
        try
        {
            var addr = new System.Net.Mail.MailAddress(email);
            return addr.Address == email;
        }
        catch
        {
            
            return false;
        }
    }

    private bool IsPasswordValid(string password)
    {
        
        return password.Length >= 8 && password.Any(char.IsUpper) && password.Any(char.IsLower) && password.Any(char.IsSymbol);
    }

    private void ValidateUsername(string username)
    {
        /*if (!IsUsernameValid(username))
        {
            _usernameErrorMessage.text = "Invalid username";
        }
        else
        {
            _usernameErrorMessage.text = "";
        }*/
        if (!IsUsernameValid(username))  
            Debug.Log("Invalid username");
    }

    private void ValidateEmail(string email)
    {

        /*if (!IsEmailValid(email))
        {
            _emailErrorMessage.text = "Invalid email";
        }
        else
        {
            _emailErrorMessage.text = "";
        }*/
        if (!IsUsernameValid(email))
            Debug.Log("Invalid email");
    }

    private void ValidatePassword(string password)
    {
        /*if (!IsPasswordValid(password))
        {
            _passwordErrorMessage.text = "Invalid password";
        }
        else
        {
            _passwordErrorMessage.text = "";
        }*/
        if (!IsUsernameValid(password))
            Debug.Log("Invalid password");
    }

    private void OnSignupFailure(object arg1, object arg2)
    {
        UIManager.Instance.HideLoading();
    }

    private void OnSignupSuccess(object arg1, object arg2)
    {

        UIManager.Instance.HideLoading();
        Hide();
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.LoginPopup);
    }

}
