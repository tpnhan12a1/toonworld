using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuickNotifyPopup : UIPopup
{
    [SerializeField] private QuickNotifyItem _quickNotifyItem;
    
    public void ShowNotify(string content, Action OnAccepted, Action OnDeny)
    {
        MoveToTop();
        _quickNotifyItem.OnShow(content, OnAccepted, OnDeny);
    }
}
