using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RunningGameGroundedState : RunningGameMotionState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        _runningGameStateMachine.StartFloatAnimation(PlayerDataManager.Instance.animationData.FAirParameterHash, 0);
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.isGrounded = true;
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        Float();
    }
    public override void OnExitState()
    {
        base.OnExitState();
        
    }

    protected override void OnContactWithGroundedExited(Collider collider)
    {
        if (IsThereGroundUnderneath()) return;

        Vector3 capsuleColloderCenterInWorld = _runningGameStateMachine.playerRunningGameController.collisionCheckColider.bounds.center;

        Ray downwardRayFromCapsuleBottom = new Ray(capsuleColloderCenterInWorld
            - new Vector3(0f, _runningGameStateMachine.playerRunningGameController.
           collisionCheckColider.bounds.extents.y, 0f), Vector3.down);

        if (!Physics.Raycast(downwardRayFromCapsuleBottom, out _,
            PlayerDataManager.Instance.playerData.groundToFallDistance,
            PlayerDataManager.Instance.layerData.groundedMask,
            QueryTriggerInteraction.Ignore))
        {
            OnFall();
        }
    }
    private bool IsThereGroundUnderneath()
    {
        BoxCollider groundCheckCollider = _runningGameStateMachine.playerRunningGameController.groundCheckCollider;
        if (groundCheckCollider != null)
        {
            Vector3 groundColloderCenterInWorld = groundCheckCollider.bounds.center;
            Collider[] overlappedGroundColliders = Physics.OverlapBox(groundColloderCenterInWorld,
            groundCheckCollider.bounds.extents, groundCheckCollider.transform.rotation,
           PlayerDataManager.Instance.layerData.groundedMask, QueryTriggerInteraction.Ignore);
            return overlappedGroundColliders.Length > 0;
        }
        return false;

    }
    protected override void AddInputActionsCallbacks()
    {
        base.AddInputActionsCallbacks();
        PlayerInput.Instance.InputAction.Player.Move.started += OnMoveStarted;
    }
    protected override void RemoveInputActionCallbacks()
    {
        base.RemoveInputActionCallbacks();
        PlayerInput.Instance.InputAction.Player.Move.started -= OnMoveStarted;

    }
    protected virtual void OnMoveStarted(UnityEngine.InputSystem.InputAction.CallbackContext context)
    {
        Vector2 movementInput = context.ReadValue<Vector2>();
        if(Vector2.Angle(movementInput, Vector2.up) < 45f)
        {
            _runningGameStateMachine.ChangeState(_runningGameStateMachine.states[StateType.RunningGameJumpingState]);
        }
    }
}
