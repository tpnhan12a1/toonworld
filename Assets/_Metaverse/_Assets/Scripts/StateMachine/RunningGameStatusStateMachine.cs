using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameStatusStateMachine : StateMachine
{
    private PlayerRunningGameController _playerRunningGameController;

    protected Dictionary<StateType, IState> _states = new Dictionary<StateType, IState>();
    public Dictionary<StateType, IState> states
    {
        get { return _states; }
        set { _states = value; }
    }
    public PlayerRunningGameController playerRunningGameController { get { return _playerRunningGameController; } }
    public RunningGameStatusStateMachine(PlayerRunningGameController playerRunningGameController)
    {
        _playerRunningGameController = playerRunningGameController;
        Init();
    }

    public override StateMachineType GetStateMachineType()
    {
        return StateMachineType.RunningGameStatusStateMachine;
    }

    public override void Init()
    {
        RunningGameEmptyStatusState runningGameEmptyStatusState = new RunningGameEmptyStatusState();
        _states.Add(runningGameEmptyStatusState.GetStateType(), runningGameEmptyStatusState);
        runningGameEmptyStatusState.SetStateMachine(this);

        RunningGameStumblingState runningGameStumblingState = new RunningGameStumblingState();
        _states.Add(runningGameStumblingState.GetStateType(), runningGameStumblingState);
        runningGameStumblingState.SetStateMachine(this);
    }
    public void StartBoolAnimation(int animationHash)
    {
        playerRunningGameController.animator.SetBool(animationHash, true);
    }
    public void StopBoolAnimation(int animationHash)
    {
        playerRunningGameController.animator.SetBool(animationHash, false);
    }
    public void StartFloatAnimation(int animationHash, float value)
    {
        playerRunningGameController.animator.SetFloat(animationHash, value);
    }
}
