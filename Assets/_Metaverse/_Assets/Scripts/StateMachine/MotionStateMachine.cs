using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotionStateMachine : StateMachine
{
    private PlayerController _playerController;
    public override StateMachineType GetStateMachineType()
    {
        return StateMachineType.Motion;
    }
    protected Dictionary<StateType, IState> _states = new Dictionary<StateType, IState>();
    public Dictionary<StateType, IState> states
    {
        get { return _states; }
        set { _states = value; }
    }
    public PlayerController playerController { get { return _playerController; } set { _playerController = value; } }

    public MotionStateMachine(PlayerController playerController)
    {
        this.playerController = playerController;
        Init();
    }
    public override void Init()
    {
        PlayerIdlingState idlingState = new PlayerIdlingState();
        idlingState.SetStateMachine(this);
        PlayerRunningState runningState = new PlayerRunningState();
        runningState.SetStateMachine(this);
        PlayerWalkingState walkingState = new PlayerWalkingState();
        walkingState.SetStateMachine(this);
        PlayerHardStoppingState hardStoppingState = new PlayerHardStoppingState();
        hardStoppingState.SetStateMachine(this);
        PlayerMediumStoppingState mediumStoppingState = new PlayerMediumStoppingState();
        mediumStoppingState.SetStateMachine(this);
        PlayerLightStoppingState lightStoppingState = new PlayerLightStoppingState();
        lightStoppingState.SetStateMachine(this);
        PlayerJumpingState jumpingState = new PlayerJumpingState();
        jumpingState.SetStateMachine(this);
        PlayerFallingState fallingState = new PlayerFallingState();
        fallingState.SetStateMachine(this);
        PlayerLightLandingState lightLandingState = new PlayerLightLandingState();
        lightLandingState.SetStateMachine(this);
        PlayerHardLandingState hardLandingState = new PlayerHardLandingState();
        hardLandingState.SetStateMachine(this);
        PlayerRollingState rollingState = new PlayerRollingState();
        rollingState.SetStateMachine(this);
        PlayerDashingState dashingState = new PlayerDashingState();
        dashingState.SetStateMachine(this);

        PlayerWaterIdlingState waterIdlingState = new PlayerWaterIdlingState();
        waterIdlingState.SetStateMachine(this);
        PlayerSwimmingState swimmingState = new PlayerSwimmingState();
        swimmingState.SetStateMachine(this);
        PlayerLightStopSwimmingState lightStopSwimmingState = new PlayerLightStopSwimmingState();
        lightStopSwimmingState.SetStateMachine(this);
        PlayerHardStopSwimmingState hardStopSwimmingState = new PlayerHardStopSwimmingState();
        hardStopSwimmingState.SetStateMachine(this);
        PlayerWaterDashingState waterDashingState = new PlayerWaterDashingState();
        waterDashingState.SetStateMachine(this);
        PlayerWaterUpState waterUpState = new PlayerWaterUpState();
        waterUpState.SetStateMachine(this);

        states.Add(idlingState.GetStateType(),
               idlingState);
        states.Add(runningState.GetStateType(),
            runningState);
        states.Add(walkingState.GetStateType(),
            walkingState);
        states.Add(hardStoppingState.GetStateType(),
            hardStoppingState);
        states.Add(mediumStoppingState.GetStateType(),
            mediumStoppingState);
        states.Add(lightStoppingState.GetStateType(),
            lightStoppingState);
        states.Add(jumpingState.GetStateType(),
            jumpingState);
        states.Add(fallingState.GetStateType(),
            fallingState);
        states.Add(lightLandingState.GetStateType(),
            lightLandingState);
        states.Add(hardLandingState.GetStateType(),
            hardLandingState);
        states.Add(rollingState.GetStateType(),
            rollingState);
       states.Add(dashingState.GetStateType(),
            dashingState);

        states.Add(waterIdlingState.GetStateType(),
            waterIdlingState);
        states.Add(swimmingState.GetStateType(),
            swimmingState);
        states.Add(lightStopSwimmingState.GetStateType(),
            lightStopSwimmingState);
        states.Add(hardStopSwimmingState.GetStateType(),
            hardStopSwimmingState);
        states.Add(waterDashingState.GetStateType(),
            waterDashingState);
        states.Add(waterUpState.GetStateType(),
            waterUpState);
    }
}
