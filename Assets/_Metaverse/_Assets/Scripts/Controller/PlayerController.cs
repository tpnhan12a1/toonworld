using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
[RequireComponent(typeof(Collider))]
public class PlayerController : NetworkBehaviour
{
    [field: Header("Colisions")]
    [SerializeField]
    private CapsuleCollider _capsuleCollider;
    [SerializeField]
    private Rigidbody _rigidbody;
    [SerializeField]
    private BoxCollider _groundCheckCollider;
    [SerializeField]
    private BoxCollider _waterCheckColider;
    [SerializeField] private NetworkAnimator _networkAnimator;
    [SerializeField] private Transform _lookAt;
    [SerializeField] private Transform _mainCameraTransform;
    [SerializeField] private Animator _animator;
    [SerializeField] private PlayerNetworkData _playerNetworkData;


    private MotionStateMachine _playerMotionStateMachine;
    private Dictionary<StateMachineType, StateMachine> _stateMachines = new Dictionary<StateMachineType, StateMachine>();
    public CapsuleCollider capsuleCollider { get {  return _capsuleCollider; } }
    public Rigidbody rgbody { get { return _rigidbody; } }
    public BoxCollider groundCheckCollider { get { return _groundCheckCollider; } }
    public BoxCollider waterCheckCollider { get { return _waterCheckColider; } }    
    public Transform mainCameraTransform { get { return _mainCameraTransform; } }
    public Animator animator { get {  return _animator; } }
    public NetworkAnimator networkAnimator { get { return _networkAnimator; } }
    public PlayerNetworkData playerNetworkData { get { return _playerNetworkData; } } 

    //public void Start()
    //{
    //    InitData();
    //}
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (IsLocalPlayer)
        {
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.MainPopup);
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.QuickChatPopup);
            InitData();
        }
    }

    private void OnDashingChanged(bool obj)
    {
        throw new NotImplementedException();
    }

    private void InitData()
    {
        _playerMotionStateMachine = new MotionStateMachine(this);
        _stateMachines.Add(StateMachineType.Motion, _playerMotionStateMachine);
        _mainCameraTransform = CameraManager.Instance.mainCamera.transform;
        CameraManager.Instance.virtualPlayerCamera.LookAt = _lookAt;
        CameraManager.Instance.virtualPlayerCamera.Follow = _lookAt;

        if (_playerMotionStateMachine != null)
        {
            IState newState = null;
            if (_playerMotionStateMachine.states.TryGetValue(StateType.Idling, out newState))
            {
                _playerMotionStateMachine.ChangeState(newState);
                _playerMotionStateMachine.CurentState = newState;
                _playerMotionStateMachine.CurrentStateType = newState.GetStateType();
            }
        }
    }
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        if(IsLocalPlayer)
        {
            _playerMotionStateMachine.CurentState?.OnExitState();
            UIManager.Instance.PopupManager.HidePopup(UIPopupName.MainPopup);
            UIManager.Instance.PopupManager.HidePopup(UIPopupName.QuickChatPopup);
        }
       
    }
    public void Update()
    {
        if (!IsLocalPlayer) return;
        _playerMotionStateMachine?.HandleInput();
        _playerMotionStateMachine?.Update();
    }
    public void FixedUpdate()
    {
        if (!IsLocalPlayer) return;
        _playerMotionStateMachine?.PhysicsUpdate();
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (!IsLocalPlayer) return;
        Debug.Log("On Trigger Enter");
        _playerMotionStateMachine?.OnTriggerEnter(collider);
    }
    private void OnTriggerExit(Collider collider)
    {
        if (!IsLocalPlayer) return;
        _playerMotionStateMachine?.OnTriggerExit(collider);
    }
    public void OnMovementStateAnimationEnterEvent()
    {
        if (!IsLocalPlayer) return;
        _playerMotionStateMachine?.OnAnimationEnterEvent();
    }
    public void OnMovementStateAnimationExitEvent()
    {
        if (!IsLocalPlayer) return;
        _playerMotionStateMachine?.OnAnimationExitsEvent();
    }
    public void OnMovementStateAnimationTransitionEvent()
    {
        if (!IsLocalPlayer) return;
        _playerMotionStateMachine.OnAnimationTransitionEvent();
    }
    public override void OnDestroy()
    {
        base.OnDestroy();
      
    }
}
