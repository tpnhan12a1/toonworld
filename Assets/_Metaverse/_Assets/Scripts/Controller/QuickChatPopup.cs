using DG.Tweening;
using Imba.UI;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public enum TypeChat: byte
{
    Global,
    Private,
    Guild,
    Party, 
    System
}
public class ChatContent
{
    public TypeChat type;
    public string displayName;
    public string content;
    public List<string> receivers; //PlayfabId
}
public class QuickChatPopup : UIPopup
{
    [SerializeField] private TMP_InputField _ipfChat;
    [SerializeField] private Button _btnChat;
    [SerializeField] private Button _btnIcon;
    [SerializeField] private Button _btnChatMode;
    [SerializeField] private TMP_Text _txtChatMode;

    [SerializeField] private ChatGridViewController _chatGridView;
    [SerializeField] private List<ChatContent> _chatContents;
    [SerializeField] private OnExtendChatUI _onExtendChatUI;

    [SerializeField] private GameObject _iconContainer;

    [SerializeField] private Color _globalTextColor;
    [SerializeField] private Color _friendTextColr;
    [SerializeField] private Color _guildTextColor;
    [SerializeField] private Color _partyTextColor;
    [SerializeField] private Color _systemTextColor;

    [SerializeField] private Button _btnSubOvelay;


    [SerializeField] private EmotionController _emotionController;

    private TypeChat _typeChat;
    public TypeChat typeChat { 
        get 
        {
            return _typeChat;
        }
        set 
        {
            _typeChat = value;
            _txtChatMode.text = value.ToString();
            switch(_typeChat)
            {
                case TypeChat.Global:
                    _txtChatMode.color = _globalTextColor;
                    break;
                case TypeChat.Private:
                    _txtChatMode.text = PlayerDataManager.Instance.globalReusableData.targetChat.displayName;
                    _txtChatMode.color = _friendTextColr;
                    break;
                case TypeChat.Guild:
                    _txtChatMode.color = _guildTextColor;
                    break;
                case TypeChat.Party:
                    _txtChatMode.color = _partyTextColor;
                    break;
            }
        } 
    }

    protected override void OnInit()
    {
        base.OnInit();
        _typeChat = TypeChat.Global;
        _txtChatMode.color = _globalTextColor;
        
        _chatContents = new List<ChatContent>();
        _chatGridView.InitListView(_chatContents.Count, OnGetContentByIndex);

      

        _btnChat.onClick.AddListener(Chat);
        _btnChatMode.onClick.AddListener(OpenChatMode);
        _btnIcon.onClick.AddListener(OnClickIconContainer);
        _btnSubOvelay.onClick.AddListener(OnClickSubOvelay);

        _emotionController.OnInit();
    }
    protected override void OnShown()
    {
        base.OnShown();
        PlayerDataManager.Instance.globalReusableData.RegisterOnTypeChatChanged(OnTypeChatChanged);
        ClientManager.Instance.OnAddChatContent += OnAddChatChatContent;
    }
    private void OnTypeChatChanged(TypeChat oldTypeChat, TypeChat newTypeChat)
    {
        typeChat = newTypeChat;
    }

    protected override void OnHidden()
    {
        base.OnHidden();
        PlayerDataManager.Instance.globalReusableData.UnRegisterOnTypeChatChanged(OnTypeChatChanged);
        ClientManager.Instance.OnAddChatContent -= OnAddChatChatContent;
    }
    private void OnClickSubOvelay()
    {
        _onExtendChatUI.SetBase();
        _btnSubOvelay.SetActive(false);
        _chatGridView.MovePanelToItemIndex(_chatContents.Count, 0);

    }

    public void SetActiveSubOverlay(bool active)
    {
        _btnSubOvelay.SetActive(active);
    }
    private LoopListViewItem2 OnGetContentByIndex(LoopListView2 listView, int index)
    {
        if (listView == null) return null;
        if(_chatContents.Count == 0) return null;
        if (index < 0) return null;

        ChatContent chatContent = _chatContents[index];
        Color color = _globalTextColor;

        switch (chatContent.type)
        {
            case TypeChat.Global:
                color = _globalTextColor;
                break;
            case TypeChat.Private:
                color = _friendTextColr;
                break;
            case TypeChat.Party:
                color = _partyTextColor;
                break;
            case TypeChat.Guild: 
                color = _guildTextColor;
                break;
            case TypeChat.System:
                color = _systemTextColor;
                break;
        }
        QuickChatContentItem chatItem = (QuickChatContentItem)listView.NewListViewItem("QuickChatContentItem");
        chatItem.OnRefresh(_chatGridView, chatContent, color);
        return chatItem;
    }

    private void OnClickIconContainer()
    {
        if(!_iconContainer.gameObject.activeSelf)
        {
            OpenIconContainer();
        }
        else
        {
            StartCoroutine(CloseIconContainer());
        }
    }
    private IEnumerator CloseIconContainer()
    {
        _iconContainer.transform.DOScaleX(0, 0.5f);
        yield return new WaitForSeconds(0.5f);
        _iconContainer.SetActive(false);
    }

    private void OpenIconContainer()
    {
        _iconContainer.gameObject.SetActive(true);
        _iconContainer.transform.DOScaleX(1, 0.5f);
    }

    private void OpenChatMode()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.ChatModePopup);
    }

    private void Chat()
    {
        if (String.IsNullOrEmpty(_ipfChat.text)) return;
        NetCodeModels.ChatContent chatContent = new NetCodeModels.ChatContent()
        {
            content = _ipfChat.text,
            displayName = PlayerDataManager.Instance.displayName,
            type = typeChat
        };
        ServerManager.Instance.ChatServerRpc(PlayfabManager.Instance.PlayfabID,PlayerDataManager.Instance.globalReusableData.targetChat.playfabId,chatContent);
        _ipfChat.text = "";
    }

   
    private void OnAddChatChatContent(NetCodeModels.ChatContent chatContent)
    {
        ChatContent chat = new ChatContent();
        chat.type = chatContent.type;
        chat.displayName = chatContent.displayName;
        chat.content = chatContent.content;
        _chatContents.Add(chat);
        _chatGridView.SetListItemCount(_chatContents.Count);
        _chatGridView.RefreshAllShownItem();
        _chatGridView.MovePanelToItemIndex(_chatContents.Count,0);
    }

}
