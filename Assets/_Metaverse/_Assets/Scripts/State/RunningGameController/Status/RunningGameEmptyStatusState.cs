using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameEmptyStatusState : IState
{
    protected RunningGameStatusStateMachine _runningGameStatusStateMachine;
    public StateType GetStateType()
    {
        return StateType.RunningGameEmptyStatusState;
    }

    public void OnAnimationEnterEvent()
    {
       
    }

    public void OnAnimationExitsEvent()
    {
       
    }

    public void OnAnimationTransitionEvent()
    {
       
    }

    public void OnEnterState()
    {
        Debug.Log($"Enter the {GetStateType()}");

    }

    public void OnExitState()
    {
        Debug.Log($"Exit the {GetStateType()}");
    }

    public void OnHandleInput()
    {
       
    }

    public void OnPhysicsUpdate()
    {
       
    }

    public void OnTriggerEnter(Collider collider)
    {
        
    }

    public void OnTriggerExit(Collider collider)
    {
        
    }

    public void OnUpdate()
    {
        
    }

    public void SetStateMachine(StateMachine stateMachine)
    {
      _runningGameStatusStateMachine = (RunningGameStatusStateMachine)stateMachine;
    }
}
