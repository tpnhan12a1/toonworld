using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameMotionStateMachine : StateMachine
{
    private PlayerRunningGameController _playerRunningGameController;

    protected Dictionary<StateType, IState> _states = new Dictionary<StateType, IState>();
    public Dictionary<StateType, IState> states
    {
        get { return _states; }
        set { _states = value; }
    }
    public PlayerRunningGameController playerRunningGameController { get { return _playerRunningGameController; } }
    public RunningGameMotionStateMachine(PlayerRunningGameController playerRunningGameController)
    {
        _playerRunningGameController = playerRunningGameController;
        Init();
    }
    
    public override StateMachineType GetStateMachineType()
    {
        return StateMachineType.RunningGameMotionStateMachine;
    }

    public override void Init()
    {
        RunningGameIdling_1State runningGameIdling_1State = new RunningGameIdling_1State();
        _states.Add(runningGameIdling_1State.GetStateType(), runningGameIdling_1State);
        runningGameIdling_1State.SetStateMachine(this);

        RunningGameIdling_2State runningGameIdling_2State = new RunningGameIdling_2State();
        _states.Add(runningGameIdling_2State.GetStateType(), runningGameIdling_2State);
        runningGameIdling_2State.SetStateMachine(this);

        RunningGameRunningState runningGameRunningState = new RunningGameRunningState();
        _states.Add(runningGameRunningState.GetStateType(),runningGameRunningState);
        runningGameRunningState.SetStateMachine(this);

        RunningGameDashingState runningGameDashingState = new RunningGameDashingState();
        _states.Add(runningGameDashingState.GetStateType(), runningGameDashingState);
        runningGameDashingState.SetStateMachine(this);

        RunningGameJumpingState runningGameJumpingState = new RunningGameJumpingState();
        _states.Add(runningGameJumpingState.GetStateType(), runningGameJumpingState);
        runningGameJumpingState.SetStateMachine(this);

        RunningGameFallingState runningGameFallingState = new RunningGameFallingState();
        _states.Add(runningGameFallingState.GetStateType(), runningGameFallingState);
        runningGameFallingState.SetStateMachine(this);
    }

    public void StartBoolAnimation(int animationHash)
    {
        playerRunningGameController.animator.SetBool(animationHash, true);
    }
    public void StopBoolAnimation(int animationHash)
    {
        playerRunningGameController.animator.SetBool(animationHash, false);
    }
    public void StartFloatAnimation(int animationHash, float value)
    {
        playerRunningGameController.animator.SetFloat(animationHash, value);
    }
}
