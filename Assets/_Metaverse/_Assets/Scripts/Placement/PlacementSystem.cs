﻿using Imba.UI;
using Mono.CSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using UnityEditor;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public enum TypePlacement
{
    Floor = 1,
    Wall = 2,
    Ceil = 3,
    Ground = 4,
    OnOther = 5
}
public class PlacementSystem : MonoBehaviour
{
    [SerializeField] private Color _acceptedColor = Color.green;
    [SerializeField] private Color _denyColor = Color.red;

    [SerializeField] private GameObject cellIndicatorXZ;
    [SerializeField] private LayerMask layerMaskColison;
    [SerializeField] private LayerMask placementObjectLayerMask;
    [SerializeField] private GameObject _placementParent;
    [SerializeField] private PlacementObject _placementObjectPreview;
    [SerializeField] private  float _radiousCheckCanPlacePlacementObject= 1f;
    [SerializeField] private LayerMask worldUIMash = new LayerMask();
    //Contein Ground/ Wall/ Cell/
    [SerializeField] private LayerMask layerCanPlace = new LayerMask();
    [SerializeField] private uint _itemRecallPerFrame = 10;

    private bool _isPlacementMode = true;
    private bool _isCanPlace = true;
    private Camera _sceneCamera;

    private PointerEventData _lastDrageventData;
    private PlacementManager _placementManager;
    public PlacementManager placementManager { get { return _placementManager; } }
    public bool isCanPlace
    {
        get { return _isCanPlace; }
    }
    public PlacementObject placementObjectPreview { get { return _placementObjectPreview; } }

  

    private void Start()
    {
        cellIndicatorXZ.SetActive(false);
        _sceneCamera = CameraManager.Instance.mainCamera;
        _placementManager = new PlacementManager();
    }
    private void OnDestroy()
    {
        RemoveHandleCallback();
        EnableZoomLook();
    }
    private void FixedUpdate()
    {
        if (!_isPlacementMode) return;
        if (_placementObjectPreview == null) return;
        if (_lastDrageventData == null) return;

        Ray ray = _sceneCamera.ScreenPointToRay(_lastDrageventData.position);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100, layerCanPlace, QueryTriggerInteraction.Ignore))
        {
            int hitLayer = hit.collider.gameObject.layer;

            if ((1 << hitLayer & GetLayerMask(_placementObjectPreview.typePlacements)) == 0) return;
          
            _placementObjectPreview.OnRotation();

            cellIndicatorXZ.transform.position = hit.point;
            _placementObjectPreview.transform.position = hit.point;  //+ offset;

            Quaternion targetRotation = Quaternion.FromToRotation(_placementObjectPreview.transform.up, hit.normal) * _placementObjectPreview.transform.rotation;
            _placementObjectPreview.transform.rotation = targetRotation;

            SetScaleCellIndicator(_placementObjectPreview.extendBounds.size);
        }
        SetCanPlace();
    }

    private void SetCanPlace()
    {
        BoxCollider collider = _placementObjectPreview.GetComponent<BoxCollider>();
        Vector3 dir = _placementObjectPreview.transform.up;
        dir *= (collider.size.y / 2);
        Vector3 targetPoint = _placementObjectPreview.transform.position + dir;
        Collider[] colliders = Physics.OverlapBox(targetPoint, (collider.size / 2) * 0.95f,
          collider.transform.rotation,
            layerMaskColison,
            QueryTriggerInteraction.Ignore);
        if (colliders.Length > 1) //Expect this
        {
            _isCanPlace = false;
            cellIndicatorXZ.GetComponent<Renderer>().material.SetColor("_BaseColor", _denyColor);
            DisableZoomLook();
        }
        else
        {
            _isCanPlace = true;
            cellIndicatorXZ.GetComponent<Renderer>().material.SetColor("_BaseColor", _acceptedColor);
        }
    }
    public void SetScaleCellIndicator(Vector3 scale)
    {
        Vector3 newscale = GetNewScale(scale);
        cellIndicatorXZ.GetComponent<Renderer>().material.SetVector("_DefaultScale", newscale);
        cellIndicatorXZ.transform.localScale = GetCellScale(scale);
    }

    private Vector3 GetNewScale(Vector3 scale)
    {
        Vector3 newscale = scale;
        Vector3 upper = _placementObjectPreview.transform.up;

        if (upper == Vector3.up || upper == Vector3.down)
        {
            newscale.x = scale.x;
            newscale.y = scale.z;
            return newscale;
        }
        if (upper == Vector3.back || upper == Vector3.forward)
        {
            newscale.x = scale.x;
            newscale.y = scale.y;
            return newscale;
        }
        newscale.x = scale.y;
        newscale.y = scale.z;
        return newscale;
    }

    public void StartPlacementMode()
    {
        _isPlacementMode = true;
        _isCanPlace = true;
        AddHandleCallback();
        PlayerDataManager.Instance.placementManager.RegisterOnPlacementDataChanged(OnPlacementPlaceDataChanged);
    }

    public void StopPlacementMode()
    {
        _isPlacementMode = false;
        RemoveHandleCallback();
        PlayerDataManager.Instance.placementManager.UnRegisterOnPlacementDataChanged(OnPlacementPlaceDataChanged);
    }

    public Vector3 GetCellScale(Vector3 scale)
    {
        Vector3 newscale = scale;
        Vector3 upper = _placementObjectPreview.transform.up;

        if (upper == Vector3.up || upper == Vector3.down)
        {
            newscale.y = 0.01f;
            return newscale;
        }
        if (upper == Vector3.back || upper == Vector3.forward)
        {
            newscale.z = 0.01f;
            return newscale;
        }
        newscale.x =0.01f;
        return newscale;
    }
    public void SetUpPlacementPreviewObject(PlacementObject placementObject)
    {
        _placementObjectPreview = placementObject;
        _placementObjectPreview.SetPlacementSystem(this);
        _placementObjectPreview.SetStatusButton(true);
        Vector3 scale = _placementObjectPreview.extendBounds.size;
        cellIndicatorXZ.transform.localScale = GetCellScale(scale);
        cellIndicatorXZ.SetActive(true);
        cellIndicatorXZ.GetComponent<Renderer>().material.SetVector("_DefaultScale", scale);     
        cellIndicatorXZ.transform.position = _placementObjectPreview.transform.position;
        SetScaleCellIndicator(scale);
    }

    public LayerMask GetLayerMask(List<TypePlacement> typePlacements)
    {
        int layermask = 0;
        foreach (TypePlacement typePlacement in typePlacements)
        {
            int newLayer = LayerMask.NameToLayer(typePlacement.ToString());
            layermask = layermask | (1 << newLayer);
        }
     
        return layermask;
    }
    public void OnSelectPlacementObjectInUI(PlacementObject placementObjectPrefab, PlacementSO placementSO, Item item,Action<bool> CanPlaceAction)
    {
        if (!_isPlacementMode) return;
        if (!_isCanPlace) return;
        if (_placementObjectPreview != null)
        {
            PlaceObject();
        }
        Vector3 placePoint = GetPositionCanPlace(placementObjectPrefab);
        if(placePoint != Vector3.zero)
        {
            PlacementObject placementObject = Instantiate(placementObjectPrefab, placePoint,Quaternion.identity, _placementParent.transform);
            placementObject.SetData(item);
            OnSelectPlacementObjectInWorld(placementObject);
            CanPlaceAction?.Invoke(true);
        }
        else
        {
            CanPlaceAction?.Invoke(false);
        }
    }
    public void OnSelectPlacementObjectInWorld(PlacementObject placementObject)
    {
        if (!_isPlacementMode) return;
        if (!_isCanPlace) return;
        if (placementObject == _placementObjectPreview) return;

        if(_placementObjectPreview != null)
        {
            PlaceObject();
        }
        SetUpPlacementPreviewObject(placementObject);
    }
    public Vector3 GetPositionCanPlace(PlacementObject placementObject)
    {
        
        LayerMask layermaskCanPlace = GetLayerMask(placementObject.typePlacements);
        if(Physics.Raycast(_sceneCamera.transform.position, _sceneCamera.transform.forward, out RaycastHit  hit, 200f, layermaskCanPlace, QueryTriggerInteraction.Ignore))
        {
            Vector3 cellPosition =hit.point;
            return cellPosition;
        }  
        return Vector3.zero;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        Ray ray = _sceneCamera.ScreenPointToRay(eventData.position);
        if (Physics.Raycast(ray, out RaycastHit hitInf, 100, placementObjectLayerMask))
        {
            OnSelectPlacementObjectInWorld(hitInf.collider.GetComponent<PlacementObject>());
            DisableZoomLook();
        }
        else
        {
            if (Physics.Raycast(ray, out RaycastHit hit, 100, worldUIMash))
            {
                if (hit.collider.TryGetComponent<Button>(out Button button))
                {
                    // button.onClick.Invoke();
                    return;
                }
            }
            else
            {
                if(_isCanPlace)
                {
                    PlaceObject();
                }
            }
        }
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        if (!_isPlacementMode) return;
        _lastDrageventData = null;
        EnableZoomLook();
    }

    public void OnDrag(PointerEventData eventData)
    {
        _lastDrageventData = eventData;
    }
    public void PlaceObject()
    {
        if (_placementObjectPreview == null) return;
        Placement placementPlaceData
            = new Placement(_placementObjectPreview.item.itemId, _placementObjectPreview.item.itemInstanceId, _placementObjectPreview.transform.position, _placementObjectPreview.transform.rotation);
        _placementObjectPreview.SetStatusButton(false);
        _lastDrageventData = null;
        _placementObjectPreview = null;
        cellIndicatorXZ.SetActive(false);
        
        _placementManager.AddPlacementData(placementPlaceData);
        EnableZoomLook();
        _isCanPlace = true;
    }
    public void RotationObject()
    {
       _placementObjectPreview.transform.Rotate(_placementObjectPreview.transform.InverseTransformDirection(_placementObjectPreview.transform.up), 90f);
        _placementObjectPreview.OnRotation();
        SetScaleCellIndicator(_placementObjectPreview.extendBounds.size);
        SetCanPlace();
    }

    public void StoreObject()
    {
        if (_placementObjectPreview == null) return;
        Placement placementPlaceData
          = new Placement(_placementObjectPreview.item.itemId, _placementObjectPreview.item.itemInstanceId, _placementObjectPreview.transform.position, _placementObjectPreview.transform.rotation);
        _placementManager.RemovePlacementData(placementPlaceData);

        Destroy(_placementObjectPreview.gameObject);
        _placementObjectPreview = null;
        _lastDrageventData = null;
        cellIndicatorXZ.SetActive(false);
        EnableZoomLook();
        _isCanPlace = true;
    }
    public void DisableZoomLook()
    {
        PlayerInput.Instance.PlayerActions.Zoom.Disable();
        PlayerInput.Instance.PlayerActions.Look.Disable();
        CameraManager.Instance.DisableLook();
    }
    public void EnableZoomLook()
    {
        PlayerInput.Instance.PlayerActions.Zoom.Enable();
        PlayerInput.Instance.PlayerActions.Look.Enable();
        CameraManager.Instance.EnableLook();
    }
    private void AddHandleCallback()
    {
        MainPopup mainPopup = (MainPopup)UIManager.Instance.PopupManager.GetPopup(UIPopupName.MainPopup);
        mainPopup.onSelected.OnDragAction += OnDrag;
        mainPopup.onSelected.OnPointerUpAction += OnPointerUp;
        mainPopup.onSelected.OnPointerDownAction += OnPointerDown;
    }
    private void RemoveHandleCallback()
    {
        MainPopup mainPopup = (MainPopup)UIManager.Instance.PopupManager.GetPopup(UIPopupName.MainPopup);
        mainPopup.onSelected.OnDragAction -= OnDrag;
        mainPopup.onSelected.OnPointerUpAction -= OnPointerUp;
        mainPopup.onSelected.OnPointerDownAction -= OnPointerDown;
    }

    private void OnPlacementPlaceDataChanged(List<Placement> placementPlaceDatas)
    {
        _placementManager.InitPlacementData(placementPlaceDatas);
    }
    public void RecallItem(List<Item> itemSelected)
    {
        CoroutineHandler.Instance.StartCoroutine(StartRecallItem(itemSelected));
    }
    public IEnumerator  StartRecallItem(List<Item> itemSelected)
    {
        UIManager.Instance.ShowLoading("Loading");
        uint count = 0;
        List<PlacementObject> placementObjects = _placementParent.GetComponentsInChildren<PlacementObject>().ToList();
        List<PlacementObject> placementObjectsSelected = new List<PlacementObject>();
        foreach(PlacementObject placementObject in placementObjects)
        {
            if (itemSelected.Contains(placementObject.item))
            {
                placementObjectsSelected.Add(placementObject);
            }
        }
        List<Placement> placements = new List<Placement>();
        if (placementObjectsSelected.Count > 0)
        {
            foreach (PlacementObject placementObject in placementObjectsSelected)
            {
                placements.Add(new Placement(placementObject.item.itemId, placementObject.item.itemInstanceId, Vector3.zero, Quaternion.identity));
                Debug.Log(placementObject.item.itemInstanceId);
            }
        }
        _placementManager.RemovePlacementDatas(placements);


        if ( placementObjectsSelected.Count > 0)
        {
            foreach(PlacementObject placementObject in placementObjectsSelected)
            {
                count++;
                Destroy(placementObject.gameObject);
                if(count > _itemRecallPerFrame)
                {
                    count = 0;
                    yield return new WaitForEndOfFrame();
                }
            }
        }
        UIManager.Instance.HideLoading();
        yield return null;
        
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (!_isPlacementMode) return;
        if (_placementObjectPreview == null) return;
        if (_lastDrageventData == null) return;
        Gizmos.color = Color.green;
        Gizmos.DrawRay(new Ray(CameraManager.Instance.mainCamera.transform.position, CameraManager.Instance.mainCamera.transform.forward));
    }

    
#endif
}
