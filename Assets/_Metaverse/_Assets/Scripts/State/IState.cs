using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum StateType
{
    None=0,
    Idling=1,
    Walking=2,
    Rolling=3,
    Running=4,
    Sprinting=5,

    HardStopping =6,
    LightStopping=7,
    MediumStopping=8,
    HardLanding=9,
    LightLanding=10,
    Jumping=11,
    Falling=12,
    Dashing=13,
    WaterIdling=14,
    Swimming=15,
    LightStopSwimming =16,
    HardStopSwimming=17,
    WaterDashing = 18,
    WaterUpState =19,


    RunningGameIdling_1 =101,
    RunningGameIdling_2 = 102,

    RunningGameRunningState = 105,
    RunningGameDashingState = 106,
    RunningGameJumpingState = 107,
    RunningGameFallingState = 108,

    RunningGameEmptySwipingState = 120,
    RunningGameLeftSwipingState = 121,
    RunningGameRightSwipingState =122,
    RunningGameRollingState = 123,

    RunningGameEmptyStatusState = 150,
    RunningGameStumblingState = 151,


    RunningGameEmptyAttackingState = 200,
    RunningGameRocketAttackingState = 201,

    RunningGameEmptyDefendState = 210,
    RunningGameShieldDefendState = 211,


    AIIdelingMonoState = 300,
    AIWalkingMonoState = 301,
    AILightStoppingMonoState =302,
    AILeftRotationMonoState =303,
    AIRightRotationMonoState =304,
    AIInteractionWithPlayerMonoState =305
}
public interface IState
{
    public void OnEnterState();
    public void OnExitState();
    public void OnHandleInput();
    public void OnUpdate();
    public void OnPhysicsUpdate();
    public void OnAnimationEnterEvent();
    public void OnAnimationExitsEvent();
    public void OnAnimationTransitionEvent();
    public StateType GetStateType();
    public void OnTriggerEnter(Collider collider);
    public void OnTriggerExit(Collider collider);
    void SetStateMachine(StateMachine stateMachine);
}
