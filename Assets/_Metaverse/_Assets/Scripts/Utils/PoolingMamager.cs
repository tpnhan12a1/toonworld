using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Pool;

public enum PoolingName:byte
{
    Particle = 0
}
public enum ParticleName
{
    ToonBlueFireTrail = 0
}

public class PoolingMamager : ManualSingletonMono<PoolingMamager>
{
    [SerializeField] private string _particlePoolingPath = "ObjectPooling/Particle/";
    [SerializeField]
    [Range(0, 200)] private int defaultCapacity = 5;

    [SerializeField]
    [Range(0, 1000)] private int maxCapacity = 10;

    private Dictionary<ParticleName, ObjectPool<ParticleSystem>> _particlePooling = new Dictionary<ParticleName, ObjectPool<ParticleSystem>>();

    public void  Start()
    {
        Array particleNames = Enum.GetValues(typeof(ParticleName));
        foreach (ParticleName particleName in particleNames)
        {
            RegisterPrefabInternal(particleName);
        }
    }
    public void RegisterPrefabInternal(ParticleName particleName)
    {
        ParticleSystem CreateFunc()
        {
            GameObject gameObject = Imba.Utils.ResourceManager.Instance.GetResourceByName<GameObject>(
                new StringBuilder().Append(_particlePoolingPath).Append(particleName).ToString());
            Debug.Log(gameObject);  
            return Instantiate(gameObject).GetComponent<ParticleSystem>();
        }
        void ActionOnGet(ParticleSystem particleSystem)
        {
            particleSystem.gameObject.SetActive(true);
            particleSystem.Play();
        }

        void ActionOnRelease(ParticleSystem particleSystem)
        {
            particleSystem.gameObject.SetActive(false);
            particleSystem.gameObject.transform.parent = transform;
            particleSystem.Stop();
        }

        void ActionOnDestroy(ParticleSystem particleSystem)
        {
            Destroy(particleSystem.gameObject);
        }
        _particlePooling.Add(particleName, new ObjectPool<ParticleSystem>(CreateFunc, ActionOnGet, ActionOnRelease, ActionOnDestroy, defaultCapacity: defaultCapacity, maxSize: maxCapacity));
        
        var prewarmObjects = new List<ParticleSystem>();
        for (var i = 0; i < defaultCapacity; i++)
        {
            ParticleSystem particleSystem = _particlePooling[particleName].Get();
            prewarmObjects.Add(particleSystem);
            particleSystem.gameObject.transform.parent = transform;
        }
        foreach (var ob in prewarmObjects)
        {
            _particlePooling[particleName].Release(ob);
        }
    }
    public ParticleSystem GetPoolingParticle(ParticleName particleName)
    {
        return _particlePooling[particleName].Get();
    }
    public void ReturnPartcle(ParticleName particleName, ParticleSystem particle)
    {
        _particlePooling[particleName].Release(particle);
    }
}
