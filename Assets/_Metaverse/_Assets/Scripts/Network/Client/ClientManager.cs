using Imba.UI;
using PlayFab.ClientModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;

public class ClientManager : ManualSingletonNetwork<ClientManager>
{
    private Dictionary<string, ulong> _clientConnected;

    public Action<NetCodeModels.ChatContent> OnAddChatContent;
    public Action<bool,string> OnResponseStatusJoinRoom;
    public Action OnHaveBeenKickedLobby;
  
    public Dictionary<string, ulong> clientConecnted { get { return _clientConnected; } }
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconnectCallback;
        NetworkManager.Singleton.OnServerStopped += OnServerStoped;
        if (IsClient)
        {
            _clientConnected = new Dictionary<string, ulong>();
            NetworkManager.Singleton.OnClientConnectedCallback += OnClientConnectedCallback;
        }
    }

    private void OnClientConnectedCallback(ulong clientId)
    {
        
    }

    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconnectCallback;
        NetworkManager.Singleton.OnServerStopped += OnServerStoped;
        if (IsClient)
        {
            NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConnectedCallback;
        }
    }

    private void OnServerStoped(bool obj)
    {
        if(NetworkManager.Singleton.IsClient) 
        {
            UIManager.Instance.PopupManager.HideAllDialog();
            SGSceneManager.Instance.ChangeSceneAsync(SceneName.SceneLobby, true, callback: () =>
            {
                UIManager.Instance.PopupManager.ShowPopup(UIPopupName.LobbyPopup);
            });
        }
    }

    private void OnClientDisconnectCallback(ulong obj)
    {
        if (!NetworkManager.Singleton.IsClient || obj != NetworkManager.Singleton.LocalClientId) return;
        UIManager.Instance.PopupManager.HideAllDialog();
        SGSceneManager.Instance.ChangeSceneAsync(SceneName.SceneLobby, true,callback: () =>
        {
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.LobbyPopup);
        });
    }

   
    [ClientRpc]
    public void ChangeRoomClientRpc(ClientRpcParams clientRpcParams = default)
    {
        Debug.Log("Helllo i change the room");
    }

    [ClientRpc]
    public void ResponseJoinRoomClientRpc(ulong roomId,SceneName sceneName, ClientRpcParams clientRpcParams = default)
    {
        PlayerDataManager.Instance.globalReusableData.currentRoomId = roomId;
        SGSceneManager.Instance.ChangeSceneAsync(sceneName, true, null,  () =>
        {
            SpawnPosition spawnPosition = FindObjectOfType<SpawnPosition>();
            Transform tf;
            if (PlayerDataManager.Instance.globalReusableData.localLobby != null)
            {
                int index = PlayerDataManager.Instance.globalReusableData.localLobby.GetLocalIndex();
                tf = spawnPosition.GetTranform(index);
            }
            else
            {
                tf = spawnPosition.getSpawnTransform();
            }
            Vector3 po = tf == null ? Vector3.zero : tf.position;
            Quaternion ro = tf == null ? Quaternion.identity : tf.rotation;


            ServerManager.Instance.SpawnPlayerObjectServerRpc(po, ro);

        });
    }
    [ClientRpc]
    public void OnCreateRoomClientRpc(NetCodeModels.CreateRoomResponse response,ClientRpcParams clientRpcParams = default)
    {
        ServerManager.Instance.ChangeRoomServerRpc(response.roomId);
    }

    [ClientRpc]
    public void ChatClientRpc(NetCodeModels.ChatContent chatContent,ClientRpcParams clientRpcParams = default)
    {
        OnAddChatContent?.Invoke(chatContent);
    }

    [ClientRpc]
    public void GetPlayerDataClientRpc(ClientRpcParams clientRpcParams)
    {
        NetCodeModels.PlayerData playerData = new NetCodeModels.PlayerData()
        {
            playfabId = PlayfabManager.Instance.PlayfabID,
            displayName = PlayerDataManager.Instance.displayName,
            avatarUrl = PlayerDataManager.Instance.avatarUrl,
            level = PlayerDataManager.Instance.level,
        };
        ServerManager.Instance.GetPlayerDataResponseServerRpc(playerData);
    }
    [ClientRpc]
    public void ResponseOnPartyChangeClientRpc(NetCodeModels.Party party, ClientRpcParams clientRpcParams = default)
    {
        Debug.Log($"Party {party.id} change");
        PlayerDataManager.Instance.partyData.party = party;
    }
    [ClientRpc]
    public void CreatePartyResponseClientRpc(NetCodeModels.Party p, ClientRpcParams clientRpcParams)
    {
        PlayerDataManager.Instance.partyData.party = p;
    }
    [ClientRpc]
    internal void ResponseOnPartyRemoveClientRpc(ClientRpcParams clientRpcParams)
    {
        PlayerDataManager.Instance.partyData.party = null;
        Debug.Log("Party is destroy");
    }

   
    [ClientRpc]
    public void ResponseLobbyCloseClientRpc(ClientRpcParams clientRpcParams = default)
    {
        PlayerDataManager.Instance.globalReusableData.localLobby = null;
    }
   
    [ClientRpc]
    internal void ReceiverChatEmotionClientRpc(NetworkObjectReference playerObject, string name, ClientRpcParams clientRpcParams = default)
    {
        if(playerObject.TryGet( out NetworkObject networkObject ))
        {
            PlayerVFXController emotionController = networkObject.gameObject.GetComponentInChildren<PlayerVFXController>();
            emotionController.InstanceEmotion(name);
        }
    }
    [ClientRpc]
    public void ResponseGetLobbyListClientRpc(NetCodeModels.GetLobbyResponse listLobbyResponse, ClientRpcParams clientRpcParams = default)
    {
        List<ToonWorld.GameRoomLobby> gameRoomLobbies = new List<ToonWorld.GameRoomLobby>();
        if(listLobbyResponse.lobbies != null)
        {
            listLobbyResponse.lobbies.ForEach(x => gameRoomLobbies.Add(x.ToClass()));
        }
        PlayerDataManager.Instance.globalReusableData.gameRoomLobbyManager.Set(gameRoomLobbies);
    }
    [ClientRpc]
    public void ResponsePlayerLeaveLobbyClientRpc(ClientRpcParams clientRpcParams = default)
    {
        PlayerDataManager.Instance.globalReusableData.PlayerLeaveLobby();
    }
    [ClientRpc]
    public void PlayerLeaveLobbyClientRpc(string playfabId, ClientRpcParams clientRpcParams = default)
    {
        PlayerDataManager.Instance.globalReusableData.localLobby.OnPlayerLeaveLobby(playfabId);
    }

    [ClientRpc]
    public void PlayerJoinLobbyClientRpc(NetCodeModels.PlayerData playerData,ClientRpcParams clientRpcParams = default)
    {
        PlayerDataManager.Instance.globalReusableData.localLobby.OnPlayerJoinLobby(playerData);
    }
    [ClientRpc]
    public void ResponseJoinLobbyRoomClientRpc(NetCodeModels.JoinLobbyResponse joinLobbyResponse, ClientRpcParams clientRpcParams = default)
    {
        PlayerDataManager.Instance.globalReusableData.PlayerJoinLobby(joinLobbyResponse);
    }
    [ClientRpc]
    public void ResponseKickedOutLobbyClientRpc(ClientRpcParams clientRpcParams = default)
    {
        OnHaveBeenKickedLobby?.Invoke();
    }
    [ClientRpc]
    public  void ResponseStatusJoinLobbyClientRpc(bool status,string message,ClientRpcParams clientRpcParams = default)
    {
        OnResponseStatusJoinRoom?.Invoke(status,message);
    }
    [ClientRpc]
    public void ResponseListPlayerClientRpc(NetCodeModels.ListString playfabIds , NetCodeModels.ListUlong clientIds, ClientRpcParams clientRpcSendParams = default)
    {
        for(int i=0;i< playfabIds.list.Count;i++)
        {
            _clientConnected.TryAdd(playfabIds.list[i], clientIds.list[i]);
        }
    }
    [ClientRpc]
    public void ResponseWhenPlayerJoinOutClientRpc(string playfabId,ulong clientId, bool status, ClientRpcParams clientRpcParams = default)
    {
        if(status)
        {
            _clientConnected.TryAdd(playfabId, clientId);
        }
        else
        {
            _clientConnected.Remove(playfabId);
        }
    }
    [ClientRpc]
    public void ResponseGuildClientRpc(NetCodeModels.GuildSender guildSender, ClientRpcParams clientRpcParams)
    {
        Debug.LogWarning("guild sender");
        switch (guildSender.guildTag)
        {
            case PlayfabContant.GuildTag.Delete:
                PlayerDataManager.Instance.guildManager.RemoveMember(guildSender.guildMemberInfor.playfabId);
                break;
            case PlayfabContant.GuildTag.KickMember:
                if(guildSender.guildMemberInfor.playfabId == PlayfabManager.Instance.PlayfabID)
                {
                    PlayerDataManager.Instance.guildManager.isHaveGuild = false;
                }
                else
                {
                    PlayerDataManager.Instance.guildManager.RemoveMember(guildSender.guildMemberInfor.playfabId);
                }
                break;
            case PlayfabContant.GuildTag.UnPollMember:
                PlayerDataManager.Instance.guildManager.Edit(guildSender.guildMemberInfor.ToGuildMember());
                break;
            case PlayfabContant.GuildTag.PollMember:
                PlayerDataManager.Instance.guildManager.Edit(guildSender.guildMemberInfor.ToGuildMember());
                break;
            case PlayfabContant.GuildTag.AuthenMember:
                PlayerDataManager.Instance.guildManager.Edit(guildSender.guildMemberInfor.ToGuildMember());
                PlayerDataManager.Instance.guildManager.StepDown(guildSender.leaderPlayfabId);
                break;
        }
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.M))
        {
            QuickNotifyPopup quickChatPopup = (QuickNotifyPopup) UIManager.Instance.PopupManager.GetPopup(UIPopupName.QuickNotifyPopup);
            quickChatPopup?.ShowNotify("Hello", null, null);
        }
        if (Input.GetKeyUp(KeyCode.N))
        {
            QuickNotifyPopup quickChatPopup = (QuickNotifyPopup)UIManager.Instance.PopupManager.GetPopup(UIPopupName.QuickNotifyPopup);
            quickChatPopup?.ShowNotify("Hello  NNNNNNNNNNNNN", null, null);
        }
    }
    [ClientRpc]
    internal void ResponseGuildInviteClientRpc(NetCodeModels.EntityKey guildEntity, ClientRpcParams clientRpcParams)
    {
        QuickNotifyPopup quickNotifyPopup =  (QuickNotifyPopup)UIManager.Instance.PopupManager.GetPopup(UIPopupName.QuickNotifyPopup);
        if(quickNotifyPopup != null)
        {
            quickNotifyPopup.ShowNotify("Guild invite you",
            () =>
            {
                APIModels.GuildRequest request = new APIModels.GuildRequest()
                {
                    requestType = PlayfabContant.GuildTag.AddMember,
                    subFunc = new APIModels.GuildAddMemberRequest()
                    {
                        guild = guildEntity.ToEntityKey(),
                        member = PlayfabManager.Instance.entityKey
                    }
                };
                PlayfabManager.Instance.ExecuteFunction<APIModels.GetGuidResponse>(
                    PlayfabContant.FunctionName.Guild,
                    request,
                    false,
                    (obj, data) =>
                    {
                        PlayerDataManager.Instance.guildManager.Init(data.isHaveGuild,data.guildEntity, data.guildName, data.introduction, data.members);
                    }, null);
                // Join Guild Sync
                NetCodeModels.GuildMemberInfor guildMemberInfor = new NetCodeModels.GuildMemberInfor()
                {
                    playfabId = PlayfabManager.Instance.PlayfabID,
                    displayName = PlayerDataManager.Instance.displayName,
                    level = PlayerDataManager.Instance.level,
                    role = PlayfabContant.PlayerTag.Members,
                    entityKey = new NetCodeModels.EntityKey()
                    {
                        Id = PlayfabManager.Instance.entityKey.Id,
                        Type = PlayfabManager.Instance.entityKey.Type
                    }
                };
                ServerManager.Instance.PlayerJoinNewGroupServerRpc(guildEntity, guildMemberInfor);

            }, null);
        }
    }

    [ClientRpc]
    internal void PlayerJoinGuildClientRpc(NetCodeModels.GuildMemberInfor guildMemberInfor, ClientRpcParams clientRpcParams = default)
    {
        PlayerDataManager.Instance.guildManager.AddGuildMember(guildMemberInfor.ToGuildMember());
    }

    [ClientRpc]
    internal void FriendResponseControllerClientRpc(NetCodeModels.FriendContent friendContent, ClientRpcParams clientRpcParams)
    {
        Debug.LogError($"Friend Message {friendContent.message}");
        switch(friendContent.message){

            case PlayfabContant.FriendSyncTag.RequestFriend:
                PlayerDataManager.Instance.friendManager.AddFriend(friendContent.friend.ToClass());
                break;
            case PlayfabContant.FriendSyncTag.RemoveRequest:
                PlayerDataManager.Instance.friendManager.RemoveFriend(friendContent.friend.ToClass());
                break;
            case PlayfabContant.FriendSyncTag.DenyFriend: 
                PlayerDataManager.Instance.friendManager.RemoveFriend(friendContent.friend.ToClass());
                break;
            case PlayfabContant.FriendSyncTag.RemoveFriend:
                PlayerDataManager.Instance.friendManager.RemoveFriend(friendContent.friend.ToClass());
                break;
            case PlayfabContant.FriendSyncTag.AcceptedFriend:
                PlayerDataManager.Instance.friendManager.AddFriend(friendContent.friend.ToClass());
                break;
        }
    }
}
