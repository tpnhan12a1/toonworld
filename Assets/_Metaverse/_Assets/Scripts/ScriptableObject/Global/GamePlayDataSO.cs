using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GamePlayData", menuName = "Custom/Global/GamePlayData")]
public class GamePlayDataSO : ScriptableObject
{
    [SerializeField]
    private GamePlayType _gamePlayType;
    [SerializeField]
    private Sprite _sprite;
    [SerializeField]
    private string _gameName;

    public GamePlayType gamePlayType { get { return _gamePlayType; } }
    public Sprite sprite { get { return _sprite; } }
    public string gameName { get { return _gameName; } }
}
