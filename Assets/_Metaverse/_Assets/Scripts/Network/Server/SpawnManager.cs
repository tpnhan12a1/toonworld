using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class SpawnManager : NetworkBehaviour
{
    [SerializeField] private NetworkObject _player;
    [SerializeField] private NetworkObject _runningGamePlayer;
    [SerializeField] private NetworkObject _renderNetworkCharacter;
    [SerializeField] private NetworkObject _runningGamePlayController;
    public NetworkObject player { get { return _player; } }
    public NetworkObject runningGamePlayer { get { return _runningGamePlayer; } }
    public NetworkObject renderNetworkCharacter { get {  return _renderNetworkCharacter; } }
    public NetworkObject runningGamePlayController { get {  return _runningGamePlayController; } }
    public void DespawnObjectInRoom(NetworkObject networkObject, Room room)
    {
        if(networkObject.IsSpawned)
        {
            networkObject.Despawn();
        }
    }
    public NetworkObject SpawnObjectInRoom(NetworkObject networkObject, Room room, Vector3 position, Quaternion rotation, Player owner = null)
    {
        GameObject objectInstan = Instantiate(networkObject.gameObject, position, rotation);
        NetworkObject network = objectInstan.GetComponent<NetworkObject>();
        network.CheckObjectVisibility = ((clientId) =>
        {
            if (room.playerManager.ContainPlayer(clientId))
            {
                return true;
            }
            else
            {
                return false;
            }
        });
        if (owner != null)
        {
            network.SpawnWithOwnership(owner.GetClientId());
        }
        else
        {
            network.Spawn();
        }
        room.networkObjects.Add(network);
        return network;
    }
    public NetworkObject SpawnObjectInLobby(NetworkObject networkObject, Lobby lobby, Vector3 position, Quaternion rotation, Player owner = null)
    {
        GameObject objectInstan = Instantiate(networkObject.gameObject,position,rotation);
        NetworkObject network = objectInstan.GetComponent<NetworkObject>();
        network.CheckObjectVisibility = ((clientId) =>
        {
            if (ServerManager.Instance.playerManager.GetPlayer(clientId) != null &&
            ServerManager.Instance.playerManager.GetPlayer(clientId)?.lobby == lobby)
            {
                return true;
            }
            else
            {
                return false;
            }
        });
        if(owner != null)
        {
            network.SpawnWithOwnership(owner.GetClientId());
        }
        else
        {
            network.Spawn();
        }
        return network;
    }
    public NetworkObject SpawnAsPlayerObject(Player player, Vector3 position, Quaternion rotation)
    {
        NetworkObject networkObject;
        Room room = player.GetRoom();
        
        switch (room.sceneName)
        {
            
            case SceneName.SceneRunningGame:
                GameObject gameObject = Instantiate(_runningGamePlayer.gameObject, position, rotation);
                networkObject = gameObject.GetComponent<NetworkObject>();
                break;
            default:

                gameObject = Instantiate(_player.gameObject, position, rotation);
                networkObject = gameObject.GetComponent<NetworkObject>();
                break;
        }
        networkObject.CheckObjectVisibility = ((client) =>
        {
            Room room = ServerManager.Instance.playerManager.GetPlayer(player.GetClientId()).GetRoom();
            if (room.playerManager.ContainPlayer(client))
            {
                Debug.Log($"Show player object {networkObject} to client {client}");
                return true;
            }
            else
            {
                Debug.Log($"Hide player object {networkObject} to client {client}");
                return false;
            }
        });

        networkObject.SpawnAsPlayerObject(player.GetClientId());
        return networkObject;
    }

    public void DespawnObject(NetworkObject networkObject)
    {
        if (networkObject.IsSpawned)
        {
            networkObject.Despawn();
        }
    }
}
