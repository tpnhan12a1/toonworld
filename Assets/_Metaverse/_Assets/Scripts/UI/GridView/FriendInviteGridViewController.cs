using SuperScrollView;
using System.Collections.Generic;
using System.Linq;
using ToonWorld;
public class FriendInviteGridViewController : LoopGridView, IGroupTab
{
    private List<Friend> _friendList;
    //private ScrollRect _scrollRect;
    public void OnInit()
    {
        _friendList = new List<Friend>();
        //_scrollRect = GetComponent<ScrollRect>();
        InitGridView(_friendList.Count, OnGetFriendItemByRowColum);
    }

    private LoopGridViewItem OnGetFriendItemByRowColum(LoopGridView gridView, int index, int row, int column)
    {
        if (_friendList.Count == 0) return null;
        FriendInviteItem friendItem = (FriendInviteItem)gridView.NewListViewItem("FriendInviteItem");
        friendItem.OnRefresh(_friendList[index], this);
        return friendItem;
    }

    public void Select()
    {
        gameObject.SetActive(true);
        PlayerDataManager.Instance.friendManager.RegisterOnListFriendChanged(OnListFriendChanged);
    }

    public void DesSelect()
    {
        gameObject.SetActive(false);
        PlayerDataManager.Instance.friendManager.UnRegisterOnListFriendChanged(OnListFriendChanged);
    }

    private void OnListFriendChanged(List<Friend> friends)
    {
        _friendList = friends.Where(x => x.friendTags.Contains(PlayfabContant.FriendTag.Pending)).OrderByDescending(x => x.level).ThenBy(x => x.displayName).ToList();
        RefreshItem();
    }
    public void RefreshItem()
    {
        SetListItemCount(_friendList.Count);
        RefreshAllShownItem();
        //Vector2 offset =  _scrollRect.normalizedPosition;
        
    }
}
