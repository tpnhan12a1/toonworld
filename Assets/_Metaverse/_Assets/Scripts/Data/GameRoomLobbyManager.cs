using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ToonWorld;
public class GameRoomLobbyManager
{
    private List<GameRoomLobby> _gameRoomLobbies = new List<GameRoomLobby>();
    public List<GameRoomLobby> gameRoomLobbies { get { return _gameRoomLobbies; } }

    private Action<List<GameRoomLobby>> OnGameRoomLobbyChanged;
    public void RegisterOnGameGameRoomLobbyChanged(Action<List<GameRoomLobby>> action)
    {
        OnGameRoomLobbyChanged += action;
        action?.Invoke(_gameRoomLobbies);
    }
    public void UnRegisterOnGameGameRoomLobbyChanged(Action<List<GameRoomLobby>> action)
    {
        OnGameRoomLobbyChanged -= action;
    }
    public void Clear()
    {
        _gameRoomLobbies.Clear();
    }
    public void Set(List<GameRoomLobby> gameRobbies)
    {
        _gameRoomLobbies = gameRobbies;
        OnGameRoomLobbyChanged?.Invoke(_gameRoomLobbies);
    }
    public void Add(GameRoomLobby gameRoomLobby)
    {
        _gameRoomLobbies.Add(gameRoomLobby);
        OnGameRoomLobbyChanged.Invoke(_gameRoomLobbies);
    }
    public void Add(List<GameRoomLobby> gameRobbies)
    {
        gameRobbies.ForEach(x => _gameRoomLobbies.Add(x));
        OnGameRoomLobbyChanged.Invoke(_gameRoomLobbies);
    }
    public void Remove(GameRoomLobby gameRoomLobby)
    {
        _gameRoomLobbies.Remove(gameRoomLobby);
        OnGameRoomLobbyChanged.Invoke(_gameRoomLobbies);
    }
}
