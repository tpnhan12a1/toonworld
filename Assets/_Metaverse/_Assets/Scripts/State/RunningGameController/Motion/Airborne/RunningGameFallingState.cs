using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameFallingState : RunningGameAirborneState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        _runningGameStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.FallParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.FallParameterHash);
    }
    public override StateType GetStateType()
    {
       return StateType.RunningGameFallingState;
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        //LimitVerticalVelocity();
    }
    private void LimitVerticalVelocity()
    {
        Vector3 playerVeticalVelocity = GetVerticalVelocity();
        if (playerVeticalVelocity.y >= -PlayerDataManager.Instance.playerData.fallSpeedLimit)
        {
            return;
        }
        Vector3 limitedVelocity = new
            Vector3(0, -PlayerDataManager.Instance.playerData.fallSpeedLimit - playerVeticalVelocity.y, 0f);
        _runningGameStateMachine.playerRunningGameController.rb.AddForce(limitedVelocity, ForceMode.VelocityChange);

    }
}
