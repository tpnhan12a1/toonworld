using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum RunningSkillType
{
    Moving,
    Attacking,
    Defending
}
public class RunningGameDataSkill
{
    public ulong count;
    public bool status = true;
    public GiftName giftName;
}
[System.Serializable]
public class RunningGameReusableData
{
    [SerializeField]
    public Vector2 movementInput = Vector2.zero;
    [SerializeField]
    [Range(0f, 10f)] 
    public float jumpForce = 10f;
    [SerializeField]
    [Range(0f, 10f)] public float movementSpeedModifier = 5f;
    [SerializeField] public int currentLane = 1;
    [SerializeField] public float widthOfRoad = 10f;
    [SerializeField] public float rootXOfRoad = -1;
    [SerializeField] public float witdthOfLane = 10f;
    [SerializeField] public float lowerLimitSpeedModifier = 3f;
    [SerializeField] public float upperLimitSpeedModifier = 10f;

    [SerializeField]
    [Range(0f, 100f)]
    public float swipeForce = 50f;
    public  bool isGrounded;

    private List<RunningGameDataSkill> skillDatas = new List<RunningGameDataSkill>();
    private Action<List<RunningGameDataSkill>> OnSkillDataChanged;
    private Action<RunningGameReusableData,GiftName> OnUseGift;

    private bool _isUseShield = false;
    private bool _isVisible = false;
    public bool isVisible { get { return _isVisible; } set { OnIsVisibleChanged?.Invoke(_isVisible, value); _isVisible = value; } }
    public bool isUseShield { get { return _isUseShield; } set { OnIsUseShieldChanged?.Invoke(_isUseShield, value); _isUseShield = value; } }

    private Action<bool, bool> OnIsUseShieldChanged;
    private Action<bool,bool> OnIsVisibleChanged;

    public void RegisterOnIsUseShieldChanged(Action<bool,bool> onIsUseShieldChanged)
    {
        OnIsUseShieldChanged += onIsUseShieldChanged;
        onIsUseShieldChanged?.Invoke(_isUseShield, _isUseShield);
    }
    public void UnRegisterIsUseShieldChanged(Action<bool, bool> onIsUseShieldChanged)
    {
        OnIsUseShieldChanged -= onIsUseShieldChanged;
    }

    public void RegisterOnIsVisibleChanged(Action<bool,bool> onIsVisibleChanged) 
    {
        OnIsVisibleChanged += onIsVisibleChanged;
        onIsVisibleChanged?.Invoke(_isVisible, _isVisible);
    }
    public void UnRegisterIsVisibleChanged(Action<bool,bool> onIsVisibleChanged) 
    { 
        OnIsVisibleChanged -= onIsVisibleChanged; 
    }
    public float GetXLocationOfLane(int lane)
    {
        return rootXOfRoad + witdthOfLane * (lane - ((float)1 / 2));
    }
    public float GetXCurrentLocationOfLane()
    {
        return GetXLocationOfLane(currentLane);
    }
    public void SetUpData(GameObject lane, GameObject rootLane)
    {
        widthOfRoad = lane.GetComponent<MeshRenderer>().bounds.size.x;
        witdthOfLane = widthOfRoad / 3;
        rootXOfRoad = rootLane.transform.position.x;
    }
    public void SetSpeedModifier(float speed)
    {
        movementSpeedModifier = Mathf.Clamp(speed, lowerLimitSpeedModifier, upperLimitSpeedModifier);
    }
    public void MinusSpeedModifier(float speed)
    {
        movementSpeedModifier = Mathf.Clamp(movementSpeedModifier - speed, lowerLimitSpeedModifier, upperLimitSpeedModifier);
    }
    public void PlusSpeedModifier(float speed)
    {
        movementSpeedModifier = Mathf.Clamp(movementSpeedModifier + speed, lowerLimitSpeedModifier, upperLimitSpeedModifier);
    }
    public void AddSkillData(GiftName giftName)
    {
        RunningGameDataSkill skillData = skillDatas.Where(x => x.giftName.Equals(giftName)).FirstOrDefault();
        if (skillData != null)
        {
            skillData.count++;
        }
        else
        {
            skillData = new RunningGameDataSkill()
            {
                count = 1,
                giftName = giftName
            };
            skillDatas.Add(skillData);
        }
        OnSkillDataChanged?.Invoke(skillDatas);
    }
    public void UseSkill(GiftName giftName)
    {
        OnUseGift.Invoke(this,giftName);
    }

    public void LockSkill(GiftName giftName)
    {
        RunningGameDataSkill runningGameDataSkill = skillDatas.Where(x => x.giftName == giftName).FirstOrDefault();
        if(runningGameDataSkill != null)
        {
            runningGameDataSkill.status = false;
            OnSkillDataChanged?.Invoke(skillDatas);
        }
    }

    public void UnLockSkill(GiftName giftName)
    {
        RunningGameDataSkill runningGameDataSkill = skillDatas.Where(x => x.giftName == giftName).FirstOrDefault();
        if(runningGameDataSkill != null)
        {
            runningGameDataSkill.status = true;
            OnSkillDataChanged?.Invoke(skillDatas);
        }
    }

    public void RemoveSkillData(GiftName giftName)
    {
        RunningGameDataSkill skillData = skillDatas.Where(x => x.giftName.Equals(giftName)).FirstOrDefault();
        if (skillData == null) { return; }
        if (skillData.count == 1)
        {
            skillDatas.Remove(skillData);
        }
        else
        {
            skillData.count--;
        }
        OnSkillDataChanged?.Invoke(skillDatas);
    }

    public void RegisterOnSkillDataChange(Action<List<RunningGameDataSkill>> onSkillDataChanged)
    {
        OnSkillDataChanged += onSkillDataChanged;
        onSkillDataChanged?.Invoke(skillDatas);
    }
    public void UnregisterOnSkillDataChange(Action<List<RunningGameDataSkill>> onSkillDataChanged)
    {
        OnSkillDataChanged -= onSkillDataChanged;
    }
    public void RegisterOnUseGift(Action<RunningGameReusableData,GiftName> onUseGift)
    {
        OnUseGift += onUseGift;
    }
    public void UnRegisterOnUseGift(Action<RunningGameReusableData,GiftName> onUseGift)
    {
        OnUseGift -= onUseGift;
    }
}
