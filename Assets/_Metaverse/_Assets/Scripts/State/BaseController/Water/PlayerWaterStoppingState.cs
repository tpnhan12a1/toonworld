using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract class PlayerWaterStoppingState : PlayerWaterState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        ResetVelocity();
        PlayerDataManager.Instance.reusableData.movementSpeedModifier = 0f;
        PlayerDataManager.Instance.reusableData.shouldSprint = false;
        StartAnimation(PlayerDataManager.Instance.animationData.WaterStoppingParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.WaterStoppingParameterHash);
    }
    protected override void OnMouseMovementStarted(InputAction.CallbackContext context)
    {
        OnMove();
    }
    protected override void AddInputActionsCallbacks()
    {
        PlayerInput.Instance.PlayerActions.Move.started += OnMovementStarted;
    }

    protected override void RemoveInputActionCallbacks()
    {
        PlayerInput.Instance.PlayerActions.Move.started -= OnMovementStarted;
    }
    private void OnMovementStarted(InputAction.CallbackContext content)
    {
        OnMove();
    }

}
