using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLobbyGridViewController : LoopGridView
{
    List<NetCodeModels.PlayerData> playerData;
   public void OnInit()
    {
         playerData = 
            PlayerDataManager.Instance.globalReusableData.localLobby != null?
            PlayerDataManager.Instance.globalReusableData.localLobby.GetPlayerDatas() : new List<NetCodeModels.PlayerData>();
        InitGridView(playerData.Count, OnGetPlayerInforInLobbyItemByRowColumn);
    }

    private LoopGridViewItem OnGetPlayerInforInLobbyItemByRowColumn(LoopGridView gridView, int index, int row, int clomn)
    {
        if(playerData?.Count == 0) return null;
        PlayerInforInLobbyItem playerInforInLobbyItem = (PlayerInforInLobbyItem)NewListViewItem("PlayerInforInLobbyItem");
        playerInforInLobbyItem.OnRefresh(playerData[index], this);
        return playerInforInLobbyItem;
    }

    public void OnShow()
    {
        PlayerDataManager.Instance.globalReusableData.RegisterOnLobbyChanged(OnLobbyChanged);
    }

    private void OnLobbyChanged(ILocalLobby oldLobby,ILocalLobby newLobby)
    {
        if(newLobby == null)
        {
            
            oldLobby?.UnRegisterOnListDataChanged(OnListDataChanged);
        }
        else
        {
            newLobby?.RegisterOnListDataChanged(OnListDataChanged);
        }
    }

    private void OnListDataChanged(List<NetCodeModels.PlayerData> list)
    {
        playerData = list;
        SetListItemCount(playerData.Count);
        RefreshAllShownItem();
    }

    public void OnHide()
    {
        PlayerDataManager.Instance.globalReusableData.UnRgisterOnLobbyChanged(OnLobbyChanged);
    }
}
