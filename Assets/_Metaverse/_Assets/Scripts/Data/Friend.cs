using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace ToonWorld
{
    public class Friend
    {
        public string friendPlayfabId;
        public string avatarUrl = "";
        public string displayName = "";
        public int level = 0;
        public List<string> friendTags = new List<string>();

        public Friend(string friendPlayfabId, string avatarUrl, string displayName, int level, List<string> friendTags)
        {
            this.friendPlayfabId = friendPlayfabId;
            this.avatarUrl = avatarUrl;
            this.displayName = displayName;
            this.level = level;
            this.friendTags = friendTags;
        }
        public override bool Equals(object obj)
        {
            Friend other = obj as Friend;
            if (other == null) return false;
            return friendPlayfabId == other.friendPlayfabId;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(friendPlayfabId);
        }

        public NetCodeModels.Friend ToStruct()
        {
            return new NetCodeModels.Friend(friendPlayfabId, avatarUrl, displayName, level, friendTags);
        }
    }

}
