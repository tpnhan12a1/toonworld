
using UnityEngine;
using System;
using PlayFab;
using Imba.UI;
using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using static Unity.Burst.Intrinsics.X86.Avx;
using PlayFab.ClientModels;

public class PlayfabContant
{
    public class FunctionName
    {
        //Base
        public const string OnLogin = "OnLogin";
        public const string Login = "Login";
        public const string LevelUp = "LevelUp";
        public const string AddExp = "AddExp";
        public const string ChangeDisplayName = "ChangeDisplayName";
        public const string ChangeIntroduction = "ChangeIntroduction";
        public const string ChangeAvatarUrl = "ChangeAvatarUrl";
        public const string SavePlayerItem = "SavePlayerItem";
        //Friend
        public const string GetFriends = "GetFriends";
        public const string AddFriend = "AddFriend";
        public const string RemoveFriend = "RemoveFriend";
        public const string GetUserSuggest = "GetUserSuggest";
        public const string AcceptFriendRequest = "AcceptFriendRequest";
        public const string GetPlayerInfo = "GetPlayerInfo";
        //Chat

        //Invetory
        //Item
        public const string GetItems = "GetItems";
        public const string GetPlayerInventory = "GetPlayerInventory";

        //Placement
        public const string UpdatePlayerPlacement = "UpdatePlayerPlacement";
        public const string GetPlayerPlacement = "GetPlayerPlacement";

        public const string Guild = "Guild";
    }

    public class FriendSyncTag
    {
        public const string RemoveFriend = "RemoveFriend";
        public const string RequestFriend = "RequestFriend";
        public const string AcceptedFriend = "AcceptedFriend";
        public const string DenyFriend = "DenyFriend";
        public const string RemoveRequest = "RemoveRequest";
    }
    public class PlayerTag
    {
        public const string SubAdmintrators = "SubAdmintrators";
        public const string Admintrators = "Administrators";
        public const string Members = "Memmbers";
        public const string Inventations = "Inventations";
        public const string Applications = "Applications";
    }
    public class GuildTag
    {
        public const string Create = "Create";
        public const string Delete = "Delete";
        public const string Get = "Get";
        public const string Invite = "Invite";
        public const string AcceptedInvite = "AcceptedInvite";
        public const string DenyInvite = "DenyInvite";
        public const string ListMember = "ListGroupMember";
        public const string ListInventation = "ListInventation";
        public const string RemoveMember = "RemoveMember";
        public const string RemoveInvention = "RemoveInvention";
        public const string UpdateIntroduce = "UpdateIntroduce";
        public const string UpdateGuildName = "UpdateGuildName";
        public const string PollMember = "PollMember";
        public const string UnPollMember = "UnPollMember";
        public const string AuthenMember = "AuthenMember";
        public const string KickMember = "KickMember";
        public const string AddMember = "AddMember";
    }
    public class FriendTag
    {
        public const string Friend = "Friend";
        public const string Received = "Received";
        public const string Pending = "Pending";
    }

    public class ItemClass
    {
        public const string Item = "Item";
        public const string Placement = "Placement";
    }
}
public class APIModels
{   
    public class SubFunc
    {
        public PlayFab.GroupsModels.EntityKey[] entityKeys;
        public PlayFab.GroupsModels.EntityKey guild;
    }
    public class GuildAddMemberRequest
    {
        public PlayFab.GroupsModels.EntityKey guild;
        public PlayFab.ClientModels.EntityKey member;
    }
    public class GuildInviteRequest
    {
        public PlayFab.GroupsModels.EntityKey guild;
        public string playfabIdOfMember;
    }
    public class GuildActionRequest
    {
        public PlayFab.GroupsModels.EntityKey guild;
        public PlayFab.GroupsModels.EntityKey memberEntity;
        public string originRoleId = string.Empty;
    }
    public class CreateGuildRequest
    {
        public string guildName = string.Empty;
    }
    public class ChangedGuildNameRequest
    {
        public string guildName = string.Empty;
        public PlayFab.GroupsModels.EntityKey guild;
    }
    public class ChangedIntroductionRequest
    {
        public string introduction = string.Empty;
        public PlayFab.GroupsModels.EntityKey guild;
    }
    public class GuildRequest
    {
        public string requestType = string.Empty;
        public object subFunc;
    }
    public class GuildListResponse
    {
        public List<GuildMember> members = new List<GuildMember> ();
    }
    public class CreateGuildResponse
    {
        public PlayFab.GroupsModels.EntityKey groupEntity;
    }
    public class GetGuidResponse
    {
        public bool isHaveGuild = false;
        public PlayFab.GroupsModels.EntityKey guildEntity;
        public string introduction = string.Empty;
        public string guildName = string.Empty;
        public List<GuildMember> members = new List<GuildMember>();
    }
    public class Quaternion
    {
        public float x;
        public float y;
        public float z;
        public float w;
    }
    public class Vector3
    {
        public float x;
        public float y;
        public float z;
    }
    public class GetPlacementsRequest
    {
        public string playfabId;
    }
    public class PlacementData
    {
        public List<Placement> placementData;
    }
    public class GetPlayerInfoResponse
    {
        public string playfabId;
        public string displayName;
        public string avatarUrl;
        public string introduction;
        public int level;
        public PlayerItem playerItem;
    }
    public class AddFriendResponse
    {
        public ToonWorld.Friend friend;
    }
    public class GetPlayerInfoRequest
    {
        public string playfabId;
    }
    public class FriendRequest
    {
        public string friendPlayfabId;
    }

    public class GetListItemsRequest
    {
        public string CatalogVersions;
    }
    public class GetFriendRequest
    {
        public string requestParam = String.Empty;
    }
    public class GetFriendsResponse
    {
        public List<ToonWorld.Friend> friends = new List<ToonWorld.Friend>();
    }
    public class GetListItemsResponse
    {
        public List<Item> items = new List<Item>();
    }

    public class CommonResponse
    {

    }
    public class CommonRequest
    {

    }
    public class ChangeAvatarIdRequest
    {
        public string avatarUrl;
    }
    public class ChangeAvatarIdResponse
    {
        public string avatarUrl;
    }
    public class ChangeIntroductionRequest
    {
        public string introduction;
    }
    public class ChangeIntroductionResponse
    {
        public string introduction;
    }
    public class ChangeDisplayNameRequest
    {
        public string displayName;
    }

    public class ChangeDisplayNameResponse
    {
        public string displayName;
    }
    public class LevelUpRequest
    {
        public int level;
        public int exp;
    }
    public class OnLoginResponse
    {
        public string displayName = "";
        public string avatarUrl = "";
        public Currency currency;
        public Equipment equipment;
        public Appearance appearance;
        public PlayerItem playerItem;
        public int exp;
        public int level;
        public string introduction = "";
    }
    public class SavePlayerItemRequest
    {
        public PlayerItem playerItem;
    }

    public class Currency
    {
        public string coin = "0";
        public string gem = "0";
    }

    public class PlayerItem : ICloneable
    {
        public string hair = "";
        public string eyeBrow = "";
        public string eyes = "";
        public string skin = "";
        public string cap = "";
        public string glasses = "";
        public string torso = "";
        public string pants = "";
        public string shoes = "";
        public string gloves = "";
        public string backpack = "";

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public NetCodeModels.PlayerItem ToStruct()
        {
            return new NetCodeModels.PlayerItem()
            {
                hair = hair,
                eyeBrow = eyeBrow,
                eyes = eyes,
                skin = skin,
                cap = cap,
                glasses = glasses,
                torso = torso,
                pants = pants,
                shoes = shoes,
                gloves = gloves,
                backpack = backpack,
            };
        }
    }

    public class GetPlayerInventory
    {
        public PlayerInventory playerInventory;
    }
    public class PlayerInventory
    {
        public List<Item> inventory;
        public Currency currency;
    }
}

public class PlayfabManager : Singleton<PlayfabManager>
{
    private string _playfabID;
    private EntityKey _entityKey;

    public bool isLoginSucess;
    public uint apiAwait = 0;
    public Action<string> OnLoginSuccess;
    public bool IsAwait()
    {
        return apiAwait > 0;
    }
    public EntityKey entityKey { get { return _entityKey; } }
    public string PlayfabID
    {
        get { return _playfabID; }
        set { _playfabID = value; }
    }
    public void RegisterPlayfabUser(string userName, string email, string password, Action<object, object> successCallback = null, Action<object, object> errorCallback = null)
    {
        PlayFab.ClientModels.RegisterPlayFabUserRequest request = new PlayFab.ClientModels.RegisterPlayFabUserRequest()
        {
            Username = userName,
            Email = email,
            Password = password
        };
        PlayFabClientAPI.RegisterPlayFabUser(
            request,
            (result) =>
            {
                successCallback?.Invoke(this, result);
            },
            (error) =>
            {
                errorCallback?.Invoke(this, error);
            }
            );
    }
    public void LoginWithEmailAddress(string email, string passworld, Action<object, object> successCallback = null, Action<object, string> errorCallback = null)
    {
        PlayFab.ClientModels.LoginWithEmailAddressRequest request = new PlayFab.ClientModels.LoginWithEmailAddressRequest
        {
            Email = email,
            Password = passworld,
        };
        PlayFabClientAPI.LoginWithEmailAddress(
            request,
            (result) =>
            {
                _playfabID = result.PlayFabId;
                _entityKey = result.EntityToken.Entity;
                isLoginSucess = true;
                successCallback?.Invoke(this, result);
                OnLoginSuccess?.Invoke(result.PlayFabId);
            },
            (error) =>
            {
                errorCallback?.Invoke(this, error.ErrorMessage);
            }
        );
    }

    public void ExecuteFunction<T>(string functionName, object parameters, bool isWaiting = false, Action<object, T> successCallback = null, Action<object, string> failureCallback = null)
    {
        PlayFab.CloudScriptModels.ExecuteFunctionRequest request = new PlayFab.CloudScriptModels.ExecuteFunctionRequest()
        {
            FunctionName = functionName,
            FunctionParameter = parameters,
            GeneratePlayStreamEvent = false
        };
        if (isWaiting)
        {
            UIManager.Instance.ShowLoading();
            apiAwait++;
        }
        Debug.Log("Executefunction: " + functionName + "Parameter: " + JsonUtility.ToJson(parameters).ToString());
        PlayFabCloudScriptAPI.ExecuteFunction(
            request,
            (result) =>
            {
                DataResponse data = JsonConvert.DeserializeObject<DataResponse>(result.FunctionResult.ToString());
                if (!data.isError)
                {
                    Debug.Log($"Execute function {functionName} success");
                    Debug.Log($"Data response {result}");
                    //data.data is json string
                    successCallback?.Invoke(this, JsonConvert.DeserializeObject <T>(data.data.ToString()));
                    if (isWaiting)
                    {
                        apiAwait--;
                        if(!IsAwait())
                            UIManager.Instance.HideLoading();
                    }
                }
                else
                {
                    Debug.Log($"Execute function {functionName} failure with request : {parameters}");
                    failureCallback?.Invoke(this,data.errorMessage);
                    if (isWaiting)
                    {
                        apiAwait--;
                        if(!IsAwait())
                            UIManager.Instance.HideLoading();
                    }
                }
            },
        (error) =>
        {
            Debug.Log($"Execute function {functionName} failure with request : {parameters}");
            Debug.LogFormat("{0} | {1} | {2} | {3}", error.HttpCode, error.HttpStatus, error.ErrorDetails, error.CustomData);
            failureCallback?.Invoke(this, error.ErrorMessage);
            if (isWaiting) UIManager.Instance.HideLoading();
        });
    }
}
public class DataResponse
{
    public bool isError = false;
    public string message = "";
    public string errorMessage = "";
    public object data = null;
}

