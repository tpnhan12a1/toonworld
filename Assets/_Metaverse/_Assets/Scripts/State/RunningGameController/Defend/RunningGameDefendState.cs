using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDefend
{
    public bool IsTakeDamge();
}
public abstract class RunningGameDefendState : IState, IDefend
{
    protected RunningGameDefendStateMachine _runningGameDefendStateMachine;
    public abstract StateType GetStateType();

    public virtual bool IsTakeDamge()
    {
        return false;
    }

    public virtual void OnAnimationEnterEvent()
    {
      
    }

    public virtual void OnAnimationExitsEvent()
    {
        
    }

    public virtual void OnAnimationTransitionEvent()
    {
        
    }

    public virtual void OnEnterState()
    {
        Debug.Log($"Enter {GetStateType()}");
    }

    public virtual void OnExitState()
    {
        Debug.Log($"Exit {GetStateType()}");
    }

    public virtual void OnHandleInput()
    {
        
    }

    public virtual void OnPhysicsUpdate()
    {
        
    }

    public virtual void OnTriggerEnter(Collider collider)
    {
       
    }

    public virtual void OnTriggerExit(Collider collider)
    {
       
    }

    public virtual void OnUpdate()
    {
        
    }

    public virtual void SetStateMachine(StateMachine stateMachine)
    {
       _runningGameDefendStateMachine = (RunningGameDefendStateMachine)stateMachine;
    }
}
