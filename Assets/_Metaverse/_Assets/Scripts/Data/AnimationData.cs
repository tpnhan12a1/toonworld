using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AnimationData
{
    [Header("State Group Parameter Names")]
    [SerializeField] private string grouderParametername = "Grounded";
    [SerializeField] private string movingParametername = "Moving";
    [SerializeField] private string stoppingParametername = "Stopping";
    [SerializeField] private string landingParametername = "Landing";
    [SerializeField] private string airborneParametername = "Airborne";
    [SerializeField] private string motionParametername = "Motion";
    [SerializeField] private string waterParametername = "Water";
    [SerializeField] private string waterStoppingParametername = "IsWaterStopping";
   
    [Header("Grounded Parameter Name")]
    [SerializeField] private string idleParametername = "IsIdling";
    [SerializeField] private string idle2Parametername = "IsIdling_2";
    [SerializeField] private string dashParametername = "IsDashing";
    [SerializeField] private string walkParametername = "IsWalking";
    [SerializeField] private string runParametername = "IsRunning";
    [SerializeField] private string sprintParametername = "IsSprinting";
    [SerializeField] private string mediumStopParametername = "IsMediumStopping";
    [SerializeField] private string hardStopParametername = "IsHardStopping";
    [SerializeField] private string rollParametername = "IsRolling";
    [SerializeField] private string hardLandParametername = "IsHardLanding";

    [Header("Water Parameter Name")]
    [SerializeField] private string swimmingParametername = "IsSwimming";
    [SerializeField] private string waterDashingParametername = "IsWaterDashing";
    [SerializeField] private string lightStopSwimmingParametername = "IsLightStopSwimming";
    [SerializeField] private string waterUpParametername = "IsWaterUp";
    [SerializeField] private string waterIdlingParametername = "IsWaterIdling";
    [Header("Airborne Parameter Name")]
    [SerializeField] private string fallParametername = "IsFalling";

    [Header("Swipe Parameter Name")]
    [SerializeField] private string swipeParametername = "Swipe";
    [SerializeField] private string leftSwipeParametername = "IsLeftSwipe";
    [SerializeField] private string rightSwipeParametername = "IsRightSwipe";

    [SerializeField] private string fAirParametername = "F_Air";
    [SerializeField] private string statusParametername = "Status";
    [SerializeField] private string stumblingParametername = "Stumbling";

    [SerializeField] private string attackingParametername = "IsAttacking";

    [Header("Editorcharacter Parameter Name")]
    [SerializeField] private string intAnim = "IntAnim";

    public int GroundedParameterHash { get; private set; }
    public int MovingParameterHash { get; private set; }
    public int StoppingParameterHash { get; private set; }
    public int LandingParameterHash { get; private set; }
    public int AirborneParameterHash { get; private set; }
    public int MotionParameterHash { get; private set; }
    public int WaterParameterHash { get;private set; }
    public int WaterStoppingParameterHash { get; private set; }
    
    public int SwimmingParameterHash { get; private set; }
    public int WaterDashingParameterHash { get; private set; }
    public int LightStopSwimmingParameterHash { get; private set; }
    public int WaterUpParameterHash { get; private set; }
    public int WaterIdlingParameterHash { get; private set; }

    public int IdleParameterHash { get; private set; }
    public int DashParameterHash { get; private set; }
    public int WalkParameterHash { get; private set; }
    public int RunParameterHash { get; private set; }
    public int SprintParameterHash { get; private set; }
    public int MediumStopParameterHash { get; private set; }
    public int HardStopParameterHash { get; private set; }
    public int RollParameterHash { get; private set; }
    public int HardLandParameterHash { get; private set; }
    public int FallParameterHash { get; private set; }



    public int Idling2ParameterHash { get; private set; }
    public int SwipeParameterHash { get; private set; }
    public int RightSwipeParameterHash { get; private set; }
    public int LeftSwipeParameterHash { get; private set; }

    public int FAirParameterHash { get; private set; }
    public int StatusParameterHash { get; private set; }
    public int StumblingParameterHash { get; private set; }

    public int AttackingParameterHash { get; private set; }

    public int IntAnimParameterHash { get; private set; }

    public void Initialize()
    {
        GroundedParameterHash = Animator.StringToHash(grouderParametername);
        MovingParameterHash = Animator.StringToHash(movingParametername);
        StoppingParameterHash = Animator.StringToHash(stoppingParametername);
        LandingParameterHash = Animator.StringToHash(landingParametername);
        AirborneParameterHash = Animator.StringToHash(airborneParametername);
        MotionParameterHash = Animator.StringToHash(motionParametername);
        WaterParameterHash = Animator.StringToHash(waterParametername);
        WaterStoppingParameterHash = Animator.StringToHash(waterStoppingParametername);

        SwimmingParameterHash = Animator.StringToHash(swimmingParametername);
        WaterDashingParameterHash = Animator.StringToHash(waterDashingParametername);
        LightStopSwimmingParameterHash = Animator.StringToHash(lightStopSwimmingParametername);
        WaterUpParameterHash = Animator.StringToHash(waterUpParametername);
        WaterIdlingParameterHash = Animator.StringToHash (waterIdlingParametername);

        IdleParameterHash = Animator.StringToHash(idleParametername);
        DashParameterHash = Animator.StringToHash(dashParametername);
        WalkParameterHash = Animator.StringToHash(walkParametername);
        RunParameterHash = Animator.StringToHash(runParametername);
        SprintParameterHash = Animator.StringToHash(sprintParametername);
        MediumStopParameterHash = Animator.StringToHash(mediumStopParametername);
        HardStopParameterHash = Animator.StringToHash(hardStopParametername);
        RollParameterHash = Animator.StringToHash(rollParametername);
        HardLandParameterHash = Animator.StringToHash(hardLandParametername);

        FallParameterHash = Animator.StringToHash(fallParametername);



        Idling2ParameterHash = Animator.StringToHash(idle2Parametername);
        SwipeParameterHash = Animator.StringToHash(swipeParametername);
        LeftSwipeParameterHash = Animator.StringToHash(leftSwipeParametername);
        RightSwipeParameterHash =Animator.StringToHash(rightSwipeParametername);
        FAirParameterHash = Animator.StringToHash(fAirParametername);
        StatusParameterHash = Animator.StringToHash(statusParametername);
        StumblingParameterHash = Animator.StringToHash(stumblingParametername);

        AttackingParameterHash = Animator.StringToHash(attackingParametername);

        IntAnimParameterHash = Animator.StringToHash(intAnim);
    }
}
