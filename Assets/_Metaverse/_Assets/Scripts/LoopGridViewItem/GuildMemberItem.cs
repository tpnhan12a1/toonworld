using Imba.UI;
using Mono.CSharp.Linq;
using PlayFab.GroupsModels;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using toonworld;
using UnityEngine;
using UnityEngine.UI;

public class GuildMemberItem : LoopListViewItem2
{
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private TMP_Text _txtLevel;
    [SerializeField] private TMP_Text _txtStatusOn;
    [SerializeField] private TMP_Text _txtStatusOff;
    [SerializeField] private TMP_Text _txtRole;
    [SerializeField] private Image _imageAatar;
    [Header("Role Admin")]
    [SerializeField] private Button _btnAuth;
    [Header("Role Admin add sub admin")]
    [SerializeField] private Button _btnKickMember;

    [SerializeField] private Button _btnOut;

    [SerializeField] private Button _btnPoll;
    [SerializeField] private Button _btnUnPoll;
    [SerializeField] private ContentSizeFitter _contentSizeFitter;
    [SerializeField] private TMP_Text _txtYou;

    private GuildMember _guildMember;
    private GuildListViewController _guildListViewController;
    private Button _btn;
    public void Awake()
    {
        _btn = GetComponent<Button>();
        _btn.onClick.AddListener(ClickThis);
        _btnOut.onClick.AddListener(OutGuild);
        _btnAuth.onClick.AddListener(AuthenMember);
        _btnPoll.onClick.AddListener(PollMember);
        _btnUnPoll.onClick.AddListener(UnPollMember);
        _btnKickMember.onClick.AddListener(KickMember);
    }

    
    public void OnRefresh(GuildMember guildMember, GuildListViewController guildListViewController)
    {
        _guildMember = guildMember;
        _guildListViewController = guildListViewController;
        _txtDisplayName.text = _guildMember.displayName;
        _txtLevel.text = _guildMember.level.ToString();
        _txtRole.text = _guildMember.role;
        _imageAatar.sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, _guildMember.avatarUrl);
        SetStatus();
        SetHideShow();
        _contentSizeFitter.horizontalFit = ContentSizeFitter.FitMode.PreferredSize;
    }

    private void SetHideShow()
    {
        bool thisPlayerIsAdmin = PlayerDataManager.Instance.guildManager.IsAdmin(_guildMember.playfabId);
        _txtYou.SetActive(_guildMember.playfabId == PlayfabManager.Instance.PlayfabID);
        if (thisPlayerIsAdmin)
        {
            _btnKickMember.SetActive(false);
            _btnOut.SetActive(false);
            _btnPoll.SetActive(false);
            _btnUnPoll.SetActive(false);
            _btnAuth.SetActive(false);
            return;
        }
        bool thisPlayerIsSubAdmin = PlayerDataManager.Instance.guildManager.IsSubAdmin(_guildMember.playfabId);
        bool isAdmin = PlayerDataManager.Instance.guildManager.isAdmin;
        bool isSubAdmin = PlayerDataManager.Instance.guildManager.isSubAdmin;
        _btnKickMember.SetActive(isAdmin || isSubAdmin && !thisPlayerIsAdmin);
        _btnAuth.SetActive(isAdmin);
        _btnPoll.SetActive(!thisPlayerIsSubAdmin && isAdmin);
        _btnUnPoll.SetActive(thisPlayerIsSubAdmin && isAdmin);
        _btnOut.SetActive(PlayfabManager.Instance.PlayfabID == _guildMember.playfabId);
    }

    private void SetStatus()
    {
        bool status =  ClientManager.Instance.clientConecnted.ContainsKey(_guildMember.playfabId);
        _txtStatusOn.SetActive(status);
        _txtStatusOff.SetActive(!status);
    }
    #region main method
    private void ClickThis()
    {
        if (PlayfabManager.Instance.PlayfabID != _guildMember.playfabId)
        {
            UserProfile userProfile = new UserProfile()
            {
                displayName = _guildMember.displayName,
                level = _guildMember.level,
                playfabId = _guildMember.playfabId,
                playerItem = null
            };
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.OtherProfilePopup, userProfile);
        }
    }

    // done sync 
    private void OutGuild()
    {
        var outGuildMemeberRequest = new APIModels.GuildRequest()
        {
            requestType = PlayfabContant.GuildTag.RemoveMember,
            subFunc = new APIModels.SubFunc()
            {
                entityKeys = new EntityKey[] { _guildMember.entityKey },
                guild = PlayerDataManager.Instance.guildManager.guildEntity
            }

        }; 
        PlayfabManager.Instance.ExecuteFunction<EmptyResponse>
             (PlayfabContant.FunctionName.Guild,
             outGuildMemeberRequest,
             true,
             (obj, data) =>
             {
                 // Notify to server
                 string guildId = PlayerDataManager.Instance.guildManager.guildEntity.Id;
                 NetCodeModels.GuildSender guildSender = new NetCodeModels.GuildSender()
                 {
                     guildTag = PlayfabContant.GuildTag.Delete,
                     guildMemberInfor = _guildMember.ToGuildMemberInfor()
                 };
                 ServerManager.Instance.OutGuildServerRpc(guildId, guildSender);
                 Debug.Log("Out Guild");
                 PlayerDataManager.Instance.guildManager.OutGuild();
                 UIManager.Instance.PopupManager.HidePopup(UIPopupName.GuildPopup);
             },
             null);   

    }
    // done sync
    private void KickMember()
    {
        var removeMembersRequest = new APIModels.GuildRequest()
        {
            requestType = PlayfabContant.GuildTag.RemoveMember,
            subFunc = new APIModels.SubFunc()
            {
                entityKeys = new EntityKey[] { _guildMember.entityKey },
                guild = PlayerDataManager.Instance.guildManager.guildEntity
            }
        };
        PlayfabManager.Instance.ExecuteFunction<EmptyResponse>
             (PlayfabContant.FunctionName.Guild,
             removeMembersRequest,
             false,
             (obj, data) =>
             {
                 string guildId = PlayerDataManager.Instance.guildManager.guildEntity.Id;
                 NetCodeModels.GuildSender guildSender = new NetCodeModels.GuildSender()
                 {
                     guildTag = PlayfabContant.GuildTag.KickMember,
                     guildMemberInfor = _guildMember.ToGuildMemberInfor()
                 };
                 ServerManager.Instance.KickMemberServerRpc(guildId, guildSender);
             },
             null);
       
    }
    //done sync
    private void UnPollMember()
    {
        APIModels.GuildRequest request = new APIModels.GuildRequest()
        {
            requestType = PlayfabContant.GuildTag.UnPollMember,
            subFunc = new APIModels.GuildActionRequest()
            {
                guild = PlayerDataManager.Instance.guildManager.guildEntity,
                memberEntity = _guildMember.entityKey,
                originRoleId = _guildMember.roleId
                
            }
        };
        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
            PlayfabContant.FunctionName.Guild,
            request,
            true,
            (obj, data) =>
            {
                _guildMember.role = PlayfabContant.PlayerTag.Members;
                PlayerDataManager.Instance.guildManager.Edit(_guildMember);

                string guildId = PlayerDataManager.Instance.guildManager.guildEntity.Id;
                NetCodeModels.GuildSender guildSender = new NetCodeModels.GuildSender()
                {
                    guildTag = PlayfabContant.GuildTag.UnPollMember,
                    guildMemberInfor = _guildMember.ToGuildMemberInfor()
                };

                ServerManager.Instance.UnPollMemberServerRpc(guildId, guildSender);

                OnRefresh(_guildMember, _guildListViewController);
            },
            null);
    }

    //done sync
    private void PollMember()
    {
        APIModels.GuildRequest request = new APIModels.GuildRequest()
        {
            requestType = PlayfabContant.GuildTag.PollMember,
            subFunc = new APIModels.GuildActionRequest()
            {
                guild = PlayerDataManager.Instance.guildManager.guildEntity,
                memberEntity = _guildMember.entityKey,
                 originRoleId = _guildMember.roleId
            }
        };
        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
            PlayfabContant.FunctionName.Guild,
            request,
            true,
            (obj, data) =>
            {
                _guildMember.role = PlayfabContant.PlayerTag.SubAdmintrators;
                PlayerDataManager.Instance.guildManager.Edit(_guildMember);

                string guildId = PlayerDataManager.Instance.guildManager.guildEntity.Id;
                NetCodeModels.GuildSender guildSender = new NetCodeModels.GuildSender()
                {
                    guildTag = PlayfabContant.GuildTag.PollMember,
                    guildMemberInfor = _guildMember.ToGuildMemberInfor()
                };

                ServerManager.Instance.PollMemberServerRpc(guildId, guildSender);
                OnRefresh(_guildMember, _guildListViewController);
            },
            null);
    }
    //done sync
    private void AuthenMember()
    {
        APIModels.GuildRequest request = new APIModels.GuildRequest()
        {
            requestType = PlayfabContant.GuildTag.AuthenMember,
            subFunc = new APIModels.GuildActionRequest()
            {
                guild = PlayerDataManager.Instance.guildManager.guildEntity,
                memberEntity = _guildMember.entityKey,
                originRoleId = _guildMember.roleId
            }
        };
        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
            PlayfabContant.FunctionName.Guild,
            request,
            true,
            (obj, data) =>
            {
                _guildMember.role = PlayfabContant.PlayerTag.Admintrators;
                PlayerDataManager.Instance.guildManager.Edit(_guildMember);
                OnRefresh(_guildMember, _guildListViewController);

                string guildId = PlayerDataManager.Instance.guildManager.guildEntity.Id;
                NetCodeModels.GuildSender guildSender = new NetCodeModels.GuildSender()
                {
                    guildTag = PlayfabContant.GuildTag.AuthenMember,
                    guildMemberInfor = _guildMember.ToGuildMemberInfor(),
                    leaderPlayfabId = PlayfabManager.Instance.PlayfabID
                };

                ServerManager.Instance.AuthenMemberServerRpc(guildId, guildSender);
                // You is a member in case
                PlayerDataManager.Instance.guildManager.StepDown();
            },
            null);
    }

    #endregion
}
