﻿using UnityEngine;
using DG.Tweening;

public class MenuButton : MonoBehaviour
{
    public enum Mode { Appearance, Clothes }
    public Mode currentMode;

    public GameObject appearanceButtons;
    public GameObject clothesButtons;
    public GameObject saveButton;

    public CanvasGroup menuAppearance;
    public CanvasGroup menuClothes;

    public float transitionDuration = 0.2f;

    public void SetModeToAppearance()
    {
        SetMode(Mode.Appearance, appearanceButtons, menuAppearance, menuClothes);
    }

    public void SetModeToClothes()
    {
        SetMode(Mode.Clothes, clothesButtons, menuClothes, menuAppearance);
    }

    public void SetMode(Mode mode, GameObject targetButtons, CanvasGroup newMenu, CanvasGroup oldMenu)
    {
        currentMode = mode;
        DoTransition(targetButtons);
        DoMenuTransition(newMenu, oldMenu);
    }

    private void DoTransition(GameObject targetObject)
    {
        Sequence sequence = DOTween.Sequence();
        sequence.Append(targetObject.transform.DOScale(0, transitionDuration / 2));
        sequence.Append(targetObject.transform.DOScale(1, transitionDuration / 2));
    }

    private void DoMenuTransition(CanvasGroup newMenu, CanvasGroup oldMenu)
    {
        oldMenu.DOFade(0, transitionDuration).OnComplete(() => oldMenu.gameObject.SetActive(false));
        newMenu.gameObject.SetActive(true);
        newMenu.DOFade(1, transitionDuration);
    }
}
