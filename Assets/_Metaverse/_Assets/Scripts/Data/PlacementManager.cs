using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class Placement
{
    public string itemId;
    public string itemInstanceId;
    public APIModels.Vector3 position;
    public APIModels.Quaternion rotation;
    public Placement(string itemId, string itemInstanceId, Vector3 position, Quaternion rotation)
    {
        this.itemId = itemId;
        this.itemInstanceId = itemInstanceId;
        this.position = new APIModels.Vector3()
        {
            x = position.x,
            y = position.y,
            z = position.z
        };
        this.rotation = new APIModels.Quaternion()
        {
            x = rotation.x,
            y = rotation.y,
            z = rotation.z,
            w = rotation.w
        };
    }
    public override int GetHashCode()
    {
        return itemId.GetHashCode() ^ itemInstanceId.GetHashCode();
    }
    public override bool Equals(object obj)
    {
        Placement data = obj as Placement;
        if (data != null)
        {
            return data.itemInstanceId == this.itemInstanceId && data.itemId == this.itemId;
        }
        return false;
    }
}
[System.Serializable]
public class PlacementManager
{
    private Action<List<Placement>> OnPlacementPlaceDataChanged;

    private List<Placement> _placementDatas = new List<Placement>();
    public List<Placement> placementData { get { return _placementDatas; } }

    public void RegisterOnPlacementDataChanged(Action<List<Placement>> onPlacementPlaceDataChanged)
    {
        OnPlacementPlaceDataChanged += onPlacementPlaceDataChanged;
        onPlacementPlaceDataChanged?.Invoke(_placementDatas);
    }
    public void UnRegisterOnPlacementDataChanged(Action<List<Placement>> onPlacementPlaceDataChanged)
    {
        OnPlacementPlaceDataChanged -= onPlacementPlaceDataChanged;
    }
    public void InitPlacementData(List<Placement> placementDatas)
    {
        _placementDatas.Clear();
        if(placementDatas != null)
        {
            _placementDatas.AddRange(placementDatas);
        }
        OnPlacementPlaceDataChanged?.Invoke(placementDatas);
    }
    public void AddPlacementData(Placement placementData)
    {
        if (_placementDatas.Contains(placementData)) return;
        _placementDatas.Add(placementData);
        OnPlacementPlaceDataChanged?.Invoke(_placementDatas);
    }
    public void RemovePlacementDatas(List<Placement> placementDatas)
    {
       foreach(Placement placementData in placementDatas)
       {
            _placementDatas.Remove(placementData);
       }
        OnPlacementPlaceDataChanged?.Invoke(_placementDatas);
    }
    public void RemovePlacementData(Placement placementData)
    {
        _placementDatas.Remove(placementData);
        OnPlacementPlaceDataChanged?.Invoke(_placementDatas);
    }
    public bool Contain(string itemId, string itemInstanceId)
    {
        foreach(var  item in _placementDatas)
        {
            if (item.itemId == itemId && item.itemInstanceId == itemInstanceId) return true;
        }
        return false;
    }
    public PlacementManager()
    {

    }
}
