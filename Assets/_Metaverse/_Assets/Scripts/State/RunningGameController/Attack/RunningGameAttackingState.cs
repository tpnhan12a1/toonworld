using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RunningGameAttackingState : IState
{
    protected RunningGameAttackingStateMachine _runningGameStateMachine;
    public abstract StateType GetStateType();
   

    public virtual void OnAnimationEnterEvent()
    {
       
    }

    public virtual void OnAnimationExitsEvent()
    {
       
    }

    public virtual void OnAnimationTransitionEvent()
    {
       
    }

    public virtual void OnEnterState()
    {
        Debug.Log($"On Enter {GetStateType()}");
        _runningGameStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.AttackingParameterHash);
    }

    public virtual void OnExitState()
    {
        Debug.Log($"On Exited {GetStateType()}");
        _runningGameStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.AttackingParameterHash);
    }

    public virtual void OnHandleInput()
    {
       
    }

    public virtual void OnPhysicsUpdate()
    {
       
    }

    public virtual void OnTriggerEnter(Collider collider)
    {
       
    }

    public virtual void OnTriggerExit(Collider collider)
    {
       
    }

    public virtual void OnUpdate()
    {
        
    }

    public void SetStateMachine(StateMachine stateMachine)
    {
        _runningGameStateMachine = (RunningGameAttackingStateMachine)stateMachine;
    }
}
