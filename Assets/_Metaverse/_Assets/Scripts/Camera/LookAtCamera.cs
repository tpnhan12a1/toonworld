using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCamera : MonoBehaviour
{
    [SerializeField] private bool isReverse = false;
    public void LookAt()
    {
        LookAtMainCamera();
    }

    private void LookAtMainCamera()
    {
        if (isReverse)
        {
            Vector3 lookPoint = transform.position + (transform.position - CameraManager.Instance.mainCamera.transform.position);
            transform.LookAt(lookPoint);
        }
        else
        {
            transform.LookAt(CameraManager.Instance.mainCamera.transform.position);
        }
    }

    public void LateUpdate()
    {
        LookAtMainCamera();
    }
}
