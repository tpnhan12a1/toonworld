using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameSkillGridViewController : LoopGridView
{
    [SerializeField]
    PlayerRunningGameController _playerRunningGameController;
    List<RunningGameDataSkill> _skillList;
   public PlayerRunningGameController playerRunningGameController
   {
        get { return _playerRunningGameController; }
        set { _playerRunningGameController = value; }
   }
    public void OnInit()
    {
         _skillList = new List<RunningGameDataSkill>();
        InitGridView(_skillList.Count, OnGetRunningGameDataSkillByRowColumn);
    }

    public void OnShow(PlayerRunningGameController playerRunningGameController)
    {
        _playerRunningGameController = playerRunningGameController;
        _playerRunningGameController.runningGameReusableData.RegisterOnSkillDataChange(OnSkilDataChanged);
    }
    public void OnHide()
    {
       _playerRunningGameController.runningGameReusableData.UnregisterOnSkillDataChange(OnSkilDataChanged);
    }

  
    private LoopGridViewItem OnGetRunningGameDataSkillByRowColumn(LoopGridView gridView, int index, int row, int column)
    {
        if (_skillList.Count == 0) return null;
        RunningGameSkillItem runningGameSkillItem = (RunningGameSkillItem)gridView.NewListViewItem("RunningGameSkillItem");
        runningGameSkillItem.OnRefresh(_skillList[index], this);
        return runningGameSkillItem;
    }

    public void Refresh()
    {
        RefreshAllShownItem();
    }
    private void OnSkilDataChanged(List<RunningGameDataSkill> skillDatas)
    {
        _skillList = skillDatas;
        SetListItemCount(_skillList.Count);
        RefreshAllShownItem();
    }

}
