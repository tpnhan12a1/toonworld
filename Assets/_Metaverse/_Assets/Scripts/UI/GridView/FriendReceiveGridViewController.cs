using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ToonWorld;
public class FriendReceiveGridViewController : LoopGridView, IGroupTab
{
    private List<Friend> _friendList;
    public void OnInit()
    {
        _friendList = new List<Friend>();
        InitGridView(_friendList.Count, OnGetFriendItemByRowColum);
    }

    private LoopGridViewItem OnGetFriendItemByRowColum(LoopGridView gridView, int index, int row, int column)
    {
        if (_friendList.Count == 0) return null;
        FriendReceiveItem friendItem = (FriendReceiveItem)gridView.NewListViewItem("FriendReceiveItem");
        friendItem.OnRefresh(_friendList[index], this);
        return friendItem;
    }

    public void Select()
    {
        gameObject.SetActive(true);
        PlayerDataManager.Instance.friendManager.RegisterOnListFriendChanged(OnListFriendChanged);
    }
    public void DesSelect()
    {
        gameObject.SetActive(false);
        PlayerDataManager.Instance.friendManager.UnRegisterOnListFriendChanged(OnListFriendChanged);
    }

    private void OnListFriendChanged(List<Friend> friends)
    {
        _friendList = friends.Where(x => x.friendTags.Contains(PlayfabContant.FriendTag.Received)).OrderByDescending(x => x.level).ThenBy(x => x.displayName).ToList();
        RefreshItem();
    }

    public void RefreshItem()
    {
        SetListItemCount(_friendList.Count);
        RefreshAllShownItem();
    }
}
