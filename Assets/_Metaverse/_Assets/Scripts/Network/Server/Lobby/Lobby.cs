using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using UnityEngine;
using System;
using System.Linq.Expressions;
using System.Text;
using UnityEngine.UIElements;

public class Lobby
{
    public PlayerManager<Player> playerManager;
    public int[] slots;
    public int id;
    public Player owner;
    public string passworld = "";
    public uint maxPlayer = 10;
    public GamePlayType gamePlayType = GamePlayType.None;
    public LobbyManager<Lobby> lobbyManager;
    public List<NetworkObject> networkObjects;

    public Lobby(LobbyManager<Lobby> lobbyManager, Player player, GamePlayType gamePlayType, string passworld = "")
    {
        this.lobbyManager = lobbyManager;
        this.networkObjects = new List<NetworkObject>();
        this.playerManager = new PlayerManager<Player>();
        this.owner = player;
        this.gamePlayType = gamePlayType;
        this.passworld = passworld;
        Debug.Log($"Lobby with owner {owner.playerData.displayName} has add");
        switch(gamePlayType)
        {
            case GamePlayType.None:
                break;
            case GamePlayType.Running:
                maxPlayer = 3;
                break;
        }

        slots = new int[maxPlayer];
        for (int i=0;i<slots.Length; i++)
        {
            slots[i] = -1;
        }
    }
   
    public void PlayerJoinLobby(Player player)
    {
        if (playerManager.GetPlayerCount() >= maxPlayer) return;

        int slotIndex = Array.IndexOf(slots, -1);
        slots[slotIndex] = (int)player.GetClientId();

        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = new ulong[] { player.GetClientId() }
            }
        };

        ulong[] targetClientIds = playerManager.GetClientIds().ToArray();
        playerManager.AddPlayer(player); 
        player.lobby = this;
        NetCodeModels.JoinLobbyResponse joinLobbyResponse = new NetCodeModels.JoinLobbyResponse();
        joinLobbyResponse.playerDatas = playerManager.GetPlayerDatas();
        joinLobbyResponse.playerData = player.playerData;
        joinLobbyResponse.gameRoomLobby = this.ToStruct();
        joinLobbyResponse.slotIndex = slotIndex;
        ShowNetworkObjectToPlayer(player);
        SceneDataSO sceneDataSO = Imba.Utils.ResourceManager.Instance.GetResourceByName<SceneDataSO>(
            new StringBuilder().Append("SceneData/").Append(SceneName.SceneGameLobby).ToString());

        NetworkObject renderObject = ServerManager.Instance.spawnManager.SpawnObjectInLobby(
            ServerManager.Instance.spawnManager.renderNetworkCharacter,
            this,
            sceneDataSO.GetPositionByIndex(slotIndex),
            Quaternion.identity, player);
        networkObjects.Add(renderObject);
        ClientManager.Instance.ResponseJoinLobbyRoomClientRpc(joinLobbyResponse, clientRpcParams);
        if (targetClientIds.Length > 0)
        {
            clientRpcParams = new ClientRpcParams()
            {
                Send = new ClientRpcSendParams()
                {
                    TargetClientIds = targetClientIds
                }
            };
            ClientManager.Instance.PlayerJoinLobbyClientRpc(player.playerData, clientRpcParams);
        }
    }
    public void LeaveOrCloseLobby(Player player)
    {
        if (player.lobby.owner == player)
        {
            player.lobby.CloseLobby();
            return;
        }
        if (player.lobby != null)
        {
            PlayerLeaveLobby(player);
        }
    }
    public void CloseLobby()
    {
        playerManager.ForEach(x => x.lobby = null);
        networkObjects.ForEach(networkObject =>
        {
            networkObject.Despawn();
        });
        playerManager = null;
        networkObjects = null;
        lobbyManager.Remove(gamePlayType, this);
    }
    public void PlayerLeaveLobby(Player player)
    {
        int slotIndex = Array.IndexOf(slots, (int)player.GetClientId());
        slots[slotIndex] = -1;

        HideNetworkObjectToPlayer(player);
        playerManager.RemovePlayer(player);
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = new ulong[] { player.GetClientId() }
            }
        };
                
        ClientManager.Instance.ResponsePlayerLeaveLobbyClientRpc(clientRpcParams);
        player.lobby = null;

        ulong[] targetClientIds = playerManager.GetClientIds().ToArray();
        clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = targetClientIds
            }
        };
        ClientManager.Instance.PlayerLeaveLobbyClientRpc(player.playerData.playfabId, clientRpcParams);

        if(playerManager.GetPlayerCount()<=0)
        {
            ServerManager.Instance.lobbyManager.Remove(gamePlayType, this);
        }
    }
   
    public void HideNetworkObjectToPlayer(Player player)
    {
        NetworkObject playerOwnerNetworkObject = null;
        networkObjects.ForEach(x =>
        {
            if(x.OwnerClientId == player.GetClientId())
            {
                playerOwnerNetworkObject = x;
            }
            else
            {
                x.NetworkHide(player.GetClientId());
            }
        });

        if(playerOwnerNetworkObject != null)
        {
            ServerManager.Instance.spawnManager.DespawnObject(playerOwnerNetworkObject);
            networkObjects.Remove(playerOwnerNetworkObject);
        }
    }
    public void ShowNetworkObjectToPlayer(Player player)
    {
        networkObjects.ForEach(x =>
        {
           x.NetworkShow(player.GetClientId());
        });
    }
    public Room StartGame()
    {
        Room room = null;
        switch(gamePlayType)
        {
            case GamePlayType.Running:
                room = new GameRoom(ServerManager.Instance.roomManager,SceneName.SceneRunningGame,slots,owner.playfabId,true,gamePlayType);
                JoinRoom(room);
                break;
        }
        CloseLobby();
        return room;
    }
    private void JoinRoom(Room room)
    {
        playerManager.ForEach(x =>
        {
            ServerManager.Instance.ChangeRoom(x, x.GetRoom(), room);
        });
    }
    public NetCodeModels.Lobby ToStruct()
    {
        return new NetCodeModels.Lobby()
        {
            ownerPlayfabId = owner.playfabId,
            ownerDisplayName = owner.playerData.displayName,
            passworld = passworld,
            gamePlayType = gamePlayType,
            avatarUrl = owner.playerData.avatarUrl,
            id = id,
            maxPlayer = maxPlayer
        };
    }

}
