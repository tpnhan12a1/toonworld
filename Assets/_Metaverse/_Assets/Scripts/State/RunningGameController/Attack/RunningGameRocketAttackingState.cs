using System.Collections;
using System.Collections.Generic;
using System.Text;
using Unity.VisualScripting;
using UnityEngine;

public class RunningGameRocketAttackingState : RunningGameAttackingState
{
    public override StateType GetStateType()
    {
        return StateType.RunningGameRocketAttackingState;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.LockSkill(GiftName.Bullet);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.UnLockSkill(GiftName.Bullet);
    }
    public override void OnAnimationTransitionEvent()
    {
        base.OnAnimationTransitionEvent();
        SceneRunningGameManager.Instance.runningGamePlayNetworkController.SpawnRocketServerRpc(_runningGameStateMachine.playerRunningGameController.NetworkObject, RocketName.BaseRocket);
        
    }
    public override void OnAnimationExitsEvent()
    {
        base.OnAnimationExitsEvent();
        _runningGameStateMachine.ChangeState(_runningGameStateMachine.states[StateType.RunningGameEmptyAttackingState]);
    }
}
