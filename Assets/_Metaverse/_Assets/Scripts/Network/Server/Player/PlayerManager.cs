using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using UnityEngine;

public class PlayerManager<T> : IPlayerManager<T> where T: Player
{
    Dictionary<ulong, T> players;
    public PlayerManager() 
    {
        players = new Dictionary<ulong, T>();
    }
    public List<NetCodeModels.PlayerData> GetPlayerDatas()
    {
        List<NetCodeModels.PlayerData> result = new List<NetCodeModels.PlayerData>();
        foreach(var kvp  in players)
        {
            result.Add(kvp.Value.playerData);
        }
        return result;
    }
    public List<ulong> GetClientIds()
    {
        return players.Keys.ToList();
    }

    public void AddPlayer(T p)
    {
        Debug.Log($"Player with clientId {p.GetClientId()} has added");
       players.Add(p.GetClientId(), p);
    }

    public void AddPlayer(ulong clientId, T p)
    {
        Debug.Log($"Player with clientId {p.GetClientId()} has added");
        players.Add(clientId, p);
    }

    public void AddPlayers(ICollection<T> list)
    {
        foreach(var item in list)
        {
            Debug.Log($"Player with clientId {item.GetClientId()} has added");
            players.Add(item.GetClientId(), item);
        }
    }

    public bool ContainPlayer(ulong clientId)
    {
        return players.ContainsKey(clientId);
    }

    public bool ContainPlayer(T p)
    {
        return players.ContainsKey(p.GetClientId());
    }

    public List<T> FilterPlayers(IPlayerManager<T>.Predicate predicate)
    {
        return players.Values.Where(x => predicate(x)).ToList();
    }

    public void ForEach(IPlayerManager<T>.Consumer action)
    {
        players.Values.ToList().ForEach(x => action(x));
    }

    public List<T> GetAllPlayer()
    {
       return players.Values.ToList();
    }

    public T GetFirstPlayer()
    {
       return players.Values.FirstOrDefault();
    }

    public T GetPlayer(ulong clientId)
    {
        players.TryGetValue(clientId, out var player);
        return player;
    }

    public int GetPlayerCount()
    {
        return players.Count;
    }

    public bool IsEmpty()
    {
        return (players.Count == 0);
    }

    public void RemovePlayers(ICollection<T> list)
    {
       foreach(var item in list)
        {
            Debug.Log($"Player with clientId {item.GetClientId()} has removed");
            players.Remove(item.GetClientId());
        }
    }

    public void RemovePlayer(T p)
    {
        Debug.Log($"Player with clientId {p.GetClientId()} has removed");
        players.Remove(p.GetClientId());
    }

    public void RemovePlayer(ulong clientId)
    {
        players.Remove(clientId);
    }
}
