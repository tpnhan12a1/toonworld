using Imba.Utils;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class InteractionIconItem : LoopGridViewItem
{
    [SerializeField] private Image _icon;
    private EmotionGridViewController _emotionGridViewController;
    private EmotionSO _emotionSO;
    [SerializeField]
    private Button _button;
    public void Awake()
    {
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        NetworkObject playerObject = NetworkManager.Singleton.LocalClient.PlayerObject;
        if (playerObject != null)
        {
            ServerManager.Instance.SendEmotionServerRpc(playerObject, _emotionSO.name);
        }
       
    }

    public void OnRefresh(EmotionGridViewController emotionGridViewController,EmotionSO emotionSO)
    {
        _emotionGridViewController = emotionGridViewController;
        _emotionSO = emotionSO;

        Sprite sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Emotion, _emotionSO.spriteName);
        if(sprite != null)
        {
            _icon.sprite = sprite;
        }
    }
}
