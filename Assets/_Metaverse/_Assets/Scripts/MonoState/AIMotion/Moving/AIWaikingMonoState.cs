﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

internal class AIWaikingMonoState : AIMovingMonoState
{
    public override void OnEnterState()
    {
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        residentStateMachine.residentController.navMeshAgent.speed = residentStateMachine.residentController.residentSO.walkSpeed;
        base.OnEnterState();
        StartAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsWalkingParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsWalkingParameterHash);
    }
    public override StateType GetStateType()
    {
        return StateType.AIWalkingMonoState;
    }
}