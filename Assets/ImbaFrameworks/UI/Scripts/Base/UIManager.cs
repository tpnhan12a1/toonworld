﻿
using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using Imba.Utils;
using TMPro;

namespace Imba.UI
{
    /// <summary>
    /// UIManager: Manage all UI element include Popups, Views, Notices, Alert ...
    /// </summary>
	public class UIManager : ManualSingletonMono<UIManager>
    {
		public Camera UICamera;
		
		public UIPopupManager PopupManager;
		public RectTransform LoadingObject;
		public TMP_Text TxtLoadingObjectTitle;
        public UIAlertManager AlertManager;

		public string defaultTitleLoading = "";
        //public LevelManager LevelSystemManager;
        //public RectTransform PopupContainer;
        //public UITooltip tooltip;
        //public GameObject objBugReport;
        
		public bool IsShowingLoading {
			get {
				if (LoadingObject) {
					return LoadingObject.gameObject.activeSelf;
				}
				return false;
			}
		}


		public override void Awake()
		{
			base.Awake();
			
			if (!PopupManager)
			{
				PopupManager = GetComponentInChildren<UIPopupManager> ();
			}
			if(!AlertManager)
			{
                AlertManager = GetComponentInChildren<UIAlertManager>();
			}

			if (LoadingObject) {
				LoadingObject.gameObject.SetActive (false);
			}

			
#if DISABLE_LOG
			Debug.unityLogger.filterLogType = LogType.Error;
#endif
		}
		
		void Start()
		{
			SceneManager.sceneLoaded += OnSceneLoaded;
		}
		
		
		void OnSceneLoaded(Scene scene, LoadSceneMode loadSceneMode)
		{
	
		}
		
		public void ShowLoading(string title = "", float timeToHide = 0)
		{
			if(string.IsNullOrEmpty(title))
			{
                TxtLoadingObjectTitle.text = defaultTitleLoading;
			}
			else
			{
                TxtLoadingObjectTitle.text = title;
            }

            LoadingObject.gameObject.SetActive(true);
            if (timeToHide <= 0) CancelInvoke("HideLoadingCallback");
            else Invoke("HideLoadingCallback", timeToHide);
        }

	
		public void HideLoading()
		{
//			Debug.Log ("HideLoading");
			CancelInvoke("HideLoadingCallback");
			LoadingObject.gameObject.SetActive (false);
		}
		
		void HideLoadingCallback()
		{
			LoadingObject.gameObject.SetActive(false);
		}
	    
	    public bool ShowDebugLog = true;

	    public static void DebugLog<T>(string message, T com)
	    {
		    #if UNITY_EDITOR
		    if (!Instance || !Instance.ShowDebugLog) return;

            string msg = string.Format("[{0}] {1}", com != null ? com.GetType().ToString() : "", message);
            Debug.Log(string.Format("<color=blue>[UIManager][{0}] {1}</color>", com.GetType(), message));
            #endif
	    }
    }
}

