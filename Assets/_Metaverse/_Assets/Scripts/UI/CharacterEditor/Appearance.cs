[System.Serializable]
public class Appearance
{
    public string hair;
    public string eyeBrow;
    public string eyes;
    public string skin;
}