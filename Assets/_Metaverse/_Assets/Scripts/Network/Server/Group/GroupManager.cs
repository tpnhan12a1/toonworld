using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GroupManager<G>:IGroupManager<G> where G :Group
{
    private Dictionary<string, G> _groups;
    public GroupManager() 
    {
        _groups = new Dictionary<string, G>();
    }
    public bool GetGroup(string groudId,out G group)
    {
        if(_groups.TryGetValue(groudId,out group))
        {
            return true;
        }return false;
    }
    public void AddGroup(G group)
    {
        _groups.TryAdd(group.groupId, group);   
    }

    public void AddGroups(IEnumerable<G> groups)
    {
        groups.ToList().ForEach(x => {  AddGroup(x); });
    }

    public void RemoveAllGroups()
    {
       _groups.Clear();
    }

    public void RemoveGroup(G group)
    {
        _groups.Remove(group.groupId);
    }
}
