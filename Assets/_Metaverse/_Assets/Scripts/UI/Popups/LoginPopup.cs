using Imba.UI;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Unity.Netcode;
using System.Collections.Generic;
using System.Linq;

public class LoginPopup : UIPopup
{
    [SerializeField] private TMP_InputField _inputFieldEmail;
    [SerializeField] private TMP_InputField _inputFieldPassword;
    [SerializeField] private Toggle _toggleRemeberMe;
    [SerializeField] private Button _btnSignUp;
    [SerializeField] private Button _btnLogin;
    [SerializeField] private Button _btnClose;

    private string email;
    private string password;

    protected override void OnInit()
    {
        base.OnInit();
        _btnLogin.onClick.AddListener(OnClickLogin);
        _btnSignUp.onClick.AddListener(OnClickSignUp);
        _btnClose.onClick.AddListener(ClosePopup);
        _toggleRemeberMe.onValueChanged.AddListener(OnValueChanged);
    }

    private void OnValueChanged(bool status)
    {
        PlayerPrefsManager.Instance.SaveRememberAcount(status);
    }

    protected override void OnShowing()
    {
        base.OnShowing();
       
        if(PlayerPrefsManager.Instance.TryGetUserLogin(out string email, out string password))
        {
            _inputFieldEmail.text = email;
            _inputFieldPassword.text = password;
        }
        bool status;
        PlayerPrefsManager.Instance.TryGetRememberAccountStatus(out status);
        _toggleRemeberMe.Set(status);
    }
    protected override void OnShown()
    {
        base.OnShown();
    }
    protected override void OnHiding()
    {
        base.OnHiding();
    }

    protected override void OnHidden()
    {
        base.OnHidden();
    }
    private void ClosePopup()
    {
        Hide();
    }


    private void OnClickLogin()
    {
        email = _inputFieldEmail.text;
        password = _inputFieldPassword.text;
        if(String.IsNullOrEmpty(email) )
        {
            UIManager.Instance.AlertManager.ShowAlertMessage(AlertType.Normal, "Please fill in email");
            return;
        }
        if(String.IsNullOrEmpty(password) )
        {
            UIManager.Instance.AlertManager.ShowAlertMessage(AlertType.Normal, "Please fill in password");
            return;
        }
        Login(email, password);
    }

    private void OnClickSignUp()
    {
        Hide();
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.SignupPopup);

    }
    private void Login(string email, string password)
    {
        UIManager.Instance.ShowLoading();
        PlayfabManager.Instance.LoginWithEmailAddress(email, password, OnLoginSuccess, OnLoginFailure);
    }

    private void OnLoginFailure(object arg1, string error)
    {
        UIManager.Instance.HideLoading();
        UIManager.Instance.AlertManager.ShowAlertMessage(error);
    }

    private void OnLoginSuccess(object arg1, object arg2)
    {
        UIManager.Instance.HideLoading();

        RememberMe(email, password);
        object loginRequest = new object();
        PlayfabManager.Instance.ExecuteFunction<APIModels.OnLoginResponse>(PlayfabContant.FunctionName.OnLogin,
            loginRequest,
            true,
            OnLoadDataLoginSuccess,
            OnLoadDataLoginFailure);
    }

    private void RememberMe(string email, string password)
    {
        if(_toggleRemeberMe.isOn)
        {
            PlayerPrefsManager.Instance.SaveUserLogin(email, password);
        }
        else
        {
            PlayerPrefsManager.Instance.RemoveUser();
        }
    }

    private void OnLoadDataLoginFailure(object arg1, string error)
    {
        UIManager.Instance.AlertManager.ShowAlertMessage(error);
    }

    private void OnLoadDataLoginSuccess(object arg1, APIModels.OnLoginResponse data)
    {
        Hide();
        PlayerDataManager.Instance.SetDataLogin(data);

        APIModels.CommonRequest request = new APIModels.CommonRequest();
        PlayfabManager.Instance.ExecuteFunction<APIModels.GetFriendsResponse>(PlayfabContant.FunctionName.GetFriends, request, true, OnGetFriendSuccess, OnGetFriendFalure);
        PlayfabManager.Instance.ExecuteFunction<APIModels.GetPlayerInventory>(PlayfabContant.FunctionName.GetPlayerInventory, request, true, OnGetPlayerInventorySuccess, OnGetInventoryFailure);
        APIModels.GetPlacementsRequest getPlacementsRequest = new APIModels.GetPlacementsRequest() { playfabId = PlayfabManager.Instance.PlayfabID };
        PlayfabManager.Instance.ExecuteFunction<APIModels.PlacementData>(PlayfabContant.FunctionName.GetPlayerPlacement, getPlacementsRequest, true, OnGetPlacementSuccess, OnGetPlacementFailue);
        LobbyPopup lobbyPopup = (LobbyPopup)UIManager.Instance.PopupManager.GetPopup(UIPopupName.LobbyPopup);

        if(lobbyPopup != null)
        {
            lobbyPopup.SetLogin();
        }
    }

    private void OnGetPlacementFailue(object arg1, string arg2)
    {
        
    }
    private void OnGetPlacementSuccess(object arg1, APIModels.PlacementData data)
    {
        PlayerDataManager.Instance.placementManager.InitPlacementData(data.placementData.ToList());
    }

    private void OnGetPlayerInventorySuccess(object arg1, APIModels.GetPlayerInventory data)
    {
        PlayerDataManager.Instance.inventoryManager.InitListItems(data.playerInventory.inventory);
        PlayerDataManager.Instance.currency = data.playerInventory.currency;
    }
    private void OnGetInventoryFailure(object arg1, string arg2)
    {
      
    }
    private void OnGetFriendFalure(object arg1, string error)
    {

    }

    private void OnGetFriendSuccess(object arg1, APIModels.GetFriendsResponse friendsResponse)
    {
        PlayerDataManager.Instance.friendManager.InitListFriend(friendsResponse.friends);
    }

    private void OnGetListItemsSuccess(object arg1, APIModels.GetListItemsResponse data)
    {
        PlayerDataManager.Instance.inventoryManager.InitListItems(data.items);
    }

    private void OnGetListItemsFailure(object arg1, string error)
    {

    }
}
