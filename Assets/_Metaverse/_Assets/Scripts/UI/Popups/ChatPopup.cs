using Firebase.Database;
using Imba.UI;
using Newtonsoft.Json;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.UI;
using System.Text;
using Mono.CSharp;
using DG.Tweening;

public class ChatPopup : UIPopup
{
    [SerializeField] private LoopGridView _friendChatGridView;
    [SerializeField] private LoopListView2 _friendChatsContentListView;
    [SerializeField] private TabController _tabController;
    [SerializeField] private Button _btnSendMessage;
    [SerializeField] private TMP_InputField _ipfTextContent;
    [SerializeField] private float _durationAnim = 0.2f;
    [SerializeField] private RectTransform _rectransfrom;
    [SerializeField] private Button _btnClosePopup;
    private List<FriendChatInfo> _friendChatInfos;
    private FriendChatInfo _friendChatInfoSelected;

    private List<Message> _messages;
    private DatabaseReference _friendChatContentReference;
    private DatabaseReference _friendChatReference;
   
    private Vector3 _anchorPointPos = Vector3.zero;
    protected override void OnInit()
    {
        base.OnInit();
       
        _anchorPointPos = _rectransfrom.anchoredPosition;
        _rectransfrom.anchoredPosition = new Vector3(-_anchorPointPos.x, _anchorPointPos.y, 0);
        _btnClosePopup.onClick.AddListener(ClosePopup);
        _friendChatInfos = new List<FriendChatInfo>();
        _friendChatGridView.InitGridView(_friendChatInfos.Count, OnGetFriendChatInfoByRowColumn);

        _messages =  new List<Message>();
        _friendChatsContentListView.InitListView(_messages.Count, OnGetMessageByIndex);
        _btnSendMessage.onClick.AddListener(SendMessage);
    }

    protected override async void OnShowing()
    {
        base.OnShowing();
        _rectransfrom.DOAnchorPosX(_anchorPointPos.x, _durationAnim).SetEase(Ease.OutExpo);
        _friendChatInfos.Clear();
        FriendChatInfo friendChatInfo = (FriendChatInfo)Parameter;
        if (friendChatInfo != null)
        {
            if(!_friendChatInfos.Contains(friendChatInfo))
                _friendChatInfos.Add(friendChatInfo);
            Select(friendChatInfo);
        }
        else
        {
            DataSnapshot dataSnapshot = await FirebaseManager.Instance.databaseReference.Child(PlayfabManager.Instance.PlayfabID).Child("chats").GetValueAsync();
            foreach (var item in dataSnapshot.Children)
            {
                Message lastMessage = JsonConvert.DeserializeObject<Message>(item.GetRawJsonValue());
                string displayName = lastMessage.isSend ? lastMessage.receiver : lastMessage.sender;
                FriendChatInfo f = new FriendChatInfo()
                {
                    friendPlayfabId = item.Key,
                    displayName = displayName,
                };
                Select(f);
                break;
            }
        }
        _friendChatReference = FirebaseManager.Instance.databaseReference.Child(PlayfabManager.Instance.PlayfabID).Child("chats");
        _friendChatReference.ChildAdded += OnNewChatAdded;

    }
    protected override void OnHiding()
    {
        base.OnHiding();
        if (_friendChatReference != null)
        {
            _friendChatReference.ChildAdded -= OnNewChatAdded;
        }
        if(_friendChatContentReference != null)
            _friendChatContentReference.ChildAdded -= OnMessageContentAdded;
        _friendChatContentReference = null;

        _friendChatReference = null;
        _friendChatInfoSelected = null;
        Parameter = null;
    }

    private void OnNewChatAdded(object sender, ChildChangedEventArgs e)
    {
        foreach(var snapshot in e.Snapshot.Children.Reverse())
        {
            Message lastMessage = JsonConvert.DeserializeObject<Message>(snapshot.GetRawJsonValue());
            string displayName = lastMessage.isSend ? lastMessage.receiver : lastMessage.sender;
            FriendChatInfo friendChatInfo = new FriendChatInfo()
            {
                friendPlayfabId = e.Snapshot.Key,
                displayName = displayName,
            };
            if(!_friendChatInfos.Contains(friendChatInfo))
            {
                _friendChatInfos.Add(friendChatInfo);
                RefreshGridView();
            }
            break;
        }
    }
    private void SendMessage()
    {
        if (!String.IsNullOrEmpty(_ipfTextContent.text))
        {

            Message message = new Message()
            {
                receiver = _friendChatInfoSelected.displayName,
                isSend = true,
                content = _ipfTextContent.text,
                sender = PlayerDataManager.Instance.displayName
            };

            FirebaseManager.Instance.SendMessage(_friendChatInfoSelected.friendPlayfabId, message);

            NetCodeModels.ChatContent chatContent = new NetCodeModels.ChatContent()
            {
                content = _ipfTextContent.text,
                displayName = PlayerDataManager.Instance.displayName,
                type = TypeChat.Private
            };
            ServerManager.Instance.ChatServerRpc(PlayfabManager.Instance.PlayfabID, _friendChatInfoSelected.friendPlayfabId, chatContent);
            _ipfTextContent.text = string.Empty;
        }
    }

    private void ClosePopup()
    {
        _rectransfrom.DOAnchorPosX(-_anchorPointPos.x, _durationAnim).SetEase(Ease.OutExpo).OnComplete(() =>
        {
            Hide();
        });
        
    }
    private LoopListViewItem2 OnGetMessageByIndex(LoopListView2 listView, int index)
    {
        if (_messages.Count == 0) return null;
        Message message = _messages[index];
        ChatContentItem item;
        if(message.isSend)
        {
            item = (ChatContentItem)listView.NewListViewItem("ChatContentItem");
        }
        else
        {
            item = (ChatContentItem)listView.NewListViewItem("OtherChatContentItem");
        }
        item.OnRefresh(message, this);
        return item;
    }

    private LoopGridViewItem OnGetFriendChatInfoByRowColumn(LoopGridView gridView, int index, int row, int column)
    {
        if (_friendChatInfos == null) return null;

        FriendChatItem friendChatItem = (FriendChatItem)gridView.NewListViewItem("FriendChatItem");
        friendChatItem.OnRefresh(_friendChatInfos[index], this, _tabController);
        if (_friendChatInfos[index].friendPlayfabId == _friendChatInfoSelected.friendPlayfabId)
        {
            friendChatItem.OnSelect();
        }
        return friendChatItem;
    }
    private void RefreshGridView()
    {
        _friendChatGridView.SetListItemCount(_friendChatInfos.Count);
        _friendChatGridView.RefreshAllShownItem();
        if(_friendChatInfoSelected != null)
        {
            int index = _friendChatInfos.IndexOf(_friendChatInfoSelected);
            _friendChatGridView.MovePanelToItemByIndex(index);
        }
    }

    private void RefreshMessage()
    {
        _friendChatsContentListView.SetListItemCount(_messages.Count);
        _friendChatsContentListView.RefreshAllShownItem();
        _friendChatsContentListView.MovePanelToItemIndex(_messages.Count - 1, 0f);
    }

    internal void Select(FriendChatInfo friendChatInfo)
    {
        _messages.Clear();
        if(_friendChatContentReference != null)
        {
            _friendChatContentReference.ChildAdded -= OnMessageContentAdded;
        }

        _friendChatInfoSelected = friendChatInfo;
        _friendChatContentReference = FirebaseManager.Instance
            .databaseReference
            .Child(PlayfabManager.Instance.PlayfabID)
            .Child("chats")
            .Child(_friendChatInfoSelected.friendPlayfabId);

        _friendChatContentReference.ChildAdded += OnMessageContentAdded;

        if (_friendChatInfoSelected != null)
        {
            int index = _friendChatInfos.IndexOf(_friendChatInfoSelected);
            _friendChatGridView.MovePanelToItemByIndex(index);
        }
        RefreshMessage();

      
    }

    private void OnMessageContentAdded(object sender, ChildChangedEventArgs e)
    {
        _messages.Add(JsonConvert.DeserializeObject<Message>(e.Snapshot.GetRawJsonValue()));
        
        RefreshMessage();
    }
}
