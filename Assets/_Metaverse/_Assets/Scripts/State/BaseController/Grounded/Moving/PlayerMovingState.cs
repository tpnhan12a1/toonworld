using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract  class PlayerMovingState : PlayerGroundedState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.MovingParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.MovingParameterHash);
    }
}
