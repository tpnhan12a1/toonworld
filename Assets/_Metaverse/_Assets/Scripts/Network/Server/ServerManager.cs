﻿using Imba.UI;
using Imba.Utils;
using Mono.CSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Netcode;
using Unity.Netcode.Components;
using UnityEngine;
using UnityEngine.Rendering.UI;
using UnityEngine.UIElements;
using static NetCodeModels;

[RequireComponent(typeof(SpawnManager))]
public class ServerManager : ManualSingletonNetwork<ServerManager>
{
    RoomManager<Room> _roomManager;
    PlayerManager<Player> _playerManager;
    PartyManager<Party> _partyManager;
    LobbyManager<Lobby> _lobbyManager;
    GroupManager<Group> _groupMamager;
    SpawnManager _spawnManager;

    public Action<NetworkObject> OnSpawnNetworkObject;
    public Action<NetworkObject> OnDespawnNetworkObject;
    private Room vituralWorld;

    public RoomManager<Room> roomManager { get { return _roomManager; } }
    public PlayerManager<Player> playerManager { get { return _playerManager; } }
    public SpawnManager spawnManager { get { return _spawnManager; } }
    public LobbyManager<Lobby> lobbyManager { get { return _lobbyManager; } }
    public GroupManager<Group> groupMamager { get {  return _groupMamager; } }
    public void Start()
    {
        _spawnManager = GetComponent<SpawnManager>();
        if (_spawnManager == null)
        {
            Debug.LogError("SpawnManager not found on the ServerManager GameObject.");
        }
    }

    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if(NetworkManager.IsServer)
        {
            _roomManager = new RoomManager<Room>();
            _playerManager = new PlayerManager<Player>();
            _partyManager = new PartyManager<Party>();
            _lobbyManager = new LobbyManager<Lobby>();
            _groupMamager = new GroupManager<Group>();

             vituralWorld = new Room(_roomManager,SceneName.SceneVirtualWorld);
            _roomManager.AddRoom(vituralWorld);

            NetworkManager.OnClientConnectedCallback += OnClientConnectedCallback;
            NetworkManager.OnClientDisconnectCallback += OnClientDisconnectedCallback;
            NetworkManager.NetworkTickSystem.Tick += Tick;
        }
    }

    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        if (NetworkManager.IsServer)
        {
            NetworkManager.OnClientConnectedCallback -= OnClientConnectedCallback;
            NetworkManager.OnClientDisconnectCallback -= OnClientDisconnectedCallback;
            NetworkManager.NetworkTickSystem.Tick -= Tick;
        }
    }
    private void Tick()
    {
        _roomManager.ForEach(x => x.Tick());
    }
    private void OnClientDisconnectedCallback(ulong clientId)
    {
        if (!IsServer) return;
        Player player = _playerManager.GetPlayer(clientId);
        player.lobby?.LeaveOrCloseLobby(player);
        player.GetRoom()?.PlayerOutRoom(player);
        player.GetGroup()?.RemovePlayerInGroup(player);
        _playerManager.RemovePlayer(player);
       

        NetworkObject playerObject = NetworkManager.Singleton.ConnectedClients[player.GetClientId()].PlayerObject;
        if (playerObject != null)
        {
            _spawnManager.DespawnObject(playerObject);
            NetworkManager.Singleton.ConnectedClients[player.GetClientId()].PlayerObject = null;
        }
        ulong[] targetClientIds = _playerManager.GetClientIds().ToArray();
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = targetClientIds
            }
        };
        ClientManager.Instance.ResponseWhenPlayerJoinOutClientRpc(player.playfabId,clientId, false);
    }
    public void JoinVituralWorld()
    {
        JoinVituralWorldServerRpc();
    }
    [ServerRpc(RequireOwnership = false)]
    private void JoinVituralWorldServerRpc(ServerRpcParams serverRpcParams = default)
    {
        ChangeRoomServerRpc(0,serverRpcParams);
    }
    [ServerRpc(RequireOwnership = false)]
    public void ChangeRoomServerRpc(ulong roomId, ServerRpcParams serverRpcParams = default)
    {
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = new ulong[] { serverRpcParams.Receive.SenderClientId }
            }
        };
        Room room = ServerManager.Instance.roomManager.GetRoom(roomId);
        Player player = ServerManager.Instance.playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
        Room currentRoom = player.GetRoom();
        ChangeRoom(player, currentRoom, room);

        ClientManager.Instance.ChangeRoomClientRpc(clientRpcParams);
    }

    [ServerRpc(RequireOwnership = false)]
    public void SpawnPlayerObjectServerRpc(Vector3 position, Quaternion rotation, ServerRpcParams serverRpcParams = default)
    {
        Player player = ServerManager.Instance.playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
        player.GetRoom()?.SpawnPlayerObjectInRoom(player, position, rotation);
        player.GetRoom()?.OnPlayerReadyInRoom(player);
    }

    private void OnClientConnectedCallback(ulong clientId)
    {
        if (!IsServer) return;
        //Set up room
        Player player = new Player(clientId, vituralWorld);

        _playerManager.AddPlayer(player);

        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams
            {
                TargetClientIds = new ulong[] { clientId }
            }
        };
        ClientManager.Instance.GetPlayerDataClientRpc(clientRpcParams);

        Room room = _roomManager.GetRoom(0);

        room.PlayerJoinRoom(player);
        //Setup party
    }
    [ServerRpc(RequireOwnership = false)]
    public void GetPlayerDataResponseServerRpc(NetCodeModels.PlayerData playerData,ServerRpcParams serverRpcParams = default)
    {
       
        Party party = _partyManager.GetPartyByPlayfabID(playerData.playfabId);
        if(party != null)
        {
            Debug.Log("Client reconnected to the party");
            party.AddClientId(serverRpcParams.Receive.SenderClientId);
        }
        Player player = _playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
        player.SetPlayerData(playerData);

        List<Player> players = _playerManager.FilterPlayers(x => x.playerData.playfabId == playerData.playfabId);
        if (players.Count >= 2)
        {
            Player p = players.Where(x => x.GetClientId() != serverRpcParams.Receive.SenderClientId).FirstOrDefault();
            if (p != null)
            {
                NetworkManager.Singleton.DisconnectClient(p.GetClientId());
            }
        }

        ulong[] targetClientIds = _playerManager.GetClientIds().ToArray();
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = targetClientIds
            }
        };
        SendListPlayer(serverRpcParams.Receive.SenderClientId);
        ClientManager.Instance.ResponseWhenPlayerJoinOutClientRpc(playerData.playfabId,serverRpcParams.Receive.SenderClientId, true, clientRpcParams);
        
    }
   

    [ServerRpc(RequireOwnership = false)]
    public void CreateOrJoinRoomServerRpc(NetCodeModels.CreateRoomRequest request,ServerRpcParams serverRpcParams =  default)
    {
        Room room = _roomManager.GetRoom(x =>x.owwer == request.owner && x.sceneName == request.sceneName).FirstOrDefault();

        if(room == null)
        {
           
            switch (request.sceneName)
            {
                case SceneName.SceneHome:
                case SceneName.SceneVirtualWorld:
                    room = new Room(_roomManager, request.sceneName, owner: request.owner, false);
                    break;
            }
            _roomManager.AddRoom(room);
        }
        NetCodeModels.CreateRoomResponse response = new NetCodeModels.CreateRoomResponse()
        {
            roomId = room.roomId
        };
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams
            {
                TargetClientIds = new ulong[] { serverRpcParams.Receive.SenderClientId }
            }
        };
        ClientManager.Instance.OnCreateRoomClientRpc(response, clientRpcParams);
    }
    [ServerRpc(RequireOwnership = false)]
    public void ChatServerRpc(string playfabId,string receiverPlayfabId, NetCodeModels.ChatContent chatContent, ServerRpcParams serverRpcParams = default) 
    {
        
        switch (chatContent.type)
        {
            case TypeChat.Global:
                ClientManager.Instance.ChatClientRpc(chatContent);
                break;
            case TypeChat.Party:
                Party party = _partyManager.GetPartyByPlayfabID(playfabId);
                if(party!= null)
                {
                    ulong[] receiverClientIds = party.GetClientInParty();
                    ClientRpcParams chatRpcParams = new ClientRpcParams()
                    {
                        Send = new ClientRpcSendParams
                        {
                            TargetClientIds = receiverClientIds
                        }
                    };
                    ClientManager.Instance.ChatClientRpc(chatContent, chatRpcParams);
                }
                break;
            case TypeChat.Private:
                Player player = _playerManager.FilterPlayers( x => x.playfabId == receiverPlayfabId).FirstOrDefault();
                if(player != null)
                {
                    ulong[] receiverClientIds = new ulong[] { player.GetClientId() , serverRpcParams.Receive.SenderClientId };
                    ClientRpcParams chatRpcParams = new ClientRpcParams()
                    {
                        Send = new ClientRpcSendParams
                        {
                            TargetClientIds = receiverClientIds
                        }
                    };
                    ClientManager.Instance.ChatClientRpc(chatContent, chatRpcParams);
                }
                break;
            case TypeChat.Guild:
                player = _playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
                if(player != null)
                {
                    Group group = player.GetGroup();
                    if(group != null)
                    {
                        ulong[] receiverClientIds = group.playerManager.GetClientIds().ToArray();
                        ClientRpcParams chatRpcParams = new ClientRpcParams()
                        {
                            Send = new ClientRpcSendParams
                            {
                                TargetClientIds = receiverClientIds
                            }
                        };
                        ClientManager.Instance.ChatClientRpc(chatContent, chatRpcParams);
                    }
                }
                break;
        }
    }
    [ServerRpc(RequireOwnership =false)]
    void CreatePartyServerRpc(string playfabId, ServerRpcParams serverRpcParams = default)
    {
        Party party = new Party(_partyManager, playfabId, serverRpcParams.Receive.SenderClientId);
        _partyManager.AddParty(party);
        NetCodeModels.Party p = party.GetParty();

        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams() { TargetClientIds = new ulong[] { serverRpcParams.Receive.SenderClientId } }
        };

        ClientManager.Instance.CreatePartyResponseClientRpc(p, clientRpcParams);

    }
    [ServerRpc(RequireOwnership =false)]
    void RemovePartyServerRpc(string playfabId, ServerRpcParams serverRpcParams = default)
    {
        Party party = new Party(_partyManager, playfabId, serverRpcParams.Receive.SenderClientId);
        if(party != null)
        {
            party.RemoveParty();
        }
    }

    [ServerRpc(RequireOwnership =false)]
    public void SendEmotionServerRpc(NetworkObjectReference playerObject, string name, ServerRpcParams serverRpcParams = default)
    {

        Player player = _playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
        Room room =  player.GetRoom();
        if(room!= null)
        {
            ulong[] targetClientIds = room.playerManager.GetClientIds().ToArray();
            ClientRpcParams clientRpcParams = new ClientRpcParams()
            {
                Send = new ClientRpcSendParams()
                {
                    TargetClientIds = targetClientIds
                }
            };
            ClientManager.Instance.ReceiverChatEmotionClientRpc(playerObject, name, clientRpcParams);
        }
    }
    [ServerRpc(RequireOwnership =false)]
    public void CreateLobbyServerRpc(GamePlayType type ,string password ="",ServerRpcParams serverRpcParams = default)
    {
        Player player = playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
        if(player != null)
        {
            Lobby lobby = new Lobby(lobbyManager,player,type,password);
            _lobbyManager.Add(type,lobby);
            lobby.PlayerJoinLobby(player);
            ClientRpcParams clientRpcParams = new ClientRpcParams()
            {
                Send = new ClientRpcSendParams()
                {
                    TargetClientIds = new ulong[] { serverRpcParams.Receive.SenderClientId }
                }
            };
            ClientManager.Instance.ResponseStatusJoinLobbyClientRpc(true, "", clientRpcParams);
        }
    }
    [ServerRpc(RequireOwnership =false)]
    public void JoinLobbyServerRpc(GamePlayType gamePlayType ,int lobbyId,ServerRpcParams serverRpcParams = default)
    {
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = new ulong[] { serverRpcParams.Receive.SenderClientId }
            }
        };
        if (!JoinLobby(gamePlayType, lobbyId, serverRpcParams.Receive.SenderClientId))
       {
           
            string message = "You cant't join this lobby";
            ClientManager.Instance.ResponseStatusJoinLobbyClientRpc(false,message, clientRpcParams);
        }
        else
        {
            ClientManager.Instance.ResponseStatusJoinLobbyClientRpc(true, "", clientRpcParams);
        }
    }

    private bool JoinLobby(GamePlayType gamePlayType, int lobbyId, ulong targetClientId)
    {
        Player player = playerManager.GetPlayer(targetClientId);
        Lobby lobby = _lobbyManager.GetLobby(gamePlayType, lobbyId);
        if (lobby != null)
        {
            lobby.PlayerJoinLobby(player);
            return true;
        }
       return false;
    }

    [ServerRpc(RequireOwnership = false)]
    public void LeaveLobbyServerRpc(ServerRpcParams serverRpcParams = default)
    {
        Player player = playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
        player.lobby.LeaveOrCloseLobby(player);
    }
    [ServerRpc(RequireOwnership = false)]
    public void GetLobbyListServerRpc(GamePlayType type,ServerRpcParams serverRpcParams = default)
    {
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = new ulong[] {serverRpcParams.Receive.SenderClientId }
            }
        };
        NetCodeModels.GetLobbyResponse response = new NetCodeModels.GetLobbyResponse()
        {
            lobbies = _lobbyManager.GetListLobbies(type)
        };
        
        ClientManager.Instance.ResponseGetLobbyListClientRpc(response,clientRpcParams);
    }
    [ServerRpc(RequireOwnership = false)]
    public void QuickJoinLobbyServerRpc(GamePlayType gamePlayType, ServerRpcParams serverRpcParams = default)
    {
        Lobby lobby =  _lobbyManager.GetLobby(gamePlayType);

        if(lobby != null)
        {
            ClientRpcParams clientRpcParams = new ClientRpcParams()
            {
                Send = new ClientRpcSendParams()
                {
                    TargetClientIds = new ulong[] { serverRpcParams.Receive.SenderClientId }
                }
            };

            if (!JoinLobby(gamePlayType, lobby.id, serverRpcParams.Receive.SenderClientId))
            {
               
                string message = "Don't have any lobby!!";
                ClientManager.Instance.ResponseStatusJoinLobbyClientRpc(false,message, clientRpcParams);
            }
            else
            {
                ClientManager.Instance.ResponseStatusJoinLobbyClientRpc(true,"", clientRpcParams);
            }
        }
    }
    [ServerRpc(RequireOwnership =false)]
    public void KickMemberInLobbyServerRpc(string playfabId, ServerRpcParams serverRpcParams = default)
    {
        Player player = _playerManager.FilterPlayers(x => x.playfabId == playfabId).FirstOrDefault();
        if(player != null)
        {
            player.lobby.PlayerLeaveLobby(player);
            ClientRpcParams clientRpcParams = new ClientRpcParams()
            {
                Send = new ClientRpcSendParams()
                {
                    TargetClientIds = new ulong[] { player.GetClientId() }
                }
            };

            ClientManager.Instance.ResponseKickedOutLobbyClientRpc(clientRpcParams);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    public void StartGameServerRpc(ServerRpcParams serverRpcParams = default)
    {
        Lobby lobby = _playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId)?.lobby;
        if(lobby != null)
        {
            Room room =lobby.StartGame();
            if(room != null)
            {
                _roomManager.AddRoom(room);
            }
        }
    }
    
    #region MainMethod
    public void ChangeRoom(Player  player, Room currentRoom, Room targetRoom)
    {
        if (currentRoom != null)
        {
            currentRoom.PlayerOutRoom(player);

            if (currentRoom.sceneName != targetRoom.sceneName)
            {
                NetworkObject playerObject = NetworkManager.Singleton.ConnectedClients[player.GetClientId()].PlayerObject;
                if (playerObject != null)
                {
                    _spawnManager.DespawnObject(playerObject);
                    NetworkManager.Singleton.ConnectedClients[player.GetClientId()].PlayerObject = null;
                }
            }
        }
        targetRoom.PlayerJoinRoom(player);
    }
    public void SendListPlayer(ulong clientId)
    {
        NetCodeModels.ListString playfabIds = new NetCodeModels.ListString()
        {
            list = new List<string>()
        };
        NetCodeModels.ListUlong clientIds = new NetCodeModels.ListUlong()
        {
            list = new List<ulong>()
        };
        _playerManager.ForEach(x =>
        {
            playfabIds.list.Add(x.playfabId);
            clientIds.list.Add(x.GetClientId());
        });

        ClientRpcParams clientRpcSendParams = new ClientRpcParams() { Send = new ClientRpcSendParams() { TargetClientIds = new ulong[] { clientId } } };
        ClientManager.Instance.ResponseListPlayerClientRpc(playfabIds,clientIds, clientRpcSendParams);
    }


    #endregion
    #region Group
    [ServerRpc(RequireOwnership = false)]
    public void ResponseGroupIdServerRpc(string groupId, ServerRpcParams serverRpcParams = default)
    {
        Group group = null;
        Player player = _playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
        if (_groupMamager.GetGroup(groupId, out group))
        {
            group.AddPlayerInGroup(player);
        }
        else
        {
            group = new Group(groupId, _groupMamager);
            _groupMamager.AddGroup(group);
            group.AddPlayerInGroup(player);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    public void OutGuildServerRpc(string guildId, NetCodeModels.GuildSender guildSender,ServerRpcParams serverRpcParams = default)
    {
        Player player = _playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
        if(player != null)
        {
            Group group = player.GetGroup();
            group?.OutGroup(player, guildSender);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    internal void KickMemberServerRpc(string guildId, GuildSender guildSender, ServerRpcParams serverRpcParams = default)
    {
        Group group = null;
        if(_groupMamager.GetGroup(guildId,out group))
        {
            Player player = _playerManager.FilterPlayers(x =>x.playfabId == guildSender.guildMemberInfor.playfabId).FirstOrDefault();
            group.KickMember(player,guildSender);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    internal void UnPollMemberServerRpc(string guildId, GuildSender guildSender, ServerRpcParams serverRpcParams = default)
    {
        Group group = null;
        if (_groupMamager.GetGroup(guildId, out group))
        {
            Player player = _playerManager.FilterPlayers(x => x.playfabId == guildSender.guildMemberInfor.playfabId).FirstOrDefault();
            group.UnPollMember(player, guildSender);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    internal void PollMemberServerRpc(string guildId, GuildSender guildSender, ServerRpcParams serverRpcParams = default)
    {
        Group group = null;
        if (_groupMamager.GetGroup(guildId, out group))
        {
            Player player = _playerManager.FilterPlayers(x => x.playfabId == guildSender.guildMemberInfor.playfabId).FirstOrDefault();
            group.PollMember(player, guildSender);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    internal void AuthenMemberServerRpc(string guildId, GuildSender guildSender, ServerRpcParams serverRpcParams = default)
    {
        Group group = null;
        if (_groupMamager.GetGroup(guildId, out group))
        {
            Player player = _playerManager.FilterPlayers(x => x.playfabId == guildSender.guildMemberInfor.playfabId).FirstOrDefault();
            
            group.AuthenMember(player, guildSender);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    internal void GuildInviteServerRpc(string playfabId, EntityKey guildEntity, ServerRpcParams serverRpcParams = default)
    {
        Player player = _playerManager.FilterPlayers(x=> x.playfabId == playfabId).FirstOrDefault();
        if (player != null)
        {
            ClientRpcParams clientRpcParams = new ClientRpcParams()
            {
                Send = new ClientRpcSendParams()
                {
                    TargetClientIds = new ulong[] { player.GetClientId() }
                }
            };
            ClientManager.Instance.ResponseGuildInviteClientRpc(guildEntity, clientRpcParams);
        }
    }
    [ServerRpc(RequireOwnership = false)]
    internal void PlayerJoinNewGroupServerRpc(EntityKey guildEntity, GuildMemberInfor guildMemberInfor, ServerRpcParams serverRpcParams = default)
    {
        Group group = null;
        Player player = _playerManager.GetPlayer(serverRpcParams.Receive.SenderClientId);
        if(_groupMamager.GetGroup(guildEntity.Id, out group))
        {
            group?.PlayerJoinGroup(player, guildMemberInfor);
        }
        else
        {
            group = new Group(guildEntity.Id, _groupMamager);
            _groupMamager.AddGroup(group);
            group?.PlayerJoinGroup(player, guildMemberInfor);
        }
    }

    [ServerRpc(RequireOwnership = false)]
    internal void FriendControllerServerRpc(NetCodeModels.FriendContent friendContent, ServerRpcParams serverRpcParams = default)
    {
        Player player = _playerManager.FilterPlayers(x =>x.playfabId == friendContent.playfabReciverId).FirstOrDefault();
        if(player != null)
        {
            ClientRpcParams clientRpcParams = new ClientRpcParams()
            {
                Send = new ClientRpcSendParams()
                {
                    TargetClientIds = new ulong[] { player.GetClientId() },
                }
            };
            ClientManager.Instance.FriendResponseControllerClientRpc(friendContent,clientRpcParams);
        }
    }
    #endregion
}
