using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ToonWorld;
public class FriendSuggestGridViewController : LoopGridView, IGroupTab
{
    private FriendManager _friendManager;
    private FriendManager _friendSearchManager;

    [SerializeField] private float limitAPICall = 60f;
    private float timeCall = 0;
    [SerializeField] private TMP_InputField _inputFieldParamSearch;
    [SerializeField] private Button _btnSearch;
    [SerializeField] private Button _btnExitedSearch;

    private bool _isSearching = false;
    public FriendManager friendManager { get { return _friendManager; } }
    public void OnInit()
    {
        _friendManager = new FriendManager();
        _friendManager.InitListFriend(new List<Friend>());
        _friendSearchManager = new FriendManager();
        _friendSearchManager.InitListFriend(new List<Friend>());
        
        InitGridView(_friendManager.GetAllList().Count, OnGetFriendItemByRowColum);
        _inputFieldParamSearch.onValueChanged.AddListener(OnFieldParamChanged);
        _btnSearch.onClick.AddListener(Search);
        _btnExitedSearch.onClick.AddListener(ExitedSearch);
    }
    private void Search()
    {
        if(String.IsNullOrEmpty(_inputFieldParamSearch.text)) { return; }

        APIModels.GetFriendRequest friendRequest = new APIModels.GetFriendRequest();
        friendRequest.requestParam = _inputFieldParamSearch.text;
        PlayfabManager.Instance.ExecuteFunction<APIModels.GetFriendsResponse>(
            PlayfabContant.FunctionName.GetUserSuggest,
            friendRequest,
            true,
            (arg,response) =>
            {
                _isSearching = true;
                _friendSearchManager.InitListFriend(response.friends);

                SetListItemCount(_friendSearchManager.GetAllList().Count);
                RefreshAllShownItem();
            },
            null
            );

    }

    private LoopGridViewItem OnGetFriendItemByRowColum(LoopGridView gridView, int index, int row, int column)
    {
        if (_friendManager.GetAllList().Count <= 0) return null;

        FriendSuggestItem friendSuggestItem = (FriendSuggestItem) gridView.NewListViewItem("FriendSuggestItem");
        if(_isSearching)
        {
            friendSuggestItem.OnRefresh(_friendSearchManager.GetAllList()[index], this);
        }
        else
        {
            friendSuggestItem.OnRefresh(_friendManager.GetAllList()[index], this);
        }
        return friendSuggestItem;
    }

    public void Select()
    {
        gameObject.SetActive(true);
        _friendManager.RegisterOnListFriendChanged(OnListFriendChanged);
        _friendSearchManager.RegisterOnListFriendChanged(OnListFriendSearchChanged);
        _inputFieldParamSearch.text = String.Empty;
        _isSearching = false;

        APIModels.CommonRequest commonRequest = new APIModels.CommonRequest();
        PlayfabManager.Instance.ExecuteFunction<APIModels.GetFriendsResponse>(
            PlayfabContant.FunctionName.GetUserSuggest,
            commonRequest,
            false,
            OnGetFriendSuggestSuccess,
            OnGetFriendSuggestFailure
            );
    }

    private void OnListFriendSearchChanged(List<Friend> friendsSearch)
    {
        if (_isSearching)
        {
            SetListItemCount(_friendSearchManager.GetAllList().Count);
            RefreshAllShownItem();
        }
    }

    private void OnGetFriendSuggestFailure(object arg1, string error)
    {
       
    }

    private void OnGetFriendSuggestSuccess(object arg1, APIModels.GetFriendsResponse data)
    {
        _friendManager.InitListFriend(data.friends);
    }

    public void DesSelect()
    {
        gameObject.SetActive(false);
        _friendManager.UnRegisterOnListFriendChanged(OnListFriendChanged);
        _friendSearchManager.UnRegisterOnListFriendChanged(OnListFriendSearchChanged);
    }
    private void OnListFriendChanged(List<Friend> friends)
    {
        if (String.IsNullOrEmpty(_inputFieldParamSearch.text))
        {
            SetListItemCount(friendManager.GetAllList().Count);
            RefreshAllShownItem();
        }
    }

    private void OnFieldParamChanged(string text)
    {
        if (String.IsNullOrEmpty(text))
        {
            _btnExitedSearch.SetActive(false);

        }
        else
        {
            _btnExitedSearch.SetActive(true);
        }
    }
    private void ExitedSearch()
    {
       _inputFieldParamSearch.text = String.Empty;
       if(_isSearching)
       {
            _isSearching = false;
            SetListItemCount(friendManager.GetAllList().Count);
            RefreshAllShownItem();
       }
       
    }
    public void DirtyRemoveFriend(Friend friend)
    {
       _friendManager.RemoveFriend(friend);
    }
}
