using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AILeftRotationMonoState : AIRotationMonoState
{
    public override StateType GetStateType()
    {
        return StateType.AILeftRotationMonoState;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsLeftRotationParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsLeftRotationParameterHash);
    }
}
