﻿Copyright (c) 2019 - 2020 Imba Games. All Rights Reserved.
This code can only be used under the standard Unity Asset Store End User License Agreement
A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms


IMPORTANT
---


REQUIREMENTS
---
DOTween is required to be installed and its initial setup performed before installing ImbaUI.
DOTween download link: https://assetstore.unity.com/packages/tools/animation/dotween-hotween-v2-27676


GETTING STARTED
---



LINKS
---


OTHER LINKS
---
Unity Manual: https://docs.unity3d.com/Manual/index.html
Unity Scripting API: https://docs.unity3d.com/ScriptReference/index.html
.NET API Browser: https://docs.microsoft.com/en-us/dotnet/api/
DOTween Website: http://dotween.demigiant.com