using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

public class CameraManager : ManualSingletonMono<CameraManager>
{
    [SerializeField] private Camera _mainCamera;
    [SerializeField] private Camera _uiCamera;
    [SerializeField] private CinemachineVirtualCamera _virtualPlayerCamera;
    [SerializeField] private CinemachineBrain _brain;
    [SerializeField] private CinemachineInputProvider _inputProvider;
    [SerializeField] private InputActionReference _lookActionRef;

    private CinemachineBlendDefinition _cinemachineBlendDefinition;
    private LayerMask _layerMask;

    public Camera mainCamera { get { return _mainCamera; } }
    public Camera uiCamera {  get { return _uiCamera; } }
    public CinemachineBrain cinemachineBrain { get { return _brain; } }
    public CinemachineVirtualCamera virtualPlayerCamera { get { return _virtualPlayerCamera; } }
    public CinemachineBlendDefinition cinemachineBlendDefinition {get { return _cinemachineBlendDefinition; } }
    public LayerMask layerMask { get { return _layerMask; } }
    public CinemachineInputProvider cinemachineInputProvider { get { return _inputProvider; } }
    public override void Awake()
    {
        base.Awake();
        _cinemachineBlendDefinition = cinemachineBrain.m_DefaultBlend;
        _layerMask = _mainCamera.cullingMask;
    }
    public void DisableLook()
    {
        _inputProvider.XYAxis = null;
    }
    public void EnableLook()
    {
        _inputProvider.XYAxis = _lookActionRef;
    }
}
