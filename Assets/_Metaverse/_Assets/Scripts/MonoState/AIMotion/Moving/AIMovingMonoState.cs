using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class AIMovingMonoState : AIMotionMonoState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsMovingParameterHash);
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        Transform currentWaitPoint = residentStateMachine.residentController.GetCurrentWaitPoint();
        if (currentWaitPoint == null)
        {
            _stateMachine.ChangeState(residentStateMachine.states[StateType.AIIdelingMonoState]);
        }
        residentStateMachine.residentController.navMeshAgent.isStopped = false;
        
        if (residentStateMachine.residentController.residentReusableData.isIntractWithPlayer == false)
        {
            residentStateMachine.residentController.UpdateCurrentWaitPoint();
            residentStateMachine.residentController.residentReusableData.targetLookAt = residentStateMachine.residentController.GetCurrentWaitPoint().position;
        }
        else
        {
            residentStateMachine.residentController.SetCurrentDestination();
            residentStateMachine.residentController.residentReusableData.isIntractWithPlayer = false;
        }
       
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();

    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsMovingParameterHash);
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        residentStateMachine.residentController.navMeshAgent.ResetPath();
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        if(residentStateMachine.residentController.navMeshAgent.remainingDistance <= residentStateMachine.residentController.navMeshAgent.stoppingDistance) 
        {
            if (!residentStateMachine.residentController.navMeshAgent.hasPath || residentStateMachine.residentController.navMeshAgent.velocity.sqrMagnitude == 0f)
            {
                // Done
                OnStop();
            }
        }
    }

    public override void OnContactWithPlayerEnter(Collider collider)
    {
        base.OnContactWithPlayerEnter(collider);
        OnStop();
    }
    protected virtual void OnStop()
    {
       ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
       residentStateMachine.ChangeState(residentStateMachine.states[StateType.AILightStoppingMonoState]);
    }
}
