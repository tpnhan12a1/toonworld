using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnumUtils
{
    public static T GetRandomEnumValue<T>()
    {
        Array values = Enum.GetValues(typeof(T));
        System.Random random = new System.Random();
        int randomIndex = random.Next(values.Length);
        T randomValue = (T)values.GetValue(randomIndex);

        return randomValue;
    }
    public static List<T> GetAllEnumValue<T>()
    {
        List<T> list = new List<T>();
       Array enumValues = Enum.GetValues(typeof(T));
        foreach (T enumValue in enumValues)
        {
            list.Add(enumValue);
        }
        return list;
    }
    public static T GetRandomEnumValueInList<T>(List<T> enumValue)
    {
        System.Random random = new System.Random();
        int randomIndex = random.Next(enumValue.Count);
        T randomValue = (T)enumValue[randomIndex];
        return randomValue;
    }
}
