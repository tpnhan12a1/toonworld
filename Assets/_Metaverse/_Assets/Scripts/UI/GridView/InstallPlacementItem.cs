using Mono.CSharp.Linq;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class InstallPlacementItem : LoopGridViewItem
{
    [SerializeField] private Image _icon;
    [SerializeField] private Image _imageSelected;
    

    private Button _btn;
    private Item _item;
    private PlacementSO _placementSO;
    private InstallItemGridViewController _installItemGridViewController;
    private TabController _tabController;
    private bool _isSelected;
    private void Awake()
    {
        _btn = GetComponent<Button>();
        _btn.onClick.AddListener(ClickItem);
    }
    public void OnRefresh(Item item, InstallItemGridViewController installItemGridViewController, bool isSelected)
    {
        _item = item;
        _installItemGridViewController = installItemGridViewController;
        _placementSO = Imba.Utils.ResourceManager.Instance.GetResourceByName<PlacementSO>(new StringBuilder().Append("Home/Placement/").Append(item.itemId).ToString());
        Sprite sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Placement, _placementSO.spriteName);
        if(sprite != null )
        {
            _icon.SetActive(true);
            _icon.sprite = sprite;
        }
        else
        {
            _icon.SetActive(false);
        }
        _isSelected = isSelected;
        _imageSelected.SetActive(isSelected);

    }
    private void ClickItem()
    {
        _isSelected = !_isSelected;

        if (_isSelected)
        {
            _installItemGridViewController.AddItemSelected(_item);
        }
        else
        {
            _installItemGridViewController.RemoveItemSelected(_item);
        }

        _imageSelected.SetActive(_isSelected);
    }
}
