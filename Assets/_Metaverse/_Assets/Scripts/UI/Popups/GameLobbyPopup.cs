using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using ToonWorld;
using UnityEngine;
using UnityEngine.UI;

public class GameLobbyPopup : UIPopup
{
    [SerializeField] private Button _btnClosePopup;
    [SerializeField] private TMP_Text _txtLobbyId;

    [SerializeField] private Button _btnStartGame;
    //[SerializeField] private Button _btnReady;

    [SerializeField] private GameLobbyGridViewController _gameLobbyGridViewController;
    protected override void OnInit()
    {
        base.OnInit();
        _btnClosePopup.onClick.AddListener(OnClickCloseButton);
        //_btnReady.onClick.AddListener(OnClickReadyButton);
        _btnStartGame.onClick.AddListener(OnClickStartGame);
        //_btnReady.SetActive(false);
        _btnStartGame.SetActive(false);
        _gameLobbyGridViewController.OnInit();
    }

    private void OnClickStartGame()
    {
        ServerManager.Instance.StartGameServerRpc();
    }

    private void OnClickCloseButton()
    {
        Hide();
        ServerManager.Instance.LeaveLobbyServerRpc();
        SGSceneManager.Instance.UnLoadSceneAdditive(SceneName.SceneGameLobby.ToString());
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.MainPopup);
    }

    protected override void OnShowing()
    {
        base.OnShowing();
       
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.MainPopup);
        UIManager.Instance.PopupManager.GetPopup(UIPopupName.QuickChatPopup)?.MoveToTop();
        _gameLobbyGridViewController?.OnShow();
        ClientManager.Instance.OnHaveBeenKickedLobby += OnHaveBeenKickedLobby;
        PlayerDataManager.Instance.globalReusableData.RegisterOnLobbyChanged(OnLobbyChanged);
    }

    
    private void OnLobbyChanged(ILocalLobby oldLobby, ILocalLobby localLobby)
    {
        if (localLobby == null)
        {
            oldLobby?.UnRegisterOnLobbyDataChanged(OnLobbyDataChanged);
            oldLobby?.UnRegisterOnListDataChanged(OnListDataChanged);
            if(SceneGameLobbyManager.Instance != null)
            {
                SGSceneManager.Instance.UnLoadSceneAdditive(SceneName.SceneGameLobby.ToString());
            }
        }
        else
        {
            localLobby?.RegisterOnLobbyDataChanged(OnLobbyDataChanged);
            localLobby?.RegisterOnListDataChanged(OnListDataChanged);
        }
    }

    private void OnListDataChanged(List<NetCodeModels.PlayerData> listData)
    {
       
    }

    private void OnLobbyDataChanged(GameRoomLobby lobbyData)
    {
        SceneGameLobbyManager.Instance?.ShowSlots(lobbyData.maxPlayer);
        bool status = lobbyData.ownerPlayfabId == PlayfabManager.Instance.PlayfabID;
        _txtLobbyId.text = lobbyData.id.ToString();
        _btnStartGame.SetActive(status);
        //_btnReady.SetActive(!status);
    }

    protected override void OnHiding()
    {
        base.OnHiding();
        ClientManager.Instance.OnHaveBeenKickedLobby -= OnHaveBeenKickedLobby;
        PlayerDataManager.Instance.globalReusableData.UnRgisterOnLobbyChanged(OnLobbyChanged);
        _gameLobbyGridViewController?.OnHide();
       
    }

    private void OnHaveBeenKickedLobby()
    {
        UIManager.Instance.AlertManager.ShowAlertMessage("You have been kick a party");
        SGSceneManager.Instance.UnLoadSceneAdditive(SceneName.SceneGameLobby.ToString());
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.MainPopup);
        Hide();
        
    }

    public void Refresh()
    {

    }
}
