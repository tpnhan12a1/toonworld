using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerRollingState : PlayerLandingState
{
    public override StateType GetStateType()
    {
        return StateType.Rolling;
    }
    public override void OnEnterState()
    {

        PlayerDataManager.Instance.reusableData.movementSpeedModifier = PlayerDataManager.Instance.playerData.speedModifier;
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.RollParameterHash);
    }

    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        if (PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero)
        {
            return;
        }
       RotateTowardsTargetRotation();
    }

    public override void OnAnimationTransitionEvent()
    {
        if (PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero)
        {
            _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.MediumStopping]);
            return;
        }
        OnMove();
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.RollParameterHash);
    }
    protected override void OnJumpStarted(InputAction.CallbackContext content)
    {

    }

}
