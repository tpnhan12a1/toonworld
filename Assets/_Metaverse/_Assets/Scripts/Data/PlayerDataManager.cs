﻿using System;
using System.Collections.Generic;
using UnityEngine;
using static APIModels;

public class PlayerDataManager : ManualSingletonMono<PlayerDataManager> 
{
    [SerializeField] private PlayerSO _playerData;
    [SerializeField] private ReusableData _reusableData;
    [SerializeField] private LayerData _layerData;
    [SerializeField] private AnimationData _animationData;
    private GlobalReusableData _globalReusableData;
    private PartyData _partyData;
    

    private string _displayName = "";
    private string _avatarUrl = "";
    private int _level = 0;
    private int _exp = 0;
    private string _introduction = "";
    private Currency _currency = new Currency();
    private List<Item> _items;
    private PlayerItem _playerItem;

    [SerializeField]
    private FriendManager _friendManager;

    [SerializeField]
    private InventoryManager _inventoryManager;

    [SerializeField]
    private PlacementManager _placementManager;

    [SerializeField]
    private GuildManager _guildManager;

    public int limitExpBaseLevel { get; private set; }

    private Action<string, string> OnDisplayNameChanged;
    private Action<List<Item>> OnListItemsChanged;
    private Action<int, int> OnLevelChanged;
    private Action<int, int> OnExpChanged;
    private Action<string, string> OnAvatarIdChanged;
    private Action<Currency, Currency> OnCurrencyChanged;
    private Action<PlayerItem, PlayerItem> OnPlayerItemChanged;
    private Action<string, string> OnIntroductionChanged;

    public override void Awake()
    {
        base.Awake();
        _animationData.Initialize();
        _friendManager = new FriendManager();
        _inventoryManager = new InventoryManager();
        _globalReusableData = new GlobalReusableData();
        _partyData = new PartyData();
        _placementManager = new PlacementManager();
        _guildManager = new GuildManager();
    }
    public string displayName
    {
        get { return _displayName; }
        set
        {
            OnDisplayNameChanged?.Invoke(_displayName, value);
            _displayName = value;
        }
    }
    public string introduction
    {
        get { return _introduction; }
        set
        {
            OnIntroductionChanged?.Invoke(_introduction, value);
            _introduction = value;
        }
    }
    public int level
    {
        get { return _level; }
        set
        {
            OnLevelChanged?.Invoke(_level, value);
            _level = value;
            limitExpBaseLevel = GetLimtExpByLevel();
        }
    }
    public int exp
    {
        get { return _exp; }
        set
        {
            OnExpChanged?.Invoke(_exp, value);
            _exp = value;
            if(_exp > limitExpBaseLevel)
            {
                LevelUpRequest request = new LevelUpRequest();
                request.level = level+1;
                request.exp = exp - limitExpBaseLevel;
                PlayfabManager.Instance.ExecuteFunction<object>(PlayfabContant.FunctionName.LevelUp, request,false,null, null);
            }
        }
    }
    public string avatarUrl
    {
        get { return _avatarUrl; }
        set 
        { 
            OnAvatarIdChanged?.Invoke(_avatarUrl, value);
            _avatarUrl = value;
        }
    }

    public Currency currency
    {
        get { return _currency; }
        set
        {
            OnCurrencyChanged?.Invoke(_currency, value);
            _currency = value;
        }
    }

    public PlayerItem playerItem
    {
        get { return _playerItem; }
        set
        {
            OnPlayerItemChanged?.Invoke(_playerItem, value);
            _playerItem = value;
        }
    }

    public PartyData partyData { get { return _partyData; } }
    public FriendManager friendManager { get { return _friendManager; } }
    public InventoryManager inventoryManager { get { return _inventoryManager; } }
    public GlobalReusableData globalReusableData { get { return _globalReusableData; } }
    public PlayerSO playerData { get { return _playerData; } }
    public ReusableData reusableData { get { return _reusableData; } }
    public LayerData layerData { get { return _layerData; } }
    public AnimationData animationData { get { return _animationData; } }
    public PlacementManager placementManager { get { return _placementManager; } }
    public GuildManager guildManager { get { return _guildManager; } }

    public void RegisterOnCurrencyChanged(Action<Currency, Currency> onCurrencyChanged)
    {
        OnCurrencyChanged += onCurrencyChanged;
        if(currency != null)
            onCurrencyChanged?.Invoke(currency, currency);
    }
    public void UnRegisterOnCurrencyChanged(Action<Currency, Currency> onCurrencyChanged)
    {
        OnCurrencyChanged -= onCurrencyChanged;
    }
    public void RegisterOnDisplayNameChanged(Action<string, string> onDisplayNameChanged)
    {
        OnDisplayNameChanged += onDisplayNameChanged;
        onDisplayNameChanged?.Invoke(displayName, displayName);
    }
    public void RegisterOnListItemsChanged(Action<List<Item>> onListItemsChanged)
    {
        OnListItemsChanged += onListItemsChanged;
        onListItemsChanged?.Invoke(_items);
    }
    public void UnRegisterOnDisplayNameChanged(Action<string, string> onDisplayNameChanged)
    {
        OnDisplayNameChanged -= onDisplayNameChanged;
    }
    public void UnRegisterOnListItemsChanged(Action<List<Item>> onListItemsChanged)
    {
        OnListItemsChanged -= onListItemsChanged;
    }
    public void RegisterOnLevelChanged(Action<int,int> onLevelChanged)
    {
        OnLevelChanged += onLevelChanged;
        onLevelChanged?.Invoke(level, level);
    }
    public void UnRegisterOnLevelChanged(Action<int, int> onLevelChanged)
    {
        OnLevelChanged -= onLevelChanged;
    }
    public void RegisterOnExpChanged(Action<int, int> onExpChanged)
    {
        OnExpChanged += onExpChanged;
        onExpChanged?.Invoke(exp, exp);
    }
    public void UnRegisterOnExpChanged(Action<int, int> onExpChanged)
    {
        OnExpChanged -= onExpChanged;
    }
    public void RegisterOnIntroductionChanged(Action<string,string> onIntroductionChanged)
    {
        OnIntroductionChanged += onIntroductionChanged;
        onIntroductionChanged?.Invoke(introduction, introduction);
    }
    public void UnRegisterOnIntroductionChanged(Action<string, string> onIntroductionChanged)
    {
        OnIntroductionChanged -= onIntroductionChanged;
    }
    public void RegisterOnAvatarIdChanged(Action<string, string> onAvatarIdChanged)
    {
        OnAvatarIdChanged += onAvatarIdChanged;
        onAvatarIdChanged?.Invoke(avatarUrl, avatarUrl);
    }
    public void UnRegisterOnAvatarIdChanged(Action<string, string> onAvatarIdChanged)
    {
        OnAvatarIdChanged -= onAvatarIdChanged;
    }

    public void RegisterOnPlayerItemChanged(Action<PlayerItem, PlayerItem> onPlayerItemChanged)
    {
        OnPlayerItemChanged += onPlayerItemChanged;
        onPlayerItemChanged?.Invoke(playerItem, playerItem);
    }

    public void UnRegisterOnPlayerItemChanged(Action<PlayerItem, PlayerItem> onPlayerItemChanged)
    {
        OnPlayerItemChanged -= onPlayerItemChanged;
    }

    public void SetDataLogin(OnLoginResponse loginResponse)
    {
        displayName = loginResponse.displayName;
        introduction = loginResponse.introduction;
        avatarUrl = loginResponse.avatarUrl; 
        exp = loginResponse.exp;
        level = loginResponse.level;
        currency = loginResponse.currency;
        playerItem = loginResponse.playerItem;
    }

    private int GetLimtExpByLevel()
    {
        return (int) Mathf.Pow(2f, level);
    }
}
