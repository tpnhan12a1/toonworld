using QFSW.QC.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class OnExtendChatUI : MonoBehaviour, IDragHandler, IEndDragHandler
{
    [SerializeField] private RectTransform _rtChat;
    [SerializeField] private float _paddingTop = 10f;
    [SerializeField] private QuickChatPopup _chatController;
    
    private Vector2 _baseSizeOfRectTransformChat;
    private float _offset;
    public void Awake()
    {
        _baseSizeOfRectTransformChat = _rtChat.sizeDelta;
        _offset = _rtChat.position.y;
    }
    
    public void OnDrag(PointerEventData eventData)
    {
        if (eventData.position.y -_offset < _baseSizeOfRectTransformChat.y)
        {
            SetBase();
            return;
        }
        if(eventData.position.y > Screen.height - _offset - _paddingTop)
        {
            SetHeight();
            return;
        }
        _rtChat.SetHeight(eventData.position.y - _offset);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if ((eventData.position.y / (float)Screen.height) > (2f / 3))
        {
            SetHeight();
        }
        else
        {
            SetBase();
        }
    }
    public void SetHeight()
    {
        _rtChat.SetHeight(Screen.height - _offset - _paddingTop);
        _chatController.SetActiveSubOverlay(true);
    }
    public void SetBase()
    {
        _rtChat.SetHeight(_baseSizeOfRectTransformChat.y);
        _chatController.SetActiveSubOverlay(false);
    }
}
