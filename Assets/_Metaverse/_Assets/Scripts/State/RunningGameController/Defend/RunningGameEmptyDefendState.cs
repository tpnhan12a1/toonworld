using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameEmptyDefendState : IState
{
    protected RunningGameDefendStateMachine _runningGameDefendStateMachine;
    public StateType GetStateType()
    {
        return StateType.RunningGameEmptyDefendState;
    }

    public void OnAnimationEnterEvent()
    {
        
    }

    public void OnAnimationExitsEvent()
    {
        
    }

    public void OnAnimationTransitionEvent()
    {
      
    }

    public void OnEnterState()
    {
        Debug.Log($"Enter {GetStateType()}");
        _runningGameDefendStateMachine.playerController.runningGameReusableData.RegisterOnUseGift(OnUseGift);
    }

    
    public void OnExitState()
    {
        Debug.Log($"Exit {GetStateType()}");
        _runningGameDefendStateMachine.playerController.runningGameReusableData.UnRegisterOnUseGift(OnUseGift);
    }

    public void OnHandleInput()
    {
        
    }

    public void OnPhysicsUpdate()
    {
       
    }

    public void OnTriggerEnter(Collider collider)
    {
       
    }

    public void OnTriggerExit(Collider collider)
    {
        
    }

    public void OnUpdate()
    {
      
    }

    public void SetStateMachine(StateMachine stateMachine)
    {
        _runningGameDefendStateMachine = (RunningGameDefendStateMachine)stateMachine;
    }
    private void OnUseGift(RunningGameReusableData runningGameReusableData, GiftName giftName)
    {
        switch(giftName)
        {
            case GiftName.Shield:
                _runningGameDefendStateMachine.ChangeState(_runningGameDefendStateMachine.states[StateType.RunningGameShieldDefendState]);
                runningGameReusableData.RemoveSkillData(giftName);
                break;
        }
    }
}
