using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player", menuName = "Custom/Resident/Data")]
public class ResidentSO : ScriptableObject
{
    [SerializeField]
    [Range(0f, 10f)] public float walkSpeed = 2f;

    [SerializeField]
    [Range(0f, 10f)]
    public float runSpeed = 4f;

}
