using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public abstract class AIRotationMonoState : AIMotionMonoState
{
    [SerializeField]
    [Range(0f, 100f)] float _rotationSpeed = 100f;
    private Quaternion _tarrgetRotation = Quaternion.identity;
    private ResidentStateMachine _residentStateMachine;

    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsRotationParameterHash);
        _residentStateMachine = (ResidentStateMachine)_stateMachine;
        _residentStateMachine.residentController.residentReusableData.targetLookAt = _residentStateMachine.residentController.GetCurrentWaitPoint().position;
        _residentStateMachine.residentController.navMeshAgent.isStopped = true;
        if(_residentStateMachine.residentController.residentReusableData.playerCollider != null)
        {
            Vector3 dir = _residentStateMachine.residentController.residentReusableData.playerCollider.transform.position - _residentStateMachine.residentController.transform.position;
            Vector3 toRotation = new Vector3(dir.x, 0f, dir.z);
            _tarrgetRotation = Quaternion.LookRotation(
               toRotation,
           Vector3.up);
        }
        else
        {
            Vector3 dir = _residentStateMachine.residentController.residentReusableData.targetLookAt - _residentStateMachine.residentController.transform.position;
            Vector3 toRotation = new Vector3(dir.x ,0f, dir.z);
            _tarrgetRotation =
           Quaternion.LookRotation
           (toRotation,
           Vector3.up);
        }
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsRotationParameterHash);
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        _residentStateMachine.residentController.transform.rotation =
            Quaternion.RotateTowards(_residentStateMachine.residentController.transform.rotation, _tarrgetRotation, _rotationSpeed * Time.deltaTime);

        if(_residentStateMachine.residentController.transform.rotation.eulerAngles.y == _tarrgetRotation.eulerAngles.y)
        {
            OnMove();
        }
    }
    public override void OnMove()
    { 
        if(_residentStateMachine.residentController.residentReusableData.playerCollider != null)
        {
            _residentStateMachine.ChangeState(_residentStateMachine.states[StateType.AIInteractionWithPlayerMonoState]);
            return;
            //NetworkObject playerObject = ((ResidentStateMachine)_stateMachine).residentController.residentReusableData.playerCollider.GetComponent<NetworkObject>();
            //if(playerObject != null && playerObject.IsOwner)
            //{
               
            //}
        }
         _residentStateMachine.ChangeState(_residentStateMachine.states[StateType.AIWalkingMonoState]);
        
      
    }
}
