using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RunningGameMotionState : IState
{
    protected RunningGameMotionStateMachine _runningGameStateMachine;

    public abstract StateType GetStateType();

    public virtual void OnAnimationEnterEvent()
    {

    }

    public virtual void OnAnimationExitsEvent() { }

    public virtual void OnAnimationTransitionEvent() { }

    public virtual void OnEnterState()
    {
        Debug.Log($"Entering  {GetStateType().ToString()}");
        AddInputActionsCallbacks();
        _runningGameStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.MotionParameterHash);
    }

    public virtual void OnExitState()
    {
        Debug.Log($"Stoping  {GetStateType().ToString()}");
        RemoveInputActionCallbacks();
        _runningGameStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.MotionParameterHash);
    }

    public virtual void OnHandleInput()
    {
        ReadPlayerInput();
    }
    
    public virtual void OnUpdate()
    {

    }

    public virtual void OnPhysicsUpdate()
    {
        
    }


    public virtual void OnTriggerEnter(Collider collider)
    {
        OnContactWithGrounded(collider);
    }

  
    public virtual void OnTriggerExit(Collider collider)
    {
        OnContactWithGroundedExited(collider);
    }


    public virtual void SetStateMachine(StateMachine stateMachine)
    {
        _runningGameStateMachine =(RunningGameMotionStateMachine)stateMachine;
    }
    protected virtual void OnContactWithGrounded(Collider collider)
    {

    }
    protected virtual void OnContactWithGroundedExited(Collider collider)
    {

    }
    private void ReadPlayerInput()
    {
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.movementInput =
            PlayerInput.Instance.InputAction.Player.Move.ReadValue<Vector2>();
    }
    protected bool IsMoveUp()
    {
        return _runningGameStateMachine.playerRunningGameController.rb.velocity.y > 0;
    }
    protected bool IsMoveDown()
    {
        return _runningGameStateMachine.playerRunningGameController.rb.velocity.y < 0;
    }
    protected Vector3 GetVerticalVelocity()
    {
        return new Vector3(0f, _runningGameStateMachine.playerRunningGameController.rb.velocity.y, 0f);
    }
    protected Vector3 GetHorizontalVelocity()
    {
        Rigidbody rb = _runningGameStateMachine.playerRunningGameController.rb;
        return new Vector3(rb.velocity.x, 0f, rb.velocity.z);
    }
    protected void ResetVelocity()
    {
        _runningGameStateMachine.playerRunningGameController.rb.velocity = Vector3.zero;
    }
    protected void ResetVerticalVelocity()
    {
        Vector3 velocity = _runningGameStateMachine.playerRunningGameController.rb.velocity;
        velocity.y = 0f;
        _runningGameStateMachine.playerRunningGameController.rb.velocity = velocity;
    }
    protected virtual void OnFall()
    {
        _runningGameStateMachine.ChangeState(_runningGameStateMachine.states[StateType.RunningGameFallingState]);
    }
    protected virtual void AddInputActionsCallbacks()
    {
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.RegisterOnUseGift(OnUseSkillCallback);
    }

    public virtual void OnUseSkillCallback(RunningGameReusableData runningGameReusableData,GiftName giftName)
    {
        switch(giftName)
        {
            case GiftName.Dash:
                _runningGameStateMachine.ChangeState(_runningGameStateMachine.states[StateType.RunningGameDashingState]);
                runningGameReusableData.RemoveSkillData(giftName);
                break;
        }
    }

    protected virtual void RemoveInputActionCallbacks()
    {
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.UnRegisterOnUseGift(OnUseSkillCallback);
    }

    public virtual void Float()
    {
        Vector3 CapsuleColliderCenterInSpace = _runningGameStateMachine
             .playerRunningGameController.capsuleCollider.bounds.center;
        Ray downwardRayFromCapsuleCenter = new Ray(CapsuleColliderCenterInSpace, Vector3.down);
        if (Physics.Raycast(downwardRayFromCapsuleCenter, out RaycastHit hit,
            PlayerDataManager.Instance.playerData.floatRayDistance,
          PlayerDataManager.Instance.layerData.groundedMask, QueryTriggerInteraction.Ignore))
        {
            float distanceToFloatingPoint =
                _runningGameStateMachine
                .playerRunningGameController.capsuleCollider.center.y *
                _runningGameStateMachine.playerRunningGameController.
                transform.localScale.y - hit.distance;
            if (distanceToFloatingPoint == 0) return;
            float amountToLift =
                distanceToFloatingPoint * PlayerDataManager.Instance.playerData.stepReachForce - GetVerticalVelocity().y;
            Vector3 liftForce = new Vector3(0, amountToLift, 0);
            _runningGameStateMachine.playerRunningGameController.rb.AddForce(liftForce, ForceMode.VelocityChange);
        }
    }
}
