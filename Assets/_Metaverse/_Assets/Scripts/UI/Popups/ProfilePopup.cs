using Imba.UI;
using PlayFab.MultiplayerModels;
using GUIPack;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.ResourceManagement;
using UnityEngine.UI;
using Imba;
using static APIModels;
using Imba.Audio;

public class ProfilePopup : UIPopup
{
    [SerializeField] private Button _btnClosePopup;
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private Image _imgAvatar;
    [SerializeField] private TMP_Text _txtExp;
    [SerializeField] private TMP_Text _txtLevel;
    [SerializeField] private Slider _sliderExp;

    [Header("Change name")]
    [SerializeField] private Button _btnOpenPopupChangeName;
    [SerializeField] private GameObject _popUpChangeName;
    [SerializeField] private Button _btnClosePopupChangeName;
    [SerializeField] private Button _btnConfirmChangeDisplayName;
    [SerializeField] private TMP_InputField _inputFieldDisplayName;

    [Header("Introduce")]
    [SerializeField] private TMP_Text _txtIntroduction;
    [SerializeField] private Button _btnOpenPopupIntroduce;
    [SerializeField] private GameObject _popUpIntroduce;
    [SerializeField] private Button _btnClosePopupIntroduce;
    [SerializeField] private Button _btnConfirmChangeIntroduce;
    [SerializeField] private TMP_InputField _inputFieldIntroduce;

    [Header("Change Avatar")]
    [SerializeField] private AvatarGridViewController _avatarLoopGridViewController;
    [SerializeField] private GameObject _avatarPopup;
    [SerializeField] private Button _btnOpenAvatarPopup;
    [SerializeField] private Button _btnCloseAvatarPopup;
    [SerializeField] private Button _btnConfirmChangeAvatar;
    protected override void OnInit()
    {
        base.OnInit();
        _btnClosePopup.onClick.AddListener(() =>
        {
            ClosePopup();
            AudioManager.Instance.PlaySFX(AudioName.PopupClose);
        });
        _btnOpenPopupChangeName.onClick.AddListener(() => 
        { 
            
            OpenPopupChangeName();
        });
        _btnClosePopupChangeName.onClick.AddListener(() => 
        { 
            AudioManager.Instance.PlaySFX(AudioName.PopupClose);
            ClosePopupChangeName(); 
        });
        _btnConfirmChangeDisplayName.onClick.AddListener(() =>
        {
            AudioManager.Instance.PlaySFX(AudioName.ClickButton);
            ChangeDisplayName(); });

        _btnOpenPopupIntroduce.onClick.AddListener(OpenPopupIntroduce);

        _btnClosePopupIntroduce.onClick.AddListener(() =>
        {
            AudioManager.Instance.PlaySFX(AudioName.PopupClose);
            ClosePopupIntroduce();
        });
        _btnConfirmChangeIntroduce.onClick.AddListener(() => 
        { AudioManager.Instance.PlaySFX(AudioName.ClickButton);
            ChangeIntroduce(); 
        });

        _avatarLoopGridViewController.Init();
        _btnOpenAvatarPopup.onClick.AddListener(OpenAvatarPopup);
        _btnCloseAvatarPopup.onClick.AddListener(() =>
        {
            AudioManager.Instance.PlaySFX(AudioName.PopupClose);
            CloseAvatarPopup();
        });
        _btnConfirmChangeAvatar.onClick.AddListener(() => 
        { 
            AudioManager.Instance.PlaySFX(AudioName.ClickButton); 
            ChangeAvatar(); 
        });
    }

    protected override void OnShowing()
    {
        base.OnShowing();
        ClosePopupChangeName();
        ClosePopupIntroduce();
        CloseAvatarPopup();
        
        PlayerDataManager.Instance.RegisterOnDisplayNameChanged(OnDisplayNameChanged);
        PlayerDataManager.Instance.RegisterOnLevelChanged(OnLevelChanged);
        PlayerDataManager.Instance.RegisterOnExpChanged(OnExpChanged);
        PlayerDataManager.Instance.RegisterOnIntroductionChanged(OnIntroductionChanged);
        PlayerDataManager.Instance.RegisterOnAvatarIdChanged(OnAvatarIdChanged);
        _avatarLoopGridViewController.OnShow();
    }

   
    protected override void OnHiding()
    {
        base.OnHiding();
        PlayerDataManager.Instance.UnRegisterOnDisplayNameChanged(OnDisplayNameChanged);
        PlayerDataManager.Instance.UnRegisterOnLevelChanged(OnLevelChanged);
        PlayerDataManager.Instance.UnRegisterOnExpChanged(OnExpChanged);
        PlayerDataManager.Instance.UnRegisterOnIntroductionChanged(OnIntroductionChanged);
        PlayerDataManager.Instance.UnRegisterOnAvatarIdChanged(OnAvatarIdChanged);
    }

    private void OnAvatarIdChanged(string oldValue, string newValue)
    {
        _imgAvatar.sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, newValue);
    }

    private void OnExpChanged(int oldExp, int newExp)
    {
        ChangeExp(newExp);
        
    }
    private void OnIntroductionChanged(string oldValue, string newValue)
    {
        ChangeIntroduction(newValue);
    }

    private void ChangeIntroduction(string newValue)
    {
        _txtIntroduction.text = newValue;
    }

    private void ChangeExp(int newExp)
    {
        _txtExp.text = new StringBuilder().Append(newExp).Append("/").Append(PlayerDataManager.Instance.limitExpBaseLevel).ToString();
        _sliderExp.value = ((float)newExp) / PlayerDataManager.Instance.limitExpBaseLevel;
    }

    private void OnLevelChanged(int oldLevel, int newLevel)
    {
        _txtLevel.text = newLevel.ToString();
        ChangeExp(PlayerDataManager.Instance.exp);
    }

    private void OnDisplayNameChanged(string oldValue, string newValue)
    {
        _txtDisplayName.text = newValue;
    }

    private void ClosePopup()
    {
        Hide();
    }
    private void OpenPopupChangeName()
    {
        
        _popUpIntroduce.SetActive(false);
        _popUpChangeName.SetActive(true);
        AudioManager.Instance.PlaySFX(AudioName.PopupOpen);
        _inputFieldDisplayName.text = PlayerDataManager.Instance.displayName;
    }
    private void ClosePopupChangeName()
    {
        _popUpChangeName.SetActive(false);
    }
    private void ChangeDisplayName()
    {
       string displayName = _inputFieldDisplayName.text;
       if(String.IsNullOrEmpty(displayName))
       {
            return;
       }
        if (displayName.Equals(PlayerDataManager.Instance.displayName)){
            return;
        }

        APIModels.ChangeDisplayNameRequest request = new APIModels.ChangeDisplayNameRequest();
        request.displayName = displayName;
        PlayfabManager.Instance.ExecuteFunction<ChangeDisplayNameResponse>(PlayfabContant.FunctionName.ChangeDisplayName, request, true, OnChangeDisplayNameSuccess, OnChangeDisplayNameFailure);
    }

    private void OnChangeDisplayNameFailure(object arg1, string error)
    {
        UIManager.Instance.AlertManager.ShowAlertMessage(error);
    }

    private void OnChangeDisplayNameSuccess(object arg1, ChangeDisplayNameResponse response)
    {
        PlayerDataManager.Instance.displayName = response.displayName;
        ClosePopupChangeName();
        CloseAvatarPopup();
    }

    private void ClosePopupIntroduce()
    {
        _popUpIntroduce.SetActive(false);
    }
    private void ChangeIntroduce()
    {
        string introduction = _inputFieldIntroduce.text;
        if (String.IsNullOrEmpty(introduction))
        {
            return;
        }
        if (introduction.Equals(PlayerDataManager.Instance.introduction))
        {
            return;
        }

        APIModels.ChangeIntroductionRequest request = new ChangeIntroductionRequest();
        request.introduction = introduction;
        PlayfabManager.Instance.ExecuteFunction<ChangeIntroductionResponse>(PlayfabContant.FunctionName.ChangeIntroduction, request, true,
            OnChangeIntroductionSuccess, OnChangeInroducionFailure);
    }

    private void OnChangeInroducionFailure(object arg1, string error)
    {
        UIManager.Instance.AlertManager.ShowAlertMessage(error);
    }

    private void OnChangeIntroductionSuccess(object arg1, ChangeIntroductionResponse data)
    {
        PlayerDataManager.Instance.introduction = data.introduction;
        ClosePopupIntroduce();
    }

    private void OpenPopupIntroduce()
    {
        _popUpIntroduce.SetActive(true);
        AudioManager.Instance.PlaySFX(AudioName.PopupOpen);
        ClosePopupChangeName();
        CloseAvatarPopup();
        _inputFieldIntroduce.text = PlayerDataManager.Instance.introduction;
    }

    private void ChangeAvatar()
    {
        string avatarUrl = _avatarLoopGridViewController.selectedAvatar;
        if(String.IsNullOrEmpty(avatarUrl))
        {
            return;
        }
        if (avatarUrl.Equals(PlayerDataManager.Instance.avatarUrl))
        {
            return;
        }
        ChangeAvatarIdRequest request = new ChangeAvatarIdRequest();
        request.avatarUrl = avatarUrl;
        PlayfabManager.Instance.ExecuteFunction<ChangeAvatarIdResponse>(PlayfabContant.FunctionName.ChangeAvatarUrl,request, false, null, null);
        PlayerDataManager.Instance.avatarUrl = avatarUrl;
        CloseAvatarPopup();
    }

    private void CloseAvatarPopup()
    {
        _avatarPopup.SetActive(false);
    }

    private void OpenAvatarPopup()
    {
        _avatarPopup.SetActive(true);
        AudioManager.Instance.PlaySFX(AudioName.PopupOpen);
        _avatarLoopGridViewController.OnShow();
        ClosePopupChangeName();
        ClosePopupIntroduce();
    }

}
