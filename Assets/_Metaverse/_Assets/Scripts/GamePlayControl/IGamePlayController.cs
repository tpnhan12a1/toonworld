using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IGamePlayController
{
    GameRoom room { get; set; }
    void StartGame();
    void SummaryGame();
    void StopGame();
}
