using SuperScrollView;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using ToonWorld;
public class FriendGridViewController : LoopGridView, IGroupTab
{
    [SerializeField] private Sprite _spriteTop1;
    [SerializeField] private Sprite _spriteTop2;
    [SerializeField] private Sprite _spriteTop3;

    private List<Friend> _friendList;
    public void OnInit()
    {
        _friendList = new List<Friend>();
        InitGridView(_friendList.Count, OnGetFriendItemByRowColum);
    }

    private LoopGridViewItem OnGetFriendItemByRowColum(LoopGridView gridView, int index, int row, int column)
    {
       if(_friendList.Count == 0) return null;
       FriendItem friendItem = (FriendItem)gridView.NewListViewItem("FriendItem");
       friendItem.OnRefresh(_friendList[index], this);
       switch(index)
       {
            case 0:
                friendItem.SetLeaderBoard(_spriteTop1);
                break;
            case 1:
                friendItem.SetLeaderBoard(_spriteTop2);
                break;
            case 2:
                friendItem.SetLeaderBoard(_spriteTop3);
                break;
            default:
                friendItem.SetOutTop(index);
                break;
        }
        
       return friendItem;
    }

    private void OnListFriendChanged(List<Friend> friends)
    {
        _friendList = friends.Where(x => x.friendTags.Contains(PlayfabContant.FriendTag.Friend)).OrderByDescending(x => x.level).ThenBy(x => x.displayName).ToList();
        RefreshItem();
    }
    public void RefreshItem()
    {
        SetListItemCount(_friendList.Count);
        RefreshAllShownItem();
    }

    public void Select()
    {
        gameObject.SetActive(true);
        PlayerDataManager.Instance.friendManager.RegisterOnListFriendChanged(OnListFriendChanged);
    }

    public void DesSelect()
    {
        gameObject.SetActive(false);
        PlayerDataManager.Instance.friendManager.UnRegisterOnListFriendChanged(OnListFriendChanged);
    }
}
