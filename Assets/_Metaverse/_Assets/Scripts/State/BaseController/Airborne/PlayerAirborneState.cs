using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PlayerAirborneState : PlayerMotionState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.AirborneParameterHash);
        PlayerDataManager.Instance.reusableData.isAirborne = true;
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.AirborneParameterHash);

    }
    protected override void OnContactWithGround(Collider collider)
    {
        base.OnContactWithGround(collider);
        MotionStateMachine stateMachine =
            (MotionStateMachine)_stateMachine;
        stateMachine.ChangeState(stateMachine.states[StateType.LightLanding]);
    }
    protected override void OnContactWithWater(Collider collider)
    {
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.WaterIdling]);
    }
    protected override void ResetSprintState()
    {
        PlayerDataManager.Instance.reusableData.shouldSprint = false;
    }
}
