using Imba.Audio;
using Imba.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVFXController : MonoBehaviour
{
    [SerializeField] private AudioSource _audioSource;
    private GameObject _emotion;
    public void InstanceEmotion(string emotionName)
    {
        EmotionSO emotionSO = Imba.Utils.ResourceManager.Instance.GetEmotionByName(emotionName);
        if(_emotion != null)
        {
            Destroy(_emotion);
        }

        _emotion = Instantiate(emotionSO.particleObject, this.gameObject.transform);
        AudioManager.Instance.PlaySoundFromSource(emotionSO.audioName, _audioSource);
        ParticleSystem particleSystem = _emotion.GetComponent<ParticleSystem>();
        Destroy(_emotion, particleSystem.main.duration);
    }
}
