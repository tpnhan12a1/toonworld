using Imba.UI;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Button))]
public class PlacementItem : LoopGridViewItem
{
    [SerializeField]
    private Image _imageIcon;
    private Button _btn;
    private Item _item;
    private PlacementSystem _placementSystem;
    private PlacementSO _placementSO;
    private void Awake()
    {
        _btn = GetComponent<Button>();
        _btn.onClick.AddListener(OnClickItem);
    }
    private PlacementGridViewController _placementGridViewController;
    internal void OnRefresh(Item item, PlacementGridViewController placementGridViewController, PlacementSystem placementSystem)
    {
       _item = item;
       _placementGridViewController = placementGridViewController;
       _placementSystem = placementSystem;
       _placementSO = Imba.Utils.ResourceManager.Instance.GetResourceByName<PlacementSO>(new StringBuilder().Append("Home/Placement/").Append(item.itemId).ToString());
        Sprite sprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Placement, _placementSO.spriteName);
       _imageIcon.sprite = sprite;
    }
    private void OnClickItem()
    {
        _placementSystem.OnSelectPlacementObjectInUI(_placementSO.placementObject,_placementSO, _item, OnPlace);
    }

    private void OnPlace(bool status)
    {
        if(status)
        {
            _placementGridViewController.OnHide();
        }
        else
        {
            UIManager.Instance.AlertManager.ShowAlertMessage("Item cam't place this position");
        }
    }
}
