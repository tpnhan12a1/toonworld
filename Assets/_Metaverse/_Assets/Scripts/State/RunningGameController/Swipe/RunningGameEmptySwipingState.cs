using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.UI;

public class RunningGameEmptySwipingState : IState
{
    protected RunningGameSwipeStateMachine _runningGameSwipeStateMachine;
    public StateType GetStateType()
    {
        return StateType.RunningGameEmptySwipingState;
    }

    public void OnAnimationEnterEvent()
    {
       
    }

    public void OnAnimationExitsEvent()
    {
       
    }

    public void OnAnimationTransitionEvent()
    {
      
    }

    public void OnEnterState()
    {
        Debug.Log($"Enter the {GetStateType()}");
        AddInputActionsCallbacks();
        ResetXInVelocity();
        Vector3 position = _runningGameSwipeStateMachine.playerRunningGameController.gameObject.transform.position;
        position.x = _runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.GetXCurrentLocationOfLane();

        _runningGameSwipeStateMachine.playerRunningGameController.ownerNetworkTransform.Teleport(position,
             _runningGameSwipeStateMachine.playerRunningGameController.gameObject.transform.rotation, Vector3.one);
    }

  
    public void OnExitState()
    {
        Debug.Log($"Exit the {GetStateType()}");
        RemoveInputActionCallbacks();
    }

    public void OnHandleInput()
    {
        
    }
    public void OnPhysicsUpdate()
    {
       
    }

    public void OnTriggerEnter(Collider collider)
    {
        
    }

    public void OnTriggerExit(Collider collider)
    {
       
    }

    public void OnUpdate()
    {
       
    }

    public void SetStateMachine(StateMachine stateMachine)
    {
        _runningGameSwipeStateMachine = (RunningGameSwipeStateMachine)stateMachine;
    }

    protected virtual void AddInputActionsCallbacks()
    {
        PlayerInput.Instance.InputAction.Player.Move.started += OnMoveStarted;
    }


    protected virtual void RemoveInputActionCallbacks()
    {
        PlayerInput.Instance.InputAction.Player.Move.started -= OnMoveStarted;

    }

    private void OnMoveStarted(UnityEngine.InputSystem.InputAction.CallbackContext context)
    {
        Vector2 movementInput = context.ReadValue<Vector2>();
        if (movementInput == Vector2.zero) return;
        int lane = _runningGameSwipeStateMachine.playerRunningGameController.runningGameReusableData.currentLane;

        if( Vector3.Angle(movementInput,Vector3.left)< 45f)
        {
            lane -= 1;
            if (lane < 1 || lane > 3) return;
            _runningGameSwipeStateMachine.ChangeState(_runningGameSwipeStateMachine.states[StateType.RunningGameLeftSwipingState]);
            return;
        }
        if (Vector3.Angle(movementInput, Vector3.right) < 45f)
        {
            lane += 1;
            if (lane < 1 || lane > 3) return;
            _runningGameSwipeStateMachine.ChangeState(_runningGameSwipeStateMachine.states[StateType.RunningGameRightSwipingState]);
            return;
        }
        if (Vector3.Angle(movementInput, Vector3.down) < 45f)
        {
            _runningGameSwipeStateMachine.ChangeState(_runningGameSwipeStateMachine.states[StateType.RunningGameRollingState]);
            return;
        }
    }
    private void ResetXInVelocity()
    {
        Rigidbody rb = _runningGameSwipeStateMachine.playerRunningGameController.rb;
        Vector3 tarrgetVelocity = rb.velocity;
        tarrgetVelocity.x = 0f;
        rb.velocity = tarrgetVelocity;
    }

}
