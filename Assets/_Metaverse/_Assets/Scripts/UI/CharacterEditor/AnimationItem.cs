using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Animation Item", menuName = "Custom/AnimationItem")]
public class AnimationItem : ScriptableObject
{
    public string displayName;
    public string description;
    public Sprite sprite;
}
