using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameShieldDefendState : RunningGameDefendState
{
    public float eclapse = 0f;
    public override StateType GetStateType()
    {
        return StateType.RunningGameShieldDefendState;
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        eclapse += Time.deltaTime;
        if(eclapse > _runningGameDefendStateMachine.playerController.playerData.timeUseShield)
        {
            _runningGameDefendStateMachine.ChangeState(_runningGameDefendStateMachine.states[StateType.RunningGameEmptyDefendState]);
        }
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        _runningGameDefendStateMachine.playerController.runningGameReusableData.isUseShield = true;
        _runningGameDefendStateMachine.playerController.runningGameReusableData.LockSkill(GiftName.Shield);
        
    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameDefendStateMachine.playerController.runningGameReusableData.isUseShield = false;
        _runningGameDefendStateMachine.playerController.runningGameReusableData.UnLockSkill(GiftName.Shield);
        eclapse = 0f;
    }
    public override bool IsTakeDamge()
    {
        _runningGameDefendStateMachine.ChangeState(_runningGameDefendStateMachine.states[StateType.RunningGameEmptyDefendState]);
        _runningGameDefendStateMachine.playerController.runningGameReusableData.isUseShield = false;
        return base.IsTakeDamge();  
    }
}
