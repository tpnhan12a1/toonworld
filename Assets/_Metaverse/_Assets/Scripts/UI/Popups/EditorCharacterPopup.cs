﻿using Imba.UI;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public enum ButtonType
{
    None,
    BtnHair,
    BtnEyebrow,
    BtnEyes,
    BtnSkin,
    BtnCap,
    BtnGlasses,
    BtnTorso,
    BtnPants,
    BtnShoes,
    BtnGloves,
    BtnBackpack
}

public class EditorCharacterPopup : UIPopup
{
    public float transitionDuration = 0.2f;

    [SerializeField] private TabController _tabController;
    [SerializeField] private TabController _tabControllerAppearance;
    [SerializeField] private TabController _tabControllerEquipment;

    [SerializeField] private GameObject _menuAppearance;
    [SerializeField] private GameObject _menuClothes;

    [SerializeField] private ItemGridViewController _itemGridViewController;

    [SerializeField] private SpriteTab _appearanceTab;
    [SerializeField] private SpriteTab _equipmentTab;
    [SerializeField] private SpriteTab _hairTab;
    [SerializeField] private SpriteTab _capTab;

    [SerializeField] private Button _btnAppearance;
    [SerializeField] private Button _btnClothes;
    [SerializeField] private Button _btnSave;
    [SerializeField] private Button _btnClose;
    [SerializeField] private Button _btnHair;
    [SerializeField] private Button _btnEyeBrow;
    [SerializeField] private Button _btnSkin;
    [SerializeField] private Button _btnCap;
    [SerializeField] private Button _btnGlasses;
    [SerializeField] private Button _btnTorso;
    [SerializeField] private Button _btnPants;
    [SerializeField] private Button _btnShoes;
    [SerializeField] private Button _btnGloves;
    [SerializeField] private Button _btnBackpack;


    private CharacterRenderController characterRenderController;

    private List<IGroupTab> groupTabs = new List<IGroupTab>();

    protected override void OnInit()
    {
        base.OnInit();

        _itemGridViewController?.OnInit();

        groupTabs.Add(_itemGridViewController);

        _btnAppearance.onClick.AddListener(OnAppearanceButtonClicked);
        _btnClothes.onClick.AddListener(OnClothesButtonClicked);
        _btnSave.onClick.AddListener(OnSaveButtonClicked);
        _btnClose.onClick.AddListener(OnCloseButtonClicked);
        _btnHair.onClick.AddListener(OnHairButtonClicked);
        _btnEyeBrow.onClick.AddListener(OnEyeBrowButtonClicked);
        _btnSkin.onClick.AddListener(OnSkinButtonClicked);
        _btnCap.onClick.AddListener(OnCapButtonClicked);
        _btnGlasses.onClick.AddListener(OnGlassesButtonClicked);
        _btnTorso.onClick.AddListener(OnTorsoButtonClicked);
        _btnPants.onClick.AddListener(OnPantsButtonClicked);
        _btnShoes.onClick.AddListener(OnShoesButtonClicked);
        _btnGloves.onClick.AddListener(OnGlovesButtonClicked);
        _btnBackpack.onClick.AddListener(OnBackpackButtonClicked);
    }

    private void ShowContentOfTab(IGroupTab groupTab)
    {
        groupTabs.ForEach(tab => {
            if (tab == groupTab)
            {
                tab.Select();
            }
            else
            {
                tab.DesSelect();
            }
        });
    }

    private void OnAppearanceButtonClicked()
    {
        _itemGridViewController.isMenuAppearance = true;

        ShowMenu(_itemGridViewController.isMenuAppearance);
        ShowContentOfTab(_itemGridViewController);
        _tabControllerAppearance.SelectedTab(_hairTab);

        _itemGridViewController.SetCurrentBtn(ButtonType.BtnHair);
        _itemGridViewController.RefreshItem();
    }

    private void OnClothesButtonClicked()
    {
        _itemGridViewController.isMenuAppearance = false;

        ShowMenu(_itemGridViewController.isMenuAppearance);
        ShowContentOfTab(_itemGridViewController);
        _tabControllerEquipment.SelectedTab(_capTab);

        _itemGridViewController.SetCurrentBtn(ButtonType.BtnCap);
        _itemGridViewController.RefreshItem();
    }

    private void ShowMenu(bool isAppearance)
    {
        _menuAppearance.gameObject.transform.DOScaleY(isAppearance ? 1 : 0, 0).SetEase(Ease.InCirc);
        _menuClothes.gameObject.transform.DOScaleY(!isAppearance ? 1 : 0, 0).SetEase(Ease.InCirc);
    }

    private void OnSaveButtonClicked()
    {
        SavePlayerItem(characterRenderController.characterRender.playerItem);
    }

    private void OnCloseButtonClicked()
    {
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.EditorCharacterPopup, true);
    }
    private void OnBackpackButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnBackpack);

    }

    private void OnGlovesButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnGloves);
    }

    private void OnShoesButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnShoes);
    }

    private void OnPantsButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnPants);
    }

    private void OnTorsoButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnTorso);
    }

    private void OnGlassesButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnGlasses);
    }

    private void OnCapButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnCap);
    }

    private void OnSkinButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnSkin);
    }

    private void OnEyeBrowButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnEyebrow);
    }

    private void OnHairButtonClicked()
    {
        _itemGridViewController.SetCurrentBtn(ButtonType.BtnHair);
    }

    protected override void OnShowing()
    {
        base.OnShowing();

        ShowMenu(true);
        ShowContentOfTab(_itemGridViewController);
        _tabController.SelectedTab(_appearanceTab);
        _tabControllerAppearance.SelectedTab(_hairTab);
        PlayerDataManager.Instance.inventoryManager.RegisterOnListItemChanged(OnListItemsChanged);
        SGSceneManager.Instance.LoadSceneAdditiveAsync(SceneName.ScenePreviewCharacter, OnLoadSceneSuccess);
    }

    private void OnListItemsChanged(List<Item> listItems)
    {
        _itemGridViewController.RefreshItem();
    }

    private void OnLoadSceneSuccess()
    {
        characterRenderController = ScenePreviewCharacterManager.Instance.characterRenderController;

        _itemGridViewController.OnInitCharacterRenderController(characterRenderController);

        characterRenderController.characterRender.LoadPlayerItem(PlayerDataManager.Instance.playerItem);

        UIManager.Instance.PopupManager.HidePopup(UIPopupName.QuickChatPopup, true);
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.MainPopup, true);
    }

    protected override void OnHiding()
    {
        base.OnHiding();
        SGSceneManager.Instance.UnLoadSceneAdditive(SceneName.ScenePreviewCharacter.ToString());
        PlayerDataManager.Instance.inventoryManager.UnRegisterOnListItemChanged(OnListItemsChanged);
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.QuickChatPopup, true);
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.MainPopup, true);
    }

    private void SavePlayerItem(APIModels.PlayerItem item)
    {
        APIModels.SavePlayerItemRequest savePlayerItem = new APIModels.SavePlayerItemRequest
        {
            playerItem = item,
        };
        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(PlayfabContant.FunctionName.SavePlayerItem,
            savePlayerItem,
            true,
            OnSaveSuccess,
            OnSaveFailure);
    }

    private void OnSaveSuccess(object arg1, object arg2)
    {
        PlayerDataManager.Instance.playerItem = characterRenderController.characterRender.playerItem;
    }

    private void OnSaveFailure(object arg1, string error)
    {
        UIManager.Instance.HideLoading();
        UIManager.Instance.AlertManager.ShowAlertMessage(error);
    }

    protected override void OnHidden()
    {
        base.OnHidden();
        groupTabs.ForEach(tab => { tab.DesSelect(); });
    }
}