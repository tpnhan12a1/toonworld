using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChatModePopup : UIPopup
{
    [SerializeField] private Button _btnClose;
    [SerializeField] private Button _btnChangeGlobal;
    [SerializeField] private Button _btnChangeGuild;
    [SerializeField] private Button _btnChangeParty;

    [SerializeField] private SpriteTab _spriteTabGlobal;
    [SerializeField] private SpriteTab _spriteTabGuild;
    [SerializeField] private SpriteTab _spriteTabParty;
    [SerializeField] private TabController _tabController;

    private TypeChat _type = TypeChat.Global;
    protected override void OnInit()
    {
        base.OnInit();
        _btnClose.onClick.AddListener(ClosePopup);
        _btnChangeGlobal.onClick.AddListener(ChangeGlobal);
        _btnChangeGuild.onClick.AddListener(ChangeGuild);
        _btnChangeParty.onClick.AddListener(ChangeParty);
        
    }

    

    protected override void OnShowing()
    {
        base.OnShowing();
        PlayerDataManager.Instance.globalReusableData.RegisterOnTypeChatChanged(OnTypeChatChanged);
        SelectCurrentChat();
    }
    protected override void OnHidden()
    {
        base.OnHidden();
        PlayerDataManager.Instance.globalReusableData.UnRegisterOnTypeChatChanged(OnTypeChatChanged);
    }
    private void OnTypeChatChanged(TypeChat arg1, TypeChat arg2)
    {
        _type = arg2;
    }
    private void SelectCurrentChat()
    {
        switch (_type)
        {
            case TypeChat.Global:
                _tabController.SelectedTab(_spriteTabGlobal);
                break;
            case TypeChat.Guild:
                _tabController.SelectedTab(_spriteTabGuild);
                break;
            case TypeChat.Party:

                if (PlayerDataManager.Instance.partyData.party == null) return;
                _tabController.SelectedTab(_spriteTabParty);
                break;
            default:
                _tabController.DeSelectTab();
                break;
        }
    }

    private void ChangeParty()
    {
        if (PlayerDataManager.Instance.partyData.party == null)
        {
            SelectCurrentChat();
            return;
        }
        PlayerDataManager.Instance.globalReusableData.typeChat = TypeChat.Party;
            _type = TypeChat.Party;
        Hide();
    }

    private void ChangeGuild()
    {
        if (PlayerDataManager.Instance.guildManager.isHaveGuild)
        {
            PlayerDataManager.Instance.globalReusableData.typeChat = TypeChat.Guild;
            _type = TypeChat.Guild;
            Hide();
        }
        else
        {
            UIManager.Instance.AlertManager.ShowAlertMessage("Create or join a guild!");
            SelectCurrentChat();
        }
    }

    private void ChangeGlobal()
    {
        PlayerDataManager.Instance.globalReusableData.typeChat = TypeChat.Global;
        _type = TypeChat.Global;
        Hide();
    }

    private void ClosePopup()
    {
        Hide();
    }
}
