using Mono.CSharp;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnPosition : MonoBehaviour
{
    [SerializeField]
    private List<Transform> _spawnTransform;
    public Transform getSpawnTransform()
    {
       Transform tf = _spawnTransform.Shuffle().FirstOrDefault();
        return tf;
    }
    public Transform GetTranform( int index)
    {
        if(_spawnTransform.Count >= index)
        {
            return _spawnTransform[index];
        }
        return null;
    }
}
