using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWaterIdlingState : PlayerWaterState
{
    public override StateType GetStateType()
    {
        return StateType.WaterIdling;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        PlayerDataManager.Instance.reusableData.movementSpeedModifier = 0f;
        ResetVelocity();
        StartAnimation(PlayerDataManager.Instance.animationData.WaterIdlingParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.WaterIdlingParameterHash);
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        if (PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero) return;
        OnMove();
    }
}
