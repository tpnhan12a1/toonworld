﻿using System.Collections.Generic;
using UnityEngine;
using System.Text;
using static APIModels;
using Imba.UI.Animation;
using System;
using System.Reflection;
using System.Resources;

public class CharacterRender : MonoBehaviour
{
    Dictionary<ItemType, Render> _characterRenders = new Dictionary<ItemType, Render>();

    private PlayerItem _playerItem;

    List<HidePart> _partsToHide = new List<HidePart>();

    [SerializeField]
    private List<ItemRender> _itemRenders;

    [SerializeField]
    private Material _skinMaterial;

    [SerializeField]
    private Material _eyesMaterial;

    [SerializeField]
    private GameObject _feet;

    [SerializeField]
    private GameObject _hand;

    [SerializeField]
    private GameObject _hair;

    public PlayerItem playerItem { get { return _playerItem; } }

    public void Awake()
    {
        _itemRenders.ForEach(i =>
        {
            Render render = new Render();
            if (i.skinnedmeshRenderer == null)
            {
                render.meshFilter = i.meshFilter;
                render.meshRenderer = i.meshRenderer;
            }
            else
            {
                render.skinnedMeshRenderer = i.skinnedmeshRenderer;
            }
            _characterRenders.Add(i.itemType, render);
        });
    }

    public void ChangeMeshWithItemType(BaseItem item)
    {
        if (item == null) return;

        if (_characterRenders.TryGetValue(item.itemType, out Render render))
        {
            List<Material> materials = new List<Material>();

            HidePartsWhenEquipped(item);

            switch (item.itemType)
            {
                case ItemType.hair:
                    _playerItem.hair = item.name;
                    _hair.SetActive(true);
                    materials.Add(item.material);
                    break;
                case ItemType.eyebrow:
                    _playerItem.eyeBrow = item.name;
                    materials.Add(item.material);
                    break;
                case ItemType.eyes:
                    _playerItem.eyes = item.name;
                    materials.Add(item.material);
                    break;
                case ItemType.skin:
                    _playerItem.skin = item.name;
                    materials.Add(item.material);
                    break;
                case ItemType.cap:
                    _playerItem.cap = item.name;
                    if (item.isItemColorIncludeSkinColor)
                    {
                        materials.Add(_skinMaterial);
                        materials.Add(item.material);
                    }
                    else
                    {
                        materials.Add(item.material);
                    }
                    break;
                case ItemType.glasses:
                    _playerItem.glasses = item.name;
                    if (item.isItemColorIncludeSkinColor)
                    {
                        materials.Add(_skinMaterial);
                        materials.Add(item.material);
                    }
                    else
                    {
                        materials.Add(item.material);
                    }
                    break;
                case ItemType.torso:
                    _playerItem.torso = item.name;
                    if (item.isItemColorIncludeSkinColor)
                    {
                        materials.Add(_skinMaterial);
                        materials.Add(item.material);
                    }
                    else
                    {
                        materials.Add(item.material);
                    }
                    break;
                case ItemType.pants:
                    _playerItem.pants = item.name;
                    if (item.isItemColorIncludeSkinColor)
                    {
                        materials.Add(_skinMaterial);
                        materials.Add(item.material);
                    }
                    else
                    {
                        materials.Add(item.material);
                    }
                    break;
                case ItemType.shoes:
                    _playerItem.shoes = item.name;
                    if (item.isItemColorIncludeSkinColor)
                    {
                        materials.Add(_skinMaterial);
                        materials.Add(item.material);
                    }
                    else
                    {
                        materials.Add(item.material);
                    }
                    break;
                case ItemType.gloves:
                    _playerItem.gloves = item.name;
                    materials.Add(item.material);
                    break;
                case ItemType.backpack:
                    _playerItem.backpack = item.name;
                    materials.Add(item.material);
                    break;                                             
            }
            render.ChangeRender(item.mesh, materials.ToArray());
        }
    }

    private void HidePartsWhenEquipped(BaseItem item)
    {
        foreach (HidePart hiddenPart in item.hideWhenEquipped)
        {
            if (hiddenPart == HidePart.hair)
            {
                _partsToHide.Add(hiddenPart);
                _hair.SetActive(false);
            }
            if (hiddenPart == HidePart.feet)
            {
                _partsToHide.Add(hiddenPart);
                _feet.SetActive(false);
            }
            if (hiddenPart == HidePart.hand)
            {
                _partsToHide.Add(hiddenPart);
                _hand.SetActive(false);
            }
        }
    }

    public void ChangeSkinMaterial(Material material)
    {
        _skinMaterial = material;
    }

    public void LoadPlayerItem(PlayerItem playerItem)
    {
        _playerItem = playerItem?.Clone() as PlayerItem;
        if(_playerItem != null)
        {
            ChangeMeshWithPlayerItem(_playerItem);
        }
    }

    private void ChangeMeshWithPlayerItem(PlayerItem playerItem)
    {
        foreach (FieldInfo field in playerItem.GetType().GetFields())
        {
            object fieldValue = field.GetValue(playerItem);
            string baseItemName = fieldValue == null ? string.Empty : fieldValue.ToString();
            BaseItem baseItem = Imba.Utils.ResourceManager.Instance.GetResourceByName<BaseItem>("Item/" + baseItemName);

            ChangeMeshWithItemType(baseItem);
        }
    }

    [System.Serializable]
    public class ItemRender
    {
        public ItemType itemType;
        public SkinnedMeshRenderer skinnedmeshRenderer;
        public MeshFilter meshFilter;
        public MeshRenderer meshRenderer;
    }

    public class Render
    {
        public SkinnedMeshRenderer skinnedMeshRenderer;
        public MeshFilter meshFilter;
        public MeshRenderer meshRenderer;
        public bool isIncludeSkinColor;
        public GameObject gameObject;

        public void ChangeRender(Mesh mesh, Material[] materials)
        {
            if (skinnedMeshRenderer != null)
            {
                skinnedMeshRenderer.sharedMesh = mesh;
                skinnedMeshRenderer.materials = materials;
            }
            else
            {
                if(meshFilter != null)
                {
                    meshFilter.mesh = mesh;
                    meshRenderer.materials = materials;
                }
            }
        }

        public Material[] GetAllMaterials()
        {
            if (skinnedMeshRenderer != null)
            {
                return skinnedMeshRenderer.materials;
            }
            else
            {
                return meshRenderer.materials;
            }
        }
        public void ChangeMaterial(Material[] materials)
        {
            if (skinnedMeshRenderer != null)
            {
                skinnedMeshRenderer.materials = materials;
            }
            else
            {
                meshRenderer.materials = materials;
            }
        }
    }
}
