[System.Serializable]
public class Equipment
{
    public string cap;
    public string glasses;
    public string torso;
    public string pants;
    public string shoes;
    public string gloves;
    public string backpack;
}