using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public abstract class AIStoppingMonoState : AIMotionMonoState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        residentStateMachine.residentController.navMeshAgent.isStopped= true;
        StartAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsStoppingParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsStoppingParameterHash);
    }
    public override void OnAnimationTransitionEvent()
    {
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        //Change state idling
        //residentStateMachine.ChangeState()
    }
}
