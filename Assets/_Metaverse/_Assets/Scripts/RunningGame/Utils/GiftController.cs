using Imba.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class GiftController : MonoBehaviour
{
    [SerializeField]
    private GiftName _giftName;

    private Vector3 currentPosition = Vector3.zero;
    private NetworkObject networkObject;
    [SerializeField] private float _distanceToRelease = 30f;
    private void OnEnable()
    {
        TryGetNetworkObject();
    }
    internal void SetPosition(Vector3 position)
    {
        currentPosition = position;
        gameObject.transform.position = position;
    }
    private void TryGetNetworkObject()
    {
        networkObject = NetworkManager.Singleton.LocalClient.PlayerObject;
    }
    private void OnTriggerEnter(Collider other)
    {
        GiftName dataAdd = _giftName;
        if(_giftName == GiftName.Random)
        {
            List<GiftName> list = EnumUtils.GetAllEnumValue<GiftName>();
            list.Remove(GiftName.Random);
            dataAdd = EnumUtils.GetRandomEnumValueInList<GiftName>(list);
        }
        other.gameObject.GetComponent<PlayerRunningGameController>().runningGameReusableData.AddSkillData(dataAdd);
        AudioManager.Instance.PlaySFX(AudioName.Collection);
        SceneRunningGameManager.Instance.ReturnPresent(this, _giftName);
    }
    public void Update()
    {
        if (currentPosition == Vector3.zero) return;
        if (networkObject == null)
        {
            TryGetNetworkObject();
        }
        if (networkObject == null) return;
        if (currentPosition.z + _distanceToRelease < networkObject.transform.position.z)
            SceneRunningGameManager.Instance.ReturnPresent(this, _giftName);
    }
}
