using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ToonWorld;
[System.Serializable]
public class FriendManager
{
    private List<Friend> friends;
    public delegate void Consumer(Friend f);
    public delegate bool Predicate(Friend f);

    public Action<List<Friend>> OnListFriendChanged;
    public void RegisterOnListFriendChanged(Action<List<Friend>> onListFriendChanged)
    {
        OnListFriendChanged += onListFriendChanged;
        onListFriendChanged?.Invoke(friends);
    }
    public void UnRegisterOnListFriendChanged(Action<List<Friend>> onListFriendChanged)
    {
        OnListFriendChanged -= onListFriendChanged;
    }
    public FriendManager() 
    {
        friends = new List<Friend>();
    }

    public void InitListFriend(List<ToonWorld.Friend> friends)
    {
        if(friends != null)
        {
            this.friends = friends.OrderByDescending(x => x.level).ThenBy(x => x.displayName).ToList();
            OnListFriendChanged?.Invoke(this.friends);
        }
       
    }

    public void DirtyAddFriend(Friend f)
    {
        Friend friend = friends.Where(x => x.friendPlayfabId == f.friendPlayfabId).FirstOrDefault();
        if(friend != null)
        {
            friend.avatarUrl = f.avatarUrl;
            friend.displayName = f.displayName;
            friend.level = f.level;
            friend.friendTags = f.friendTags;
        }
        else
        {
            friends.Add(f);
        }
       
        this.friends.OrderByDescending(x => x.level).ThenBy(x => x.displayName).ToList();
    }
    public void AddFriend(Friend f)
    {
        Friend friend = friends.Where(x => x.friendPlayfabId == f.friendPlayfabId).FirstOrDefault();
        if (friend != null)
        {
            friend.avatarUrl = f.avatarUrl;
            friend.displayName = f.displayName;
            friend.level = f.level;
            friend.friendTags = f.friendTags;
        }
        else
        {
            friends.Add(f);
        }
        this.friends.OrderByDescending(x => x.level).ThenBy(x => x.displayName).ToList();
        OnListFriendChanged?.Invoke(this.friends);
    }
    public void RemoveFriend(Friend f)
    {
        friends.Remove(f);
        OnListFriendChanged?.Invoke(this.friends);
    }
    public void UpdateFriendData(Friend f)
    {
        Friend friend = friends.Where(x => x.friendPlayfabId == f.friendPlayfabId).FirstOrDefault();
        if(friend != null)
        {
            friend.avatarUrl = f.avatarUrl;
            friend.displayName = f.displayName;
            friend.friendTags = f.friendTags;
            friend.level = f.level;
            OnListFriendChanged?.Invoke(this.friends);
        }
    }
    public bool ContainInList(string friendPlayfabId)
    {
        return friends.Where( x =>x.friendPlayfabId.Equals(friendPlayfabId)).Any();
    }
    public bool IsFriend(string friendPlayfabId)
    {
        return friends.Where( x=>x.friendPlayfabId.Equals(friendPlayfabId) && x.friendTags.Contains(PlayfabContant.FriendTag.Friend)).Any();
    }
    public Friend GetFriend(string friendfabId)
    {
        foreach (Friend friend in friends)
        {
            if(friend.friendPlayfabId.Equals(friendfabId) && friend.friendTags.Contains(PlayfabContant.FriendTag.Friend))
                return friend;
        }
        return null;
    }
    public Friend GetFriendByPlayfabId(string playfabId)
    {
        return friends.Where(x =>x.friendPlayfabId == playfabId).FirstOrDefault();
    }
    public List<Friend> GetFriends()
    {
        return friends.Where(x => 
            x.friendTags
            .Contains(PlayfabContant.FriendTag.Friend))
            .OrderByDescending(x => x.level)
            .ThenBy( x =>x.displayName)
            .ToList();
    }
    public List<Friend> GetAllList()
    {
        return friends;
    }
    public void ForEach(Consumer action)
    {
        friends.ForEach(f => action(f));
    }
    public List<Friend> Filter(Predicate predicate)
    {
        return friends.Where(friends => predicate(friends)).ToList();
    }
    public int GetFriendCount()
    {
        return friends.Count;
    }
}
