using DG.Tweening;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PlacementGridViewController : LoopGridView
{
    [SerializeField] private Button _btnClose;
    [SerializeField] private float durationAnim = 0.5f;
    [SerializeField] private float offsetAnim = 100f;
    [SerializeField] private RectTransform _parentRectTransform;
    private PlacementSystem _placementSystem;
    private List<Item> _items;

    public void Awake()
    {
        _btnClose.onClick.AddListener(CloseThis);
    }

    private void CloseThis()
    {
        OnHide();
    }

    public void OnInit()
    {
        _items = new List<Item>();
        InitGridView(_items.Count, OnGetPlacementItemByRowColumn);
    }

    public void OnShow(PlacementSystem placementSystem)
    {
        _placementSystem = placementSystem;
        _parentRectTransform.gameObject.SetActive(true);
        _placementSystem.placementManager.RegisterOnPlacementDataChanged(OnPlacementPlaceDataChanged);
        _parentRectTransform.DOAnchorPosX(-_parentRectTransform.GetWidth() / 2, durationAnim).SetEase(Ease.InSine);
    }

    public void OnHide()
    {
        _placementSystem.placementManager.RegisterOnPlacementDataChanged(OnPlacementPlaceDataChanged);
        _parentRectTransform.DOAnchorPosX(_parentRectTransform.GetWidth()+ offsetAnim, durationAnim).SetEase(Ease.InSine).OnComplete(() =>
        {
            _parentRectTransform.gameObject.SetActive(false);
        });
    }

    private LoopGridViewItem OnGetPlacementItemByRowColumn(LoopGridView gridView, int index, int row, int column)
    {
        if(_items.Count == 0) { return null; }
        PlacementItem placementItem = (PlacementItem) gridView.NewListViewItem("PlacementItem");
        placementItem.OnRefresh(_items[index], this, _placementSystem);
        return placementItem;
    }
    
    private void OnPlacementPlaceDataChanged(List<Placement> placements)
    {
        List<Item> items = PlayerDataManager.Instance.inventoryManager.items
            .Where(x => x.itemClass == PlayfabContant.ItemClass.Placement)
            .Where(x => !_placementSystem.placementManager.Contain(x.itemId, x.itemInstanceId)).ToList();
        _items = items;
        OnRefresh();
    }
    public void OnRefresh()
    {
        SetListItemCount(_items.Count);
        RefreshAllShownItem();
    }
}
