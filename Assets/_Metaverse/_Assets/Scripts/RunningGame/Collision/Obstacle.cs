﻿using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using System;
using Unity.VisualScripting;
using Imba.Audio;
using Imba.UI;
using UnityEditor;
using DG.Tweening;

namespace RunningGame
{
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent (typeof(Collider))]
    [RequireComponent(typeof(Animation))]
    public class Obstacle : MonoBehaviour
    {
        [SerializeField] private LayerMask _collisionCheckLayer;
        [SerializeField] private LayerMask _shieldLayer;
        [SerializeField] private float _distanceToRelease = 30f;
        [SerializeField] private ObstacleName _obstacleName;
        [SerializeField] private AudioName _audioNameCollision = AudioName.ObstacleBinCollision;
        Animation _animationOnCollision;
        private Collider _colliderCheckCollision;

        private AudioSource _audioSource;
       
        private NetworkObject networkObject;
        private Vector3 currentPosition = Vector3.zero;
        

        public ObstacleName obstacleName { get { return obstacleName; } }
        private void Awake()
        {
            _audioSource = GetComponent<AudioSource>();
            _colliderCheckCollision = GetComponent<Collider>();
            _animationOnCollision = GetComponent<Animation>();
           
        }
        public void SetPosition(Vector3 position)
        {
            ResetData();
            currentPosition = position;
            gameObject.transform.position = position;
        }
        public void OnEnable()
        {
            TryGetNetworkObject();
        }

        private void ResetData()
        {
            _colliderCheckCollision.enabled = true;
            _animationOnCollision.Rewind();
            _animationOnCollision.Play();
            _animationOnCollision.Sample();
           
            _animationOnCollision.Stop();
        }

        private void TryGetNetworkObject()
        {
            networkObject = NetworkManager.Singleton.LocalClient.PlayerObject;
        }

        public void OnTriggerEnter(Collider other)
        {
            _colliderCheckCollision.enabled = false;
            _animationOnCollision.Play();
            AudioManager.Instance.PlaySoundFromSource(_audioNameCollision,_audioSource);

            if (LayerData.ContainLayer(_collisionCheckLayer, other.gameObject.layer))
            {
                PlayerRunningGameController runningGameController = other.gameObject.transform.parent.GetComponentInParent<PlayerRunningGameController>();
                runningGameController.TakeDamge();
                return;
            }
        }

        public void Update()
        {
            if (currentPosition == Vector3.zero) return;
            if (networkObject == null)
            {
                TryGetNetworkObject();
            }
            if (networkObject == null) return;
            if (currentPosition.z + _distanceToRelease < networkObject.transform.position.z)
                SceneRunningGameManager.Instance.ReturnObstacle(this, _obstacleName);
        }
        public void OnAnimationExitPlay()
        {
            Debug.Log("En Animation");
            SceneRunningGameManager.Instance.ReturnObstacle(this, _obstacleName);
        }
    }
}