using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class LookAtMainCamera : MonoBehaviour
{
    [SerializeField] private bool  isReverse = false;

    private Transform cameraTransfrom;
    public void LateUpdate()
    {
        cameraTransfrom = CameraManager.Instance.mainCamera.transform;
        if (isReverse)
        {
            Vector3 lookPoint = transform.position + (transform.position - cameraTransfrom.position);
            transform.LookAt(lookPoint);
        }
        else
        {
            transform.LookAt(CameraManager.Instance.mainCamera.transform.position);
        }
       
    }
}
