using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;

public class OnFingerGlide : OnScreenControl, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    [InputControl(layout = "Vector2")]
    [SerializeField]
    private string m_ControlPath;

    private Vector2 _pointerDownPos;
    protected override string controlPathInternal
    {
        get => m_ControlPath;
        set => m_ControlPath = value;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Vector2 dir = (eventData.position - _pointerDownPos).normalized;
        if((eventData.position - _pointerDownPos).magnitude >0.5)
        {
            SendValueToControl(dir);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
       _pointerDownPos = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
       SendValueToControl(Vector2.zero);
    }
}
