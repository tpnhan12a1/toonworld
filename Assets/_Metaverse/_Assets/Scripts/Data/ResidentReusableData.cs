using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResidentReusableData
{
    public float speedModifier = 0f;
    public Vector3 targetLookAt = Vector3.zero;
    public Collider playerCollider = null;
    public bool isIntractWithPlayer = false;
}
