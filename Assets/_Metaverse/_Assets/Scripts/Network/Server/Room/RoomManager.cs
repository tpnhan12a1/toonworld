using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RoomManager<R> : IRoomManager<R> where R : Room
{
    public Dictionary<ulong, R> rooms;

    public RoomManager()
    {
        rooms = new Dictionary<ulong, R>();
    }
    public void AddRoom(R room)
    {
       
        room.roomId = (ulong) rooms.Count();
        Debug.Log($"Room with roomId {room.roomId} has added");
        rooms.Add(room.roomId, room);
    }

    public void AddRooms(ICollection<R> rooms)
    {
        foreach(var room in rooms)
        {
            
            room.roomId = (ulong)rooms.Count();
            Debug.Log($"Room with roomId {room.roomId} has added");
            this.rooms.Add(room.roomId, room);
        }
    }

    public bool ContainRoom(ulong roomId)
    {
       return rooms.ContainsKey(roomId);
    }

    public bool ContainsRoom(R room)
    {
       return rooms.ContainsKey((ulong)room.roomId);
    }

    public void ForEach(IRoomManager<R>.Consumer p)
    {
       rooms.Values.ToList().ForEach(x => p(x));
    }

    public Room GetRoom(ulong roomId)
    {
        if(rooms.TryGetValue(roomId,out  var room)) return room;
        return null;
    }

    public List<R> GetRoom(IRoomManager<R>.Predicate p)
    {
        return rooms.Values.Where(x => p(x)).ToList();
    }

    public int GetRoomCount()
    {
        return rooms.Count;
    }

    public List<R> GetRoomList()
    {
        return rooms.Values.ToList();
    }

    public bool IsAvalable()
    {
        return true;
    }

    public void RemoveRoom(ulong roomId)
    {
        rooms.Remove(roomId);
        Debug.Log($"Remove room with roomId = {roomId}");
    }

    public void RemoveRoom(R room)
    {
        rooms.Remove((ulong)room.roomId);
        Debug.Log($"Remove room with roomId = {room.roomId}");
    }
}
