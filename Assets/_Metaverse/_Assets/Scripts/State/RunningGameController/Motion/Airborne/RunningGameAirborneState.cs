using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public abstract class RunningGameAirborneState : RunningGameMotionState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        _runningGameStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.AirborneParameterHash);
        _runningGameStateMachine.StartFloatAnimation(PlayerDataManager.Instance.animationData.FAirParameterHash, 1);
        _runningGameStateMachine.playerRunningGameController.runningGameReusableData.isGrounded = false;
    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.AirborneParameterHash);
    }
    protected override void OnContactWithGrounded(Collider collider)
    {
        base.OnContactWithGrounded(collider);
        _runningGameStateMachine.ChangeState(_runningGameStateMachine.states[StateType.RunningGameRunningState]);
    }
}
