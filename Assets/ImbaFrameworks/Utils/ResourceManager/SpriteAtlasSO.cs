﻿
using System;
using UnityEngine;
using UnityEngine.U2D;

[CreateAssetMenu(fileName = "New atlas data", menuName = "Custom/Atlas")]
public class SpriteAtlasSO: ScriptableObject
{
    public AtlasName atlasName;
    public SpriteAtlas atlas;
}
