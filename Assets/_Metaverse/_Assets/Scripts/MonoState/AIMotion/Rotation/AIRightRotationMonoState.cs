using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIRightRotationMonoState : AIRotationMonoState
{
    public override StateType GetStateType()
    {
        return StateType.AIRightRotationMonoState;
    }
}
