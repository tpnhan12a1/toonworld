using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWaterUpState : PlayerWaterState
{
    public override StateType GetStateType()
    {
        return StateType.WaterUpState;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.WaterUpParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.WaterUpParameterHash);
    }
    public override void OnAnimationTransitionEvent()
    {
        OnMove();
    }
    protected override void OnMove()
    {
        if(PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero)
        {
            MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
            stateMachine.ChangeState(stateMachine.states[StateType.WaterIdling]);
            return;
        }
        base.OnMove();

    }
    protected override void AddInputActionsCallbacks()
    {
       
    }
    protected override void RemoveInputActionCallbacks()
    {
       
    }
}
