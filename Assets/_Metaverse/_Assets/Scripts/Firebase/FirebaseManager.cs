using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using Firebase;
using Firebase.Extensions;
using Newtonsoft.Json;
using Firebase.Messaging;
using System;

public class FirebaseManager : ManualSingletonMono<FirebaseManager>
{
    DatabaseReference _databaseReference;
    private string _deviceToken = string.Empty;
    public string deviceToken { get { return _deviceToken; } }
    public DatabaseReference databaseReference { get { return _databaseReference; } }
    private void Start()
    {

        FirebaseMessaging.TokenReceived += OnTokenReceived;
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                PlayfabManager.Instance.OnLoginSuccess += OnLoginSuccess;
                Firebase.Messaging.FirebaseMessaging.TokenReceived += OnTokenReceived;
                Firebase.Messaging.FirebaseMessaging.MessageReceived += OnMessageReceived;

                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });
        

    }
    
    protected override void OnDestroy()
    {
        PlayfabManager.Instance.OnLoginSuccess -= OnLoginSuccess;
        Firebase.Messaging.FirebaseMessaging.TokenReceived -= OnTokenReceived;
        Firebase.Messaging.FirebaseMessaging.MessageReceived -= OnMessageReceived;
    }
    public void OnTokenReceived(object sender, Firebase.Messaging.TokenReceivedEventArgs token)
    {
        UnityEngine.Debug.Log("Received Registration Token: " + token.Token);
    }

    public void OnMessageReceived(object sender, Firebase.Messaging.MessageReceivedEventArgs e)
    {
        UnityEngine.Debug.Log("Received a new message from: " + e.Message.From);
    }
    public void OnLoginSuccess(string playfabId)
    {
        _databaseReference = FirebaseDatabase.DefaultInstance.RootReference;
    }
    public void OnLogOut(string playfabId)
    {
    }

    public int GetUnitKey(string keyOne, string keyTwo)
    {
        List<string> keys = new List<string>();
        keys.Add(keyOne);
        keys.Add(keyTwo);
        keys.Sort();
        return keys.GetHashCode();    
    }
    public void SendMessage(string friendPlayfabId, Message message)
    {
        DatabaseReference databaseReference = _databaseReference.Child(PlayfabManager.Instance.PlayfabID).Child("chats").Child(friendPlayfabId).Push();
        databaseReference.SetRawJsonValueAsync(JsonConvert.SerializeObject(message));

        message.isSend = false;
        databaseReference = _databaseReference.Child(friendPlayfabId).Child("chats").Child(PlayfabManager.Instance.PlayfabID).Push();
        databaseReference.SetRawJsonValueAsync(JsonConvert.SerializeObject(message));

    }
}
public class Message
{
    public string receiver;
    public string sender;
    public string content;
    public bool isSend;
}