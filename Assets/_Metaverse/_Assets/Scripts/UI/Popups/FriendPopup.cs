using Imba.Audio;
using Imba.UI;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FriendPopup : UIPopup
{
    [SerializeField] private TabController _tabController;
    [SerializeField] private FriendGridViewController _friendGridViewController;
    [SerializeField] private FriendInviteGridViewController _friendInviteGridViewController;
    [SerializeField] private FriendReceiveGridViewController _friendReceiveGridViewController;
    [SerializeField] private FriendSuggestGridViewController _friendSuggestGridViewController;

    [SerializeField] private SpriteTab _friendTab;
    [SerializeField] private Button _friendButton;
    [SerializeField] private Button _friendInviteButton;
    [SerializeField] private Button _friendReceiveButton;
    [SerializeField] private Button _friendSuggestButton;

    [SerializeField] private Button _btnClose;


    private List<IGroupTab> groupTabs = new List<IGroupTab>();
    protected override void OnInit()
    {
        base.OnInit();
        _friendGridViewController?.OnInit();
        _friendReceiveGridViewController?.OnInit();
        _friendInviteGridViewController?.OnInit();
        _friendSuggestGridViewController?.OnInit();

        groupTabs.Add(_friendGridViewController);
        groupTabs.Add(_friendInviteGridViewController);
        groupTabs.Add(_friendReceiveGridViewController);
        groupTabs.Add(_friendSuggestGridViewController);

        _friendButton.onClick.AddListener(OnSelectFriendTab);
        _friendInviteButton.onClick.AddListener(OnSelectInviteTab);
        _friendReceiveButton.onClick.AddListener(OnSelectReceiveTab);
        _friendSuggestButton.onClick.AddListener(OnSelectSuggestTab);
        _btnClose.onClick.AddListener(CloseFriendPopup);
    }

    private void CloseFriendPopup()
    {
        AudioManager.Instance.PlaySFX(AudioName.PopupClose);
        Hide();
    }

    private void OnSelectFriendTab()
    {
        ShowContentOfTab(_friendGridViewController);
        _friendGridViewController.RefreshItem();
    }
    private void OnSelectInviteTab()
    {
        ShowContentOfTab(_friendInviteGridViewController);
        _friendInviteGridViewController.RefreshItem();
    }
    private void OnSelectReceiveTab()
    {
        ShowContentOfTab(_friendReceiveGridViewController);
        _friendReceiveGridViewController.RefreshItem();
    }
    private void OnSelectSuggestTab()
    {
        ShowContentOfTab(_friendSuggestGridViewController);
    }
    private void ShowContentOfTab(IGroupTab groupTab)
    {
        groupTabs.ForEach(tab => {
            if (tab == groupTab)
            {
                tab.Select();
            }
            else
            {
                tab.DesSelect();
            }
        });
    }

    protected override void OnShowing()
    {
        base.OnShowing();
        ShowContentOfTab(_friendGridViewController);
        _tabController.SelectedTab(_friendTab);

        APIModels.CommonRequest request = new APIModels.CommonRequest();
        PlayfabManager.Instance.ExecuteFunction<APIModels.GetFriendsResponse>(PlayfabContant.FunctionName.GetFriends,
            request,
            false, 
            OnGetFriendSuccess,
            null);
    }

    private void OnGetFriendSuccess(object arg1, APIModels.GetFriendsResponse data)
    {
        PlayerDataManager.Instance.friendManager.InitListFriend(data.friends);
    }

    protected override void OnHidden()
    {
        base.OnHidden();
        groupTabs.ForEach(tab => { tab.DesSelect(); });
    }
}
