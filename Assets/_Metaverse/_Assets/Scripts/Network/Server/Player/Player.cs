using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Player 
{
    private ulong _clientId;
    private NetCodeModels.PlayerData _playerData;
    private Room _room;
    private Lobby _lobby;
    private Group _group;

    public Lobby lobby
    {
        get { return _lobby; }
        set { _lobby = value; }
    }
    public NetCodeModels.PlayerData playerData { get { return _playerData; } }
    public string playfabId { get { return _playerData.playfabId; } }
    public Player(ulong clientId, Room room)
    {
        this._clientId = clientId;
        this._room = room;
    }
    public Group GetGroup() { return _group;}
    public void SetGroup(Group group) { _group = group; }
    public Room GetRoom()
    {
        return _room;
    }
    public void SetRoom(Room room)
    {
        _room = room;   
    }
    public void SetPlayerData(NetCodeModels.PlayerData playerData)
    {
        _playerData = playerData;
    }
    public ulong GetClientId()
    {
        return _clientId;
    }
}
