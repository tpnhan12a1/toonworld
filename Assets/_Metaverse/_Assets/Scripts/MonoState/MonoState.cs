using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MonoState : MonoBehaviour, IState
{
    public StateMachine _stateMachine;
    public abstract StateType GetStateType();

    public virtual void OnAnimationEnterEvent()
    {
        
    }

    public virtual void OnAnimationExitsEvent()
    {
       
    }

    public virtual void OnAnimationTransitionEvent()
    {
        
    }

    public virtual void OnEnterState()
    {
        //Debug.Log($"Enter {GetStateType()}");
    }

    public virtual void OnExitState()
    {

        //Debug.Log($"Exit {GetStateType()}");
    }

    public virtual void OnHandleInput()
    {
      
    }

    public virtual void OnPhysicsUpdate()
    {
       
    }

    public virtual void OnTriggerEnter(Collider collider)
    {
       
    }

    public virtual void OnTriggerExit(Collider collider)
    {
       
    }

    public virtual void OnUpdate()
    {
        
    }

    public virtual void SetStateMachine(StateMachine stateMachine)
    {
       _stateMachine = stateMachine;
    }
}
