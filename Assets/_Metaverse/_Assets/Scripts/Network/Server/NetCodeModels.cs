using DG.Tweening;
using Mono.CSharp;
using PlayFab;
using PlayFab.EconomyModels;
using PlayFab.GroupsModels;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine.InputSystem.XR;
using WebSocketSharp;

public class NetCodeModels
{
    public struct Friend : INetworkSerializable
    {
        public string friendPlayfabId;
        public string avatarUrl;
        public string displayName;
        public int level;
        public ListString friendTags;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            if(friendPlayfabId == null) friendPlayfabId = string.Empty;
            if(avatarUrl == null) avatarUrl = string.Empty;
            if(displayName == null) displayName = string.Empty;

            serializer.SerializeValue(ref friendPlayfabId);
            serializer.SerializeValue(ref avatarUrl);
            serializer.SerializeValue(ref displayName);
            serializer.SerializeValue(ref level);
            serializer.SerializeValue(ref friendTags);
        }
        public Friend(string friendPlayfabId, string avatarUrl, string displayName, int level, List<string> friendTags)
        {
            this.friendPlayfabId = friendPlayfabId;
            this.avatarUrl = avatarUrl;
            this.displayName = displayName;
            this.level = level;
            this.friendTags = new ListString();
            this.friendTags.list = friendTags;
        }
        public ToonWorld.Friend ToClass()
        {
            return new ToonWorld.Friend(friendPlayfabId, avatarUrl, displayName, level, friendTags.list);
        }
    }
    public struct FriendContent : INetworkSerializable
    {
        public string message;
        public string playfabReciverId;
        public Friend friend;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            if (message == null) message = string.Empty;
            if(playfabReciverId == null) playfabReciverId = string.Empty;
            serializer.SerializeValue(ref message);
            serializer.SerializeValue(ref playfabReciverId);
            serializer.SerializeValue(ref friend);
        }
        public FriendContent(string message, string playfabReciverId, Friend friend)
        {
            this.message = message;
            this.friend = friend;
            this.playfabReciverId = playfabReciverId;
        }
    }
    public struct EntityKey : INetworkSerializable
    {
        public string Id;
        public string Type;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref Id);
            serializer.SerializeValue(ref Type);
        }
        public PlayFab.GroupsModels.EntityKey ToEntityKey()
        {
            return new PlayFab.GroupsModels.EntityKey() { Id = Id, Type = Type };
        }
    }
    public struct GuildMemberInfor : INetworkSerializable
    {
        public string playfabId;
        public string avatarUrl;
        public string displayName;
        public int level;
        public string role;
        public EntityKey entityKey;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            if(playfabId == null) playfabId = string.Empty;
            if(avatarUrl == null) avatarUrl = string.Empty;
            if(displayName == null) displayName = string.Empty;
            if(role == null) role = string.Empty;

            serializer.SerializeValue(ref playfabId);
            serializer.SerializeValue(ref avatarUrl);
            serializer.SerializeValue(ref displayName);
            serializer.SerializeValue(ref level);
            serializer.SerializeValue(ref role);
            serializer.SerializeValue(ref entityKey);
        }
        public GuildMember ToGuildMember()
        {
            return new GuildMember(playfabId, avatarUrl, displayName, level, role, new PlayFab.GroupsModels.EntityKey()
            {
                Id = entityKey.Id,
                Type = entityKey.Type
            });
        }
    }
    public struct GuildSender: INetworkSerializable
    {
        public string guildTag;
        public string leaderPlayfabId;
        public GuildMemberInfor guildMemberInfor;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            if(guildTag == null) guildTag =String.Empty;
            if(leaderPlayfabId == null) leaderPlayfabId = String.Empty;

            serializer.SerializeValue(ref guildTag);
            serializer.SerializeValue(ref leaderPlayfabId);
            serializer.SerializeValue(ref guildMemberInfor);
        }
    }
    public struct ListUlong : INetworkSerializable
    {
        public List<ulong> list;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            int count = 0;
            ulong[] array = new ulong[count];
            if (!serializer.IsReader)
            {
                array = list != null ? list.ToArray() : new ulong[0];
                count = array.Length;
            }
            serializer.SerializeValue(ref count);
            if (serializer.IsReader)
            {
                array = new ulong[count];
            }
            for (int n = 0; n < count; ++n)
            {
                serializer.SerializeValue(ref array[n]);
            }
            if (serializer.IsReader)
            {
                list = array.ToList();
            }
        }
    }
    public struct ListString : INetworkSerializable
    {
        public List<string> list;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            int count = 0;
            string[] array = new string[count];
            if (!serializer.IsReader)
            {
                array = list != null ? list.ToArray() : new string[0];
                count = array.Length;
            }
            serializer.SerializeValue(ref count);
            if (serializer.IsReader)
            {
                array = new string[count];
            }
            for (int n = 0; n < count; ++n)
            {
                serializer.SerializeValue(ref array[n]);
            }
            if (serializer.IsReader)
            {
                list = array.ToList();
            }
        }
    }
    public struct Segment : IEquatable<Segment>, INetworkSerializable
    {
        public SegmentName Value;
        public bool Equals(Segment other)
        {
            return Value == other.Value;
        }

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref Value);
        }
    }
    public struct PresentData : INetworkSerializable
    {
        public GiftName giftName;
        public float z;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref giftName);
            serializer.SerializeValue(ref z);
        }
    }
    public struct PresentDatas : INetworkSerializable
    {
         public List<PresentData> gifts;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            int giftCount = 0;
            PresentData[] giftArray = new PresentData[giftCount];
            if (!serializer.IsReader)
            {
                giftArray = gifts != null ? gifts.ToArray() : new PresentData[0];
                giftCount = giftArray.Length;
            }
            serializer.SerializeValue(ref giftCount);
            if (serializer.IsReader)
            {
                giftArray = new PresentData[giftCount];
            }
            for (int n = 0; n < giftCount; ++n)
            {
                serializer.SerializeValue(ref giftArray[n]);
            }
            if (serializer.IsReader)
            {
                gifts = giftArray.ToList();
            }
        }
    }
    public struct ObstacleDatas : INetworkSerializable
    {
        public List<ObstacleData> obstacles;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            int obstacleCount = 0;
            ObstacleData[] obstacleDataArray = new ObstacleData[obstacleCount];
            if (!serializer.IsReader)
            {
                obstacleDataArray = obstacles != null ? obstacles.ToArray() : new ObstacleData[0];
                obstacleCount = obstacleDataArray.Length;
            }
            serializer.SerializeValue(ref obstacleCount);
            if (serializer.IsReader)
            {
                obstacleDataArray = new ObstacleData[obstacleCount];
            }
            for (int n = 0; n < obstacleCount; ++n)
            {
                serializer.SerializeValue(ref obstacleDataArray[n]);
            }
            if (serializer.IsReader)
            {
                obstacles = obstacleDataArray.ToList();
            }
        }
    }
    public struct ObstacleData : INetworkSerializable
    {
        public ObstacleName obstacleName;
        public int lane;
        public float z;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref obstacleName);
            serializer.SerializeValue(ref lane);
            serializer.SerializeValue(ref z);
        }
    }
    public struct RunningGamePlayData : INetworkSerializable
    {
        public GenrationGamePlay genrationGamePlay;
        public List<SegmentName> segmentNames;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref genrationGamePlay);
            int segmentNamesCount = 0;
            SegmentName[] segmentNamesArray = new SegmentName[segmentNamesCount];
            if (!serializer.IsReader)
            {
                segmentNamesArray = segmentNames != null ? segmentNames.ToArray() : new SegmentName[0];
                segmentNamesCount = segmentNamesArray.Length;
            }
            serializer.SerializeValue(ref segmentNamesCount);
            if (serializer.IsReader)
            {
                segmentNamesArray = new SegmentName[segmentNamesCount];
            }
            for (int n = 0; n < segmentNamesCount; ++n)
            {
                serializer.SerializeValue(ref segmentNamesArray[n]);
            }
            if (serializer.IsReader)
            {
                segmentNames = segmentNamesArray.ToList();
            }
        }
    }
    public struct JoinLobbyResponse : INetworkSerializable
    {
        public Lobby gameRoomLobby;
        public List<PlayerData> playerDatas;
        public PlayerData playerData;
        public int slotIndex;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            int playerDataCount = 0;
            PlayerData[] playerDataArray = new PlayerData[playerDataCount];
            if (!serializer.IsReader)
            {
                playerDataArray = playerDatas != null ? playerDatas.ToArray() : new PlayerData[0];
                playerDataCount = playerDataArray.Length;
            }
            serializer.SerializeValue(ref playerDataCount);

            if (serializer.IsReader)
            {
                playerDataArray = new PlayerData[playerDataCount];
            }
            serializer.SerializeValue(ref playerDataCount);

            for (int n = 0; n < playerDataCount; ++n)
            {
                serializer.SerializeValue(ref playerDataArray[n]);
            }
            if (serializer.IsReader)
            {
                playerDatas = playerDataArray.ToList();
            }

            serializer.SerializeValue(ref gameRoomLobby);
            serializer.SerializeValue(ref playerData);
            serializer.SerializeValue(ref slotIndex);
        }
    }
    public struct GetLobbyResponse : INetworkSerializable
    {
        public List<Lobby> lobbies;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            int lobbyCount = 0;
            Lobby[] lobbiesArray = new Lobby[lobbyCount];
            if (!serializer.IsReader)
            {
                lobbiesArray = lobbies!= null ? lobbies.ToArray(): new Lobby[0];
                lobbyCount = lobbiesArray.Length;
            }
            serializer.SerializeValue(ref lobbyCount);

            if (serializer.IsReader)
            {
                lobbiesArray = new Lobby[lobbyCount];
            }
            serializer.SerializeValue(ref lobbyCount);

            for (int n = 0; n < lobbyCount; ++n)
            {
                serializer.SerializeValue(ref lobbiesArray[n]);
            }
            if (serializer.IsReader)
            {
                lobbies = lobbiesArray.ToList();
            }
        }
    }
    public struct Lobby : INetworkSerializable
    {
        public int id;
        public string ownerPlayfabId;
        public uint maxPlayer;
        public string avatarUrl;
        public string ownerDisplayName;
        public string passworld;
        public GamePlayType gamePlayType;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref id);
            serializer.SerializeValue(ref maxPlayer);
            ownerPlayfabId = String.IsNullOrEmpty(ownerPlayfabId) ? "" : ownerPlayfabId;
            ownerDisplayName = String.IsNullOrEmpty(ownerDisplayName)?"":ownerDisplayName;
            passworld = String.IsNullOrEmpty(passworld) ? "" : passworld;
            avatarUrl = String.IsNullOrEmpty(avatarUrl) ? "" : avatarUrl;
           
            serializer.SerializeValue(ref ownerDisplayName);
            serializer.SerializeValue(ref passworld);
            serializer.SerializeValue(ref gamePlayType);
            serializer.SerializeValue(ref ownerPlayfabId);
        }
        public ToonWorld.GameRoomLobby ToClass()
        {
            return new ToonWorld.GameRoomLobby()
            {
                id = id,
                ownerPlayfabId= ownerPlayfabId,
                gamePlayType = gamePlayType,
                maxPlayer = maxPlayer,
                owner = String.IsNullOrEmpty(ownerDisplayName) ? "" : ownerDisplayName,
                password = String.IsNullOrEmpty(passworld) ? "" : passworld,
                avatarUrl = String.IsNullOrEmpty(avatarUrl)?"":avatarUrl,
            };
        }
    }
    public struct PlayerData: INetworkSerializable
    {
        public string playfabId;
        public string displayName;
        public string avatarUrl;
        public int level;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref playfabId);
            serializer.SerializeValue(ref displayName);
            serializer.SerializeValue(ref avatarUrl);
            serializer.SerializeValue(ref level);
        }
    }
    public struct Party : INetworkSerializable
    {
        public ulong id;
        public string owner;
        public List<string> members;
        public List<ulong> clientIds;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref id);
            serializer.SerializeValue(ref owner);

            int memberLength = 0;
            string[] memberArray = new string[memberLength];

            if (!serializer.IsReader)
            {
                memberArray = members.ToArray();
                memberLength = memberArray.Length;
            }
            serializer.SerializeValue(ref memberLength);

            if (serializer.IsReader)
            {
                memberArray = new string[memberLength];
            }
            serializer.SerializeValue(ref memberLength);

            for (int n = 0; n < memberLength; ++n)
            {
                serializer.SerializeValue(ref memberArray[n]);
            }

            if(serializer.IsReader)
            {
                members = memberArray.ToList();
            }


            int clientLength = 0;
            ulong[] clientArray = new ulong[clientLength];

            if (!serializer.IsReader)
            {
                clientArray = clientIds.ToArray();
                clientLength = clientArray.Length;
            }
            serializer.SerializeValue(ref clientLength);

            if (serializer.IsReader)
            {
                clientArray = new ulong[clientLength];
            }
            serializer.SerializeValue(ref clientLength);

            for (int n = 0; n < clientLength; ++n)
            {
                serializer.SerializeValue(ref clientArray[n]);
            }

            if (serializer.IsReader)
            {
                clientIds = clientArray.ToList();
            }


        }
    }
    public struct CreateRoomRequest: INetworkSerializable
    {
        public SceneName sceneName;
        public string owner;
        public GamePlayType gamePlayType;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref sceneName);
            serializer.SerializeValue(ref owner);
            serializer.SerializeValue(ref gamePlayType);
        }
    }
    public struct CreateRoomResponse : INetworkSerializable
    {
        public ulong roomId;
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref roomId);  
        }
    }
    public struct ChatContent : INetworkSerializable
    {
        public TypeChat type;
        public string displayName;
        public string content;
        public ulong[] reciverClientIds;
       // public NetworkList<string> receivers //PlayfabId
        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            serializer.SerializeValue(ref type);
            serializer.SerializeValue(ref displayName);
            serializer.SerializeValue(ref content);

            if (reciverClientIds == null) return;

            int length = 0;
            ulong[] array = new ulong[length];
            if (!serializer.IsReader)
            {
                array = reciverClientIds;
                length = array.Length;
            }
            serializer.SerializeValue(ref length);

            for (int n = 0; n < length; ++n)
            {
                serializer.SerializeValue(ref array[n]);
            }

            if (serializer.IsReader)
            {
                reciverClientIds = array;
            }
            // serializer.SerializeValue(ref receiver);
        }
    }

    public struct PlayerItem : INetworkSerializable
    {
        public string hair;
        public string eyeBrow;
        public string eyes;
        public string skin;

        public string cap;
        public string glasses;
        public string torso;
        public string pants;
        public string shoes;
        public string gloves;
        public string backpack;

        public void NetworkSerialize<T>(BufferSerializer<T> serializer) where T : IReaderWriter
        {
            hair = String.IsNullOrEmpty(this.hair) ? "" : this.hair;
            eyeBrow = String.IsNullOrEmpty(this.eyeBrow) ? "" : this.eyeBrow;
            eyes = String.IsNullOrEmpty(this.eyes) ? "" : this.eyeBrow;
            skin = String.IsNullOrEmpty(this.skin) ? "" : this.skin;
            cap = String.IsNullOrEmpty(this.cap) ? "" : this.cap;
            glasses = String.IsNullOrEmpty(this.glasses) ? "" : this.glasses;
            torso = String.IsNullOrEmpty(this.torso) ? "" : this.torso;
            pants = String.IsNullOrEmpty(this.pants) ? "" : this.pants;
            shoes = String.IsNullOrEmpty(this.shoes) ? "" : this.shoes;
            gloves = String.IsNullOrEmpty(this.gloves) ? "" : this.shoes;
            backpack = String.IsNullOrEmpty(this.backpack) ? "" : this.shoes;

            serializer.SerializeValue(ref hair);
            serializer.SerializeValue(ref eyeBrow);
            serializer.SerializeValue(ref eyes);
            serializer.SerializeValue(ref skin);
            serializer.SerializeValue(ref cap);
            serializer.SerializeValue(ref glasses);
            serializer.SerializeValue(ref torso);
            serializer.SerializeValue(ref pants);
            serializer.SerializeValue(ref shoes);
            serializer.SerializeValue(ref gloves);
            serializer.SerializeValue(ref backpack);
        }

        public APIModels.PlayerItem ToClass()
        {
            return new APIModels.PlayerItem()
            {
                hair = hair,
                eyeBrow = eyeBrow,
                eyes = eyes,
                skin = skin,
                cap = cap,
                glasses = glasses,
                torso = torso,
                pants = pants,
                shoes = shoes,
                gloves = gloves,
                backpack = backpack
            };
        }
    }
}
