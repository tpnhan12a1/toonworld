using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ToonWorld;
public class FriendInviteItem : LoopGridViewItem
{
    [SerializeField] private Image _avatar;
    [SerializeField] private TMP_Text _txtLevel;
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private Button _btnDelete;
    [SerializeField] private TMP_Text _txtStatus;

    private Friend _friend;
    private FriendInviteGridViewController _friendInviteGridViewController;
    public void Awake()
    {
        _btnDelete.onClick.AddListener(DeleteRequestFriend);
    }

    public void OnRefresh(Friend friend, FriendInviteGridViewController friendInviteGridViewController)
    {
        _friendInviteGridViewController = friendInviteGridViewController;
        _friend = friend;
        Sprite avatar = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, friend.avatarUrl);
        _avatar.sprite = avatar;
        _txtDisplayName.text = friend.displayName;
        _txtLevel.text = friend.level.ToString();
    }
    private void DeleteRequestFriend()
    {
        APIModels.FriendRequest friendRequest = new APIModels.FriendRequest();
        friendRequest.friendPlayfabId = _friend.friendPlayfabId;
        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
            PlayfabContant.FunctionName.RemoveFriend,
            friendRequest,
            false,
            null,
            null);
        NetCodeModels.FriendContent friendContent = new NetCodeModels.FriendContent();
        friendContent.message = PlayfabContant.FriendSyncTag.RemoveRequest;
        friendContent.playfabReciverId = _friend.friendPlayfabId;
        friendContent.friend = new NetCodeModels.Friend(PlayfabManager.Instance.PlayfabID,
            PlayerDataManager.Instance.avatarUrl,
            PlayerDataManager.Instance.displayName,
            PlayerDataManager.Instance.level,
            new List<string>() { PlayfabContant.FriendTag.Received });

        ServerManager.Instance.FriendControllerServerRpc(friendContent);

        PlayerDataManager.Instance.friendManager.RemoveFriend(_friend);
        _friendInviteGridViewController.RefreshItem();
    }
}
