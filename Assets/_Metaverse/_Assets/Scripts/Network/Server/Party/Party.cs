using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Netcode;
using UnityEngine;

public class MemberData
{
    public ulong clientId;
}
public class Party
{
    private ulong _id;
    private string _owner;
    private List<string> _members;
    private List<ulong> _clientIds;
    private Dictionary<string, MemberData> _membersv2;
    private PartyManager<Party> _partyManager;

    public ulong id { get { return _id; } set { _id = value; } }
    public Party(PartyManager<Party> partyManager, string playfabId, ulong clientId)
    {
        _partyManager = partyManager;
        _membersv2 = new Dictionary<string, MemberData>();
        _members = new List<string>();
        _clientIds = new List<ulong>();
        _owner = playfabId;
        _clientIds.Add(clientId);

     

        NetworkManager.Singleton.OnClientDisconnectCallback += OnClientDisconectedCallback;
        NetworkManager.Singleton.OnClientConnectedCallback += OnClientConectedCallback;
    }
    public void AddMember(string member, ulong clientId)
    {
        _members.Add(member);
        _clientIds.Add(clientId);
        SendChange();
    }
    public void AddClientId(ulong clientId)
    {
        _clientIds.Add(clientId);
        SendChange();
    }
    public bool IsOwer(string playfabId)
    {
        return _owner == playfabId;
    }
    public void SetOwner(string owner)
    {
        this._owner = owner;
        SendChange();
    }

    private void OnClientConectedCallback(ulong clientId)
    {
       
    }
    public void RemoveParty()
    {
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = _clientIds.ToArray(),
            }
        };
        ClientManager.Instance.ResponseOnPartyRemoveClientRpc(clientRpcParams);

        _partyManager.RemoveParty(this);
    }

    ~Party()
    {
        NetworkManager.Singleton.OnClientDisconnectCallback -= OnClientDisconectedCallback;
        NetworkManager.Singleton.OnClientConnectedCallback -= OnClientConectedCallback;
    }
    public bool IsIn(string playfabId)
    {
        return _members.Contains(playfabId) || _owner.Equals(playfabId);
    }
    private void OnClientDisconectedCallback(ulong clientId)
    {
       if(_clientIds.Contains(clientId))
       {
            _clientIds.Remove(clientId);
            SendChange();
       }
    }
    
    private void SendChange()
    {
        NetCodeModels.Party party = GetParty();

        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = _clientIds.ToArray(),
            }
        };
        ClientManager.Instance.ResponseOnPartyChangeClientRpc(party, clientRpcParams);
    }
    public ulong[] GetClientInParty()
    {
        return _clientIds.ToArray();
    }
    public NetCodeModels.Party GetParty()
    {
        NetCodeModels.Party party = new NetCodeModels.Party()
        {
            id = _id,
            owner = _owner,
            clientIds = _clientIds,
            members = _members,
        };
        return party;
    }

    
}
