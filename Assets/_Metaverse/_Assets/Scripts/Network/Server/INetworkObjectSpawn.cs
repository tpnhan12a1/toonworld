using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public interface INetworkObjectSpawn
{
    void OnSpawnNetworkObject(NetworkObject networkObject);
}
public interface INetworkObjectDespawn
{
    void OnDespawnNetworkObject(NetworkObject networkObject);
}
