using System;
using TMPro;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using Unity.Netcode;
using UnityEngine;

public class PlayerNetworkData : NetworkBehaviour
{
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private CharacterRender _characterRender;
    [SerializeField] private NetworkObject _networkObject;

    [SerializeField] private Color _ownerDisplayNameColor;
    [SerializeField] private Color _displayNameColor;

    [HideInInspector]
    public NetworkVariable<FixedString128Bytes> displayName = new NetworkVariable<FixedString128Bytes>();
    [HideInInspector]
    public NetworkVariable<FixedString128Bytes> playfabId = new NetworkVariable<FixedString128Bytes>(writePerm:NetworkVariableWritePermission.Owner);
    [HideInInspector]
    public NetworkVariable<NetCodeModels.PlayerItem> playerItem = new NetworkVariable<NetCodeModels.PlayerItem>(writePerm: NetworkVariableWritePermission.Owner);
    [HideInInspector]
    public NetworkVariable<bool> isDashing = new NetworkVariable<bool>(writePerm:NetworkVariableWritePermission.Owner);

    private ParticleSystem _particleSystem;
    private ParticleName _particleName = ParticleName.ToonBlueFireTrail;
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        displayName.OnValueChanged += OnDisplayNameChanged;
        playerItem.OnValueChanged += OnNetworkPlayerItemChanged;
        isDashing.OnValueChanged += OnNetworkDashingChanged;
        DisplayData();
        if (!IsOwner)
        {
            _txtDisplayName.color = _displayNameColor;
            return;
        }
        _txtDisplayName.color = _ownerDisplayNameColor;
        PlayerDataManager.Instance.reusableData.OnIsDashChanged += OnIsDashingChanged;

        PlayerDataManager.Instance.RegisterOnDisplayNameChanged(OnLocalDisplayNameChanged);
        PlayerDataManager.Instance.RegisterOnPlayerItemChanged(OnLocalPlayerItemChanged);
        playfabId.Value = PlayfabManager.Instance.PlayfabID;
    }

    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();
        displayName.OnValueChanged -= OnDisplayNameChanged;
        playerItem.OnValueChanged -= OnNetworkPlayerItemChanged;
        isDashing.OnValueChanged -= OnNetworkDashingChanged;
        if (!IsOwner) return;
        
        PlayerDataManager.Instance.UnRegisterOnDisplayNameChanged(OnLocalDisplayNameChanged);
        PlayerDataManager.Instance.UnRegisterOnPlayerItemChanged(OnLocalPlayerItemChanged);
    }

    private void OnNetworkPlayerItemChanged(NetCodeModels.PlayerItem previousValue, NetCodeModels.PlayerItem newValue)
    { 
        _characterRender.LoadPlayerItem(newValue.ToClass());
    }

    private void OnDisplayNameChanged(FixedString128Bytes previousValue, FixedString128Bytes newValue)
    {
        _txtDisplayName.text = newValue.ToString();
    }

    private void OnLocalDisplayNameChanged(string oldValue, string newValue)
    {
        ChangeDisplayNameServerRpc(newValue);
    }
    [ServerRpc(RequireOwnership = false)]
    public void ChangeDisplayNameServerRpc(string  newName,  ServerRpcParams serverRpcParams = default)
    {
        displayName.Value = newName;
    }

    private void OnLocalPlayerItemChanged(APIModels.PlayerItem oldItem, APIModels.PlayerItem newItem)
    {
        ChangePlayerItem(newItem);
    }

    public void ChangePlayerItem(APIModels.PlayerItem newItem)
    {
        playerItem.Value = newItem.ToStruct();
    }

    private void DisplayData()
    {
        _txtDisplayName.text = displayName.Value.ToString();
        if (playerItem != null)
        {
            _characterRender.LoadPlayerItem(playerItem.Value.ToClass());
        }
    }
    private void OnIsDashingChanged(bool obj)
    {
        isDashing.Value = obj;
    }
    private void OnNetworkDashingChanged(bool previousValue, bool newValue)
    {
        if(newValue == false)
        {
            if (_particleSystem == null) return;
            PoolingMamager.Instance.ReturnPartcle(_particleName, _particleSystem);
        }
        else
        {
            _particleSystem = PoolingMamager.Instance.GetPoolingParticle(_particleName);
            _particleSystem.transform.parent = _networkObject.transform;
            _particleSystem.transform.rotation = Quaternion.identity;
            _particleSystem.transform.position = _networkObject.transform.position;
        }
    }
}
