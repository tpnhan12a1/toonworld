using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameIdling_2State : RunningGameIdlingState
{
    public override StateType GetStateType()
    {
        return StateType.RunningGameIdling_2;
    }
}
