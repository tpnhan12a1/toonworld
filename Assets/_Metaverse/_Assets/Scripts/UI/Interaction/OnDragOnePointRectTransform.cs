using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.Layouts;
using UnityEngine.InputSystem.OnScreen;

namespace toonworld
{
    public class OnDragOnePointRectTransform : OnScreenControl, IPointerDownHandler, IPointerUpHandler, IDragHandler
    {
        [InputControl(layout = "Vector2")]
        [SerializeField]
        private string m_ControlPath;
        private List<PointerEventData> _pointerEventDatas = new List<PointerEventData>();
        private Vector2 _startPosition = Vector2.zero;
        [SerializeField][Range(0f, 10f)] private float _sentivityX = 5f;
        [SerializeField][Range(0f, 10f)] private float _sentivityY = 5f;
        protected override string controlPathInternal
        {
            get => m_ControlPath;
            set => m_ControlPath = value;
        }

        public void OnDrag(PointerEventData eventData)
        {
           
            if (_pointerEventDatas.Count == 1)
            {
                Vector2 dir = eventData.position - _startPosition;
                _startPosition = eventData.position;
                if (Mathf.Abs(dir.x) < _sentivityX) { dir.x = 0f; }
                if (Mathf.Abs(dir.y) < _sentivityY) { dir.y = 0f; }
                dir.x = -dir.x;
                SendValueToControl(-dir);
            }
        }

        public void OnPointerDown(PointerEventData eventData)
        {
           
            _startPosition = eventData.position;
            _pointerEventDatas.Add(eventData);
            if (_pointerEventDatas.Count == 1)
            {
                PlayerInput.Instance.InputAction.Player.Look.Enable();
                return;
            }
            SendValueToControl(Vector2.zero);
            PlayerInput.Instance.InputAction.Player.Look.Disable();
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (_pointerEventDatas.Count == 1)
            {
                SendValueToControl(Vector2.zero);
                PlayerInput.Instance.InputAction.Player.Look.Disable();
            }
            _pointerEventDatas.Remove(eventData);

            if (_pointerEventDatas.Count == 1)
            {
                PlayerInput.Instance.InputAction.Player.Look.Enable();
            }

        }

        #region Networking

        #endregion
    }
}

