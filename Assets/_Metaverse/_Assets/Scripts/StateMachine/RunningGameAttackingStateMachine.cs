using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameAttackingStateMachine : StateMachine
{
    private PlayerRunningGameController _playerRunningGameController;

    protected Dictionary<StateType, IState> _states = new Dictionary<StateType, IState>();
    public Dictionary<StateType, IState> states
    {
        get { return _states; }
        set { _states = value; }
    }
    public PlayerRunningGameController playerRunningGameController { get { return _playerRunningGameController; } }
    
    public override StateMachineType GetStateMachineType()
    {
        return StateMachineType.RunningGameAttackingStateMachine;
    }

    public RunningGameAttackingStateMachine(PlayerRunningGameController playerRunningGameController)
    {
        _playerRunningGameController = playerRunningGameController;
        Init();
    }
    public override void Init()
    {
        RunningGameEmptyAttackingState runningGameEmptyAttackingState = new RunningGameEmptyAttackingState();
        _states.Add(runningGameEmptyAttackingState.GetStateType(), runningGameEmptyAttackingState);
        runningGameEmptyAttackingState.SetStateMachine(this);

        RunningGameRocketAttackingState runningGameRocketAttackingState = new RunningGameRocketAttackingState();
        _states.Add(runningGameRocketAttackingState.GetStateType(), runningGameRocketAttackingState);
        runningGameRocketAttackingState.SetStateMachine(this);
    }

    public void StartBoolAnimation(int animationHash)
    {
        playerRunningGameController.animator.SetBool(animationHash, true);
    }
    public void StopBoolAnimation(int animationHash)
    {
        playerRunningGameController.animator.SetBool(animationHash, false);
    }
    public void StartFloatAnimation(int animationHash, float value)
    {
        playerRunningGameController.animator.SetFloat(animationHash, value);
    }
}
