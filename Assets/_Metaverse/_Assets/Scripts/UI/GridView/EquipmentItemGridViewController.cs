/*using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class EquipmentItemGridViewController : LoopGridView, IGroupTab
{
    private List<EquipmentItem> _equipmentItemList;

    private CharacterRenderController _characterRenderController;
    public void OnInit(CharacterRenderController characterRenderController)
    {
        _characterRenderController = characterRenderController;

        _equipmentItemList = new List<EquipmentItem>(Resources.LoadAll<EquipmentItem>("Item"));

        InitGridView(_equipmentItemList.Count, OnGetEquipmentItemByRowColum);
    }

    private LoopGridViewItem OnGetEquipmentItemByRowColum(LoopGridView gridView, int index, int row, int column)
    {
        if (_equipmentItemList.Count == 0) return null;

        ItemPrefab itemPrefab = (ItemPrefab)gridView.NewListViewItem("ItemPrefab");
        itemPrefab.InitEquipmentItem(_equipmentItemList[index], this, _characterRenderController);

        return itemPrefab;
    }

    public void RefreshItem()
    {
        SetListItemCount(_equipmentItemList.Count);
        RefreshAllShownItem();
    }

    public void Select()
    {
        gameObject.SetActive(true);
    }

    public void DesSelect()
    {
        gameObject.SetActive(false);
    }
}
*/