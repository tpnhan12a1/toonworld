using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMediumStoppingState : PlayerStoppingState
{
    public override StateType GetStateType()
    {
        return StateType.MediumStopping;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.MediumStopParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.MediumStopParameterHash);
    }
}
