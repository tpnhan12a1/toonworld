using Imba.Audio;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RunningGameSwipingState : IState
{
    protected RunningGameSwipeStateMachine _runningGameSwipeStateMachine;
    public abstract StateType GetStateType();

    public virtual void OnAnimationEnterEvent()
    {
        
    }

    public virtual void OnAnimationExitsEvent()
    {
       
    }

    public virtual void OnAnimationTransitionEvent()
    {
        
    }

    public virtual void OnEnterState()
    {
        Debug.Log($"Enter the {GetStateType()}");
        _runningGameSwipeStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.SwipeParameterHash);
        AudioManager.Instance.PlaySoundFromSource (AudioName.Slide,_runningGameSwipeStateMachine.playerRunningGameController.audioSource);
    }

    public virtual void OnExitState()
    {
        _runningGameSwipeStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.SwipeParameterHash);
    }

    public virtual void OnHandleInput()
    {
       
    }

    public virtual void OnPhysicsUpdate()
    {
        
    }

    public virtual void OnTriggerEnter(Collider collider)
    {
       
    }

    public virtual void OnTriggerExit(Collider collider)
    {
       
    }

    public virtual void OnUpdate()
    {
       
    }

    public void SetStateMachine(StateMachine stateMachine)
    {
        _runningGameSwipeStateMachine = (RunningGameSwipeStateMachine)stateMachine;
    }

    public Vector3 GetHorizontalVelocity()
    {
        return new Vector3(_runningGameSwipeStateMachine.playerRunningGameController.rb.velocity.x, 0f,
            _runningGameSwipeStateMachine.playerRunningGameController.rb.velocity.z);
    }


}
