using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FriendChatInfo
{
    public string friendPlayfabId;
    public string displayName = "";
    public override int GetHashCode()
    {
        return friendPlayfabId.GetHashCode();
    }
    public override bool Equals(object obj)
    {
        FriendChatInfo other = obj as FriendChatInfo;
        if (other == null) return false;
        return other.friendPlayfabId.Contains(friendPlayfabId);
    }
}