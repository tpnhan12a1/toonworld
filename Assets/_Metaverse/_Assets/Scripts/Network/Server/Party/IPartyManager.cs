using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPartyManager<P> where P : Party
{
    public void AddParty(P p);
    public void RemoveParty(P p);
    public void RemoveParty(ulong id);
    public bool Contain(P p);
    public P GetParty(ulong id);
}
