﻿using UnityEngine;
using SuperScrollView;
using UnityEngine.UI;
using Imba.UI.Animation;

[RequireComponent(typeof(Button))]
public class ItemPrefab : LoopGridViewItem
{
    [SerializeField] public Image _itemImage;
    [SerializeField] private Button _btnItem;

    CharacterRenderController _characterRenderController;

    private ItemGridViewController _itemGridViewController;

    private BaseItem _baseItem;

    void Awake()
    {
        _btnItem.onClick.AddListener(OnItemClick);
    }

    public void OnRefresh(Item item, ItemGridViewController itemGridViewController, CharacterRenderController characterRenderController)
    {
        _itemGridViewController = itemGridViewController;

        _characterRenderController = characterRenderController;

        BaseItem baseItem = Imba.Utils.ResourceManager.Instance.GetResourceByName<BaseItem>("Item/" + item.itemId);

        _baseItem = baseItem;

        Sprite itemSprite = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Item, item.itemId);

        _itemImage.sprite = itemSprite;
    }

    private int RandomInt()
    {
        System.Random random = new System.Random();
        return random.Next(0, 6);
    }

    public void OnItemClick()
    {
        if (_characterRenderController == null)
        {
            Debug.LogError("CharacterRender is not assigned in the inspector");
            return;
        }
        else
        {
            if (_baseItem != null)
            {
                _characterRenderController.animator.SetInteger(PlayerDataManager.Instance.animationData.IntAnimParameterHash, RandomInt());
                _characterRenderController.animator.SetBool(PlayerDataManager.Instance.animationData.IdleParameterHash, false);
                _characterRenderController.characterRender.ChangeMeshWithItemType(_baseItem);
            }
            else
            {
                Debug.LogError("AppearanceItem or EquipmentItem is not assigned in the inspector");
                return;
            }
        }
    }
}