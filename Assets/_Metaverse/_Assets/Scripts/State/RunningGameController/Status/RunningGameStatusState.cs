using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class RunningGameStatusState : IState
{
    protected RunningGameStatusStateMachine _runningGameStatusStateMachine;
    public abstract StateType GetStateType();

    public virtual void OnAnimationEnterEvent()
    {
        
    }

    public  virtual void OnAnimationExitsEvent()
    {
       
    }

    public virtual void OnAnimationTransitionEvent()
    {
        ChangeEmptyState();
    }

    public virtual void OnEnterState()
    {
        Debug.Log($"Enter the {GetStateType()}");
        _runningGameStatusStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.StatusParameterHash);
    }

    public virtual void OnExitState()
    {
        Debug.Log($"Exit the {GetStateType()}");
        _runningGameStatusStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.StatusParameterHash);
    }

    public virtual void OnHandleInput()
    {
        
    }

    public virtual void OnPhysicsUpdate()
    {
        
    }

    public virtual void OnTriggerEnter(Collider collider)
    {
        
    }

    public virtual void OnTriggerExit(Collider collider)
    {
        
    }

    public virtual void OnUpdate()
    {
        
    }

    public virtual void SetStateMachine(StateMachine stateMachine)
    {
        _runningGameStatusStateMachine = (RunningGameStatusStateMachine)stateMachine;
    }
    public void ChangeEmptyState()
    {
        _runningGameStatusStateMachine.ChangeState(_runningGameStatusStateMachine.states[StateType.RunningGameEmptyStatusState]);
    }
}
