﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AILightStoppingMonoState : AIStoppingMonoState
{
    private float startTime = 0f;
    private float timeToExistThis = 2f;
    public override StateType GetStateType()
    {
        return StateType.AILightStoppingMonoState;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        startTime = 0f;
        StartAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsLightStoppingParamenterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(((ResidentStateMachine)_stateMachine).residentController.residentAnimationData.IsLightStoppingParamenterHash);
    }
    public override void OnAnimationTransitionEvent()
    {
        OnMove();
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        startTime += Time.deltaTime;
        if(timeToExistThis < startTime)
        {
            OnMove();
        }
    }
    public override void OnMove()
    {
        base.OnMove();
       
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        Quaternion tarrgetRotation = Quaternion.LookRotation(
            residentStateMachine.residentController.GetNextWaitPoint().position - residentStateMachine.residentController.transform.position,
            Vector3.up
            );
        float angle = tarrgetRotation.eulerAngles.y - residentStateMachine.residentController.transform.rotation.eulerAngles.y;
        float angle360 = angle < 0 ? 360 + angle : angle;
        //Debug.Log(angle360);
        if (angle360 > 180)
        {
            // Quay sang  trái
            residentStateMachine.ChangeState(residentStateMachine.states[StateType.AILeftRotationMonoState]);
        }
        else
        {
            //Quay sang phải
            residentStateMachine.ChangeState(residentStateMachine.states[StateType.AIRightRotationMonoState]);
        }

    }
}
