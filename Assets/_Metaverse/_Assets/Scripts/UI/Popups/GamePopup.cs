using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePopup : UIPopup
{
    [SerializeField] private Button _btnClosePopup;
    [SerializeField] private GameGridViewController _gameGridViewController;

    protected override void OnInit()
    {
        base.OnInit();
        _btnClosePopup.onClick.AddListener(HidePopup);
        _gameGridViewController?.OnInit();
    }
    protected override void OnShowing()
    {
        base.OnShowing();
        _gameGridViewController?.OnShow();
      
        
    }
    protected override void OnHiding()
    {
        base.OnHiding();
        _gameGridViewController?.OnHide();
       
    }

    private void HidePopup()
    {
        Hide();
    }

    private void OpenGameRoomPopup()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.GameRoomLobbyPopup,GamePlayType.Running);
    }

}
