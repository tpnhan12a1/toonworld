using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class Group 
{
    private string _groupId = string.Empty;
    private PlayerManager<Player> _playerManager;

    private GroupManager<Group> _groupManager;
    public override int GetHashCode()
    {
        return _groupId.GetHashCode();
    }
    public override bool Equals(object obj)
    {
        Group group = obj as Group;
        if (group == null) return false;
        return _groupId == group._groupId; 
    }
    public Group(string groupId, GroupManager<Group> groupManager) 
    {
        _groupId = groupId;
        _playerManager = new PlayerManager<Player>();
        _groupManager = groupManager;
    }
    public string groupId { get { return _groupId; } }
    public PlayerManager<Player> playerManager { get { return _playerManager; } } 
    
    public void AddPlayerInGroup(Player player)
    {
        _playerManager.AddPlayer(player);
        player.SetGroup(this);
    }
    public void RemovePlayerInGroup(Player player)
    {
        _playerManager.RemovePlayer(player); 
        if(_playerManager.GetPlayerCount() == 0) 
        {
            _groupManager.RemoveGroup(this); 
        }
    }
    //New Player join group
    public void PlayerJoinGroup(Player player, NetCodeModels.GuildMemberInfor guildMemberInfor)
    {
        //Notify to all client
        ulong[] targetClientIds = _playerManager.GetClientIds().ToArray();
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = targetClientIds
            }
        };
        ClientManager.Instance.PlayerJoinGuildClientRpc(guildMemberInfor, clientRpcParams);
        _playerManager.AddPlayer(player);
        
    }
    public void OutGroup(Player player, NetCodeModels.GuildSender guildSender)
    {
        
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = _playerManager.GetClientIds()
            }
        };
        ClientManager.Instance.ResponseGuildClientRpc(guildSender, clientRpcParams);
        _playerManager.RemovePlayer(player);
    }
    public void KickMember(Player player, NetCodeModels.GuildSender guildSender)
    {
        
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = _playerManager.GetClientIds()
            }
        };
        ClientManager.Instance.ResponseGuildClientRpc(guildSender, clientRpcParams);
        _playerManager.RemovePlayer(player);
    }
    public void UnPollMember( Player player , NetCodeModels.GuildSender guildSender)
    {
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = _playerManager.GetClientIds()
            }
        };
        ClientManager.Instance.ResponseGuildClientRpc(guildSender, clientRpcParams);
    }
    public void PollMember(Player player, NetCodeModels.GuildSender guildSender)
    {
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = _playerManager.GetClientIds()
            }
        };
        ClientManager.Instance.ResponseGuildClientRpc(guildSender, clientRpcParams);
    }
    public void AuthenMember(Player player, NetCodeModels.GuildSender guildSender)
    {

        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = _playerManager.GetClientIds()
            }
        };
        ClientManager.Instance.ResponseGuildClientRpc(guildSender, clientRpcParams);
    }
}
