using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpingState : PlayerAirborneState
{
    private bool shouldKeepRotation;
    private bool canStartFalling;

    public override StateType GetStateType()
    {
        return StateType.Jumping;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        MotionStateMachine stateMachine =
            (MotionStateMachine)_stateMachine;
        PlayerDataManager.Instance.reusableData.movementSpeedModifier = 0f;
        PlayerDataManager.Instance.reusableData.movementDecelerationForce = PlayerDataManager.Instance.playerData.decelerationForce;
        shouldKeepRotation = PlayerDataManager.Instance.reusableData.movementInput != Vector2.zero;
        Jump();
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        if (shouldKeepRotation)
        {
            RotateTowardsTargetRotation();
        }
        //if (IsMovingUp())
        //{
        //    DecelerateVertical();
        //}
    }

    public override void OnUpdate()
    {
        base.OnUpdate();
        if (!canStartFalling && IsMovingUp(0f))
        {
            canStartFalling = true;
        }
        if (!canStartFalling || GetPlayerVerticalVelocity().y > 0) return;
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Falling]);

    }
    public override void OnExitState()
    {
        base.OnExitState();
        SetBaseRotatonData();
        canStartFalling = false;
    }
    protected void Jump()
    {
        Vector3 JumpForce = PlayerDataManager.Instance.reusableData.currentJumpForce;
        Vector3 JumpDirection = ((MotionStateMachine)_stateMachine).playerController.transform.forward;

        if (shouldKeepRotation)
        {
            UpdateTargetRotation(GetMovementDirection());
            JumpDirection = GetTargetRotaionDirection(PlayerDataManager.Instance.reusableData.currentTargetRotation.y);
        }
        JumpForce.x *= JumpDirection.x;
        JumpForce.z *= JumpDirection.z;
        Vector3 capsuleCollierCenterInWorld = ((MotionStateMachine)_stateMachine).playerController.capsuleCollider.bounds.center;
        Ray downwardsRayFromCapsuleCenter = new Ray(capsuleCollierCenterInWorld, Vector3.down);
        if (Physics.Raycast(downwardsRayFromCapsuleCenter, out RaycastHit hit,
            PlayerDataManager.Instance.playerData.jumpToGroundRayDistance, 
            PlayerDataManager.Instance.layerData.groundedMask,
            QueryTriggerInteraction.Ignore))
        {
            float groundAngle = Vector2.Angle(hit.normal, -downwardsRayFromCapsuleCenter.direction);
            //if (IsMovingUp())
            //{
            //    float forceModifier = PlayerDataManager.Instance.playerData.jumpForceModifierOnSlopeUpwards.Evaluate(groundAngle);
            //    JumpForce.x *= forceModifier;
            //    JumpForce.z *= forceModifier;
            //}
            //if (IsMovingDown())
            //{
            //    float forceModifier = jumpData.JumpForceModifierOnSlopeDownwards.Evaluate(groundAngle);
            //    JumpForce.y *= forceModifier;
            //}
        }

        ResetVelocity();
        ((MotionStateMachine)_stateMachine).playerController.rgbody.AddForce(JumpForce, ForceMode.VelocityChange);
    }
    #region main method

    #endregion
}
