using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerSwimmingState : PlayerWaterState
{
    public override StateType GetStateType()
    {
        return StateType.Swimming;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        PlayerDataManager.Instance.reusableData.movementSpeedModifier =
           PlayerDataManager.Instance.playerData.runSpeed;
        StartAnimation(PlayerDataManager.Instance.animationData.SwimmingParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.SwimmingParameterHash);
    }
    protected override void OnMovementCanceled(InputAction.CallbackContext context)
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        _stateMachine.ChangeState(stateMachine.states[StateType.LightStopSwimming]);
    }
    protected override void AddInputActionsCallbacks()
    {
        base.AddInputActionsCallbacks();
        PlayerInput.Instance.PlayerActions.Dash.started += OnDashStarted;
    }
    protected override void RemoveInputActionCallbacks()
    {
        base.RemoveInputActionCallbacks();
        PlayerInput.Instance.PlayerActions.Dash.started -= OnDashStarted;
    }
    private void OnDashStarted(UnityEngine.InputSystem.InputAction.CallbackContext content)
    {
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.WaterDashing]);
    }
}
