using Imba.Audio;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class RocketController : MonoBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] [Range(0f, 20f)] private float _flyForce = 10f;
    [SerializeField] private LayerMask _collisionCheckLayer;
    [SerializeField] private LayerMask _obstacleLayer;
    [SerializeField] private float _distanceToActive = 50f;
    [SerializeField] RocketName _rocketName;
    [SerializeField] private Collider _collider;

    private Vector3 _startToCheckDistance = Vector3.zero;
    private ulong ownerId = 1000;
    private bool isCollision= false;
    private void Awake()
    {
        _collider = GetComponent<Collider>();   
    }
    private void OnDisable()
    {
        ownerId = 1000;
        _collider.isTrigger = false;
        isCollision = true;
    }
    public void SetData(Vector3 position, ulong ownerId)
    {
        gameObject.transform.position = position;
        this.ownerId = ownerId;
        _startToCheckDistance = position;
        _collider.isTrigger = true;
        isCollision=true;
    }
    public void Update()
    {
        if (_startToCheckDistance == Vector3.zero) return;
        float distance = Vector3.Distance(_startToCheckDistance, gameObject.transform.position);
        if(distance > _distanceToActive)
        {
            SceneRunningGameManager.Instance.ReturnRocket(this, _rocketName);
        }
    }
    private void FixedUpdate()
    {
        Vector3 dir = transform.forward;
        dir.y = 0f;

        Vector3 flyForce = dir * _flyForce - new Vector3(_rigidbody.velocity.x, 0f, _rigidbody.velocity.z);
        _rigidbody.AddForce(flyForce, ForceMode.VelocityChange);

    }
    private void OnTriggerEnter(Collider other)
    {
        if(isCollision == false ) return;
        if(_startToCheckDistance == Vector3.zero) return;

        if (LayerData.ContainLayer(_collisionCheckLayer, other.gameObject.layer))
        {
            PlayerRunningGameController runningGameController = other.gameObject.transform.parent.GetComponentInParent<PlayerRunningGameController>();

            if (runningGameController.OwnerClientId == ownerId) return;
            runningGameController.TakeDamge();
            SceneRunningGameManager.Instance.ReturnRocket(this, _rocketName);
            return;
        }
       
        if (LayerData.ContainLayer(_obstacleLayer, other.gameObject.layer))
        {
            RunningGame.Obstacle obstacle = other.gameObject.GetComponent<RunningGame.Obstacle>();
            SceneRunningGameManager.Instance.ReturnRocket(this, _rocketName);
            return;
        }
    }
}
