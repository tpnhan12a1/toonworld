using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerWaterDashingState : PlayerWaterState
{
    public override StateType GetStateType()
    {
        return StateType.WaterDashing;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        PlayerDataManager.Instance.reusableData.movementSpeedModifier =
           PlayerDataManager.Instance.playerData.dashSpeed;
        StartAnimation(PlayerDataManager.Instance.animationData.WaterDashingParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.WaterDashingParameterHash);
    }
    protected override void OnMovementCanceled(InputAction.CallbackContext context)
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        _stateMachine.ChangeState(stateMachine.states[StateType.HardStopSwimming]);
    }
}
