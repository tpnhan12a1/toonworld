using SuperScrollView;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuickChatContentItem : LoopListViewItem2
{
    [SerializeField] private TMP_Text _txtContent;
    [SerializeField] private ContentSizeFitter _sizeFitter;
    private ChatContent _chatContent;
    ChatGridViewController _chatGridViewController;
    public void OnRefresh(ChatGridViewController chatGridViewController, ChatContent chatContent,Color color)
    {
        _chatGridViewController = chatGridViewController;
        _txtContent.SetText(new StringBuilder()
            .Append("[")
            .Append(chatContent.type.ToString())
            .Append("] ")
            .Append(chatContent.displayName)
            .Append(": ")
            .Append(chatContent.content).ToString());

        _sizeFitter.SetLayoutVertical();
        Vector2 size;
        size.x = _txtContent.GetComponent<RectTransform>().sizeDelta.x;
        size.y = _txtContent.GetComponent<RectTransform>().sizeDelta.y + 20;

        GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
       
        _txtContent.color = color;
    }
}
