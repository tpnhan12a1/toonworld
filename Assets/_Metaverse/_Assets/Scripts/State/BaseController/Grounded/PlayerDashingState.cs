using Imba.Audio;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerDashingState : PlayerGroundedState
{
    bool shouldKeepRotating = false;
    float startTime = 0f;
    private int cosecutiveDashesUsed;

    public override StateType GetStateType()
    {
        return StateType.Dashing;
    }
    public override void OnEnterState()
    {
        PlayerDataManager.Instance.reusableData.movementSpeedModifier = PlayerDataManager.Instance.playerData.dashSpeed;
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.DashParameterHash);
        PlayerDataManager.Instance.reusableData.currentJumpForce = PlayerDataManager.Instance.playerData.strongForce;
        Dash();
       
        shouldKeepRotating = PlayerDataManager.Instance.reusableData.movementInput != Vector2.zero;
        UpdateConsecutiveDashes();
        PlayerDataManager.Instance.reusableData.isDash = true;
        AudioManager.Instance.PlaySFX(AudioName.Dash);
        startTime = Time.time;
        

    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.DashParameterHash);
        PlayerDataManager.Instance.reusableData.isDash = false;
        SetBaseRotatonData();
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        if (CheckColliderInFront())
        {
            MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
            stateMachine.ChangeState(stateMachine.states[StateType.HardStopping]);
            return;
        }
        if (!shouldKeepRotating) return;

        RotateTowardsTargetRotation();
    }

    private void Dash()
    {
        Vector3 dashDirection = ((MotionStateMachine)_stateMachine).playerController.transform.forward;
        //dashDirection.y = 0f;
        UpdateTargetRotation(dashDirection, false);
        if (PlayerDataManager.Instance.reusableData.movementInput != Vector2.zero)
        {
            UpdateTargetRotation(GetMovementDirection());
            dashDirection = GetTargetRotaionDirection(PlayerDataManager.Instance.reusableData.currentTargetRotation.y);
        }
           ((MotionStateMachine)_stateMachine).playerController.rgbody.velocity = dashDirection * 0.5f * GetMovementSpeed(false);
    }
    private void UpdateConsecutiveDashes()
    {
        if (!IsConsecutive())
        {
            cosecutiveDashesUsed = 0;
        }
        ++cosecutiveDashesUsed;
        if (cosecutiveDashesUsed == PlayerDataManager.Instance.playerData.consecutiveDashesLimitAmount)
        {
            cosecutiveDashesUsed = 0;
           PlayerInput.Instance.DisableActionFor(PlayerInput.Instance.PlayerActions.Dash, PlayerDataManager.Instance.playerData.dashLimitReachedCooldown);
        }
    }
    private bool IsConsecutive()
    {
        return Time.time < startTime + PlayerDataManager.Instance.playerData.timeToColliderConsecutive;
    }
    protected override void AddInputActionsCallbacks()
    {
        base.AddInputActionsCallbacks();
        PlayerInput.Instance.PlayerActions.Move.performed += OnMovemmentPerformed;
    }

    public override void OnAnimationTransitionEvent()
    {
        base.OnAnimationTransitionEvent();
        if (PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero)
        {
            ((MotionStateMachine)(_stateMachine)).playerController.rgbody.velocity = Vector2.zero;
            _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Idling]);

            return;
        }
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Running]);

    }
    private void OnMovemmentPerformed(InputAction.CallbackContext context)
    {
        shouldKeepRotating = true;
    }
    protected override void OnDashStarted(InputAction.CallbackContext context)
    {

    }
}
