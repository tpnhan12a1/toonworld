using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameSwipeStateMachine : StateMachine
{
    private PlayerRunningGameController _playerRunningGameController;

    protected Dictionary<StateType, IState> _states = new Dictionary<StateType, IState>();
    public Dictionary<StateType, IState> states
    {
        get { return _states; }
        set { _states = value; }
    }
    public PlayerRunningGameController playerRunningGameController { get { return _playerRunningGameController; } }
    public RunningGameSwipeStateMachine(PlayerRunningGameController playerRunningGameController)
    {
        _playerRunningGameController = playerRunningGameController;
        Init();
    }

    public override StateMachineType GetStateMachineType()
    {
        return StateMachineType.RunningGameSwipeStateMachine;
    }

    public override void Init()
    {
       RunningGameEmptySwipingState runningGameEmptySwipingState = new RunningGameEmptySwipingState();
        _states.Add(runningGameEmptySwipingState.GetStateType(), runningGameEmptySwipingState);
        runningGameEmptySwipingState.SetStateMachine(this);

        RunningGameLeftSwipingState runningGameLeftSwipingState = new RunningGameLeftSwipingState();
        _states.Add(runningGameLeftSwipingState.GetStateType(), runningGameLeftSwipingState);
        runningGameLeftSwipingState.SetStateMachine(this);

        RunningGameRightSwipingState runningGameRightSwipingState = new RunningGameRightSwipingState();
        _states.Add(runningGameRightSwipingState.GetStateType(), runningGameRightSwipingState);
        runningGameRightSwipingState.SetStateMachine(this);

        RunningGameRollingState runningGameRollingState = new RunningGameRollingState();
        _states.Add(runningGameRollingState.GetStateType(), runningGameRollingState);
        runningGameRollingState.SetStateMachine(this);
    }
    public void StartBoolAnimation(int animationHash)
    {
        playerRunningGameController.animator.SetBool(animationHash, true);
    }
    public void StopBoolAnimation(int animationHash)
    {
        playerRunningGameController.animator.SetBool(animationHash, false);
    }
    public void StartFloatAnimation(int animationHash, float value)
    {
        playerRunningGameController.animator.SetFloat(animationHash, value);
    }
}
