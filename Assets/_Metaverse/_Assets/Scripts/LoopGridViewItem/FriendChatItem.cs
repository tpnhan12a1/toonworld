using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FriendChatItem : LoopGridViewItem
{
    [SerializeField] private SpriteTab _spriteTab;
    [SerializeField] private TMP_Text _txtOnline;
    [SerializeField] private TMP_Text _txtOffline;
    [SerializeField] private TMP_Text _txtDisplayName;
  
    private Button _btnSelect;
    private ChatPopup _chatPopup;
    private FriendChatInfo _friendChatInfo;
    private TabController _tabController;
    public void Awake()
    {
        _btnSelect = GetComponent<Button>();
        _btnSelect.onClick.AddListener(OnClickThis);
    }
   
    public void OnRefresh(FriendChatInfo friendChatInfo, ChatPopup chatPopup, TabController tabController)
    {
        _friendChatInfo = friendChatInfo;
        _chatPopup = chatPopup;
        _tabController = tabController;
        _tabController.AddTab(_spriteTab);
        _txtDisplayName.text = _friendChatInfo.displayName;
        SetStatus();
    }

    private void SetStatus()
    {
        bool status = ClientManager.Instance.clientConecnted.ContainsKey(_friendChatInfo.friendPlayfabId);
        _txtOnline.SetActive(status);
        _txtOffline.SetActive(!status);
    }

    public void OnSelect()
    {
        _spriteTab.tabController.SelectedTab(_spriteTab);
    }
    private void OnClickThis()
    {
        OnSelect();
        _chatPopup.Select(_friendChatInfo);

        TargetChat targetChat = new TargetChat()
        {
            displayName = _friendChatInfo.displayName,
            playfabId = _friendChatInfo.friendPlayfabId
        };
        PlayerDataManager.Instance.globalReusableData.SetTargetChat(targetChat);
        PlayerDataManager.Instance.globalReusableData.typeChat = TypeChat.Private;
    }
}
