using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "RuningGameSkill", menuName = "Custom/Skill/RunningGameSkill")]
public class RuninngGameSkillSO : ScriptableObject
{
    public Sprite icon;
    public GiftName giftName;
}
