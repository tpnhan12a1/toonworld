using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHardStoppingState : PlayerStoppingState
{
    public override StateType GetStateType()
    {
        return StateType.HardStopping;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        StartAnimation(PlayerDataManager.Instance.animationData.HardStopParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.HardStopParameterHash);
    }
}
