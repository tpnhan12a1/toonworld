using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameEmptyAttackingState : IState
{
    protected RunningGameAttackingStateMachine _runningGameAttackingStateMachine;
    public StateType GetStateType()
    {
        return StateType.RunningGameEmptyAttackingState;
    }

    public void OnAnimationEnterEvent()
    {
       
    }

    public void OnAnimationExitsEvent()
    {
       
    }

    public void OnAnimationTransitionEvent()
    {
        
    }

    public void OnEnterState()
    {
        Debug.Log($"Enter {GetStateType()}");
        _runningGameAttackingStateMachine.playerRunningGameController.runningGameReusableData.RegisterOnUseGift(OnUseSkill);
    }

    public void OnExitState()
    {
        Debug.Log($"Exit {GetStateType()}");
        _runningGameAttackingStateMachine.playerRunningGameController.runningGameReusableData.UnRegisterOnUseGift(OnUseSkill);
    }

    private void OnUseSkill(RunningGameReusableData runningGameReusableData,GiftName giftName)
    {
       switch(giftName)
       {
            case GiftName.Bullet:
                _runningGameAttackingStateMachine.ChangeState(_runningGameAttackingStateMachine.states[StateType.RunningGameRocketAttackingState]);
                runningGameReusableData.RemoveSkillData(giftName);
                break;
       }
    }

    public void OnHandleInput()
    {
        
    }

    public void OnPhysicsUpdate()
    {
       
    }

    public void OnTriggerEnter(Collider collider)
    {
       
    }

    public void OnTriggerExit(Collider collider)
    {
       
    }

    public void OnUpdate()
    {

    }

    public void SetStateMachine(StateMachine stateMachine)
    {
        _runningGameAttackingStateMachine = (RunningGameAttackingStateMachine)stateMachine;
    }
}
