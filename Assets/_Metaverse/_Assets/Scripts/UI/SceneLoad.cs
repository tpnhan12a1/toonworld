using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SceneLoad : MonoBehaviour
{
    [SerializeField] private TMP_Text _txtLoadPersent;
    [SerializeField] private Slider _slider;
    [SerializeField] private string _preTextLoader = "Loading...";
    public void SetLoadValue(float loadValue)
    {
        _slider.value = loadValue;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(_preTextLoader).Append((int)(loadValue * 100)).Append("%");
        _txtLoadPersent.text = stringBuilder.ToString();
    }
}
