using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ReusableData 
{
    public Vector2 movementInput = new Vector2();
    [field: SerializeField]
    [field: Range(0f, 10f)]
    public float movementSpeedModifier = 1f;
    [field: SerializeField]
    [field: Range(0f, 10f)]
    public float movementOnSlopeSpeedModifier = 0f;
    public bool shouldSprint = false;
    [field: SerializeField]
    [field: Range(0f, 10f)]
    public float movementDecelerationForce = 1f;
    [field: SerializeField] public Vector3 targetRotationReachTime;

    [SerializeField]
    [Range(0f, 10f)] private float _delayAttack = 0.5f;

    private bool _isDash = false;
    private bool _isGround = false;
    private bool _isWater = false;

    private Vector3 _currentTargetRotation;
    private Vector3 _damgedTargetRotationCurrentVelocity;
    private Vector3 _timeReachTargetRotation;
    private Vector3 _damgedTargetRotationPassedTime;

    public Action<bool> OnIsDashChanged;
    public Action<bool> OnIsGroundChanged;
    public Action<bool> OnWaterChanged;

    public bool isGround { get { return _isGround; } set { OnIsGroundChanged?.Invoke(value); _isGround = value; } }
    public bool isWater { get { return _isWater; }  set { OnWaterChanged?.Invoke(value); _isWater = value; } }
    public bool isAttack { get; internal set; }
    public bool isAirborne { get; internal set; }
    public bool canMove { get; internal set; }
    public float delayAttack { get { return _delayAttack; } }
    public Vector3 currentJumpForce { get; set; }
    public bool isDash { get { return _isDash; } set { OnIsDashChanged?.Invoke(value); _isDash = value; } }

    public  Vector3 timeReachTargetRotation
    {
        get { return _timeReachTargetRotation; }
        set { _timeReachTargetRotation = value;}
    }
    public ref Vector3 damgedTargetRotationCurrentVelocity
    {
        get { return ref _damgedTargetRotationCurrentVelocity; }
    }
    public ref Vector3 currentTargetRotation
    {
        get { return ref _currentTargetRotation; }
    }
    public ref Vector3 damgedTargetRotationPassedTime
    {
        get { return ref _damgedTargetRotationPassedTime; }
    }


    public float GetMagnitude()
    {
        return movementInput.magnitude;
    }
    public bool ShouldWalk()
    {
        float magnitude = GetMagnitude();
        return magnitude < 0.5f;
    }
}
