using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningGameStumblingState : RunningGameStatusState
{
    public override StateType GetStateType()
    {
        return StateType.RunningGameStumblingState;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        _runningGameStatusStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.StumblingParameterHash);

        _runningGameStatusStateMachine.playerRunningGameController.runningGameReusableData.MinusSpeedModifier(
            _runningGameStatusStateMachine.playerRunningGameController.playerData.speedWhenStumbling);

    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameStatusStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.StumblingParameterHash);
    }
   
}
