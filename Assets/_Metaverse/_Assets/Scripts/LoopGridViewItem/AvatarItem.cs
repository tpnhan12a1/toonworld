using Imba.Audio;
using Imba.Utils;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteTab))]
[RequireComponent (typeof(Button))]
public class AvatarItem : LoopGridViewItem
{
    private SpriteTab _spriteTab;
    private Button _button;
    private TabController _tabController;
    [SerializeField] private Image _avatar;
    private string _id;
    private AvatarGridViewController _avatarGridViewController;
    private void Awake()
    {
        _spriteTab = GetComponent<SpriteTab>();
        _button = GetComponent<Button>();
        _button.onClick.AddListener(OnClick);
    }

    public void OnRefresh(string id, AvatarGridViewController controller)
    {
        _avatarGridViewController = controller;
        _id = id;
        _avatar.sprite = ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, id);
    }
    private void OnClick()
    {
        OnSelect();
        AudioManager.Instance.PlaySFX(AudioName.ClickButton);
        _avatarGridViewController.selectedAvatar = _id;
    }
    public void OnSelect()
    {
        _spriteTab.tabController.SelectedTab(_spriteTab);

    }
    public void SetTabController(TabController controller)
    {
        _tabController = controller;
        _tabController.AddTab(_spriteTab);
    }
}
