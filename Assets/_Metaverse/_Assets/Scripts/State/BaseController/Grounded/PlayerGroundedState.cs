using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public abstract  class PlayerGroundedState : PlayerMotionState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        PlayerDataManager.Instance.reusableData.isAirborne = false;
        StartAnimation(PlayerDataManager.Instance.animationData.GroundedParameterHash);
        PlayerDataManager.Instance.reusableData.isGround = true; 
        //Update should print
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        Float();
    }

    public virtual bool CheckColliderInFront()
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        RaycastHit hit;
        if (stateMachine.playerController.rgbody
            .SweepTest(stateMachine.playerController.transform.forward, out hit, PlayerDataManager.Instance.playerData.testCollitionDistance, QueryTriggerInteraction.Ignore))
        {
            return true;
        }return false;
    }
    
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.GroundedParameterHash);
        PlayerDataManager.Instance.reusableData.isGround = false;
    }
    protected override void OnContactWithGroundExited(Collider collider)
    {
        if (IsThereGroundUnderneath()) return;

        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;

        Vector3 capsuleColloderCenterInWorld = stateMachine.playerController.capsuleCollider.bounds.center;

        Ray downwardRayFromCapsuleBottom = new Ray(capsuleColloderCenterInWorld
            - new Vector3(0f, ((MotionStateMachine)_stateMachine).playerController.
           capsuleCollider.bounds.extents.y, 0f), Vector3.down);

        if (!Physics.Raycast(downwardRayFromCapsuleBottom, out _,
            PlayerDataManager.Instance.playerData.groundToFallDistance,
            PlayerDataManager.Instance.layerData.groundedMask,
            QueryTriggerInteraction.Ignore))
        {
            OnFall();
        }
    }
    protected override void OnContactWithWater(Collider collider)
    {
        if(PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero)
        {
            _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.WaterIdling]);
            return;
        }
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Swimming]);
    }
    private bool IsThereGroundUnderneath()
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        BoxCollider groundCheckCollider = stateMachine.playerController.groundCheckCollider;
        if (groundCheckCollider != null)
        {
            Vector3 groundColloderCenterInWorld = groundCheckCollider.bounds.center;
            Collider[] overlappedGroundColliders = Physics.OverlapBox(groundColloderCenterInWorld,
            groundCheckCollider.bounds.extents, groundCheckCollider.transform.rotation,
           PlayerDataManager.Instance.layerData.groundedMask, QueryTriggerInteraction.Ignore);
            return overlappedGroundColliders.Length > 0;
        }
        return false;

    }
    #region main method
    protected virtual void OnMove()
    {

        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        if (PlayerDataManager.Instance.reusableData.shouldSprint)
        {
            stateMachine.ChangeState(stateMachine.states[StateType.Sprinting]);
            return;
        }
        if (PlayerDataManager.Instance.reusableData.ShouldWalk())
        {
            stateMachine.ChangeState(stateMachine.states[StateType.Walking]);
            return;
        }

        stateMachine.ChangeState(stateMachine.states[StateType.Running]);

    }

    private float SetSlopeSpeedModifierOnAngle(float angle)
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        float slopeSpeedModifier = PlayerDataManager.Instance.playerData.slopeSpeedAngle.Evaluate(angle);
        if (PlayerDataManager.Instance.reusableData.movementOnSlopeSpeedModifier != slopeSpeedModifier)
        {
            PlayerDataManager.Instance.reusableData.movementOnSlopeSpeedModifier = slopeSpeedModifier;
            //Update camera
        }
        return slopeSpeedModifier;
    }

    private void Float()
    {
        Vector3 CapsuleColliderCenterInSpace = ((MotionStateMachine)_stateMachine)
            .playerController.capsuleCollider.bounds.center;
        Ray downwardRayFromCapsuleCenter = new Ray(CapsuleColliderCenterInSpace,Vector3.down);
        if (Physics.Raycast(downwardRayFromCapsuleCenter, out RaycastHit hit, 
            PlayerDataManager.Instance.playerData.floatRayDistance,
          PlayerDataManager.Instance.layerData.groundedMask, QueryTriggerInteraction.Ignore))
        {
            float groundAngle = Vector3.Angle(hit.normal, -downwardRayFromCapsuleCenter.direction);
            float slopeSpeedModifier = SetSlopeSpeedModifierOnAngle(groundAngle);

            if (slopeSpeedModifier == 0f) return;

            float distanceToFloatingPoint =
                ((MotionStateMachine)_stateMachine)
                .playerController.capsuleCollider.center.y *
                ((MotionStateMachine)_stateMachine).playerController.
                transform.localScale.y - hit.distance;
            if (distanceToFloatingPoint == 0) return;
            float amountToLift =
                distanceToFloatingPoint * PlayerDataManager.Instance.playerData.stepReachForce - GetPlayerVerticalVelocity().y;
            Vector3 liftForce = new Vector3(0, amountToLift, 0);
            ((MotionStateMachine)_stateMachine).playerController.rgbody.AddForce(liftForce, ForceMode.VelocityChange);
        }

    }
    #endregion

    #region call back
    protected override void AddInputActionsCallbacks()
    {
        base.AddInputActionsCallbacks();
        PlayerInput.Instance.PlayerActions.Jump.started += OnJumpStarted;
        PlayerInput.Instance.PlayerActions.Dash.started += OnDashStarted;
    }
    protected override void RemoveInputActionCallbacks()
    {
        base.RemoveInputActionCallbacks();
        PlayerInput.Instance.PlayerActions.Jump.started -= OnJumpStarted;
       PlayerInput.Instance.PlayerActions.Dash.started -= OnDashStarted;
    }
    protected virtual void OnJumpStarted(InputAction.CallbackContext content)
    {

        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        _stateMachine.ChangeState(stateMachine.states[StateType.Jumping]);
    }


    protected virtual void OnDashStarted(InputAction.CallbackContext context)
    {
        if(!CheckColliderInFront())
            _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Dashing]);
    }

    #endregion
}
