using Cinemachine;
using RunningGame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.Pool;
using UnityEngine.UIElements;
using static NetCodeModels;

public class SceneRunningGameManager : ManualSingletonMono<SceneRunningGameManager>
{
    [SerializeField]
    private GameObject _rootMap;
    [SerializeField]
    private GameObject _road;
    [SerializeField]
    private LayerMask _markDetectObstacle;
    [SerializeField]
    [Range(0, 200)] private int defaultCapacity = 10;
    [SerializeField]
    [Range(0, 1000)] private int maxCapacity = 200;

    [SerializeField] private List<RocketName> rocketInScenes;
   
    [SerializeField]
    private Dictionary<SegmentName, ObjectPool<GameObject>> _segmentPools = new Dictionary<SegmentName, ObjectPool<GameObject>>();

    [SerializeField]
    private Dictionary<ObstacleName, ObjectPool<RunningGame.Obstacle>> _obstaclePools = new Dictionary<ObstacleName, ObjectPool<RunningGame.Obstacle>>();

    [SerializeField]
    private Dictionary<GiftName, ObjectPool<GiftController>> _prensentPools = new Dictionary<GiftName, ObjectPool<GiftController>>();

    [SerializeField]
    private Dictionary<RocketName, ObjectPool<RocketController>> _rocketPools = new Dictionary<RocketName, ObjectPool<RocketController>>();

    [SerializeField] private Transform _intCameraTranform;
    [SerializeField] private CinemachineVirtualCamera _cinemachineVirtualCamera;
    public GameObject rootMap => _rootMap;
    public GameObject road { get { return _road; } }
    public Transform initCameraTranform { get { return _intCameraTranform; } }
    public CinemachineVirtualCamera cinemachineVirtualCamera { get { return _cinemachineVirtualCamera; } }

    private PlayerRunningGameController _playerRunningGameController;
    private RunningGamePlayNetworkController _runningGamePlayNetworkController;

    protected List<TrackSegment> _segments = new List<TrackSegment>();
    List<SegmentName> _instanceSegmentNames = new List<SegmentName>();
    List<SegmentName> _segmentNames = new List<SegmentName>();
    protected List<TrackSegment> _pastSegments = new List<TrackSegment>();
    List<SegmentName> _pastSegmentName = new List<SegmentName>();

    protected const int k_DesiredSegmentCount = 10;
    protected const float k_SegmentRemovalDistance = -30f;
    private int _spawnedSegments = 0;
    private int _index = 0;
    public RunningGamePlayNetworkController runningGamePlayNetworkController { get { return _runningGamePlayNetworkController; } set { _runningGamePlayNetworkController = value; } }
    public PlayerRunningGameController playerRunningGameController { get { return _playerRunningGameController; } set { _playerRunningGameController = value; } }
    private void OnEnable()
    {
        Array segmentNames = Enum.GetValues(typeof(SegmentName));
        foreach (SegmentName seg  in segmentNames)
        {
            RegisterPrefabInternal(seg);
        }

        Array obstacleNames = Enum.GetValues(typeof(ObstacleName));
        foreach(ObstacleName obstacleName in obstacleNames)
        {
            RegisterPrefabInternal(obstacleName);
        }

        Array presentNames = Enum.GetValues(typeof(GiftName));
        foreach (GiftName presentName in presentNames)
        {
            RegisterPrefabInternal(presentName);
        }

        foreach(RocketName rocketInScene in rocketInScenes)
        {
            RegisterPrefabInternal(rocketInScene);
        }
        //ClientManager.Instance.OnSpawnRocket += OnSpawnRocket;
    }
    protected override void OnDestroy()
    {
        base.OnDestroy();
        _obstaclePools.Clear();
        _instanceSegmentNames.Clear();
        _prensentPools.Clear();
        _rocketPools.Clear();
    }

    public GameObject GetSegment(SegmentName segmentName, Vector3 position, Quaternion rotation)
    {
        GameObject gameObjet = _segmentPools[segmentName].Get();
        gameObject.transform.position = position;
        gameObject.transform.rotation = rotation;
        return gameObjet;
    }
    public void ReturnSegment(GameObject gameObject, SegmentName segmentName)
    {
        _segmentPools[segmentName].Release(gameObject);
    }
    public void ReturnObstacle(RunningGame.Obstacle obstacle, ObstacleName obstacleName)
    {
        _obstaclePools[obstacleName].Release(obstacle);
    }
    internal void ReturnPresent(GiftController giftController, GiftName giftName)
    {
        _prensentPools[giftName].Release(giftController);
    }
    public void ReturnRocket(RocketController rocketController, RocketName rocketName)
    {
        _rocketPools[rocketName].Release(rocketController);
    }
    public void RegisterPrefabInternal(RocketName rocketInScene)
    {
        void ActionOnDestroy(RocketController obj)
        {
            Destroy(obj.gameObject);
        }

        void ActionOnRelease(RocketController obj)
        {
            obj.gameObject.SetActive(false);
        }

        void ActionOnGet(RocketController obj)
        {
            obj.gameObject.SetActive(true);
        }

        RocketController CreateFunc()
        {
            GameObject gameObject = Imba.Utils.ResourceManager.Instance.GetResourceByName<GameObject>(
              new StringBuilder().Append("RunningGame/Projectile/").Append(rocketInScene).ToString());
            return Instantiate(gameObject).GetComponent<RocketController>();
        }

        _rocketPools.Add(rocketInScene, new ObjectPool<RocketController>(CreateFunc, ActionOnGet, ActionOnRelease, ActionOnDestroy, defaultCapacity: defaultCapacity, maxSize: 10));

        var prewarmObjects = new List<RocketController>();
        for (var i = 0; i < defaultCapacity; i++)
        {
            prewarmObjects.Add(_rocketPools[rocketInScene].Get());
        }
        foreach (var ob in prewarmObjects)
        {
            _rocketPools[rocketInScene].Release(ob);
        }
    }
    public void RegisterPrefabInternal(ObstacleName obstacleName)
    {
        void ActionOnDestroy(RunningGame.Obstacle obj)
        {
            Destroy(obj.gameObject);
        }

        void ActionOnRelease(RunningGame.Obstacle obj)
        {
            obj.gameObject.SetActive(false);
        }

        void ActionOnGet(RunningGame.Obstacle obj)
        {
            obj.gameObject.SetActive(true);
        }

        RunningGame.Obstacle CreateFunc()
        {
            GameObject gameObject = Imba.Utils.ResourceManager.Instance.GetResourceByName<GameObject>(
              new StringBuilder().Append("RunningGame/Obstacle/").Append(obstacleName).ToString());
            return Instantiate(gameObject).GetComponent<RunningGame.Obstacle>();
        }

        _obstaclePools.Add(obstacleName, new ObjectPool<RunningGame.Obstacle>(CreateFunc, ActionOnGet, ActionOnRelease, ActionOnDestroy, defaultCapacity: defaultCapacity,maxSize: 30));
        
        var prewarmObjects = new List<RunningGame.Obstacle>();
        for (var i = 0; i < defaultCapacity; i++)
        {
            prewarmObjects.Add(_obstaclePools[obstacleName].Get());
        }
        foreach (var ob in prewarmObjects)
        {
            _obstaclePools[obstacleName].Release(ob);
        }
    }
    public void RegisterPrefabInternal(GiftName giftName)
    {
        void ActionOnDestroy(GiftController obj)
        {
            Destroy(obj.gameObject);
        }

        void ActionOnRelease(GiftController obj)
        {
            obj.gameObject.SetActive(false);
        }

        void ActionOnGet(GiftController obj)
        {
            obj.gameObject.SetActive(true);
        }

        GiftController CreateFunc()
        {
            GameObject gameObject = Imba.Utils.ResourceManager.Instance.GetResourceByName<GameObject>(
              new StringBuilder().Append("RunningGame/Present/").Append(giftName).ToString());
            return Instantiate(gameObject).GetComponent<GiftController>();
        }

        _prensentPools.Add(giftName, new ObjectPool<GiftController>(CreateFunc, ActionOnGet, ActionOnRelease, ActionOnDestroy, defaultCapacity: 3, maxSize: 10));

        var prewarmObjects = new List<GiftController>();
        for (var i = 0; i < defaultCapacity; i++)
        {
            prewarmObjects.Add(_prensentPools[giftName].Get());
        }
        foreach (var ob in prewarmObjects)
        {
            _prensentPools[giftName].Release(ob);
        }
    }

    private void RegisterPrefabInternal(SegmentName seg)
    {
        GameObject CreateFunc()
        {
            GameObject gameObject = Imba.Utils.ResourceManager.Instance.GetResourceByName<GameObject>(
                new StringBuilder().Append("RunningGame/Segment/").Append(seg).ToString());
            return Instantiate(gameObject);
        }

        void ActionOnGet(GameObject gamObject)
        {
            gamObject.SetActive(true);
        }

        void ActionOnRelease(GameObject gamObject)
        {
            gamObject.SetActive(false);
        }

        void ActionOnDestroy(GameObject gamObject)
        {
            Destroy(gamObject);
        }
        _segmentPools.Add(seg, new ObjectPool<GameObject>(CreateFunc, ActionOnGet, ActionOnRelease, ActionOnDestroy, defaultCapacity: defaultCapacity));

        var prewarmObjects = new List<GameObject>();
        for (var i = 0; i < defaultCapacity; i++)
        {
            prewarmObjects.Add(_segmentPools[seg].Get());
        }
        foreach (var ob in prewarmObjects)
        {
            _segmentPools[seg].Release(ob);
        }
    }
    private void Update()
    {
        if (playerRunningGameController == null) return;

        while (_spawnedSegments < k_DesiredSegmentCount &&
            _segmentNames.Count > _index)
        {
            SpawnNewSegment();
        }
        if (_segments.Count <= 0) return;

        if (_playerRunningGameController.transform.position.z > (_segments[0].transform.position.z - k_SegmentRemovalDistance))
        {
            ReturnSegment(_segments[0].gameObject, _instanceSegmentNames[0]);
            _segments.Remove(_segments[0]);
            _instanceSegmentNames.Remove(_instanceSegmentNames[0]);
            _spawnedSegments--;
        }
    }
    public void SpawnNewSegment()
    {
        SegmentName segmentName = _segmentNames[_index];
        _index++;
        GameObject segment = GetSegment(segmentName, Vector3.zero, Quaternion.identity);
        TrackSegment newSegment = segment.GetComponent<TrackSegment>();
        Vector3 currentExitPoint;

        Transform currentEndTransfrom;
        Transform currentEntryTransfrom;
        if (_segments.Count > 0)
        {
            _segments[_segments.Count - 1].GetPoint(out currentEntryTransfrom, out currentEndTransfrom);
            currentExitPoint = currentEndTransfrom.position;
            
        }
        else
        {
            currentExitPoint = Vector3.zero;
        }
        
        Transform entryTransfrom;
        Transform endTransfrom;
        newSegment.GetPoint(out entryTransfrom,out endTransfrom);

        Vector3 pos = currentExitPoint + (newSegment.transform.position - entryTransfrom.position);
        Debug.Log(pos);
        newSegment.transform.position = pos;

        _segments.Add(newSegment);
        _instanceSegmentNames.Add(segmentName);
        _spawnedSegments++;
    }

    
    private void OnSpawnRocket(NetworkObject networkObject, RocketName rocketName, ulong clientId)
    {
        Vector3 position = networkObject.GetComponent<PlayerRunningGameController>().initBulletTranform.position;
        RocketController rocketController = _rocketPools[rocketName].Get();
        rocketController.SetData(position, clientId);
    }
    //==============================New version==============================================//
    public void AddSegment(SegmentName segmentName)
    {
        _segmentNames.Add(segmentName);
    }
    internal void SetupData(RunningGamePlayNetworkController runningGamePlayNetworkController)
    {
        _runningGamePlayNetworkController = runningGamePlayNetworkController;
        foreach (var segment in runningGamePlayNetworkController.segmentNames)
        {
            AddSegment(segment.Value);
        }
        runningGamePlayNetworkController.OnSegmentAdd += OnSegmentAdd;
        runningGamePlayNetworkController.OnSpawnObstacle += OnSpawnObstacle;
        runningGamePlayNetworkController.OnSpawnPresent += OnSpawnPresent;
        runningGamePlayNetworkController.OnSpawnRocket += OnSpawnRocket;
    }

    private void OnSpawnPresent(PresentData presentData)
    {
        if (_playerRunningGameController == null) return;

        float y = 1f;
        float x = _playerRunningGameController.runningGameReusableData.GetXLocationOfLane(2);
        Vector3 position = new Vector3(x, 0f, presentData.z);
        Collider[] colliders = Physics.OverlapSphere(position, radius: 2f, layerMask: _markDetectObstacle);
        if (colliders.Length > 0)
        {
            y = 3f;
        }
        for (int i = 1; i <= 3; i++)
        {
            GiftController giftController = _prensentPools[presentData.giftName].Get();
            x = _playerRunningGameController.runningGameReusableData.GetXLocationOfLane(i);
            position = new Vector3(x, y, presentData.z);
            giftController.SetPosition(position);
        }
    }

    private void OnSpawnObstacle(NetCodeModels.ObstacleData obstacleData)
    {
        if (_playerRunningGameController == null) return;
        RunningGame.Obstacle obstacle = _obstaclePools[obstacleData.obstacleName].Get();
        float x = _playerRunningGameController.runningGameReusableData.GetXLocationOfLane(obstacleData.lane);
        switch (obstacleData.obstacleName)
        {
            case ObstacleName.ObstacleHighBarrier:
            case ObstacleName.ObstacleLowBarrier:
                x = _playerRunningGameController.runningGameReusableData.GetXLocationOfLane(2);
                break;
        }
        Vector3 position = new Vector3(x, 0f, obstacleData.z);
        obstacle.SetPosition(position);
    }

    private void OnSegmentAdd(SegmentName segmentName)
    {
        AddSegment(segmentName);
    }
}
