using SuperScrollView;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChatContentItem : LoopListViewItem2
{
    [SerializeField] private TMP_Text _txtChatContent;
    [SerializeField] private ContentSizeFitter _contentSizeFitter;
    [SerializeField] private float _offset = 50f;

    private Message _message;
    private ChatPopup _chatPopup;

    public void OnRefresh(Message message, ChatPopup chatPopup)
    {
        _message = message;
        _chatPopup = chatPopup;
        _txtChatContent.text = message.content;
        _contentSizeFitter.SetLayoutVertical();

        Vector2 size;
        size.x = _txtChatContent.GetComponent<RectTransform>().sizeDelta.x;
        size.y = _txtChatContent.GetComponent<RectTransform>().sizeDelta.y + _offset;
        GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
    }
}
