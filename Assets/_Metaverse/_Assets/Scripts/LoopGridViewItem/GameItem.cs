using Imba.UI;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class GameItem : LoopGridViewItem
{
    [SerializeField] private TMP_Text _txtGamePlayName;
    [SerializeField] private Image _imgGameAvatar;
   
   
    private Button _btn;

    private GamePlayDataSO _gamePlayDataSO;
    private GameGridViewController _gameGridViewController;
    private void Awake()
    {
        _btn = GetComponent<Button>();
        _btn.onClick.AddListener(OpenGameRoomLobbyPopup);
    }
    public void OnRefresh(GamePlayDataSO gamePlayDataSO, GameGridViewController gameGridViewController)
    {
        _gamePlayDataSO = gamePlayDataSO;
        _gameGridViewController = gameGridViewController;
        _txtGamePlayName.text = _gamePlayDataSO.gameName;
        _imgGameAvatar.sprite = _gamePlayDataSO.sprite;
    }
    private void OpenGameRoomLobbyPopup()
    {
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.GamePopup);
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.GameRoomLobbyPopup, _gamePlayDataSO.gamePlayType);
    }
}
