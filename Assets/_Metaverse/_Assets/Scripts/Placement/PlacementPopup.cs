using DG.Tweening;
using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PlacementPopup : UIPopup
{
    [SerializeField] private Button _btnClosePopup;
    [SerializeField] private Button _btnInventory;
    [SerializeField] private Button _btnInEnvironment;
    [SerializeField] private PlacementSystem _placementSystem;

    [SerializeField]private PlacementGridViewController _placementGridViewController;
    [SerializeField] private InstallItemGridViewController _installItemGridViewController;

    [SerializeField] private Camera _camera;
    [SerializeField] private LayerMask _worldUILayerMask;
    [SerializeField] private LayerMask _placementObjectLayerMask;

    protected override void OnInit()
    {
        base.OnInit();
        _btnClosePopup.onClick.AddListener(ClosePopup);
        _btnInventory.onClick.AddListener(OpenInventory);
        _btnInEnvironment.onClick.AddListener(OpenRecallItemPopup);
        _placementGridViewController.OnInit();
        _installItemGridViewController.OnInit();
    }

    private void OpenRecallItemPopup()
    {
        _installItemGridViewController.OnShow(_placementSystem);
    }

    private void OpenInventory()
    {
        if(_placementSystem.placementObjectPreview != null)
        {
            if (_placementSystem.isCanPlace)
            {
                _placementSystem.PlaceObject();
            }else
            {
                _placementSystem.StoreObject();
            }    
        }
        _placementGridViewController.OnShow(_placementSystem);
    }
    private void CloseInstallItemPopup()
    {
        _installItemGridViewController.OnHide();
    }
    private void CloseInventory()
    {
        _placementGridViewController.OnHide();
    }
    private void ClosePopup()
    {
        Hide();
        MainPopup mainPopup= (MainPopup) UIManager.Instance.PopupManager.GetPopup(UIPopupName.MainPopup);
        mainPopup.SetActiveHeader(true);
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.QuickChatPopup);

        APIModels.PlacementData placementDataRequest = new APIModels.PlacementData()
        {
            placementData = _placementSystem.placementManager.placementData,
        };

        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
            PlayfabContant.FunctionName.UpdatePlayerPlacement, placementDataRequest, true, (obj, data) =>
            {
                PlayerDataManager.Instance.placementManager.InitPlacementData(_placementSystem.placementManager.placementData);
            }, null);
    }

    protected override void OnShowing()
    {
        base.OnShowing();
        _camera = CameraManager.Instance.mainCamera;
        _placementSystem = SceneHomeManager.Instance.playmentSystem;
        MainPopup mainPopup = (MainPopup)UIManager.Instance.PopupManager.GetPopup(UIPopupName.MainPopup);
        mainPopup.SetActiveHeader(false);
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.QuickChatPopup);
        _placementSystem.StartPlacementMode();
    }
    protected override void OnHiding()
    {
        base.OnHiding();
        _placementSystem.StopPlacementMode();
    }
}
