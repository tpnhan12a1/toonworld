using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ResidentAnimationData
{
    [SerializeField] private string _isIdlingParameterName = "IsIdling";
    [SerializeField] private string _isStoppingParamenterName = "IsStopping";
    [SerializeField] private string _isMovingParamenterName = "IsMoving";
    [SerializeField] private string _isRotationParameterName = "IsRotation";
    [SerializeField] private string _isWalkingParamenterName = "IsWalking";
    [SerializeField] private string _isLightStoppingParamenterName = "IsLightStopping";
    [SerializeField] private string _isLeftRotationParamenteerName = "IsLeftRotation";
    [SerializeField] private string _isInteractionWithPlayerParameterName = "IsInteractionWithPlayer";
   
    public int IsIdlingParamenterHash  { get; private set; }
    public int IsStoppingParameterHash { get; private set; }
    public int IsMovingParameterHash { get; private set; }
    public int IsRotationParameterHash { get; private set; }
    public int IsWalkingParameterHash { get; private set; }
    public int IsLightStoppingParamenterHash { get; private set; }
    public int IsLeftRotationParameterHash { get; private set; }
    public int IsInteractionWithPlayerParamenterHash { get; private set; }
    public void Initialize()
    {
        IsIdlingParamenterHash = Animator.StringToHash(_isIdlingParameterName);
        IsStoppingParameterHash = Animator.StringToHash(_isStoppingParamenterName);
        IsMovingParameterHash = Animator.StringToHash(_isMovingParamenterName);
        IsRotationParameterHash = Animator.StringToHash(_isRotationParameterName);
        IsWalkingParameterHash = Animator.StringToHash(_isWalkingParamenterName);
        IsLightStoppingParamenterHash = Animator.StringToHash(_isLightStoppingParamenterName);
        IsLeftRotationParameterHash = Animator.StringToHash(_isLeftRotationParamenteerName);
        IsInteractionWithPlayerParamenterHash = Animator.StringToHash(_isInteractionWithPlayerParameterName);
    }
}
