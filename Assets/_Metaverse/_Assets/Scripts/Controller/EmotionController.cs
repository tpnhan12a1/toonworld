using Imba.Utils;
using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmotionController : MonoBehaviour
{
    [SerializeField] private EmotionGridViewController _emotionGridViewController;
    private List<EmotionSO> _emotionSOs;
    public void OnInit()
    {
        _emotionSOs = ResourceManager.Instance.GetAllEmotion();
        _emotionGridViewController.InitGridView(_emotionSOs.Count, OnGetEmotionByRowColumn);
    }

    private LoopGridViewItem OnGetEmotionByRowColumn(LoopGridView gridView, int index, int row, int column)
    {
        if (_emotionSOs.Count < 0) return null;
        InteractionIconItem interactionIconItem = (InteractionIconItem) gridView.NewListViewItem("InteractionIconItem");
        interactionIconItem.OnRefresh(_emotionGridViewController, _emotionSOs[index]);
        return interactionIconItem;
    }
}
