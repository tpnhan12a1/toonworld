using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ToonWorld;
public class FriendReceiveItem : LoopGridViewItem
{
    [SerializeField] private Image _avatar;
    [SerializeField] private TMP_Text _txtLevel;
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private Button _btnAccept;
    [SerializeField] private Button _btnDeny;
    [SerializeField] private TMP_Text _txtStatus;

    private Friend _friend;
    private FriendReceiveGridViewController _friendReceiveGridViewController;
    public void Awake()
    {
        _btnAccept.onClick.AddListener(AcceptFriendFriend);
        _btnDeny.onClick.AddListener(DenyFriendRequest);
    }

   
    public void OnRefresh(Friend friend, FriendReceiveGridViewController friendReceiveGridViewController)
    {
        _friendReceiveGridViewController = friendReceiveGridViewController;
        _friend = friend;
        Sprite avatar = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, friend.avatarUrl);
        _avatar.sprite = avatar;
        _txtDisplayName.text = friend.displayName;
        _txtLevel.text = friend.level.ToString();
    }
    private void DenyFriendRequest()
    {
        APIModels.FriendRequest friendRequest = new APIModels.FriendRequest();
        friendRequest.friendPlayfabId = _friend.friendPlayfabId;
        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
            PlayfabContant.FunctionName.RemoveFriend,
            friendRequest,
            false,
            null,
            null);
        NetCodeModels.FriendContent friendContent = new NetCodeModels.FriendContent();
        friendContent.message = PlayfabContant.FriendSyncTag.DenyFriend;
        friendContent.playfabReciverId = _friend.friendPlayfabId;
        friendContent.friend = new NetCodeModels.Friend(PlayfabManager.Instance.PlayfabID,
           PlayerDataManager.Instance.avatarUrl,
           PlayerDataManager.Instance.displayName,
           PlayerDataManager.Instance.level,
           new List<string>() { PlayfabContant.FriendTag.Pending });
        ServerManager.Instance.FriendControllerServerRpc(friendContent);

        PlayerDataManager.Instance.friendManager.RemoveFriend(_friend);

    }
    private void AcceptFriendFriend()
    {
        APIModels.FriendRequest friendRequest = new APIModels.FriendRequest();
        friendRequest.friendPlayfabId = _friend.friendPlayfabId;
        PlayfabManager.Instance.ExecuteFunction<APIModels.CommonResponse>(
            PlayfabContant.FunctionName.AcceptFriendRequest, 
            friendRequest, 
            false,
            null, 
            null);
        _friend.friendTags.Remove(PlayfabContant.FriendTag.Received);
        _friend.friendTags.Add(PlayfabContant.FriendTag.Friend);

        NetCodeModels.FriendContent friendContent = new NetCodeModels.FriendContent();
        friendContent.message = PlayfabContant.FriendSyncTag.AcceptedFriend;
        friendContent.playfabReciverId = _friend.friendPlayfabId;
        friendContent.friend = new NetCodeModels.Friend(PlayfabManager.Instance.PlayfabID,
           PlayerDataManager.Instance.avatarUrl,
           PlayerDataManager.Instance.displayName,
           PlayerDataManager.Instance.level,
           new List<string>() { PlayfabContant.FriendTag.Friend });
        ServerManager.Instance.FriendControllerServerRpc(friendContent);

        PlayerDataManager.Instance.friendManager.UpdateFriendData(_friend);
    }
}
