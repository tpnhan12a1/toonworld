using Cinemachine;
using Imba.Audio;
using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerRunningGameController : NetworkBehaviour
{
    [SerializeField] private Rigidbody _rigidbody;
    [SerializeField] private BoxCollider _groundCheckCollider;
    [SerializeField] private CapsuleCollider _collisionCheckCollider;
    [SerializeField] private CapsuleCollider _capsuleCollider;
    [SerializeField] private Transform _lookAt;
    [SerializeField] private Transform _mainCameraTransform;
    [SerializeField] private Animator _animator;
    [SerializeField] private RunningGameReusableData _runningGameReusableData;
    [SerializeField] private RunningGamePlayerSO _playerData;
    [SerializeField] private GameObject _rocket;
    [SerializeField] private GameObject _rightHandHolder;
    [SerializeField] private GameObject _leftHandHolder;
    [SerializeField] private Transform _initBulletTransform;
    [SerializeField] private Transform _centerRootTransform;
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private OwnerNetworkTransform _ownerNetworkTransform;

    private RunningGameMotionStateMachine _runningGameMotionStateMachine;
    private RunningGameSwipeStateMachine _runningGameSwipeStateMachine;
    private RunningGameStatusStateMachine _runningGameStatusStateMachine;
    private RunningGameAttackingStateMachine _runningGameAttackingStateMachine;
    private RunningGameDefendStateMachine _runningGameDefendStateMachine;
    
    private Dictionary<StateMachineType, StateMachine> _stateMachines = new Dictionary<StateMachineType, StateMachine>();

    public Rigidbody rb { get { return _rigidbody; }}
    public CapsuleCollider collisionCheckColider { get { return _collisionCheckCollider; } }
    public BoxCollider groundCheckCollider { get { return _groundCheckCollider; } }
    public CapsuleCollider capsuleCollider {  get { return _capsuleCollider; } }
    public Animator animator { get { return _animator; } }

    public RunningGameReusableData runningGameReusableData { get { return _runningGameReusableData; } }
    public RunningGamePlayerSO playerData { get { return _playerData; } }

    public RunningGameMotionStateMachine runningGameMotionStateMachine { get { return _runningGameMotionStateMachine; } }
    public RunningGameSwipeStateMachine runningGameSwipeStateMachine { get { return _runningGameSwipeStateMachine ; } }
    public RunningGameStatusStateMachine runningGameStatusStateMachine { get { return _runningGameStatusStateMachine; } }
    public RunningGameAttackingStateMachine RunningGameAttackingStateMachine { get { return _runningGameAttackingStateMachine ; } }
    public RunningGameDefendStateMachine RunningGameDefendStateMachine { get { return _runningGameDefendStateMachine; } }
    public GameObject rocket { get { return _rocket; } }
    public Transform initBulletTranform {  get { return _initBulletTransform; } }
    public AudioSource audioSource { get { return _audioSource; } }
    public OwnerNetworkTransform ownerNetworkTransform { get { return _ownerNetworkTransform; } }

  
   public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if(IsLocalPlayer)
        {
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.RunningGameControlPopup,this);
            SetUpRoadData();
            InitData();
            InitCamera();
            SceneRunningGameManager.Instance.playerRunningGameController = this;
        }
    }

    private void InitCamera()
    {
        Transform transform =  SceneRunningGameManager.Instance.initCameraTranform;
        SceneRunningGameManager.Instance.cinemachineVirtualCamera.Follow = _lookAt.transform;
    }

    //public override void OnDestroy()
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();

        if (IsLocalPlayer)
        {
            UIManager.Instance.PopupManager.HidePopup(UIPopupName.RunningGameControlPopup);
            foreach (var kvp in _stateMachines)
            {
                kvp.Value?.CurentState?.OnExitState();
            }
        }
    }
    private void SetUpRoadData()
    {
        _runningGameReusableData.currentLane = PlayerDataManager.Instance.globalReusableData.localLobby.GetLocalIndex()+1;

        _runningGameReusableData.SetUpData(SceneRunningGameManager.Instance.road, SceneRunningGameManager.Instance.rootMap);
    }
    private void InitData()
    {
       
        _runningGameMotionStateMachine = new RunningGameMotionStateMachine(this);
        _stateMachines.Add(_runningGameMotionStateMachine.GetStateMachineType(), _runningGameMotionStateMachine);

        _runningGameSwipeStateMachine = new RunningGameSwipeStateMachine(this);
        _stateMachines.Add(_runningGameSwipeStateMachine.GetStateMachineType(), _runningGameSwipeStateMachine);
        
        _runningGameStatusStateMachine = new RunningGameStatusStateMachine(this);
        _stateMachines.Add(_runningGameStatusStateMachine.GetStateMachineType(), _runningGameStatusStateMachine);

        _runningGameAttackingStateMachine = new RunningGameAttackingStateMachine(this);
        _stateMachines.Add(_runningGameAttackingStateMachine.GetStateMachineType(), _runningGameAttackingStateMachine);

        _runningGameDefendStateMachine = new RunningGameDefendStateMachine(this);
        _stateMachines.Add(_runningGameDefendStateMachine.GetStateMachineType(), _runningGameDefendStateMachine);

        //_mainCameraTransform = CameraManager.Instance.mainCamera.transform;
        //CameraManager.Instance.virtualPlayerCamera.LookAt = _lookAt;
        //CameraManager.Instance.virtualPlayerCamera.Follow = _lookAt;
        if (_runningGameMotionStateMachine != null)
        {
            IState newState = null;
            if (_runningGameMotionStateMachine.states.TryGetValue(StateType.RunningGameIdling_1, out newState))
            {
                _runningGameMotionStateMachine.ChangeState(newState);
                _runningGameMotionStateMachine.CurentState = newState;
                _runningGameMotionStateMachine.CurrentStateType = newState.GetStateType();
            }
        }

        if(_runningGameSwipeStateMachine != null)
        {
            IState newState = null;
            if (_runningGameSwipeStateMachine.states.TryGetValue(StateType.RunningGameEmptySwipingState, out newState))
            {
                _runningGameSwipeStateMachine.ChangeState(newState);
                _runningGameSwipeStateMachine.CurentState = newState;
                _runningGameSwipeStateMachine.CurrentStateType = newState.GetStateType();
            }
        }
        if(_runningGameStatusStateMachine != null)
        {
            IState newState = null;
            if (_runningGameStatusStateMachine.states.TryGetValue(StateType.RunningGameEmptyStatusState, out newState))
            {
                _runningGameStatusStateMachine.ChangeState(newState);
                _runningGameStatusStateMachine.CurentState = newState;
                _runningGameStatusStateMachine.CurrentStateType = newState.GetStateType();
            }
        }

        if(_runningGameAttackingStateMachine != null)
        {
            IState newState = null;
            if(_runningGameAttackingStateMachine.states.TryGetValue(StateType.RunningGameEmptyAttackingState, out newState))
            {
                _runningGameAttackingStateMachine.ChangeState(newState);
                _runningGameAttackingStateMachine.CurentState=newState;
                _runningGameAttackingStateMachine.CurrentStateType = newState.GetStateType();
            }
        }
        if(_runningGameDefendStateMachine != null)
        {
            IState newState = null;
            if(_runningGameDefendStateMachine.states.TryGetValue(StateType.RunningGameEmptyDefendState, out newState))
            {
                _runningGameDefendStateMachine.ChangeState(newState);
                _runningGameDefendStateMachine.CurentState=newState;
                _runningGameDefendStateMachine.CurrentStateType = newState.GetStateType();
            }
        }
    }
    public void Update()
    {
        if (!IsLocalPlayer) return;
        foreach (var kvp in _stateMachines)
        {
            kvp.Value?.Update();
            kvp.Value?.HandleInput();
        }
    }
    public void FixedUpdate()
    {
        if (!IsLocalPlayer) return;
        foreach (var kvp in _stateMachines)
        {
            kvp.Value?.PhysicsUpdate();
        }
    }
    
    
    private void OnTriggerEnter(Collider collider)
    {
        if (!IsLocalPlayer) return;
        Debug.Log("On Trigger Enter");
        foreach (var kvp in _stateMachines)
        {
            kvp.Value?.OnTriggerEnter(collider);
        }
    }
    private void OnTriggerExit(Collider collider)
    {
        if (!IsLocalPlayer) return;
        foreach (var kvp in _stateMachines)
        {
            kvp.Value?.OnTriggerExit(collider);
        }
    }
    public void OnMovementStateAnimationEnterEvent()
    {
        if (!IsLocalPlayer) return;
        foreach (var kvp in _stateMachines)
        {
            kvp.Value?.OnAnimationEnterEvent();
        }
    }
    public void OnMovementStateAnimationExitEvent()
    {
        if (!IsLocalPlayer) return;
        foreach (var kvp in _stateMachines)
        {
            kvp.Value?.OnAnimationExitsEvent();
        }
    }
    public void OnMovementStateAnimationTransitionEvent()
    {
         if (!IsLocalPlayer) return;
        foreach (var kvp in _stateMachines)
        {
            kvp.Value?.OnAnimationTransitionEvent();
        }
    }
    public void InitObject( GameObject gameObject, Vector3 position, Quaternion rotation)
    {
        Instantiate(gameObject, position, rotation);
    }
    private void OnStartGamePlay()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.RunningGameControlPopup, this);
        _runningGameMotionStateMachine.ChangeState(_runningGameMotionStateMachine.states[StateType.RunningGameRunningState]);
    }
    public void OnPlayGame()
    {
        _runningGameMotionStateMachine.ChangeState(_runningGameMotionStateMachine.states[StateType.RunningGameRunningState]);
    }
    public void TakeDamge()
    {
        if (!IsOwner) return;
        if (_runningGameDefendStateMachine.IsTakeDamge())
        {
            Debug.LogWarning("Defend Fail");
            _runningGameStatusStateMachine.ChangeState(runningGameStatusStateMachine.states[StateType.RunningGameStumblingState]);
        }
        else
        {
            Debug.LogWarning("Defend Successfull");
        }
    }
}
