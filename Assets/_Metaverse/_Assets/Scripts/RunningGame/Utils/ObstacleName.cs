using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObstacleName: byte
{
    ObstacleBin=0,
    ObstacleHighBarrier =1,
    ObstacleLowBarrier=2,
    ObstacleRoadworksBarrier=3,
    ObstacleRoadworksCone = 4, ObstacleWheelyBin=5
}
