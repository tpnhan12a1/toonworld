using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Player", menuName = "Custom/Characters/Player")]
public class PlayerSO : ScriptableObject
{
    public AnimationCurve slopeSpeedAngle = new AnimationCurve();
    public float testCollitionDistance = 0.5f;
    #region jump data

    [field: SerializeField] public Vector3 stationaryForce { get; private set; }
    [field: SerializeField] public Vector3 weakForce { get; private set; }
    [field: SerializeField] public Vector3 mediumForce { get; private set; }
    [field: SerializeField] public Vector3 strongForce { get; private set; }

    [field: SerializeField][field: Range(0, 5f)] public float jumpToGroundRayDistance { get; private set; } = 2f;
    [field: SerializeField][field: Range(0f, 10f)] public float decelerationForce { get; private set; } = 1f;
    #endregion

    #region fall data
    [field: SerializeField][field: Range(1f, 15f)] public float fallSpeedLimit { get; private set; } = 15f;
    [field: SerializeField] public Vector3 gravity { get; private set; } = new Vector3(0f, -2f, 0f);
    [field: SerializeField][field: Range(1f, 100f)] public float minimumDisstanceToBeConsideredHardFall { get; private set; } = 3f;
    #endregion
    #region grounded data
    [field: SerializeField][field: Range(0f, 25f)] public float baseSpeed { get; private set; } = 2f;
    [field: SerializeField][field: Range(0f, 5f)] public float groundToFallDistance { get; private set; } = 0.5f;
    #endregion

    #region rolling
    [field: SerializeField][field: Range(0f, 25f)] public float speedModifier { get; private set; } = 2f;
    #endregion
    #region sprinting
    [field: SerializeField][field: Range(0f, 25f)] public float sprintSpeed { get; private set; } = 5f;
    [field: SerializeField][field: Range(0f, 5f)] public float sprintToRunTime { get; private set; } = 1f;
    [field: SerializeField][field: Range(0f, 2f)] public float runToWalkTime { get; private set; } = 0.5f;
    #endregion
    #region walking
    [field: SerializeField][field: Range(0f, 25f)] public float walkSpeed { get; private set; } = 4f;
    #endregion
    #region running
    [field: SerializeField][field: Range(0f, 25f)] public float runSpeed { get; private set; } = 2f;
    #endregion
    [field: SerializeField][field: Range(0f, 1f)] public float stepHeightPercentage { get; private set; } = 0.25f;
    [field: SerializeField][field: Range(0f, 5f)] public float floatRayDistance { get; private set; } = 2f;
    [field: SerializeField][field: Range(0f, 50f)] public float stepReachForce { get; private set; } = 25f;

    [field: SerializeField] public Vector3 targetRotationReachTime { get; private set; }

    [field: SerializeField] public float dashSpeed { get; private set; } = 10f;
    [field: SerializeField] public float consecutiveDashesLimitAmount { get; private set; } = 2f;
    [field: SerializeField] public float dashLimitReachedCooldown { get; private set; } = 1.75f;
    [field: SerializeField] public float timeToColliderConsecutive { get; private set; } = 1f;

    #region water data
    [field:SerializeField] [Range(0f, 10f)] public float buoyancyForce = 1.0f;
    [field: SerializeField][Range(0f, 10f)] public float frequencyFloating = 1.0f;
    [field: SerializeField][Range(0f, 10f)] public float amplitudeFloating = 0.5f;
    #endregion
}
