using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class AIIdlingMonoState : AIMotionMonoState
{
    [SerializeField] private float _timeToWalk = 3f;
    private float _timeStart = 0f;
    public override StateType GetStateType()
    {
        return StateType.AIIdelingMonoState;
    }
    public override void OnExitState()
    {
        base.OnExitState();
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        residentStateMachine.residentController.navMeshAgent.isStopped = true;
        residentStateMachine.residentController.navMeshAgent.speed = 0;
        _timeStart = Time.time;
    }
    public override void OnUpdate()
    {

        _timeStart += Time.deltaTime;
        if (_timeStart > _timeToWalk)
        {
            _stateMachine.ChangeState(((ResidentStateMachine)_stateMachine).states[StateType.AIWalkingMonoState]);
        }
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        ResidentStateMachine residentStateMachine = (ResidentStateMachine)_stateMachine;
        _timeStart = 0f;
    }
}
