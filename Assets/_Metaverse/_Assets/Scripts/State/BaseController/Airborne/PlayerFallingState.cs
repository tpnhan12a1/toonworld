using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFallingState : PlayerAirborneState
{
    private Vector3 playerPositionOnEnter;
    private Vector3 velocitySpeed = Vector3.zero;
    private Vector3 jumForce = Vector3.zero;
    public override StateType GetStateType()
    {
        return StateType.Falling;
    }
    public override void OnEnterState()
    {
        base.OnEnterState();
        playerPositionOnEnter = ((MotionStateMachine)_stateMachine).playerController.transform.position;
        PlayerDataManager.Instance.reusableData.movementSpeedModifier = 0f;
        StartAnimation(PlayerDataManager.Instance.animationData.FallParameterHash);
        ResetVerticalVelocity();
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        LimitVerticalVelocity();
    }
    public override void OnExitState()
    {
        base.OnExitState();
        StopAnimation(PlayerDataManager.Instance.animationData.FallParameterHash);
    }
    protected override void OnContactWithGround(Collider collider)
    {
        Debug.Log("Contact ground");
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        float fallDistance = playerPositionOnEnter.y - stateMachine.playerController.transform.position.y;
        if (fallDistance < PlayerDataManager.Instance.playerData.minimumDisstanceToBeConsideredHardFall)
        {
            if (PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero)
            {
                _stateMachine.ChangeState(stateMachine.states[StateType.LightLanding]);
                return;
            }
            if (PlayerDataManager.Instance.reusableData.ShouldWalk())
            {
                _stateMachine.ChangeState(stateMachine.states[StateType.Walking]);
                return;
            }
            if (PlayerDataManager.Instance.reusableData.shouldSprint)
            {
                _stateMachine.ChangeState(stateMachine.states[StateType.Sprinting]);
                return;
            }
            _stateMachine.ChangeState(stateMachine.states[StateType.Running]);
            return;
        }
        if ((PlayerDataManager.Instance.reusableData.shouldSprint || PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero))
        {
            _stateMachine.ChangeState(stateMachine.states[StateType.HardLanding]);
            return;
        }
        _stateMachine.ChangeState(stateMachine.states[StateType.Rolling]);

    }
    protected override void OnContactWithWater(Collider collider)
    {
        MotionStateMachine stateMachine = (MotionStateMachine)_stateMachine;
        float fallDistance = playerPositionOnEnter.y - stateMachine.playerController.transform.position.y;
        if (fallDistance > PlayerDataManager.Instance.playerData.minimumDisstanceToBeConsideredHardFall)
        {
            _stateMachine.ChangeState(stateMachine.states[StateType.WaterUpState]);
            return;
        }
        base.OnContactWithWater(collider);
    }

    private void LimitVerticalVelocity()
    {
        Vector3 playerVeticalVelocity = GetPlayerVerticalVelocity();
        if (playerVeticalVelocity.y >= -PlayerDataManager.Instance.playerData.fallSpeedLimit)
        {
            return;
        }
        Vector3 limitedVelocity = new
            Vector3(0, -PlayerDataManager.Instance.playerData.fallSpeedLimit - playerVeticalVelocity.y, 0f);
        ((MotionStateMachine)_stateMachine).playerController.rgbody.AddForce(limitedVelocity, ForceMode.VelocityChange);

    }
    #region main method
    #endregion
}
