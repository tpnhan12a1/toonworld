using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLightStoppingState : PlayerStoppingState
{
    public override StateType GetStateType()
    {
        return StateType.LightStopping;
    }
}
