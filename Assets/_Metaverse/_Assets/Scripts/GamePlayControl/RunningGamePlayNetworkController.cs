using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Unity.Collections;
using Unity.Netcode;
using UnityEngine;
using static NetCodeModels;

[System.Serializable]
public class RunningGamePlayData
{
    [SerializeField] [Range(0f,120f)] private float _gameTime = 120f;
    [SerializeField] private int _segmentInit = 10;
    [SerializeField] private float _distanceToSpawnSegment = 60f;
    [SerializeField] private float _distanceToSpawnObstacle = 10f;
    [SerializeField] private float _distanceToSpawnPresent = 150f;

   
    public float gameTime { get => _gameTime;}
    public int segmentInit { get => _segmentInit;}
    public float distanceToSpawnSegment { get => _distanceToSpawnSegment; }
    public float distanceToSpawnObstacle { get => _distanceToSpawnObstacle;}
    public float distanceToSpawnPresent { get => _distanceToSpawnPresent; }
}
public class RunningGamePlayNetworkController : NetworkBehaviour, IGamePlayController
{
    [SerializeField]
    private RunningGamePlayData _runningGamePlayData;
    private GameRoom _gameRoom;
    private Dictionary<SegmentName, TrackSegment> _trackSegments = new Dictionary<SegmentName, TrackSegment>();
    private float _currentMapLengh = 0f;
    private float _curentObstanceZ = 0f;
    private float _curentPresentZ = 0f;

    [HideInInspector]
    public NetworkList<NetCodeModels.Segment> segmentNames;
    [HideInInspector]
    public NetworkVariable<GenrationGamePlay> generationGamePlay = new NetworkVariable<GenrationGamePlay>(GenrationGamePlay.PreGame, readPerm:NetworkVariableReadPermission.Everyone);

    //Run in client
    public Action<SegmentName> OnSegmentAdd;
    public Action<NetCodeModels.ObstacleData> OnSpawnObstacle;
    public Action<NetCodeModels.PresentData> OnSpawnPresent;
    public Action<NetworkObject, RocketName, ulong> OnSpawnRocket;

    private Action<RunningGamePlayNetworkController,GenrationGamePlay> OnGenerationGamePlayChanged;
    public void RegisterOnGenerationGamePlayChanged(Action<RunningGamePlayNetworkController, GenrationGamePlay> onGenerationGamePlayChanged)
    {
        OnGenerationGamePlayChanged += onGenerationGamePlayChanged;
        onGenerationGamePlayChanged?.Invoke(this, generationGamePlay.Value);
    }
    public void UnRegisterOnGenerationGamePlayChanged(Action<RunningGamePlayNetworkController, GenrationGamePlay> onGenerationGamePlayChanged)
    {
        OnGenerationGamePlayChanged -= onGenerationGamePlayChanged;
    }
    public RunningGamePlayData runningGamePlayData { get { return _runningGamePlayData; } } 
    public GameRoom room
    { get { return _gameRoom; }
        set 
        {
            _gameRoom = value;
            _isInit = true;
        } 
    }

    private bool _isInit = false;
    private void Awake()
    {
        segmentNames = new NetworkList<NetCodeModels.Segment>(readPerm: NetworkVariableReadPermission.Everyone);
    }
    public override void OnNetworkSpawn()
    {
        base.OnNetworkSpawn();
        if (IsServer)
        {
            
            GetSegmentData();
        }
        if (IsClient)
        {
            SetupSceneData();
            segmentNames.OnListChanged += OnListSegmentNameChanged;
           
        }
        generationGamePlay.OnValueChanged += OnClientGenerationGamePlayChanged;
    }

   
    public override void OnNetworkDespawn()
    {
        base.OnNetworkDespawn();

        
        if (IsClient)
        {
            segmentNames.OnListChanged -= OnListSegmentNameChanged;
           
        }
        generationGamePlay.OnValueChanged -= OnClientGenerationGamePlayChanged;
    }
    private void Update()
    {
        if (!IsServer || !_isInit) return;
        if(_currentMapLengh - _runningGamePlayData.distanceToSpawnSegment < GetMaxPosition().z)
        {
            AddSegment();
        }
    }
    #region server method

    private void GetSegmentData()
    {
        List<TrackSegment> segments =
        Imba.Utils.ResourceManager.Instance.GetAllResourseByName<TrackSegment>(new StringBuilder().Append("RunningGame/Segment/").ToString());
        segments.ForEach(x =>
        {
            _trackSegments.Add((SegmentName)Enum.Parse(typeof(SegmentName), x.gameObject.name), x);
        });
    }
    public void AddSegment()
    {
        for(int i=0;i< _runningGamePlayData.segmentInit; i++)
        {
            SegmentName segmentName = EnumUtils.GetRandomEnumValue<SegmentName>();
            NetCodeModels.Segment segment = new NetCodeModels.Segment()
            {
                Value = segmentName
            };
            segmentNames.Add(segment);

            float lenght = _trackSegments[segmentName].GetLenght();
            _currentMapLengh += lenght;
            _curentObstanceZ += lenght;

            if (_curentObstanceZ > _runningGamePlayData.distanceToSpawnObstacle)
            {
                ObstacleName obstacleName = EnumUtils.GetRandomEnumValue<ObstacleName>();
                int lane = RandomLane();
                _curentObstanceZ = 0f;
                NetCodeModels.ObstacleData obstacleData = new NetCodeModels.ObstacleData()
                {
                    obstacleName = obstacleName,
                    z = _currentMapLengh,
                    lane = lane,
                };
                ulong[] targetClientIds = _gameRoom.playerManager.GetClientIds().ToArray();
                ClientRpcParams clientRpcParams = new ClientRpcParams()
                {
                    Send = new ClientRpcSendParams()
                    {
                        TargetClientIds = targetClientIds,
                    }
                };
                SpawnObstancleCLientRpc(obstacleData, clientRpcParams);
            }

            _curentPresentZ += lenght;
            if (_curentPresentZ > _runningGamePlayData.distanceToSpawnObstacle)
            {
                _curentPresentZ = 0f;
                PresentData presentData = new PresentData()
                {
                    z = _currentMapLengh + 10f,
                    giftName = EnumUtils.GetRandomEnumValue<GiftName>()
                };
                ulong[] targetClientIds = _gameRoom.playerManager.GetClientIds().ToArray();
                ClientRpcParams clientRpcParams = new ClientRpcParams()
                {
                    Send = new ClientRpcSendParams()
                    {
                        TargetClientIds = targetClientIds,
                    }
                };
                SpawnPresentClientRpc(presentData, clientRpcParams);
            }
        }
    }

   
    private int RandomLane()
    {
        return new System.Random().Next(1, 3);
    }
    private Vector3 GetMaxPosition()
    {
        Vector3 maxPosition = Vector3.zero;
        _gameRoom.playerObjects.ForEach(x =>
        {
            if (maxPosition.z < x.transform.position.z)
            {
                maxPosition = x.transform.position;
            }
        });
        return maxPosition;
    }
    [ServerRpc(RequireOwnership =false)]
    internal void SpawnRocketServerRpc(NetworkObjectReference networkObject, RocketName baseRocket, ServerRpcParams serverRpcParams = default)
    {
        ulong[] targetClientIds = _gameRoom.playerManager.GetClientIds().ToArray();
        ClientRpcParams clientRpcParams = new ClientRpcParams()
        {
            Send = new ClientRpcSendParams()
            {
                TargetClientIds = targetClientIds
            }
        };
        SpawnRocketClientRpc(networkObject, baseRocket, serverRpcParams.Receive.SenderClientId, clientRpcParams);
    }
    public void StartGame()
    {
        Debug.Log("============================START GANE=================================");
        generationGamePlay.Value = GenrationGamePlay.InGame;
        CoroutineHandler.Instance.StartCoroutine(WaitForEndGame());
    }

    public void SummaryGame()
    {
        generationGamePlay.Value = GenrationGamePlay.Summary;
    }

    public void StopGame()
    {

    }
    private IEnumerator WaitForEndGame()
    {
        yield return new WaitForSeconds(_runningGamePlayData.gameTime);
        generationGamePlay.Value = GenrationGamePlay.Summary;
    }

    #endregion

    #region client method
    private void OnClientGenerationGamePlayChanged(GenrationGamePlay previousValue, GenrationGamePlay newValue)
    {
        OnGenerationGamePlayChanged?.Invoke(this, newValue);
    }

    [ClientRpc]
    private void SpawnRocketClientRpc(NetworkObjectReference networkObject, RocketName baseRocket, ulong senderClientId, ClientRpcParams clientRpcParams)
    {
        OnSpawnRocket?.Invoke(networkObject, baseRocket, senderClientId);
    }
    [ClientRpc]
    private void SpawnPresentClientRpc(PresentData presentData, ClientRpcParams clientRpcParams)
    {
        OnSpawnPresent?.Invoke(presentData);
    }

    [ClientRpc]
    private void SpawnObstancleCLientRpc(ObstacleData obstacleData, ClientRpcParams clientRpcParams)
    {
        OnSpawnObstacle?.Invoke(obstacleData);
    }
    private void OnListSegmentNameChanged(NetworkListEvent<NetCodeModels.Segment> changeEvent)
    {
        switch(changeEvent.Type)
        {
            case NetworkListEvent<NetCodeModels.Segment>.EventType.Add:
                OnSegmentAdd?.Invoke(changeEvent.Value.Value);
                break;
        }
    }
    private void SetupSceneData()
    {
        SceneRunningGameManager.Instance.SetupData(this);
    }

    #endregion
}
