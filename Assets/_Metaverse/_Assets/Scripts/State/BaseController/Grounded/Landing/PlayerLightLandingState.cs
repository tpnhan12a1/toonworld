using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLightLandingState : PlayerLandingState
{
    public override StateType GetStateType()
    {
        return StateType.LightLanding;
    }
    public override void OnEnterState()
    {
        PlayerDataManager.Instance.reusableData.movementSpeedModifier = 0f;
        base.OnEnterState();
        ResetVelocity();
        PlayerDataManager.Instance.reusableData.currentJumpForce = PlayerDataManager.Instance.playerData.stationaryForce;
    }
    public override void OnUpdate()
    {
        base.OnUpdate();
        if (PlayerDataManager.Instance.reusableData.movementInput == Vector2.zero) return;
        OnMove();
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        if (!IsMovingHorizontally()) return;
        ResetVelocity();
    }
    public override void OnAnimationTransitionEvent()
    {
        base.OnAnimationTransitionEvent();
        _stateMachine.ChangeState(((MotionStateMachine)_stateMachine).states[StateType.Idling]);
    }
}
