using UnityEngine;

public class CharacterRenderController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    public Animator animator { get { return _animator; } }

    [SerializeField] private CharacterRender _characterRender;
    public CharacterRender characterRender { get { return _characterRender; } }

    private void OnAnimationExitEvent()
    {
        animator.SetBool(PlayerDataManager.Instance.animationData.IdleParameterHash, true);
    }
}
