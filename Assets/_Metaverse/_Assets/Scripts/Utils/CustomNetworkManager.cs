using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class CustomNetworkManager : NetworkManager
{
    public void Awake()
    {
        if(NetworkManager.Singleton != null)
        {
            Destroy(gameObject);
        }
    }
}
