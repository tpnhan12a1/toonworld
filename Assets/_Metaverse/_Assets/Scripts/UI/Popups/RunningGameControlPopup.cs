using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class RunningGameControlPopup : UIPopup
{
    [SerializeField] RunningGameSkillGridViewController _runningGameSkillGridViewController;
    [SerializeField] private PlayerRunningGameController _playerRunningGameController;
    [SerializeField] private Image _countDownImange;
    [SerializeField] private CountDownController _countDownController;
    [SerializeField] private Button _btnOutGame;
    [SerializeField] private TMP_Text _txtTimeGameTick;
    [SerializeField] private Image _overlay;

    private float _timeGame = 0f;
    private Coroutine _coroutine;
    private RunningGamePlayNetworkController _runningGamePlayNetworkController;
    protected override void OnInit()
    {
        base.OnInit();
        _runningGameSkillGridViewController.OnInit();
        _btnOutGame.onClick.AddListener(OutGame);
    }

    private void OutGame()
    {
        Hide();
        ServerManager.Instance.JoinVituralWorld();
        UIManager.Instance.AlertManager.ShowAlertMessage("Stop game by server");
    }

    protected override void OnShowing()
    {
        base.OnShowing();
        _overlay.SetActive(true);
        _countDownImange.SetActive(false);
        _playerRunningGameController = (PlayerRunningGameController)Parameter;
        _runningGamePlayNetworkController = SceneRunningGameManager.Instance.runningGamePlayNetworkController;

        _runningGameSkillGridViewController.OnShow(_playerRunningGameController);
        _countDownController.OnEndCountDownAction += OnEndCountDown;
        _runningGamePlayNetworkController.RegisterOnGenerationGamePlayChanged(OnGenerationGamePlayChanged);
        UIManager.Instance.PopupManager.HidePopup(UIPopupName.GameLobbyPopup);
    }

    protected override void OnHiding()
    {
        base.OnHiding();
        _runningGameSkillGridViewController.OnHide();
        _countDownController.OnEndCountDownAction -= OnEndCountDown;
        _runningGamePlayNetworkController.UnRegisterOnGenerationGamePlayChanged(OnGenerationGamePlayChanged);
        _countDownImange.SetActive(false);
        StopCoroutine(PlayTickTimeGame(_timeGame));
    }

    private void SummaryGamePlay(object obj)
    {
        Hide();
        ServerManager.Instance.JoinVituralWorld();
        UIManager.Instance.AlertManager.ShowAlertMessage("Stop game by server");
        //Show summary
    }

    private void StartGamePlay(object timeGame)
    {
        _timeGame = (float)timeGame;
        _timeGame = _runningGamePlayNetworkController.runningGamePlayData.gameTime;
        _countDownImange.SetActive(true);
    }

    public void OnEndCountDown()
    {
        _countDownImange.SetActive(false);
        _txtTimeGameTick.SetActive(true);
        _overlay.SetActive(false);
        StartCoroutine(PlayTickTimeGame(_timeGame));
        _playerRunningGameController.runningGameMotionStateMachine
            .ChangeState(_playerRunningGameController.runningGameMotionStateMachine.states[StateType.RunningGameRunningState]);

    }
    public IEnumerator PlayTickTimeGame(float timeGame)
    {
        float eclap = 0f;
        while(eclap < timeGame)
        {
            eclap += Time.deltaTime;
            float timeDisplay = timeGame - eclap;
            _txtTimeGameTick.text = ((int)(timeDisplay / 60)).ToString() + ":" + ((int)(timeDisplay % 60)).ToString();
            yield return null;
        }
    }
    private void OnGenerationGamePlayChanged(RunningGamePlayNetworkController runningGamePlayNetworkController, GenrationGamePlay arg2)
    {
        switch (arg2)
        {
            case GenrationGamePlay.InGame:
                StartGamePlay(_runningGamePlayNetworkController.runningGamePlayData.gameTime);
                break;
            case GenrationGamePlay.Summary:
                SummaryGamePlay(arg2);
                break;
        }
    }
}
