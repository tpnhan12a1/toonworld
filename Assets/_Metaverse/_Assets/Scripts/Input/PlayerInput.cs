using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

    public class PlayerInput : ManualSingletonMono<PlayerInput>
    {
        public PlayerInputAction InputAction { get; private set; }
        public PlayerInputAction.PlayerActions PlayerActions { get; private set; }

        public override void Awake()
        {
            base.Awake();
            InputAction = new PlayerInputAction();
            PlayerActions = InputAction.Player;
        }
        private void OnEnable()
        {
            InputAction.Enable();
        }
        private void OnDisable()
        {
            InputAction.Enable();
        }
        public void DisableActionFor(InputAction action, float seconds)
        {
            StartCoroutine(DisableAction(action, seconds));

        }
        private IEnumerator DisableAction(InputAction action, float seconds)
        {
            action.Disable();
            yield return new WaitForSeconds(seconds);
            action.Enable();
        }

    }
