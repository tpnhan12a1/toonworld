using Imba.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunningTest : MonoBehaviour
{
    [SerializeField]
    private PlayerRunningGameController gameController;

    public void Update()
    {

        if (Input.GetKeyUp(KeyCode.U))
        {
            UIManager.Instance.PopupManager.ShowPopup(UIPopupName.RunningGameControlPopup, gameController);
        }
    }
}
