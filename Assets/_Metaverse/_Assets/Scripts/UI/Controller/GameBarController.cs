using Imba.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class GameBarController : MonoBehaviour
{
    [SerializeField]
    [Range(0f, 5f)] private float _radiousInteraction;
    [SerializeField]
    private Button _btnInteraction;
    [SerializeField] private LayerMask _layerInteraction;
    [SerializeField] private SphereCollider _sphereCollider;
    private void OnValidate()
    {
        _sphereCollider.radius = _radiousInteraction;
    }
    public void Awake()
    {
        _btnInteraction.onClick.AddListener(ShowGamePopup);
        _btnInteraction.SetActive(false);
    }

    private void ShowGamePopup()
    {
        UIManager.Instance.PopupManager.ShowPopup(UIPopupName.GamePopup);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(gameObject.transform.position, _radiousInteraction);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (LayerData.ContainLayer(_layerInteraction, other.gameObject.layer))
        {
            if(other.gameObject.TryGetComponent<NetworkObject>(out NetworkObject playerObject))
            {
                if (playerObject.IsLocalPlayer)
                {
                    _btnInteraction.SetActive(true);
                }
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (LayerData.ContainLayer(_layerInteraction, other.gameObject.layer))
        {
            if (other.gameObject.TryGetComponent<NetworkObject>(out NetworkObject playerObject))
            {
                if (playerObject.IsLocalPlayer)
                {
                    _btnInteraction.SetActive(false);
                }
            }
        }
    }
}
