using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public abstract class RunningGameIdlingState : RunningGameMotionState
{
    public override void OnEnterState()
    {
        base.OnEnterState();
        _runningGameStateMachine.StartBoolAnimation(PlayerDataManager.Instance.animationData.IdleParameterHash);
    }
    public override void OnExitState()
    {
        base.OnExitState();
        _runningGameStateMachine.StopBoolAnimation(PlayerDataManager.Instance.animationData.IdleParameterHash);
    }
    public override void OnPhysicsUpdate()
    {
        base.OnPhysicsUpdate();
        Float();
    }
}
