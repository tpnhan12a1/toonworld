using Imba.UI;
using PlayFab.GroupsModels;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GuildInventationPopup : UIPopup
{
    [SerializeField] private Button _btnCreateGuild;
    [SerializeField] private TMP_InputField _ipfGuildName;
    [SerializeField] private Button _btnClosePopup;
    //[SerializeField] private LoopListView2 _loopListViewInventation;

    //private List<GuildInformation> _guildInformations;
    protected override void OnInit()
    {
        base.OnInit();
        _btnCreateGuild.onClick.AddListener(OnCreateGuild);
        _btnClosePopup.onClick.AddListener(ClosePopup);
        //_guildInformations = new List<GuildInformation>();
        //_loopListViewInventation.InitListView(_guildInformations.Count, OnGetItemByIndex);
    }

    private void ClosePopup()
    {
        Hide();
    }

    private void OnCreateGuild()
    {
        string guildName = _ipfGuildName.text;
        if (!string.IsNullOrEmpty(guildName))
        {
            APIModels.GuildRequest request = new APIModels.GuildRequest();
            request.requestType = PlayfabContant.GuildTag.Create;
            request.subFunc = new APIModels.CreateGuildRequest()
            {
                guildName = guildName,
            };
            PlayfabManager.Instance.ExecuteFunction<APIModels.CreateGuildResponse>(PlayfabContant.FunctionName.Guild,
                request,true, (obj, data) =>
                {
                    GetGuildInformation(data.groupEntity);
                });
        }
    }

    private void GetGuildInformation(EntityKey groupEntity)
    {
        APIModels.GuildRequest guildRequest = new APIModels.GuildRequest()
        {
            requestType = PlayfabContant.GuildTag.Get
        };
        PlayfabManager.Instance.ExecuteFunction<APIModels.GetGuidResponse>
            (PlayfabContant.FunctionName.Guild, guildRequest, true,
            OnGetGuildSuccess,
            OnGetGuildFalure);
    }

    protected override void OnShowing()
    {
        base.OnShowing();
    }
    protected override void OnHiding()
    {
        base.OnHiding();
    }

    //private LoopListViewItem2 OnGetItemByIndex(LoopListView2 listView, int index)
    //{
    //    return null;
    //}
    private void OnGetGuildFalure(object arg1, string arg2)
    {

    }
    private void OnGetGuildSuccess(object arg1, APIModels.GetGuidResponse data)
    {
        PlayerDataManager.Instance.guildManager.Init(data.isHaveGuild, data.guildEntity, data.guildName, data.introduction, data.members);
        if (data.isHaveGuild && PlayerDataManager.Instance.guildManager.isSubAdmin ||
            PlayerDataManager.Instance.guildManager.isAdmin)
        {
            APIModels.GuildRequest guildRequest = new APIModels.GuildRequest()
            {
                requestType = PlayfabContant.GuildTag.ListInventation,
                subFunc = data.guildEntity
            };
            PlayfabManager.Instance.ExecuteFunction<APIModels.GuildListResponse>(PlayfabContant.FunctionName.Guild,
                guildRequest, true, (obj, data) =>
                {
                    PlayerDataManager.Instance.guildManager.SetInventation(data.members);
                    Hide();
                    UIManager.Instance.PopupManager.ShowPopup(UIPopupName.GuildPopup);
                }, null);

            ServerManager.Instance.ResponseGroupIdServerRpc(data.guildEntity.Id);
        }
    }
}
