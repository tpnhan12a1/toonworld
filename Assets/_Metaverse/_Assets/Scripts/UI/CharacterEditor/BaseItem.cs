using SuperScrollView;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Base Item", menuName = "Custom/BaseItem")]
public class BaseItem : ScriptableObject
{
    public string displayName;
    public string description;
    public Mesh mesh;
    public Material material;
    public Sprite sprite;
    public bool isItemColorIncludeSkinColor;

    public ItemType itemType;
    public Transform itemTransform;

    public List<HidePart> hideWhenEquipped = new List<HidePart>();
}

public enum HidePart { hair = 0, face = 1, body = 2, hand = 3, legs = 4, feet = 5}

public enum ItemType {
    none = -1,
    hair = 0,
    eyebrow = 1,
    eyes = 2,
    skin = 3,
    cap = 4,
    glasses = 5,
    torso = 6,
    pants = 7,
    shoes = 8,
    gloves = 9,
    backpack = 10
}