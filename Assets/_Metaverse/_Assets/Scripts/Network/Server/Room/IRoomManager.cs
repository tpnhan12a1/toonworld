using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRoomManager<R> where R : Room
{
    public delegate void Consumer(R p);
    public delegate bool Predicate(R p);
    void AddRoom(R room);
    void AddRooms(ICollection<R> rooms);
    void RemoveRoom(R room);
    void RemoveRoom(ulong roomId);
    Room GetRoom(ulong roomId);
    List<R> GetRoom(Predicate p);
    void ForEach(Consumer p);
    List<R> GetRoomList();
    int GetRoomCount();
    bool ContainRoom(ulong roomId);
    bool ContainsRoom(R room);
    bool IsAvalable();
}
