using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TabController : MonoBehaviour
{
    [SerializeField]
    private List<SpriteTab> tabs;
    [SerializeField] private bool _isMultiSelectedTab = false;

    public bool IsMultiSelectedTab {  get { return _isMultiSelectedTab; } set { _isMultiSelectedTab = value; } }
   
    public void Awake()
    {
        tabs.ForEach(tab =>
        {
            tab.SetTabController(this);
        });
    }
    public void DeSelectTab()
    {
        tabs.ForEach(tab => { tab.Deselected(); });
    }
    public void AddTab(SpriteTab tab)
    {
        if (tabs.Contains(tab)) return;

        tabs.Add(tab);
        tab.SetTabController(this);
    }
    public void SelectedTab(ITab tabSelect)
    {
        if (_isMultiSelectedTab)
        {
            tabSelect.Selected();
        }
        else
        {
            tabs.ForEach(tab =>
            {
                if ((ITab)tab == tabSelect)
                {
                    tabSelect.Selected();
                }
                else
                {
                    tab.Deselected();
                }
            });
        }
    }
    public void DeSelectedTab(ITab tabDeselect)
    {
        if(_isMultiSelectedTab)
        {
            tabDeselect.Deselected();
        }
    }
}
