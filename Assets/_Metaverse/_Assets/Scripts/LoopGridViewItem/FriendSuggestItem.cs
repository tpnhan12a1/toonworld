using SuperScrollView;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FriendSuggestItem : LoopGridViewItem
{
    [SerializeField] private Image _avatar;
    [SerializeField] private TMP_Text _txtLevel;
    [SerializeField] private TMP_Text _txtDisplayName;
    [SerializeField] private Button _btnAdd;
    [SerializeField] private TMP_Text _txtStatus;

    private ToonWorld.Friend _friend;
    private FriendSuggestGridViewController _friendSuggestGridViewController;
    public void Awake()
    {
        _btnAdd.onClick.AddListener(AddRequestFriend);
    }

    public void OnRefresh(ToonWorld.Friend friend, FriendSuggestGridViewController friendSuggestGridViewController)
    {
        _friendSuggestGridViewController = friendSuggestGridViewController;
        _friend = friend;
        Sprite avatar = Imba.Utils.ResourceManager.Instance.GetSpriteById(AtlasName.Avatar, friend.avatarUrl);
        _avatar.sprite = avatar;
        _txtDisplayName.text = friend.displayName;
        _txtLevel.text = friend.level.ToString();
        _btnAdd.SetActive(!PlayerDataManager.Instance.friendManager.ContainInList(_friend.friendPlayfabId));
    }
    private void AddRequestFriend()
    {
        APIModels.FriendRequest friendRequest = new APIModels.FriendRequest();
        friendRequest.friendPlayfabId = _friend.friendPlayfabId;
        PlayfabManager.Instance.ExecuteFunction<APIModels.AddFriendResponse>(
            PlayfabContant.FunctionName.AddFriend,
            friendRequest,
            false,
            OnAddFriendSuccess,
            null);
        ToonWorld.Friend friend = new ToonWorld.Friend(_friend.friendPlayfabId, _friend.avatarUrl, _friend.displayName, _friend.level, new List<string>());
        PlayerDataManager.Instance.friendManager.DirtyAddFriend(friend);
        _friendSuggestGridViewController.DirtyRemoveFriend(friend);
        //Add to list Friend
    }

    private void OnAddFriendSuccess(object arg1, APIModels.AddFriendResponse data)
    {

        ToonWorld.Friend friend = PlayerDataManager.Instance.friendManager.GetFriendByPlayfabId(data.friend.friendPlayfabId);
        if(friend != null)
        {
            friend.friendTags = data.friend.friendTags;
            PlayerDataManager.Instance.friendManager.OnListFriendChanged?.Invoke(PlayerDataManager.Instance.friendManager.GetAllList());
            NetCodeModels.FriendContent friendContent = new NetCodeModels.FriendContent();
            friendContent.message = PlayfabContant.FriendSyncTag.RequestFriend;
            friendContent.playfabReciverId = data.friend.friendPlayfabId;
            friendContent.friend = new NetCodeModels.Friend(PlayfabManager.Instance.PlayfabID,
           PlayerDataManager.Instance.avatarUrl,
           PlayerDataManager.Instance.displayName,
           PlayerDataManager.Instance.level,
           new List<string>() { PlayfabContant.FriendTag.Received });

            ServerManager.Instance.FriendControllerServerRpc(friendContent);
        }
    }
}
